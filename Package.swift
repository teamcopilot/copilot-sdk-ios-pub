// swift-tools-version:5.9
import PackageDescription

let package = Package(
    name: "CopilotCX",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "CopilotCX",
            targets: ["CopilotCX"]
        )
    ],
    dependencies: [
        .package(url: "https://github.com/Moya/Moya.git", .upToNextMajor(from: "14.0.0"))
    ],
    targets: [
        .target(
            name: "CopilotCX",
            dependencies: [
                "Moya"
            ],
            path: "Sources",
            exclude: [
                "BLELayer",
            ],
            resources: [
                .process("Resources")
            ]
        )
    ],
    swiftLanguageVersions: [.v5]
)
