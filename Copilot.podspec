Pod::Spec.new do |s|
s.name = "Copilot"
s.version = '6.0.7'
s.summary = "Copilot SDK for iOS."
s.homepage = "https://bitbucket.org/teamcopilot/copilot-sdk-ios-pub"
s.license = "Apache 2.0"
s.author = "Copilot.cx"
s.platform = :ios, '11.0'
s.source = { :git => "https://bitbucket.org/teamcopilot/copilot-sdk-ios-pub.git", :tag => "#{s.version}" }
s.source_files = 'Sources/**/*.{h,m,swift}'
s.exclude_files = ['Sources/BLELayer/**/*', 'Sources/**/*/ZemingoBLELayer.h']
s.resources = ['Sources/Resources/**/*', 'Sources/Resources/**/*.{xcassets}']
s.module_name = "CopilotCX"
s.swift_version = '5.0'

s.dependency "Moya", "~> 14.0"
s.dependency "Result", "~> 4.1" end
