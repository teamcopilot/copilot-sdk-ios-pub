//
// Created by Alex Gold on 12/01/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

protocol SupportsRotation {
	func shouldRotate() -> Bool
}