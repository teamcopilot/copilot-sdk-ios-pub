//
//  InAppInbox.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 23/12/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation


class InAppInbox: InAppInboxInteractable {
    @AtomicWrite private(set) var messages = [InAppMessage]()
    private let inboxQueue = DispatchQueue(label: "InAppInboxQueue")
    internal var triggerDelegate: Triggerable?
    
    //MARK: - Public
    func populateMessages(_ newMessages: [InAppMessage]) {
        inboxQueue.sync {
            self._messages.mutate { (localMessages) in
                localMessages.forEach { (message) in
                    newMessages.filter { $0.id == message.id }.last?.status = message.status
                }
                localMessages.removeAll()
                localMessages.insert(contentsOf: newMessages, at: 0)
            }
        }
    }
    
    func readyToTrigger() {
        inboxQueue.sync {
            guard let isThereMessageToVerify = messages.first(where: { $0.status == .ready }) else {
                ZLogManagerWrapper.sharedInstance.logInfo(message: "Couldn't find message to trigger")
                return
            }
            dispatch(message: isThereMessageToVerify)
        }
    }
    
    func analyticsEventReceived(_ analyticsEvent: AnalyticsEvent) {
        inboxQueue.sync {
            if let messageToVerify = messages.first(where: { $0.trigger.analyticsEventLogged(analyticsEvent) && $0.status != .evicted }) {
                messageToVerify.status = .ready
                dispatch(message: messageToVerify)
            }
        }
    }
    
    func resetInbox() {
        inboxQueue.sync {
            self._messages.mutate { $0.removeAll() }
        }
    }
    
    //MARK: - Private
    private func dispatch(message: InAppMessage) {
        inboxQueue.async { [weak self] in
            self?.triggerDelegate?.triggerMessage(message: message)
        }
    }
    
    deinit {
        triggerDelegate = nil
    }
}

protocol Triggerable {
    func triggerMessage(message: InAppMessage)
}
