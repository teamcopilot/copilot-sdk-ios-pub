//
//  InAppRenderingManager.swift
//  CopilotAPIAccess
//
//  Created by Elad on 10/02/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation

class InAppRenderingManager: InAppRendererManager {

    func canDisplay(inAppMessage: InAppMessage) -> Bool {
		InAppConditionVerifier().verifyConditions(message: inAppMessage)
    }

    func render(inAppMessage: InAppMessage, reporter: InAppReporter, delegate: AppNavigationDelegate?) -> Bool {
        guard let renderer = RendererFactory().getRenderer(forInApp: inAppMessage, navigationDelegate: delegate) else { return false }

        let actionHolder = renderer as? InAppRendererActionHolder

        actionHolder?.onRemoveFromBlocked  = {
            reporter.clearPreviousBlocked(messageId: inAppMessage.id)
        }

        actionHolder?.onDismiss = {
            reporter.reportInAppDismissed(generalParameters: inAppMessage.report.parameters)
        }

        actionHolder?.onCtaInteracted = { ctaType in
            reporter.reportMessageCtaClicked(generalParameters: inAppMessage.report.parameters, ctaReportParam: ctaType.report)
        }

        if let npsRenderer = renderer as? InAppNpsRenderer {
            npsRenderer.onNpsCtaInteracted = { cta in
                if cta == NpsDialogView.ASK_ANOTHER_TIME {
                    reporter.reportMessageCtaClicked(generalParameters: inAppMessage.report.parameters, ctaReportParam: cta)
                } else {
                    reporter.reportNPSSelected(generalParameters: inAppMessage.report.parameters, npsAnswer: cta)
                }
            }
        }

        DispatchQueue.main.async {
            renderer.render()
        }
        return true
    }
}



