//
// Created by Alex Gold on 30/01/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

class RendererFactory {
	func getRenderer(forInApp message: InAppMessage, navigationDelegate: AppNavigationDelegate?) -> Renderer? {
		let model = message.presentation
		var presentation: InAppMessagePresentation?
		switch model {
		case is SimpleInAppPresentationModel:
			presentation = model as! SimpleInAppPresentationModel
		case is HtmlInAppPresentationModel:
			presentation = model as! HtmlInAppPresentationModel
		case is NpsInAppPresentationModel:
			let presentation = model as! NpsInAppPresentationModel
			return InAppNpsRenderer(presentationModel: presentation, appNavigationDelegate: navigationDelegate)
		case is FullscreenInAppPresentationModel:
			presentation = model as! FullscreenInAppPresentationModel
		default:
			presentation = nil
		}
		guard let presentation = presentation else { return nil }
		return InAppRenderer(presentationModel: presentation, appNavigationDelegate: navigationDelegate)
	}
}