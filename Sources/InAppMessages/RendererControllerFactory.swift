//
// Created by Alex Gold on 01/06/2022.
// Copyright (c) 2022 Copilot.cx. All rights reserved.
//

import Foundation

class RendererControllerFactory {
	func initController(from model: InAppMessagePresentation, dismissDelegate: DismissDelegate, actionDelegate: ActionDelegate) -> InAppViewController? {
		switch model {
		case is FullscreenInAppPresentationModel:
			return FullScreenInAppController(presentationModel: model as! FullscreenInAppPresentationModel, dismissDelegate: dismissDelegate, actionDelegate: actionDelegate)
		case is SimpleInAppPresentationModel:
			return SimpleInAppDialogViewController(presentationModel: model as! SimpleInAppPresentationModel, dismissDelegate: dismissDelegate, actionDelegate: actionDelegate)
		case is HtmlInAppPresentationModel:
			return HtmlInAppDialogViewController(presentationModel: model as! HtmlInAppPresentationModel, dismissDelegate: dismissDelegate, actionDelegate: actionDelegate)
		case is NpsInAppPresentationModel:
			return NpsInAppDialogViewController(presentationModel: model as! NpsInAppPresentationModel, dismissDelegate: dismissDelegate, actionDelegateNps: actionDelegate as! ActionDelegateNps)
		default:
			return nil
		}
	}
}