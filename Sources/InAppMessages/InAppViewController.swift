//
// Created by Alex Gold on 12/01/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import UIKit

public class InAppViewController: UIViewController, SupportsRotation {

	internal var dismissDelegate: DismissDelegate
	internal var actionDelegate: ActionDelegate

	let presentationModel: InAppMessagePresentation

	init(presentationModel: InAppMessagePresentation, dismissDelegate: DismissDelegate, actionDelegate: ActionDelegate) {
		self.presentationModel = presentationModel
		self.dismissDelegate = dismissDelegate
		self.actionDelegate = actionDelegate
		super.init(nibName: nil, bundle: nil)
	}

	func shouldRotate() -> Bool {
		true
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}