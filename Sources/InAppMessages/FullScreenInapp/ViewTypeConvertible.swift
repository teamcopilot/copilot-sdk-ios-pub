//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import Foundation

protocol ViewTypeConvertible {
	func setupUI(inView hostingView: AnchoredContentView)
}