//
// Created by Alex Gold on 26/07/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import Foundation

enum ViewAnchor: Codable, Equatable {
	case Top(ViewAnchorPosition, CGFloat = 0)
	case Quarter(ViewAnchorPosition, CGFloat = 0)
	case Half(ViewAnchorPosition, CGFloat = 0)
	case ThreeQuarter(ViewAnchorPosition, CGFloat = 0)
	case Bottom(ViewAnchorPosition, CGFloat = 0)
	case ID(String, ViewAnchorPosition, CGFloat = 0)

	func getViewId() -> String? {
		switch (self) {
		case .ID(let ViewId, _,_):
			return ViewId
		default:
			return nil
		}
	}
}

enum ViewAnchorPosition: Codable {
	case Above
	case Below
	case Center
}
