//
// Created by Alex Gold on 08/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import Foundation

class ButtonsBarInAppData: AnchorableViewData {
	private struct Keys {
		static let buttonSpacing = "buttonSpacing"
		static let ctas = "ctas"
		static let viewId = "viewId"
		static let anchor = "anchor"
		static let layoutWidth = "layoutWidth"
		static let orientation = "orientation"
	}

	var buttonCtaData: [CtaButtonType]
	var buttonSpacing: CGFloat
	var layoutWidth: ButtonsBarLayoutWidth = .Full
	var orientation: ButtonsBarOrientation = .Horizontal

	static let DEFAULT_SPACING: CGFloat = 5

	init(buttonCtaData: [CtaButtonType], viewId: String, anchor: ViewAnchor) {
		self.buttonCtaData = buttonCtaData
		self.buttonSpacing = 0
		super.init(viewId: viewId, anchor: anchor)
	}

	init?(withDictionary dictionary: [String: Any]) {
		if let spacing = dictionary[Keys.buttonSpacing] as? CGFloat {
			if spacing < ButtonsBarInAppData.DEFAULT_SPACING { buttonSpacing = ButtonsBarInAppData.DEFAULT_SPACING }
			else {buttonSpacing = spacing }
		} else { buttonSpacing = ButtonsBarInAppData.DEFAULT_SPACING }

		guard let ctasDict = dictionary[Keys.ctas] as? [[String: Any]] else { return nil }
		var ctasFromJson: [CtaType?] = []

		ctasDict.forEach { dict in
			ctasFromJson.append(CtaTypeMapper.map(withDictionary: dict))
		}
		buttonCtaData = ctasFromJson.compactMap{ ($0 as? CtaButtonType) } // removes nils
		if ctasFromJson.count > 2 || ctasFromJson.count < 1 { return nil }
		if buttonCtaData.count != ctasFromJson.count { return nil }

		guard let viewId = dictionary[Keys.viewId] as? String else { return nil }
		guard let anchorDict = dictionary[Keys.anchor] as? [String: Any] else { return nil }
		guard let width = ButtonsBarInAppData.parseLayoutWidth(dictionary[Keys.layoutWidth] as? String) else { return nil }
		layoutWidth = width
		orientation = ButtonsBarInAppData.parseOrientation(dictionary[Keys.orientation] as? String)

		guard let viewAnchor = ViewAnchorParser.parseDict(anchorDict) else { return nil }
		super.init(viewId: viewId, anchor: viewAnchor)
	}

	fileprivate static func parseLayoutWidth(_ dictString: String?) -> ButtonsBarLayoutWidth? {
		switch (dictString?.lowercased()) {
		case "wrap": return .Wrap
		case "full": return .Full
		default: return nil
		}
	}

	fileprivate static func parseOrientation(_ dictString: String?) -> ButtonsBarOrientation {
		switch (dictString?.lowercased()) {
		case "horizontal": return .Horizontal
		case "vertical": return .Vertical
		default: return .Horizontal
		}
	}
}
