//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

class TextInAppData: AnchorableViewData {
	private struct Keys {
		static let contentText = "contentText"
		static let textSize = "textSize"
		static let textColorHex = "textColorHex"
		static let textColorAlpha = "textColorAlpha"
		static let viewId = "viewId"
		static let anchor = "anchor"
		static let textAlignment = "textAlignment"
		static let textWeight = "textWeight"
	}

	var text: String
	var textSize: CGFloat
	var textColor: UIColor
	var textColorAlpha: String
	var textAlignment: TextAlignment
	var textWeight: TextWeight = .REGULAR

	init?(withDictionary dictionary: [String: Any]) {
		if let contentText = dictionary[Keys.contentText] as? String { text = contentText } else { return nil }
		if let textSize = dictionary[Keys.textSize] as? CGFloat { self.textSize = textSize } else { return nil }
		textColorAlpha = (dictionary[Keys.textColorAlpha] as? String)?.validateAlpha() ?? "FF"
		if let textColorHex = dictionary[Keys.textColorHex] as? String { textColor = UIColor(hexString: textColorHex.withAlpha(textColorAlpha)) } else { return nil }
		if let textAlignment = dictionary[Keys.textAlignment] as? String {
			let textAlignment = TextInAppData.parseTextAlignment(textAlignment)
			self.textAlignment = textAlignment
		} else { return nil }

		guard let viewId = dictionary[Keys.viewId] as? String else { return nil }
		guard let anchorDict = dictionary[Keys.anchor] as? [String: Any] else { return nil }
		textWeight = TextInAppData.parseTextWeight(dictionary[Keys.textWeight] as? String)

		guard let viewAnchor = ViewAnchorParser.parseDict(anchorDict) else { return nil }
		super.init(viewId: viewId, anchor: viewAnchor)
	}

	class func parseTextAlignment(_ alignment: String) -> TextAlignment {
		switch (alignment.lowercased()) {
		case "center": return .CENTER
		case "start": return .START
		case "end": return .END
		default: return .CENTER
		}
	}

	class func parseTextWeight(_ weight: String?) -> TextWeight {
		switch (weight?.lowercased()) {
		case "regular": return .REGULAR
		case "bold": return .BOLD
		default: return .REGULAR
		}
	}
}