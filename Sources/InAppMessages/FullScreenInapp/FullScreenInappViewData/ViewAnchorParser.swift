//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

class ViewAnchorParser {

	class func setAnchor(anchor: ViewAnchor, child: UIView, contentView: AnchoredContentView) {
		switch (anchor) {
		case .Top(let position, let spacing):
			getOriginAnchor(for: child, position: position).constraint(equalTo: contentView.safeTopAnchor.centerYAnchor, constant: getSpacing(for: position, spacing)).isActive = true
		case .Quarter(let position, let spacing):
			getOriginAnchor(for: child, position: position).constraint(equalTo: contentView.quarterAnchor.centerYAnchor, constant: getSpacing(for: position, spacing)).isActive = true
		case .Half(let position, let spacing):
			getOriginAnchor(for: child, position: position).constraint(equalTo: contentView.halfAnchor.centerYAnchor, constant: getSpacing(for: position, spacing)).isActive = true
		case .ThreeQuarter(let position, let spacing):
			getOriginAnchor(for: child, position: position).constraint(equalTo: contentView.threeQuarterAnchor.centerYAnchor, constant: getSpacing(for: position, spacing)).isActive = true
		case .Bottom(let position, let spacing):
			getOriginAnchor(for: child, position: position).constraint(equalTo: contentView.safeBottomAnchor.centerYAnchor, constant: getSpacing(for: position, spacing)).isActive = true
		case .ID(let viewId, let position, let spacing):
			if let toView = contentView.findView(byId: viewId)?.view {
				let anchor: NSLayoutYAxisAnchor
				switch (position) {
				case .Below:
					anchor = toView.bottomAnchor
				case .Above:
					anchor = toView.topAnchor
				case .Center:
					anchor = toView.centerYAnchor
				}
				getOriginAnchor(for: child, position: position).constraint(equalTo: anchor, constant: getSpacing(for: position, spacing)).isActive = true
			}
		}
	}

	static func parseDict(_ dict: [String: Any]) -> ViewAnchor? {
		guard let anchor = dict[Keys.viewAnchor] as? String else { return nil }
		guard let position = dict[Keys.anchorPosition] as? String else { return nil }
		var spacing = dict[Keys.spacing] as? Int ?? 0
		spacing = spacing < 0 ? 0 : spacing

		let anchorToViewId = dict[Keys.anchorToViewId] as? String ?? ""
		return parseString(anchor: anchor, position: position, spacing: spacing, toViewId: anchorToViewId)
	}

	class func parseString(anchor: String, position: String, spacing: Int, toViewId: String = "") -> ViewAnchor? {
		guard let anchorPosition = parseString(position: position) else { return nil }
		let spacing = CGFloat(spacing)
		switch anchor.lowercased() {
		case "top":
			if anchorPosition == ViewAnchorPosition.Above || anchorPosition == ViewAnchorPosition.Center { return nil }
			return .Top(anchorPosition, spacing)
		case "quarter": return .Quarter(anchorPosition, spacing)
		case "half": return .Half(anchorPosition, spacing)
		case "threequarter": return .ThreeQuarter(anchorPosition, spacing)
		case "bottom":
			if anchorPosition == ViewAnchorPosition.Below || anchorPosition == ViewAnchorPosition.Center { return nil }
			return .Bottom(anchorPosition, spacing)
		case "id": return .ID(toViewId, anchorPosition, spacing)
		default: return nil
		}
	}

	struct Keys {
		static let viewAnchor = "viewAnchor"
		static let anchorPosition = "anchorPosition"
		static let spacing = "spacing"
		static let anchorToViewId = "anchorToViewId"
	}
	
	private class func parseString(position: String) -> ViewAnchorPosition? {
		switch position.lowercased() {
		case "above": return .Above
		case "below": return .Below
		case "center": return .Center
		default: return nil
		}
	}

	private class func getOriginAnchor(for view: UIView, position: ViewAnchorPosition) -> NSLayoutYAxisAnchor {
		switch (position) {
		case .Above:
			return view.bottomAnchor
		case .Below:
			return view.topAnchor
		case .Center:
			return view.centerYAnchor
		}
	}

	private class func getSpacing(for position: ViewAnchorPosition, _ spacing: CGFloat) -> CGFloat {
		(position == ViewAnchorPosition.Below || position == ViewAnchorPosition.Center) ? spacing : -spacing
	}
}