//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import Foundation

class AnchorableViewData: ViewData, Equatable {
	var anchor: ViewAnchor
	var viewId: String

	init(viewId: String, anchor: ViewAnchor) {
		self.viewId = viewId
		self.anchor = anchor
	}

	static func ==(lhs: AnchorableViewData, rhs: AnchorableViewData) -> Bool {
		lhs.viewId == rhs.viewId
	}
}

protocol ViewData {}

class ViewTypeConvertibleFactory {
	static func toViewType(data: ViewData, actionDelegate: ActionDelegate) -> ViewTypeConvertible? {
		switch (data) {
		case is ImageInAppData:
			return ImageInAppViewType(data: data as! ImageInAppData)
		case is TextInAppData:
			return TextInAppViewType(data: data as! TextInAppData)
		case is ButtonsBarInAppData:
			return ButtonsBarInAppViewType(data: data as! ButtonsBarInAppData, actionDelegate: actionDelegate)
		default:
			return nil
		}
	}
}