//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import Foundation
import UIKit

class ImageInAppData: ViewData {
	private struct Keys {
		static let imageUrl = "imageUrl"
		static let imageAlignment = "imageAlignment"
	}

	let imageUrl: String
	let imageAlignment: InAppImageAlignment
	let contentMode: UIImageView.ContentMode

	init(imageUrl: String, imageSize: InAppImageAlignment, contentMode: UIView.ContentMode = .scaleAspectFill) {
		self.imageUrl = imageUrl
		self.contentMode = contentMode
		self.imageAlignment = imageSize
	}

	init?(withDictionary dictionary: [String: Any]) {
		let imageUrlStr = dictionary[Keys.imageUrl] as? String
		guard let imageUrl = imageUrlStr?.validateUrl() else { return nil }
		self.imageUrl = imageUrl
		if let imageAlignment = dictionary[Keys.imageAlignment] as? String {
			guard let size = ImageInAppData.parseImageAlignment(imageAlignment) else { return nil }
			self.imageAlignment = size } else { return nil }
		contentMode = .scaleAspectFill
	}

	static func parseImageAlignment(_ alignment: String) -> InAppImageAlignment? {
		switch (alignment.lowercased()) {
		case "full":
			return .Full
		case "tophalf":
			return .TopHalf
		case "bottomhalf":
			return .BottomHalf
		default:
			return nil
		}
	}
}