//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

class ButtonsBarInAppViewType: BaseInAppViewType<ButtonsBarInAppData> {
	private let actionDelegate: ActionDelegate

	init(data: ButtonsBarInAppData, actionDelegate: ActionDelegate) {
		self.actionDelegate = actionDelegate
		super.init(data: data)
	}

	override func getView() -> UIView {
		ButtonsBarInAppView(data: data, actionDelegate: actionDelegate)
	}
}
