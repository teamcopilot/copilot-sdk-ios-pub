//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

class ImageInAppViewType: ViewTypeConvertible {
	private let data: ImageInAppData

	init(data: ImageInAppData) {
		self.data = data
	}

	func setupUI(inView hostingView: AnchoredContentView) {
		let view = ImageInAppView(data: data)
		view.translatesAutoresizingMaskIntoConstraints = false
		hostingView.addSubView(view)

		var imageAnchors = [
			view.leadingAnchor.constraint(equalTo: hostingView.leadingAnchor),
			view.trailingAnchor.constraint(equalTo: hostingView.trailingAnchor),
		]

		if data.imageAlignment == .BottomHalf {
			imageAnchors.append(view.topAnchor.constraint(equalTo: hostingView.halfAnchor.topAnchor))
			imageAnchors.append(view.bottomAnchor.constraint(equalTo: hostingView.bottomAnchor))
		} else {
			imageAnchors.append(view.topAnchor.constraint(equalTo: hostingView.topAnchor))
			if data.imageAlignment == .TopHalf {
				imageAnchors.append(view.bottomAnchor.constraint(equalTo: hostingView.halfAnchor.topAnchor))
			} else { // Fullscreen
				imageAnchors.append(view.bottomAnchor.constraint(equalTo: hostingView.bottomAnchor))
			}
		}
		imageAnchors.forEach { $0.isActive = true }
	}
}

