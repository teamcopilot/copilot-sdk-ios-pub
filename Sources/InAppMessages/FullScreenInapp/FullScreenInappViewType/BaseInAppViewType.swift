//
// Created by Alex Gold on 30/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit


class BaseInAppViewType<T: AnchorableViewData> : ViewTypeConvertible {

	internal let data: T

	init(data: T) {
		self.data = data
	}

	func setupUI(inView hostingView: AnchoredContentView) {
		let view: UIView = getView()
		view.translatesAutoresizingMaskIntoConstraints = false
		hostingView.addSubView(view, viewId: data.viewId)

		[
			view.leadingAnchor.constraint(equalTo: hostingView.leadingAnchor),
			view.trailingAnchor.constraint(equalTo: hostingView.trailingAnchor),
		].forEach { $0.isActive = true }

		ViewAnchorParser.setAnchor(anchor: data.anchor, child: view, contentView: hostingView)
	}

	func getView() -> UIView {
		fatalError()
	}
}