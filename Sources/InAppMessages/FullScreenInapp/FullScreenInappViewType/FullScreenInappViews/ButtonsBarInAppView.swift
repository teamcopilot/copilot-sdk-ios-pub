//
// Created by Alex Gold on 08/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

class ButtonsBarInAppView: InAppView {
	private var data: ButtonsBarInAppData
	private lazy var buttonArray = [button1, button2]
	private let actionDelegate: ActionDelegate

	private static let buttonTextSize: CGFloat = 22
	private static let horizontalInsets: CGFloat = 15
	private static let verticalInset: CGFloat = 11
	private var isVertical: Bool = true

	private let button1: UIButton = {
		createButton()
	}()

	private let button2: UIButton = {
		let bt = createButton()
		bt.isHidden = true
		return bt
	}()

	init(data: ButtonsBarInAppData, actionDelegate: ActionDelegate) {
		self.data = data
		self.actionDelegate = actionDelegate
		super.init(frame: .zero)
		setupUI()
	}

	fileprivate func setupUI() {
		let buttonStackView = UIStackView(arrangedSubviews: [button1, button2])
		isVertical = shouldChangeToVerticalAlignment()
		buttonStackView.axis = isVertical ? .vertical : .horizontal

		if data.orientation == .Horizontal {
			buttonStackView.distribution = .fillEqually
		} else {
			if data.layoutWidth == .Wrap {
				buttonStackView.distribution = .fillProportionally
			} else { //Full
				buttonStackView.distribution = .fillEqually
			}
		}

		buttonStackView.spacing = data.buttonSpacing
		buttonStackView.translatesAutoresizingMaskIntoConstraints = false

		addSubviews(buttonStackView)

		[
			buttonStackView.centerXAnchor.constraint(equalTo: centerXAnchor),
			buttonStackView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: padding),
			buttonStackView.trailingAnchor.constraint(greaterThanOrEqualTo: trailingAnchor, constant: -padding),
			buttonStackView.topAnchor.constraint(equalTo: topAnchor),
			buttonStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
		].enumerated().forEach { (index, constraint) in
			switch (index) {
			case 2:
				if isVertical {
					constraint.isActive = data.layoutWidth == .Full
				} else {
					if data.buttonCtaData.count == 1 {
						constraint.isActive = data.layoutWidth == .Full
					} else {
						constraint.isActive = true
					}
				}
			default: constraint.isActive = true
			}
		}

		setButtonStyle()
	}

	fileprivate class func createButton() -> UIButton {
		let bt = UIButton(type: .system)
		bt.translatesAutoresizingMaskIntoConstraints = false
		bt.setTitleColor(.white, for: .normal)
		bt.titleLabel?.lineBreakMode = .byTruncatingTail
		bt.titleLabel?.numberOfLines = 1
		bt.titleLabel?.font = UIFont.systemFont(ofSize: buttonTextSize)
		bt.contentEdgeInsets = .init(top: verticalInset, left: horizontalInsets, bottom: verticalInset, right: horizontalInsets)
		bt.layer.cornerRadius = 8
		return bt
	}

	fileprivate func setButtonStyle() {
		let buttonNumber: InAppButtonsBarLayout = data.buttonCtaData.count == 1 ? .single: .dual
		button2.isHidden = buttonNumber == .dual ? false : true
		switch buttonNumber {
		case .single:
			button1.setTitle(data.buttonCtaData.first?.text, for: .normal)
		case .dual:
			button1.setTitle(data.buttonCtaData[0].text, for: .normal)
			button2.setTitle(data.buttonCtaData[1].text, for: .normal)
		}

		for (index, cta) in data.buttonCtaData.enumerated() {
			let button = buttonArray[index]
			button.setBackgroundColor(color: UIColor(hexString: cta.backgroundColorHex.withAlpha(cta.backgroundColorAlpha)), forState: .normal)
			button.setTitleColor(UIColor(hexString: cta.textColorHex.withAlpha(cta.textColorAlpha)), for: .normal)
			if let textSize = data.buttonCtaData[index].textSize {
				button.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(textSize))
			}

			button.addTarget(self, action: #selector(handleAction), for: .touchUpInside)
		}
	}

	fileprivate func shouldChangeToVerticalAlignment() -> Bool {
		data.orientation == .Vertical
/*		let contentWidth = UIScreen.main.bounds.size.width - (24 * 2)
		let widths = data.buttonCtaData.map { getTextSize(text: $0.text, textSize: $0.textSize).width }
		guard let maxWidth = widths.max() else { return false }
		let additionalWidth = ButtonsBarInAppView.edgeInset * 2 + (data.buttonSpacing / 2) + 8
		return (maxWidth + additionalWidth) > contentWidth / 2*/
	}

	fileprivate func getTextSize(text: String, textSize: CGFloat?) -> CGSize {
		text.size(withAttributes:[.font: UIFont.systemFont(ofSize: textSize ?? ButtonsBarInAppView.buttonTextSize)])
	}

	@objc fileprivate func handleAction(_ sender: UIButton) {
		var index = 0
		for (i, button) in buttonArray.enumerated() {
			if button == sender {
				index = i
			}
		}
		let action = data.buttonCtaData[index]
		actionDelegate.performAction(action, completion: nil)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}