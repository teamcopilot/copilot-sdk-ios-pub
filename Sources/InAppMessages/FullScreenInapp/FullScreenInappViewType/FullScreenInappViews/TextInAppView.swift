//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

class InAppView: UIView {
	var padding: CGFloat = 24
}

class TextInAppView: InAppView {

	private var data: TextInAppData

	private lazy var textLabel: UILabel = {
		let lb = UILabel()
		lb.font = UIFont.systemFont(ofSize: 25)
		lb.textColor = .white
		lb.textAlignment = .center
		lb.numberOfLines = 0
		lb.translatesAutoresizingMaskIntoConstraints = false
		return lb
	}()

	init(data: TextInAppData) {
		self.data = data
		super.init(frame: .zero)
		setupUI()
	}

	fileprivate func setupUI() {
		textLabel.translatesAutoresizingMaskIntoConstraints = false
		addSubview(textLabel)
		[
			textLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
			textLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
			textLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
			textLabel.topAnchor.constraint(equalTo: topAnchor),
			textLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
		].forEach { $0.isActive = true }
		textLabel.text = data.text
		textLabel.textColor = data.textColor
		textLabel.textAlignment = parseAlignment()

		let textWeight : UIFont.Weight = data.textWeight == .REGULAR ? .regular : .bold
		let scaledFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: data.textSize, weight: textWeight))
		textLabel.font = scaledFont
	}

	fileprivate func parseAlignment() -> NSTextAlignment {
		switch (data.textAlignment) {
		case .START:
			return .left
		case .END:
			return .right
		case .CENTER:
			return .center
		}
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

