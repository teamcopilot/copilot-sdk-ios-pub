//
// Created by Alex Gold on 12/12/2022.
// Copyright (c) 2022 Copilot.cx. All rights reserved.
//

import Foundation

enum InAppButtonsBarLayout: Codable {
	case single,
		 dual
}