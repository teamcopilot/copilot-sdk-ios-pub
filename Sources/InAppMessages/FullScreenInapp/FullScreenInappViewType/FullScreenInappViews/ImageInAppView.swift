//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

class ImageInAppView: UIView {
	private var data: ImageInAppData

	lazy var imageView: UIImageView = {
		let image = UIImageView()
		image.clipsToBounds = true
		image.translatesAutoresizingMaskIntoConstraints = false
		return image
	}()

	init(data: ImageInAppData) {
		self.data = data
		super.init(frame: .zero)

		setupUI()
		loadImage()
	}

	func setupUI() {
		addSubview(imageView)
		imageView.contentMode = data.contentMode
		[
			imageView.topAnchor.constraint(equalTo: topAnchor),
			imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
			imageView.bottomAnchor.constraint(equalTo: bottomAnchor),
			imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
		].forEach { $0.isActive = true }
	}

	func loadImage() {
		ImageCacher(dataManager: DataFileManager()).loadImage(forUrl: data.imageUrl) { result in
			switch result {
			case .Success(let image):
				self.imageView.image = image
			case .Failure:
				self.imageView.image = nil
			}
		}
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
