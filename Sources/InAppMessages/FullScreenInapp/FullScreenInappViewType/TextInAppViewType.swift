//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

class TextInAppViewType: BaseInAppViewType<TextInAppData> {
	override func getView() -> UIView {
		TextInAppView(data: data)
	}
}