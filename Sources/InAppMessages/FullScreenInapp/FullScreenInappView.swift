//
// Created by Alex Gold on 08/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

public class FullScreenInAppController: InAppViewController {
	private var configElements: [ViewData]?
	private let dismissButtonEnabled: Bool

	private var closeButton: UIButton = {
		let bt = UIButton(type: .system)
		let image = UIImage(named: "clear", in: Bundle(for: Copilot.self), compatibleWith: .none)?.withRenderingMode(.alwaysOriginal)
		bt.translatesAutoresizingMaskIntoConstraints = false
		bt.contentHorizontalAlignment = .fill
		bt.contentVerticalAlignment = .fill
		bt.setImage(image, for: .normal)
		bt.backgroundColor = .none
		return bt
	}()

	var contentView: AnchoredContentView = {
		let view = AnchoredContentView()
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()

	init(presentationModel: FullscreenInAppPresentationModel, dismissDelegate: DismissDelegate, actionDelegate: ActionDelegate) {
 		configElements = presentationModel.viewData
		dismissButtonEnabled = presentationModel.dismissButtonEnabled ?? true

		contentView.translatesAutoresizingMaskIntoConstraints = false
		super.init(presentationModel: presentationModel, dismissDelegate: dismissDelegate, actionDelegate: actionDelegate)
		view.backgroundColor = UIColor(hexString: presentationModel.backgroundColorHex)
	}
	
	public override func viewDidLoad() {
		super.viewDidLoad()

		setupUI()
	}

	fileprivate func setupUI() {
		view.addSubviews(contentView)

		if dismissButtonEnabled { addCloseButton() }

		let horizontalPadding: CGFloat = 24
		let verticalPadding: CGFloat = 16
		[
			contentView.topAnchor.constraint(equalTo: view.topAnchor,constant: 0),
			contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 0),
			contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: 0),
			contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: 0),
		].forEach { $0.isActive = true }

		closeButton.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
		setupSubviews()
	}

	func setupSubviews() {
		let validator = AnchorValidator()

		guard let configElements = configElements else { return }


		configElements.filter{ !($0 is AnchorableViewData) } // Image first
				.forEach { data in setupViewType(viewData: data) }

		let anchorables = configElements.filter { $0 is AnchorableViewData } as! [AnchorableViewData]
		validator.sortAnchors(anchors: anchorables)
				.forEach { setupViewType(viewData: $0) }
	}

	private func setupViewType(viewData: ViewData) {
		ViewTypeConvertibleFactory.toViewType(data: viewData, actionDelegate: actionDelegate)?.setupUI(inView: contentView)
	}

	fileprivate func isAbsolute(anchor: ViewAnchor?) -> Bool {
		switch (anchor) {
		case .ID(_, _, _): return false
		default: return true
		}
	}

	fileprivate func addCloseButton() {
		let padding: CGFloat = 16
		view.addSubview(closeButton)
		let topConstraint: NSLayoutYAxisAnchor

		if #available(iOS 11, *) {
			topConstraint = view.safeAreaLayoutGuide.topAnchor
		} else {
			topConstraint = view.topAnchor
		}
		
		let buttonSize: CGFloat = 40

		[
			closeButton.topAnchor.constraint(equalToSystemSpacingBelow: topConstraint, multiplier: 0),
			closeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
			closeButton.widthAnchor.constraint(equalToConstant: buttonSize),
			closeButton.heightAnchor.constraint(equalToConstant: buttonSize),
		].forEach { $0.isActive = true }
	}

	@objc fileprivate func handleClose() {
		dismissDelegate.dismissPressed()
	}

	override func shouldRotate() -> Bool {
		false
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

