//
// Created by Alex Gold on 04/05/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

class AnchoredContentView: UIView {

	lazy var quarterAnchor = createAnchoredView()
	lazy var halfAnchor = createAnchoredView()
	lazy var threeQuarterAnchor = createAnchoredView()

	lazy var safeTopAnchor = createAnchoredView()
	lazy var safeBottomAnchor = createAnchoredView()

	private var subViews = [SubView]()

	private func createAnchoredView() -> UIView {
		let view = UIView()
		view.translatesAutoresizingMaskIntoConstraints = false
		view.backgroundColor = .black
		view.heightAnchor.constraint(equalToConstant: 0).isActive = true
		return view
	}

	init() {
		super.init(frame: .zero)
		setupUI()
	}

	func setupUI() {
		translatesAutoresizingMaskIntoConstraints = false
		layoutIfNeeded()
		addSubviews(safeTopAnchor, quarterAnchor, halfAnchor, threeQuarterAnchor, safeBottomAnchor)

		let topConstraint: NSLayoutYAxisAnchor
		let bottomConstraint: NSLayoutYAxisAnchor

		if #available(iOS 11, *) {
			topConstraint = safeAreaLayoutGuide.topAnchor
			bottomConstraint = safeAreaLayoutGuide.bottomAnchor
		} else {
			topConstraint = topAnchor
			bottomConstraint = bottomAnchor
		}

			[
				safeTopAnchor.topAnchor.constraint(equalTo: topConstraint, constant: 40), // closebuttonSize is 40
				NSLayoutConstraint(item: quarterAnchor, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 0.5, constant: 0), //Quarter
				halfAnchor.centerYAnchor.constraint(equalTo: centerYAnchor), //Half
				NSLayoutConstraint(item: threeQuarterAnchor, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.5, constant: 0), //ThreeQuarters
				safeBottomAnchor.topAnchor.constraint(equalTo: bottomConstraint),

				halfAnchor.widthAnchor.constraint(equalTo: widthAnchor),
				quarterAnchor.widthAnchor.constraint(equalTo: widthAnchor),
				threeQuarterAnchor.widthAnchor.constraint(equalTo: widthAnchor),
				safeTopAnchor.widthAnchor.constraint(equalTo: widthAnchor),
				safeBottomAnchor.widthAnchor.constraint(equalTo: widthAnchor),
			].forEach { $0.isActive = true }
	}

	func addSubView(_ view: UIView, viewId: String? = nil) {
		subViews.append(.init(view: view, viewId: viewId))
		addSubview(view)
	}

	func findView(byId viewId: String) -> SubView? {
		subViews.first {$0.viewId == viewId}
	}

	required init?(coder: NSCoder) { fatalError("required init:") }
}

class SubView {
	var view: UIView
	var viewId: String?

	init(view: UIView, viewId: String?) {
		self.view = view
		self.viewId = viewId
	}
}