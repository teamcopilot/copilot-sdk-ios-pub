//
//  PopupDialogViewController.swift
//  CopilotAPIAccess
//
//  Created by Elad on 19/02/2020.
//  Copyright © 2020 Elad. All rights reserved.
//

import UIKit


class SimpleInAppDialogViewController: InAppViewController {
    var dialogView: SimpleInAppDialogView!

    var buttons: [InAppButton] {
        get {
            dialogView.buttonStackView.arrangedSubviews as? [InAppButton] ?? []
        }
        set {
            guard newValue.count > 0 else { return }
            newValue.forEach { dialogView.buttonStackView.addArrangedSubview( $0 ) }
        }
    }

    init(presentationModel: SimpleInAppPresentationModel, dismissDelegate: DismissDelegate, actionDelegate: ActionDelegate) {
        super.init(presentationModel: presentationModel, dismissDelegate: dismissDelegate, actionDelegate: actionDelegate)
    }

    override func loadView() {
        super.loadView()
        guard let model = presentationModel as? SimpleInAppPresentationModel else { return }
        let hasImage = model.imageUrl != nil
        dialogView = SimpleInAppDialogView(frame: .zero, hasImage: hasImage)
        dialogView.dismissDelegate = dismissDelegate

        view.addSubview(dialogView)
        [
            dialogView.topAnchor.constraint(equalTo: view.topAnchor),
            dialogView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            dialogView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            dialogView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ].forEach { $0.isActive = true }

        setupUI(model)
    }

    private func setupUI(_ model: SimpleInAppPresentationModel) {
        dialogView.titleLabel.text = model.title
        dialogView.messageLabel.text = model.bodyText
        if let backgroundHexColor = model.backgroundColorHex{ dialogView.backgroundColor = UIColor(hexString: backgroundHexColor) }

        if let textHexColor = model.titleColorHex {
            dialogView.titleColor = UIColor(hexString: textHexColor.withAlpha(model.titleColorAlpha))
            dialogView.messageColor = UIColor(hexString: textHexColor.withAlpha(model.titleColorAlpha))
        }

        if let dismissButtonEnabled = model.dismissButtonEnabled {
            dialogView.dismissButton.isHidden = dismissButtonEnabled ? false : true
        }

        setButtons(ctaList: model.ctas)


        if let imageUrl = model.imageUrl {
            dialogView.imageView.image = UIImage()
            ImageCacher(dataManager: DataFileManager()).loadImage(forUrl: imageUrl) { result in
                switch result {
                case .Success(let image):
                    self.dialogView.imageView.image = image
                case .Failure:
                    self.dialogView.imageView.image = nil
                }
            }
        }
    }

    private func setButtons(ctaList: [CtaType]) {
        ctaList.forEach { ctaType in
            if let cta = ctaType as? CtaButtonType {
                let button = InAppButton(title: cta.text, textColor: UIColor(hexString: cta.textColorHex.withAlpha(cta.textColorAlpha)), bdColor: UIColor(hexString: cta.backgroundColorHex.withAlpha(cta.backgroundColorAlpha))) {
                    self.actionDelegate.performAction(cta) {
                        ZLogManagerWrapper.sharedInstance.logInfo(message: "cta perform action completed")
                    }
                }
                buttons.append(button)
            } else {
                ZLogManagerWrapper.sharedInstance.logError(message: "can't cast to ctaButtonType")
            }
        }

        buttons.forEach { button in
            button.addTarget(self, action: #selector(handleButton), for: .touchUpInside)
        }
    }

    @objc fileprivate func handleButton(_ button: InAppButton) {
        button.buttonAction?()
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if traitCollection.verticalSizeClass == .regular {//portrait

            buttons.count == 3 ? (dialogView.buttonStackView.axis = .vertical) : (dialogView.buttonStackView.axis = .horizontal)
            dialogView.fullStackView.axis = .vertical
            if dialogView.imageView.image != nil {
                dialogView.fullStackView.distribution = .fill
                dialogView.imageHeightConstraint?.constant = 202
            }
            else {
                dialogView.fullStackView.axis = .vertical
            }


        } else if traitCollection.verticalSizeClass == .compact {//landscape
            dialogView.buttonStackView.axis = .horizontal

            if(buttons.count == 3) {
                dialogView.fullStackView.axis = .vertical
            } else {
                dialogView.fullStackView.axis = .horizontal
            }

            if dialogView.imageView.image != nil {
                buttons.count == 3 ? (dialogView.fullStackView.distribution = .fill) : (dialogView.fullStackView.distribution = .fillEqually)
                buttons.count == 3 ? (dialogView.imageHeightConstraint?.constant = 0) : (dialogView.imageHeightConstraint?.constant = 282)
            } else {
                dialogView.fullStackView.axis = .vertical
            }
        }
    }

    required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
