//
//  NpsDialogViewController.swift
//  CopilotAPIAccess
//
//  Created by Elad on 17/05/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import UIKit


class NpsInAppDialogViewController: InAppViewController {
    var dialogView : NpsDialogView!

	init(presentationModel: NpsInAppPresentationModel, dismissDelegate: DismissDelegate, actionDelegateNps: ActionDelegateNps) {
        super.init(presentationModel: presentationModel, dismissDelegate: dismissDelegate, actionDelegate: actionDelegateNps)
    }

    override func loadView() {
        super.loadView()
        guard let model = presentationModel as? NpsInAppPresentationModel else { return }
        let hasImage = model.imageUrl != nil
        dialogView = NpsDialogView(frame: .zero, hasImage: hasImage)

        dialogView.dismissDelegate = dismissDelegate
        dialogView.npsCompletion = { (ctaNumber: String?) in
            if let ctaNumber = ctaNumber {
                (self.actionDelegate as? ActionDelegateNps)?.performAction(npsSelected: ctaNumber)
            } else {
                ZLogManagerWrapper.sharedInstance.logInfo(message: "user dismissed the NPS survey message")
            }
        }

        dialogView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(dialogView)
        [
            dialogView.topAnchor.constraint(equalTo: view.topAnchor),
            dialogView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            dialogView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            dialogView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ].forEach { $0.isActive = true }
        setupUI(model)
    }

    fileprivate func setupUI(_ model: NpsInAppPresentationModel) {
        let color = UIColor(hexString: model.ctaBackgroundColorHex)
        let backgroundColor = UIColor(hexString: model.backgroundColorHex)
        dialogView.labelQuestionText = model.labelQuestionText

        dialogView.backgroundColor = backgroundColor
        dialogView.contentView.backgroundColor = backgroundColor
        dialogView.bgColor = .clear

        dialogView.labelNotLikely = model.labelNotLikely
        dialogView.labelExtremelyLikely = model.labelExtremelyLikely
        dialogView.labelAskMeAnotherTime = model.labelAskMeAnotherTime
        dialogView.labelDone = model.labelDone
        dialogView.sendButton.setTitle(model.labelAskMeAnotherTime, for: .normal)
        dialogView.doneText = model.labelDone
        dialogView.sendButton.backgroundColor = .clear
        dialogView.titleLabel.textColor = color
        dialogView.selectedColor = color
        dialogView.notLikelyLabel.textColor = color
        dialogView.veryLikelyLabel.textColor = color
        dialogView.seperateView.backgroundColor = color
        dialogView.sendButton.setTitleColor(color, for: .normal)

        dialogView.ctaBackgroundColor = color
		dialogView.ctaTextColor = color

        if let imageUrl = model.imageUrl {
            dialogView.image = UIImage()
            ImageCacher(dataManager: DataFileManager()).loadImage(forUrl: imageUrl) { result in
                switch result {
                case .Success(let image):
                    self.dialogView.imageView.image = image
                case .Failure:
                    self.dialogView.imageView.image = nil
                }
            }
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
