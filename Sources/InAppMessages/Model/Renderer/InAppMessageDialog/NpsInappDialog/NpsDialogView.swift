//
//  NpsDialogView.swift
//  CopilotAPIAccess
//
//  Created by Elad on 17/05/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import UIKit


class NpsDialogView: UIView {

    static let ASK_ANOTHER_TIME = "ask_me_another_time"
    //MARK: - outlets
    
    let kCONTENT_XIB_NAME = "NpsDialogView"
    @IBOutlet var contentView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var notLikelyLabel: UILabel!
    @IBOutlet weak var veryLikelyLabel: UILabel!
    @IBOutlet var surveyButtons: [UIButton]!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var seperateView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bottomBackgroundView: UIView!
    @IBOutlet weak var thankYouLabel: UILabel!
	@IBOutlet weak var bottomStackView: UIStackView!
	@IBOutlet weak var stackView: UIStackView!

    let shouldShowImage: Bool

    //MARK: - Consts
    
    private struct Consts {
        
        static let titleFontSize: CGFloat = 18.0
        static let labelsFontSize: CGFloat = 12.0
        static let doneButtonFontSize: CGFloat = 13.0
        static let thankYouButtonFontSize: CGFloat = 18.0
        static let surveyButtonsFontSize: CGFloat = 13.0
        static let buttonBorderWidth: CGFloat = 1.0
        static let buttonBackgroundAlpha: CGFloat = 0.08
        static let textAlpha: CGFloat = 0.54
    }

    //MARK: - Properties
    
    var npsCompletion: ((String?) -> Void)?
	var dismissDelegate :DismissDelegate?
    
    var labelQuestionText: String? {
        didSet {
            titleLabel.text = labelQuestionText
        }
    }
    
    var ctaBackgroundColor: UIColor? {
        didSet {
            guard let ctaBackgroundColor = ctaBackgroundColor else { return }
            surveyButtons.forEach {
                $0.layer.borderColor = ctaBackgroundColor.cgColor
                $0.layer.borderWidth = Consts.buttonBorderWidth
                $0.setBackgroundColor(color: ctaBackgroundColor, forState: .selected)
                $0.setBackgroundColor(color: ctaBackgroundColor.withAlphaComponent(Consts.buttonBackgroundAlpha), forState: .normal)
            }
            selectedCta = nil//Set the bottom buttons
        }
    }
    
    var ctaTextColor: UIColor? {
        didSet {
            surveyButtons.forEach {
                $0.setTitleColor(ctaTextColor, for: .normal)
                guard let backgroundColor = ctaBackgroundColor else { return }
                if ColorUtils.calculateLuminance(hexColor: ColorUtils.convertUIColorToHex(color: backgroundColor)) > 160 {
                    $0.setTitleColor(.black, for: .selected)
                } else {
                    $0.setTitleColor(.white, for: .selected)
                }
            }
        }
    }
    
    var bgColor: UIColor? {
        didSet {
            bgView.backgroundColor = bgColor
            selectedCta = nil//Set the bottom buttons
        }
    }
    
    var textColorHex: UIColor? {
        didSet {
            guard let textColorHex = textColorHex else { return }
            titleLabel.textColor = textColorHex
            seperateView.backgroundColor = textColorHex.withAlphaComponent(Consts.textAlpha)
            veryLikelyLabel.textColor = textColorHex.withAlphaComponent(Consts.textAlpha)
            notLikelyLabel.textColor = textColorHex.withAlphaComponent(Consts.textAlpha)
            doneButton.setTitleColor(textColorHex.withAlphaComponent(Consts.textAlpha), for: .normal)
        }
    }
    
    var labelNotLikely: String? {
        didSet {
            notLikelyLabel.text = labelNotLikely
        }
    }
    
    var labelExtremelyLikely: String? {
        didSet {
            veryLikelyLabel.text = labelExtremelyLikely
        }
    }
    
    var labelAskMeAnotherTime: String? {
        didSet {
            doneButton.setTitle(labelAskMeAnotherTime, for: .normal)
        }
    }
    
    var labelDone: String? {
        didSet {
            doneButton.setTitle(labelDone, for: .selected)
        }
    }
    
    var labelThankYou: String? {
        didSet {
            thankYouLabel?.text = labelThankYou
        }
    }
    
    var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    
    private var selectedCta: Int? {
        didSet {
            if selectedCta != nil {
				changeToSaveButton()
                doneButton.isSelected = true
				doneButton.titleLabel?.font = UIFont.systemFont(ofSize: Consts.doneButtonFontSize)
                thankYouLabel.textColor = .white
            }
            else {
                doneButton.isSelected = false
				doneButton.titleLabel?.font = UIFont.systemFont(ofSize: Consts.doneButtonFontSize)
                thankYouLabel.textColor = .clear
            }
        }
    }

    var selectedColor: UIColor?
    lazy var doneText: String = ""
    
    //MARK: - Initializers
    
    override init(frame: CGRect) {
        shouldShowImage = true
        super.init(frame: frame)
        commonInit()
    }

    init(frame: CGRect, hasImage: Bool) {
        shouldShowImage = hasImage
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        shouldShowImage = true
        super.init(coder: aDecoder)
        commonInit()
    }

    private var isPortrait: Bool {
        get { InAppConditionVerifier().isPortrait() }
    }

    private func commonInit() {
        let viewBundle = Bundle(for: NpsDialogView.self)
        viewBundle.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        
        frame = contentView.frame;
        addSubview(contentView);
        contentView.anchorToSuperview()

        setupViews()
        selectedCta = nil
    }
	
	lazy var portraitImageConstraint = imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 7/10)
    lazy var landscapeImageConstraint = imageView.heightAnchor.constraint(equalToConstant: 0)

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
		DispatchQueue.main.async {
			self.setImageConstraints()
			self.layoutIfNeeded()
		}
    }

    private func setImageConstraints() {
        if shouldShowImage {
            self.portraitImageConstraint.isActive = self.isPortrait
            self.landscapeImageConstraint.isActive = !self.isPortrait
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        //Survey button corner radii need to be arranged only after laying out
        surveyButtons.forEach {
            $0.setNeedsLayout()
            $0.layoutIfNeeded()
            $0.layer.cornerRadius = $0.bounds.width/2
            $0.layer.masksToBounds = true
        }
    }
    
    //MARK: - Private
    lazy var sendButton: UIButton = {
		let bt = UIButton(type: .system)
		bt.setHeight(40)
		bt.layer.cornerRadius = 5
		bt.addTarget(self, action: #selector(handleSendButton), for: .touchUpInside)
		bt.translatesAutoresizingMaskIntoConstraints = false
        return bt
    }()
    
    private func setupViews() {
        setupLabelsAppearance()
        setupButtonsAppearance()

        //add buttons
        bottomStackView.backgroundColor = .clear
		bottomStackView.insertArrangedSubview(sendButton, at: 0)
    }

	@objc fileprivate func handleSendButton() {
        if let cta = selectedCta {
            npsCompletion?("\(cta)")
        } else {
            npsCompletion?(NpsDialogView.ASK_ANOTHER_TIME)
        }
	}
    
    private func setupLabelsAppearance() {
        //title label
		titleLabel.font = UIFont.systemFont(ofSize: Consts.titleFontSize, weight: .bold)
        
        //bottom labels
		veryLikelyLabel.font = UIFont.systemFont(ofSize: Consts.labelsFontSize, weight: .regular)
        
		notLikelyLabel.font = UIFont.systemFont(ofSize: Consts.labelsFontSize, weight: .regular)
    }
    
    private func setupButtonsAppearance() {
        
        //survey buttons
        surveyButtons.forEach {
			$0.titleLabel?.font = UIFont.systemFont(ofSize: Consts.surveyButtonsFontSize, weight: .regular)
        }
        
        //dismiss button
		doneButton.titleLabel?.font = UIFont.systemFont(ofSize: Consts.doneButtonFontSize, weight: .regular)
        doneButton.setTitleColor(.white, for: .selected)
        
        //Thank you label
		thankYouLabel.font = UIFont.systemFont(ofSize: Consts.thankYouButtonFontSize, weight: .bold)
        thankYouLabel.textColor = .white
    }

	private func changeToSaveButton() {
        if let selectedColor = selectedColor {
            sendButton.backgroundColor = selectedColor
            sendButton.setTitleColor(ColorUtils.setColorBasedOnLuminance(color: selectedColor, forLightColor: .black, forDarkColor: .white), for: .normal)
        }

		sendButton.setTitle(doneText, for: .normal)
	}
    
    //MARK: - Actions
    @IBAction func npsSurveyButtonPressed(_ sender: UIButton) {
        surveyButtons.forEach {
            $0.isSelected = false
        }
        sender.isSelected = true
        selectedCta = sender.tag
    }

    @IBAction func dismissButtonTapped(_ sender: UIButton) {
		npsCompletion?(nil)
        dismissDelegate?.dismissPressed()
    }
}
