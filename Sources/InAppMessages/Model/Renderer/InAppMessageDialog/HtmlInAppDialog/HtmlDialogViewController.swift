//
//  HtmlPopupDialogViewController.swift
//  CopilotAPIAccess
//
//  Created by Elad on 05/03/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation
import WebKit


class HtmlInAppDialogViewController: InAppViewController {
    var dialogView: HtmlDialogView!
    var htmlButtonCompletion: (() -> ())?

    init(presentationModel: HtmlInAppPresentationModel, dismissDelegate: DismissDelegate, actionDelegate: ActionDelegate) {
        super.init(presentationModel: presentationModel, dismissDelegate: dismissDelegate, actionDelegate: actionDelegate)
        htmlButtonCompletion = {
            ZLogManagerWrapper.sharedInstance.logInfo(message: "html button action completed")
        }
    }

    override func loadView() {
        super.loadView()
        dialogView = HtmlDialogView(frame: .zero)
        dialogView.dismissDelegate = dismissDelegate
        dialogView.webview.navigationDelegate = self
        dialogView.webContent = (presentationModel as? HtmlInAppPresentationModel)?.content

        guard let model = presentationModel as? HtmlInAppPresentationModel else { return }
        dialogView.dismissButton.isHidden = model.dismissButtonEnabled ? false : true

        view.addSubview(dialogView)
        [
            dialogView.topAnchor.constraint(equalTo: view.topAnchor),
            dialogView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            dialogView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            dialogView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ].forEach { $0.isActive = true }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HtmlInAppDialogViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let navigationActionUrl = navigationAction.request.url, navigationActionUrl.absoluteString.contains("cplt://") {
            let actionString = navigationActionUrl.absoluteString.replacingOccurrences(of: "cplt://", with: "")
            performHtmlAction(with: actionString)
        }
        decisionHandler(.allow)
    }

    private func performHtmlAction(with urlActionString: String) {
        (presentationModel as? HtmlInAppPresentationModel)?.ctas.forEach { [weak self] in
            if $0.redirectId == urlActionString {
                actionDelegate.performAction($0) {
                    self?.htmlButtonCompletion?()
                }
            }
        }
    }
}