//
// Created by Alex Gold on 09/06/2022.
// Copyright (c) 2022 Copilot.cx. All rights reserved.
//

import Foundation

public protocol DismissDelegate: AnyObject {
	func dismissPressed()
}