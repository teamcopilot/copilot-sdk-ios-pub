//
// Created by Alex Gold on 08/06/2022.
// Copyright (c) 2022 Copilot.cx. All rights reserved.
//

import UIKit
import MessageUI

import SafariServices
import StoreKit

protocol ActionPerformer {
	func perform(onController viewController: UIViewController, completion: (() -> ())?)
}