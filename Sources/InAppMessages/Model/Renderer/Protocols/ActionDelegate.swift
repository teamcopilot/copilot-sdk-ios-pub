//
// Created by Alex Gold on 09/06/2022.
// Copyright (c) 2022 Copilot.cx. All rights reserved.
//

import Foundation

protocol ActionDelegate {
	func performAction(_ actionType: CtaType, completion: (() -> ())?)
}

protocol ActionDelegateNps: ActionDelegate {
	func performAction(npsSelected: String)
}