//
// Created by Alex Gold on 09/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation
import UIKit

class InAppConditionVerifier {
	func verifyConditions(message: InAppMessage) -> Bool {
		verifyGeneralInAppConstraints() && verifySpecificInAppPresentationConstraints(for: message.presentation)
	}

	private func verifyGeneralInAppConstraints() -> Bool {
		var areConditionsVerified = false
		if Thread.isMainThread {
			areConditionsVerified = checkAllConditions()
		} else {
			DispatchQueue.main.sync {
				areConditionsVerified = checkAllConditions()
			}
		}
		return areConditionsVerified
	}

	private func verifySpecificInAppPresentationConstraints(for presentation: InAppMessagePresentation) -> Bool {
		switch presentation {
		case is FullscreenInAppPresentationModel:
			return isPortrait()
		default: return true
		}
	}

	private func checkAllConditions() -> Bool {
		!checkPopupDialogShowing() && checkIfApplicationStateIsActive() && !KeyboardStateListener.shared.isVisible
	}

	//MARK: - private
	private func checkPopupDialogShowing() -> Bool {
		let dialogController = UIApplication.shared.windows.first { window in
			let controller = window.visibleViewController()
			return controller is Renderer || controller is UIAlertController || controller is UIActivityViewController
		}
		return dialogController != nil
	}

	private func checkIfApplicationStateIsActive() -> Bool {
		var returnValue = false
		switch UIApplication.shared.applicationState {
		case .background, .inactive:
			returnValue = false
		case .active:
			returnValue = true
		default:
			break
		}
		return returnValue
	}

	public func isPortrait() -> Bool {
		let width = UIScreen.main.bounds.width
		let height = UIScreen.main.bounds.height
		return width < height
	}
}
