//
// Created by Alex Gold on 31/01/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

class InAppNpsRenderer: InAppRenderer<NpsInAppPresentationModel>, ActionDelegateNps {
	var onNpsCtaInteracted: ((String) -> ())?

	override init(presentationModel: NpsInAppPresentationModel, appNavigationDelegate: AppNavigationDelegate?) {
		super.init(presentationModel: presentationModel, appNavigationDelegate: appNavigationDelegate)
	}

	func performAction(npsSelected: String) {
		onNpsCtaInteracted?(npsSelected)
		self.dismiss()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}