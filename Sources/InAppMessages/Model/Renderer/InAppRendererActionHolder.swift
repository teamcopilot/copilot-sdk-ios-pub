//
// Created by Alex Gold on 31/01/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation
import UIKit

class InAppRendererActionHolder: UIViewController {
	var onDismiss: (() -> ())?
	var onRemoveFromBlocked: (() -> ())?
	var onCtaInteracted: ((CtaType) -> ())?

	init() {
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
