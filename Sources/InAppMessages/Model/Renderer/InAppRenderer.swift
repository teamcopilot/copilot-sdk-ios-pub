//
// Created by Alex Gold on 09/06/2022.
// Copyright (c) 2022 Copilot.cx. All rights reserved.
//

import UIKit


class InAppRenderer<PresentationModel: InAppMessagePresentation>: InAppRendererActionHolder, Renderer, DismissDelegate {
	private let preferredControllerWidth: CGFloat = 340

	private let presentationModel: PresentationModel
	private let presentationManager: PresentationManager
	private let appNavigationDelegate: AppNavigationDelegate?
	private var viewController: InAppViewController!
	private var popupContainerView: PopupDialogContainerView?

	private lazy var window: UIWindow = {
		let window = createWindow()
		let root = UIViewController()
		root.view.backgroundColor = .clear
		window.rootViewController = root
		return window
	}()

	init(presentationModel: PresentationModel, appNavigationDelegate: AppNavigationDelegate?) {
		self.presentationModel = presentationModel
		self.appNavigationDelegate = appNavigationDelegate

		presentationManager = PresentationManager(transitionStyle: .bounceUp)
		super.init()

		transitioningDelegate = presentationManager
		modalPresentationStyle = .custom

		viewController = RendererControllerFactory()
				.initController(from: presentationModel, dismissDelegate: self, actionDelegate: self)
		if #available(iOS 16.0, *) {
			setNeedsUpdateOfSupportedInterfaceOrientations()
		}

		setView()
		addChild(viewController)
		view.addSubview(viewController.view)
		(view as? PopupDialogContainerView)?.mainStackView.insertArrangedSubview(viewController.view, at: 0)
		viewController.didMove(toParent: self)
	}

	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		if viewController.shouldRotate() {
			return .allButUpsideDown
		} else {
			return .portrait
		}
	}

	override var shouldAutorotate: Bool { //Deprecated from iOS 16
		viewController.shouldRotate()
	}

	func render() {
		show(animated: false) {
			ZLogManagerWrapper.sharedInstance.logInfo(message: "In App Message is presented successfully")
		}
	}

	func dismiss(_ completion: (() -> Void)? = nil) {
		onRemoveFromBlocked?()
		viewController.dismiss(animated: true) {
			self.window.dismiss()
			completion?()
		}
	}

	func dismissPressed() {
		onDismiss?()
		dismiss()
	}

	//MARK: - Private
	private func show(animated: Bool = true, completion: (() -> Void)? = nil)  {
		if let rootViewController = window.rootViewController{
			window.makeKeyAndVisible()
			rootViewController.present(self, animated: animated, completion: completion)
		}
	}

	private func setView() {
		view.translatesAutoresizingMaskIntoConstraints = false
		var inAppMessageType: InAppMessageType?
		switch presentationModel {
		case is SimpleInAppPresentationModel:
			inAppMessageType = .simple
		case is NpsInAppPresentationModel:
			inAppMessageType = .nps
		case is HtmlInAppPresentationModel:
			inAppMessageType = .html
		default:
			inAppMessageType = nil
		}
		guard let inAppMessageType = inAppMessageType else { return }
		view = PopupDialogContainerView(frame: UIScreen.main.bounds, preferredWidth: preferredControllerWidth, inAppMessageType: inAppMessageType)
	}

	private func createWindow() -> UIWindow {
		var window: UIWindow
		if #available(iOS 13.0, *) {
			let windowScene = UIApplication.shared.connectedScenes
					.filter { $0.activationState == .foregroundActive }
					.first
			if let windowScene = windowScene as? UIWindowScene {
				window = UIWindow(windowScene: windowScene)
			} else {
				window = UIWindow(frame: UIScreen.main.bounds)
			}
		} else {
			window =  UIWindow(frame: UIScreen.main.bounds)
		}
		window.windowLevel = UIWindow.Level.statusBar + 1
		return window
	}

	required init?(coder: NSCoder) {
		fatalError("Base class")
	}
}

extension InAppRenderer: ActionDelegate {
	func performAction(_ actionType: CtaType, completion: (() -> ())?) {
		onCtaInteracted?(actionType)

		if let action = actionType.ctaAction as? Action.AppNavigation {
			action.appNavigationDelegate = appNavigationDelegate
		}
		actionType.ctaAction.perform(onController: viewController, completion: { self.dismiss() })
		completion?()
	}
}
