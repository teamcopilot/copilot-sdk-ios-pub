//
// Created by Alex Gold on 27/07/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit
import MessageUI

import SafariServices
import StoreKit

class Action: NSObject, ActionPerformer {
	private override init() {}
	func perform(onController viewController: UIViewController, completion: (() -> ())? = nil) { fatalError() }

	class SendEmail: Action,  MFMailComposeViewControllerDelegate {
		private let mailTo: String?
		private let subject: String?
		private let body: String?
		private var dismissAction: (() -> ())?

		init(mailTo: String?, subject: String?, body: String?) {
			self.mailTo = mailTo
			self.subject = subject
			self.body = body
			super.init()
		}

		override func perform(onController viewController: UIViewController, completion: (() -> ())?) {
			dismissAction = completion
			if MFMailComposeViewController.canSendMail() {
				let mail = MFMailComposeViewController()
				mail.mailComposeDelegate = self
				if let sendTo = mailTo {
					mail.setToRecipients([sendTo])
				}
				if let messageBody = body {
					mail.setMessageBody(messageBody, isHTML: false)
				}
				mail.setSubject(subject ?? "")

				viewController.present(mail, animated: true)
			} else {
				let mailtoString = formatMailtoString(to: mailTo, subject: subject, body: body)
				if let mailtoUrl = URL(string: mailtoString) {
					if UIApplication.shared.canOpenURL(mailtoUrl) {
						UIApplication.shared.open(mailtoUrl, options: [:], completionHandler: nil)
					}

				} else {
					ZLogManagerWrapper.sharedInstance.logError(message: "can't send email")
				}

			}
			completion?()
		}

		private func formatMailtoString(to: String?, subject: String?, body: String?) -> String {
			"mailto:\(to ?? "")?subject=\(subject ?? "")&body=\(body ?? "")"
					.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
		}

		func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
			dismissAction?()
		}
	}

	class Share: Action {
		private let text: String?

		init(text: String?) {
			self.text = text
			super.init()
		}

		override func perform(onController viewController: UIViewController, completion: (() -> ())?) {
			let items = [text]
			let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
			activityController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
				completion?()
			}
			viewController.present(activityController, animated: true)
		}
	}

	class AppNavigation: Action {
		let appNavigationCommand: String
		var appNavigationDelegate: AppNavigationDelegate?

		init(appNavigationCommand: String) {
			self.appNavigationCommand = appNavigationCommand
			super.init()
		}

		override func perform(onController viewController: UIViewController, completion: (() -> ())?) {
			completion?()
			appNavigationDelegate?.handleAppNavigation(appNavigationCommand)
		}
	}

	class ExternalLink: Action {
		private let identifier: String?
		private let iosLink: String?
		private let webLink: String?

		init(identifier: String?, iosLink: String?, webLink: String?) {
			self.identifier = identifier
			self.iosLink = iosLink
			self.webLink = webLink
			super.init()
		}

		override func perform(onController viewController: UIViewController, completion: (() -> ())?) {
			if let identifier = identifier, let scheme = URL(string: identifier), UIApplication.shared.canOpenURL(scheme) {
				if let url = iosLink, let validUrl = URL(string: url){
					UIApplication.shared.open(validUrl) { _ in
						completion?()
					}
				}
			} else if let webLink = webLink, let validUrl = URL(string: webLink), UIApplication.shared.canOpenURL(validUrl) {
				UIApplication.shared.open(validUrl){ _ in
					completion?()
				}
			} else {
				let alert = UIAlertController(title: nil, message: "Can't find supporting application", preferredStyle: .alert)
				alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { _ in
					completion?()
				}))
				viewController.present(alert, animated: true, completion: nil)
			}
		}
	}

	class WebNavigation: Action, SFSafariViewControllerDelegate {
		private let url: String
		private var dismissAction: (() -> ())?

		init(url: String) {
			self.url = url
			super.init()
		}

		override func perform(onController viewController: UIViewController, completion: (() -> ())?) {
			dismissAction = completion
			if let url = URL(string: url) {
				openSafariVC(viewController: viewController, with: url)
			} else {
				ZLogManagerWrapper.sharedInstance.logError(message: "cta url is invalid")
			}
		}

		private func openSafariVC(viewController: UIViewController, with url: URL) {
			let safariVC = SFSafariViewController(url: url)
			safariVC.delegate = self
			viewController.present(safariVC, animated: true)
		}

		func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
			dismissAction?()
		}
	}

	class RateOnAppstore: Action {
		public override init() {}
		override func perform(onController viewController: UIViewController, completion: (() -> ())?) {
			if #available( iOS 10.3,*) {
				SKStoreReviewController.requestReview()
			}
			completion?()
		}
	}

	class CallTo: Action {
		private let number: String

		init(number: String) {
			self.number = number
			super.init()
		}

		override func perform(onController viewController: UIViewController, completion: (() -> ())?) {
			guard let numberToCall = URL(string: "tel://" + number) else { return }
			UIApplication.shared.open(numberToCall)
			completion?()
		}
	}

	class None: Action {
		public override init() {}

		override func perform(onController viewController: UIViewController, completion: (() -> ())?) {
			completion?()
		}
	}
}
