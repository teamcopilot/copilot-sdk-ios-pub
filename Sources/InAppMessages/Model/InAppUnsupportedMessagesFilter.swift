//
// Created by Michael Noy on 02/08/2021.
// Copyright (c) 2021 Zemingo. All rights reserved.
//

import Foundation

class InAppUnsupportedMessagesFilter {
    func filter(appNavigationHandler: AppNavigationDelegate?, messages: [InAppMessage]) -> [InAppMessage] {
        messages.filter { message in
            var ctas: [CtaType]
            if (message.presentation is SimpleInAppPresentationModel) {
                let presentation = message.presentation as! SimpleInAppPresentationModel
                ctas = presentation.ctas
            } else if (message.presentation is HtmlInAppPresentationModel) {
                let presentation = message.presentation as! HtmlInAppPresentationModel
                ctas = presentation.ctas
            } else if (message.presentation is FullscreenInAppPresentationModel) {
                let presentation = message.presentation as! FullscreenInAppPresentationModel
                let buttonBars = ((presentation as! FullscreenInAppPresentationModel).viewData.filter { $0 is ButtonsBarInAppData } as! [ButtonsBarInAppData])
                ctas = buttonBars.flatMap { $0.buttonCtaData } as! [CtaType]
                print("")
            } else {
                return true
            }

            return ctas.allSatisfy { cta in
                checkCtaSatisfy(cta: cta, appNavigationHandler: appNavigationHandler)
            }
        }
    }

    private func checkCtaSatisfy(cta: CtaType, appNavigationHandler: AppNavigationDelegate?) -> Bool {
        var ctaAction: Action? = nil
        if (cta is CtaButtonType) {
            let ctaButtonType = cta as! CtaButtonType
            ctaAction = ctaButtonType.ctaAction
        } else if (cta is CtaHtmlType) {
            let ctaHtmlType = cta as! CtaHtmlType
            ctaAction = ctaHtmlType.ctaAction
        }

        switch ctaAction {
        case is Action.AppNavigation:
            if let appNavigationHandler = appNavigationHandler {
                guard let command = (ctaAction as? Action.AppNavigation)?.appNavigationCommand else { return false }
                return appNavigationHandler.getSupportedAppNavigationCommands().contains(command)
            } else {
                return false
            }
        default:
            return true
        }
    }
}