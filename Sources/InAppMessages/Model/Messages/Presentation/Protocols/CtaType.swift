//
//  CtaTypeProtocol.swift
//  CopilotAPIAccess
//
//  Created by Elad on 28/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation

protocol CtaType {
    var report: String? { get }
    var shouldDismiss: Bool { get }
    var ctaAction: Action { get }
}
