//
//  CtaAction.swift
//  CopilotAPIAccess
//
//  Created by Elad on 28/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation


enum CtaActionType: String {
    case WebNavigation = "WebNavigation"
    case AppStoreRate = "AppStoreRate"
    case SendEmail = "SendEmail"
    case Call = "Call"
    case None = "None"
    case Share = "Share"
    case AppNavigation = "AppNavigation"
    case ExternalLink = "ExternalLink"
}

struct CtaActionMapper {

    //MARK: - Consts
    private struct Keys {
        static let type = "_type"
    }

    //MARK: - Factory
    static func mapAction(withDictionary dictionary: [String: Any]) -> Action? {
        let typeResponse = dictionary[Keys.type] as? String
        var type: Action?
        switch typeResponse {
        case CtaActionType.WebNavigation.rawValue:
            if let webCtaType = WebNavigationCtaActionType(withDictionary: dictionary) {
                type = Action.WebNavigation(url: webCtaType.url)
            }
        case CtaActionType.AppStoreRate.rawValue:
            type = Action.RateOnAppstore()
        case CtaActionType.SendEmail.rawValue:
            let sendEmailType = SendEmailCtaActionType(withDictionary: dictionary)
            type = Action.SendEmail(mailTo: sendEmailType.mailTo, subject: sendEmailType.subject, body: sendEmailType.body)
        case CtaActionType.Call.rawValue:
            if let callActionType = CallCtaActionType(withDictionary: dictionary) {
                type = Action.CallTo(number: callActionType.phoneNumber)
            }
        case CtaActionType.None.rawValue:
            type = Action.None()
        case CtaActionType.Share.rawValue:
            if let shareType = ShareCtaActionType(withDictionary: dictionary) {
                type = Action.Share(text: shareType.text)
            }
        case CtaActionType.ExternalLink.rawValue:
            if let openAnotherAppType = ExternalLinkCtaActionType(withDictionary: dictionary) {
                type = Action.ExternalLink(identifier: openAnotherAppType.iosObject?.identifier, iosLink: openAnotherAppType.iosObject?.url, webLink: openAnotherAppType.appUrl)
            }
        case CtaActionType.AppNavigation.rawValue:
            if let appNavigation = AppNavigationCtaActionType(withDictionary: dictionary) {
                type = Action.AppNavigation(appNavigationCommand: appNavigation.appNavigationCommand)
            }
        default:
            ZLogManagerWrapper.sharedInstance.logError(message: "try to map unknown ctaType")
        }
        return type
    }
}
