//
// Created by Alex Gold on 09/05/2022.
// Copyright (c) 2022 Copilot.cx. All rights reserved.
//

import Foundation


class FullscreenInAppPresentationModel: InAppMessagePresentation {
	private struct Keys {
		static let viewTypes = "viewTypes"
 		static let backgroundColorHex = "backgroundColorHex"
		static let dismissButtonEnabled = "dismissButtonEnabled"
	}

	let backgroundColorHex: String
	let viewData: [ViewData]
	let dismissButtonEnabled: Bool?

	init?(withDictionary dictionary: [String: Any]) {
		guard let viewTypesDict = dictionary[Keys.viewTypes] as? [[String: Any]],
			  let viewDataConfig = FullscreenInappViewMapper().mapToViewData(fromDictionary: viewTypesDict)
		else {
			return nil
		}
		guard let backgroundColorHex = dictionary[Keys.backgroundColorHex] as? String, backgroundColorHex.isValidColor() else { return nil }
		self.backgroundColorHex = backgroundColorHex
		viewData = viewDataConfig
		if let dismissButtonEnabled = dictionary[Keys.dismissButtonEnabled] as? Bool { self.dismissButtonEnabled = dismissButtonEnabled } else { self.dismissButtonEnabled = true }
	}
}

class FullscreenInappViewMapper {
	private enum FullscreenPresentationType: String {
		case imageViewType = "ImageViewType"
		case textViewType = "TextViewType"
		case buttonViewType = "ButtonsBarViewType"
	}

	private struct Keys {
		static let type = "_type"
	}

	func mapToViewData(fromDictionary viewTypes: [[String: Any]]) -> [ViewData]? {
		var viewTypeData = [ViewData]()

		for viewType in viewTypes {
			var data: ViewData?
			let modelType = viewType[Keys.type] as? String
			switch modelType {
			case FullscreenPresentationType.imageViewType.rawValue:
				data = ImageInAppData(withDictionary: viewType)
			case FullscreenPresentationType.textViewType.rawValue:
				data = TextInAppData(withDictionary: viewType)
			case FullscreenPresentationType.buttonViewType.rawValue:
				data = ButtonsBarInAppData(withDictionary: viewType)
			default:
				ZLogManagerWrapper.sharedInstance.logError(message: "Failed to parse FullScreenPresentationModel")
				return nil
			}

			guard let data = data else {
				ZLogManagerWrapper.sharedInstance.logError(message: "Failed to parse \(String(describing: modelType))")
				return nil
			}
			viewTypeData.append(data)
		}
		let dataToValidate = viewTypeData.filter { $0 is AnchorableViewData }
		return !viewTypeData.isEmpty && validateModel(input: dataToValidate as! [AnchorableViewData]) ? viewTypeData : nil
	}

	func validateModel(input: [AnchorableViewData]) -> Bool {
		AnchorValidator().validate(input)
	}
}