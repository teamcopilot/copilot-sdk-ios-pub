//
//  CtaHtmlType.swift
//  CopilotAPIAccess
//
//  Created by Elad on 29/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation

struct CtaHtmlType: CtaType {

	//MARK: - Consts
	private struct Keys {
		static let redirectId = "redirectId"
		static let shouldDismiss = "shouldDismiss"
		static let action = "action"
		static let report = "report"
	}

	//MARK: - Properties
	let redirectId: String
	let shouldDismiss: Bool
	let report: String?
	let ctaAction: Action

	// MARK: - Init
	init?(withDictionary dictionary: [String: Any]) {
		guard let redirectId = dictionary[Keys.redirectId] as? String,
			  let actionDict = dictionary[Keys.action] as? [String: AnyObject],
			  let ctaAction = CtaActionMapper.mapAction(withDictionary: actionDict) else { return nil }

		self.redirectId = redirectId
		self.ctaAction = ctaAction

		if let shouldDismiss = dictionary[Keys.shouldDismiss] as? Bool { self.shouldDismiss = shouldDismiss } else { shouldDismiss = true }
		if let report = dictionary[Keys.report] as? String { self.report = report } else { report = nil }
	}
}
