//
//  CtaButtonType.swift
//  CopilotAPIAccess
//
//  Created by Elad on 28/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation

struct CtaButtonType: CtaType {
    
    //MARK: - Consts
    private struct Keys {
        static let backgroundColorHex = "backgroundColorHex"
        static let backgroundColorAlpha = "backgroundColorAlpha"
        static let text = "text"
        static let textColorHex = "textColorHex"
        static let textColorAlpha = "textColorAlpha"
        static let textSize = "textSize"
        static let action = "action"
        static let shouldDismiss = "shouldDismiss"
        static let report = "report"
    }
    
    //MARK: - Properties
    let backgroundColorHex: String
    let backgroundColorAlpha: String
    let text: String
    let textColorHex: String
    let textColorAlpha: String
    let textSize: CGFloat?
    let shouldDismiss: Bool
    let ctaAction: Action
    let report: String?
    
    // MARK: - Init
    init?(withDictionary dictionary: [String: Any]) {
        guard let text = dictionary[Keys.text] as? String,
              let actionDict = dictionary[Keys.action] as? [String : AnyObject],
              let ctaAction = CtaActionMapper.mapAction(withDictionary: actionDict),
              let textColorHex = dictionary[Keys.textColorHex] as? String,
              let backgroundColorHex = dictionary[Keys.backgroundColorHex] as? String else { return nil }

        let textSize = dictionary[Keys.textSize] as? CGFloat

        textColorAlpha = (dictionary[Keys.textColorAlpha] as? String)?.validateAlpha() ?? "FF"
        backgroundColorAlpha = (dictionary[Keys.backgroundColorAlpha] as? String)?.validateAlpha() ?? "FF"

        self.text = text
        self.ctaAction = ctaAction
        self.textColorHex = textColorHex
        self.backgroundColorHex = backgroundColorHex
        self.textSize = textSize

        if let shouldDismiss = dictionary[Keys.shouldDismiss] as? Bool { self.shouldDismiss = shouldDismiss } else { self.shouldDismiss = true }
        if let report = dictionary[Keys.report] as? String { self.report = report } else { self.report = nil }
    }
}
