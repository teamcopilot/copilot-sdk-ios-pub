//
// Created by Alex Gold on 27/07/2022.
// Copyright (c) 2022 Copilot.cx. All rights reserved.
//

import Foundation


class AnchorValidator {
	func validate(_ input: [AnchorableViewData]) -> Bool {
		let viewIds = input.map { $0.viewId }.filter { !$0.isEmpty }
		if viewIds.count != input.count {
			ZLogManagerWrapper.sharedInstance.logError(message: "Some of the anchored views don't have a viewId")
			return false
		} //All AnchoredViews should have a ViewId
		if Set(viewIds).count != viewIds.count {
			ZLogManagerWrapper.sharedInstance.logError(message: "Multiple anchored views with the same viewId")
			return false
		}

		let relativeViewIds = input.filter { isID(anchor: $0.anchor) }

		for anchorData in relativeViewIds { //check all relational anchors have a viewId
			if anchorData.anchor.getViewId()!.isEmpty { return false }
		}

		let allIdsReferences = relativeViewIds.allSatisfy { data in
			switch data.anchor {
			case .ID(let viewId, _, _):
				if viewIds.contains(viewId) {
					return true
				}
				return false
			default:
				return false
			}
		}

		if !allIdsReferences { return false }
		return validateLoops(anchors: input)
	}


	func validateLoops(anchors: [AnchorableViewData]) -> Bool {
		anchors.allSatisfy { data in
			var history = [AnchorableViewData]()
			var current = data

			while (isID(anchor: current.anchor) && !history.contains(current)) {
				history.append(current)
				current = anchors.first { $0.viewId == current.anchor.getViewId()}!
			}
			if !isID(anchor: current.anchor) {
				return true
			} else if (history.contains(current)) {
				return false
			}
			return false
		}
	}

	func validateAnchorsInOrder(anchors: [AnchorableViewData]) -> Bool {
		var anchorList = anchors.filter { isAbsolute(anchor: $0.anchor) }
				.map{ $0.viewId } //Starting with absolutes
		let idAnchors = anchors.filter { isID(anchor: $0.anchor) }
		for anchor in idAnchors {
			let sourceViewId = anchor.anchor.getViewId()
			if !anchorList.contains(sourceViewId!) { return false }
			anchorList.append(anchor.viewId)
		}

		return true
	}

	func sortAnchors(anchors: [AnchorableViewData]) -> [AnchorableViewData] {
		var sorted = anchors.filter { isAbsolute(anchor: $0.anchor) }
		var idAnchors = anchors.filter { isID(anchor: $0.anchor) } // without Absolutes

		while !idAnchors.isEmpty {
			idAnchors.forEach { anchor in
				if sorted.map ({ $0.viewId }).contains(anchor.anchor.getViewId()) {
					sorted.append(anchor)
					idAnchors.removeAll { $0.viewId == anchor.viewId }
				}
			}
		}
		return sorted
	}

	func isID(anchor: ViewAnchor?) -> Bool {
		switch anchor {
		case .ID(_, _, _):
			return true
		default:
			return false
		}
	}

	func isAbsolute(anchor: ViewAnchor?) -> Bool {
		!isID(anchor: anchor)
	}
}
