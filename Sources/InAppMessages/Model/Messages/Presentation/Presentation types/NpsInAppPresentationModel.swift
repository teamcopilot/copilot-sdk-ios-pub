//
//  NpsInappModel.swift
//  CopilotAPIAccess
//
//  Created by Elad on 13/05/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation


class NpsInAppPresentationModel: InAppMessagePresentation {
    
    //MARK: - Consts
    private struct Keys {
        static let ctaBackgroundColorHex = "ctaBackgroundColorHex"
        static let labelQuestionText = "labelQuestionText"
        static let labelNotLikely = "labelNotLikely"
        static let labelExtremelyLikely = "labelExtremelyLikely"
        static let labelAskMeAnotherTime = "labelAskMeAnotherTime"
        static let labelDone = "labelDone"
        static let backgroundColorHex = "backgroundColorHex"
        static let imageUrl = "imageUrl"
    }
    
    //MARK: - Properties
    let ctaBackgroundColorHex: String
    let labelQuestionText: String
    let labelNotLikely: String
    let labelExtremelyLikely: String
    let labelAskMeAnotherTime: String
    let labelDone: String
    let backgroundColorHex: String

    let imageUrl: String?
    
    // MARK: - Init
    init?(withDictionary dictionary: [String: Any]) {
        guard let ctaBackgroundColorHex = dictionary[Keys.ctaBackgroundColorHex] as? String, ctaBackgroundColorHex.isValidColor() else { return nil }
        guard let backgroundColorHex = dictionary[Keys.backgroundColorHex] as? String, backgroundColorHex.isValidColor() else { return nil }

        guard
            let labelQuestionText = dictionary[Keys.labelQuestionText] as? String,
            let labelNotLikely = dictionary[Keys.labelNotLikely] as? String,
            let labelExtremelyLikely = dictionary[Keys.labelExtremelyLikely] as? String,
            let labelAskMeAnotherTime = dictionary[Keys.labelAskMeAnotherTime] as? String,
            let labelDone = dictionary[Keys.labelDone] as? String
            else { return nil }

        self.ctaBackgroundColorHex = ctaBackgroundColorHex
        self.labelQuestionText = labelQuestionText
        self.labelNotLikely = labelNotLikely
        self.labelExtremelyLikely = labelExtremelyLikely
        self.labelAskMeAnotherTime = labelAskMeAnotherTime
        self.labelDone = labelDone
        self.backgroundColorHex = backgroundColorHex

        if let imageUrl = dictionary[Keys.imageUrl] as? String { self.imageUrl = imageUrl } else { self.imageUrl = nil }
    }
}


