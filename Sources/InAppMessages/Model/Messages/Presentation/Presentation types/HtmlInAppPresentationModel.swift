//
//  HtmlInappPresentationModel.swift
//  CopilotAPIAccess
//
//  Created by Elad on 29/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation

class HtmlInAppPresentationModel: InAppMessagePresentation {
    
    //MARK: - Consts
    private struct Keys {
        static let content = "content"
        static let ctas = "ctas"
        static let dismissButtonEnabled = "dismissButtonEnabled"
    }
    
    //MARK: - Properties
    let content: String
    let dismissButtonEnabled: Bool
    var ctas: [CtaHtmlType] = []
    
    // MARK: - Init
    init?(withDictionary dictionary: [String: Any]) {
        guard let content = dictionary[Keys.content] as? String else { return nil }
        
        self.content = content
        
        if let ctasArr = dictionary[Keys.ctas] as? [[String : AnyObject]] {
            var ctasArrFromJson: [CtaHtmlType?] = []
            ctasArr.forEach { ctasArrFromJson.append(CtaTypeMapper.map(withDictionary: $0) as? CtaHtmlType) }
            if ctasArrFromJson.contains(where: {$0 == nil}) { return nil }
            ctas = ctasArrFromJson.compactMap{$0}
        }
        if let dismissButtonEnabled = dictionary[Keys.dismissButtonEnabled] as? Bool { self.dismissButtonEnabled = dismissButtonEnabled } else { self.dismissButtonEnabled = true }
    }
}