//
//  SimpleInappPresentationModel.swift
//  CopilotAPIAccess
//
//  Created by Elad on 28/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation


class SimpleInAppPresentationModel: InAppMessagePresentation {
    
    //MARK: - Consts
    private struct Keys {
        static let title = "title"
        static let body = "body"
        static let imageUrl = "imageUrl"
        static let ctas = "ctas"
        static let titleColorHex = "textColorHex"
        static let titleColorAlpha = "textColorAlpha"
        static let backgroundColorHex = "backgroundColorHex"
        static let dismissButtonEnabled = "dismissButtonEnabled"
    }
    
    //MARK: - Properties
    let title: String
    let imageUrl: String?
    let bodyText: String?
    let titleColorHex: String?
    let titleColorAlpha: String
    let backgroundColorHex: String?
    let dismissButtonEnabled: Bool?
    let ctas: [CtaType]
    
    // MARK: - Init
    init?(withDictionary dictionary: [String: Any]) {
        guard let title = dictionary[Keys.title] as? String,
            let ctasArr = dictionary[Keys.ctas] as? [[String : AnyObject]], 1...3 ~= ctasArr.count  else { return nil }

        var ctasFromJson: [CtaType?] = []
        ctasArr.forEach { ctasFromJson.append(CtaTypeMapper.map(withDictionary: $0)) }
        if ctasFromJson.contains(where: {$0 == nil}) { return nil }
        ctas = ctasFromJson.compactMap{$0} // removes nils

        self.title = title

        if let body = dictionary[Keys.body] as? String { self.bodyText = body.trimmingCharacters(in: .whitespacesAndNewlines) } else { self.bodyText = nil }
        if let imageUrl = dictionary[Keys.imageUrl] as? String { self.imageUrl = imageUrl } else { self.imageUrl = nil }
        if let titleColorHex = dictionary[Keys.titleColorHex] as? String { self.titleColorHex = titleColorHex } else { self.titleColorHex = nil }
        titleColorAlpha = (dictionary[Keys.titleColorAlpha] as? String)?.validateAlpha() ?? "FF"
        if let backgroundColorHex = dictionary[Keys.backgroundColorHex] as? String { self.backgroundColorHex = backgroundColorHex } else { self.backgroundColorHex = nil }
        if let dismissButtonEnabled = dictionary[Keys.dismissButtonEnabled] as? Bool { self.dismissButtonEnabled = dismissButtonEnabled } else { self.dismissButtonEnabled = true }
    }
}