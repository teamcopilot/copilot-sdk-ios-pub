//
// Created by Michael Noy on 11/08/2021.
// Copyright (c) 2021 Zemingo. All rights reserved.
//

import Foundation

class AppNavigationReporter {

    private let reporter: ReportAPIAccess
    private let appNavigationDelegate: AppNavigationDelegate?
    private var userDefaults: CopilotSettings

    init(reporter: ReportAPIAccess, appNavigationDelegate: AppNavigationDelegate?, userDefaults: CopilotSettings) {
        self.reporter = reporter
        self.appNavigationDelegate = appNavigationDelegate
        self.userDefaults = userDefaults
    }

    func reportSupportedActions() {
        if let appNavigationDelegate = appNavigationDelegate {
            let navigationCommands = appNavigationDelegate.getSupportedAppNavigationCommands()
            if didSupportedCommandsChange(commands: navigationCommands) {
                if navigationCommands.isEmpty {
                    reporter.log(event: SupportedActionsAnalyticsEvent(supportedAppNavigationCommand: ""))
                } else {
                    for command in appNavigationDelegate.getSupportedAppNavigationCommands() {
                        reporter.log(event: SupportedActionsAnalyticsEvent(supportedAppNavigationCommand: command))
                    }
                }
            }
        }
    }

    private func didSupportedCommandsChange(commands: [String]) -> Bool {
        let existingCommands = userDefaults.navigationCommands
        if !(existingCommands.isEmpty) {
            if existingCommands.count == commands.count && Set(existingCommands) == Set(commands) {
                return false
            } else {
                userDefaults.navigationCommands = commands
                return true
            }
        }
        if commands.isEmpty {
            return false
        } else {
            userDefaults.navigationCommands = commands
            return true
        }
    }
}