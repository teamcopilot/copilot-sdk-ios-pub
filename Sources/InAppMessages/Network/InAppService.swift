//
//  InAppService.swift
//  CopilotAPIAccess
//
//  Created by Elad on 22/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation
import Moya


internal enum InAppService {
    case getInAppMessages(locale: String? ,authType: AuthorizationType?)
    case postMessageDisplayed(locale: String?, messageId: String, authType: AuthorizationType?)
    case getInAppConfiguration(locale: String?, os: String, appVersion: String, sdkVerion: String)

    private func localeHeader(locale: String?) -> [String: String] {
        let UNKNOWN = "Unknown"
        guard let locale = locale else { return ["x-copilot-locale": UNKNOWN] }
        return ["x-copilot-locale": locale]
    }
}

extension InAppService: TargetType, AccessTokenAuthorizable {
    private struct Consts {
        static let servicePath = "\(NetworkParameters.shared.apiVersion)/inapp"
        
        static let iamMessagesPathSuffix = "/messages"
        static let iamMessageDisplayedPathSuffix = "/displayed"
        static let iamConfiguration = "/config"
        
        //parameters
        static let osParam = "osType"
        static let appVersionParam = "appVersion"
        static let sdkVersionParam = "sdkVersion"
        
    }
    
    public var baseURL: URL {
        return NetworkParameters.shared.baseURL
    }
    
    var authorizationType: AuthorizationType? {
        switch self {
        case .getInAppMessages(locale: _, let authType),
             .postMessageDisplayed(_, _, let authType):
            return authType
        case .getInAppConfiguration:
            return nil
        }
    }
    
    public var path: String {
        switch self {
        case .getInAppMessages:
            return Consts.servicePath + Consts.iamMessagesPathSuffix
        case .postMessageDisplayed(_, let messageId, _):
            return Consts.servicePath + Consts.iamMessagesPathSuffix + "/" + messageId + Consts.iamMessageDisplayedPathSuffix
        case .getInAppConfiguration:
            return Consts.servicePath + Consts.iamConfiguration
        }
    }
    
    public var method: Moya.Method  {
        switch self {
        case .getInAppMessages, .getInAppConfiguration:
            return .get
        case .postMessageDisplayed(_, _ , _):
            return .post
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
        case .getInAppMessages, .postMessageDisplayed:
            return nil
        case .getInAppConfiguration(_, let os, let appVersion, let sdkVerion):
            return [Consts.osParam: os, Consts.appVersionParam: appVersion, Consts.sdkVersionParam: sdkVerion]
        }
    
    }
    
    public var parameterEncoding: ParameterEncoding {
        switch self {
        case .getInAppMessages, .postMessageDisplayed:
            return JSONEncoding.default
        case .getInAppConfiguration:
            return URLEncoding.default
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        if let params = parameters {
            return .requestParameters(parameters: params, encoding: parameterEncoding)
        } else {
            return .requestPlain
        }
    }
    
    public var headers: [String : String]? {
        switch self {
        case .getInAppMessages(let locale, _):
			return localeHeader(locale: locale)
        case .getInAppConfiguration(let locale, _, _, _):
            return localeHeader(locale: locale)
        case .postMessageDisplayed(let locale, _, _):
            return localeHeader(locale: locale)
        default:
            return nil
        }
    }
}
