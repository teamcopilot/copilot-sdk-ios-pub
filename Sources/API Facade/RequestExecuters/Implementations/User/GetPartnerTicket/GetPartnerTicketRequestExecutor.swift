//
// Created by Alex Gold on 14/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation


class GetPartnerTicketRequestExecutor: RequestExecuter<GetPartnerTicketResponse, GetPartnerTicketError> {
	private let partnerId: String
	private let dependencies: HasAuthenticationServiceInteraction

	init(partnerId: String, dependencies: HasAuthenticationServiceInteraction) {
		self.partnerId = partnerId
		self.dependencies = dependencies
		super.init()
	}

	override func execute(_ closure: @escaping (Response<GetPartnerTicketResponse, GetPartnerTicketError>) -> ()) {
		guard let applicationId = BundleConfigurationProvider().applicationId else {
			ZLogManagerWrapper.sharedInstance.logInfo(message: "Couldn't read application Id from Plist file")
			return
		}
		if partnerId.isEmpty {
			closure(.failure(error: .InvalidPartnerId(debugMessage: "InvalidPartnerId")))
			return
		}
		dependencies.authenticationServiceInteraction.getPartnerTicket(applicationID: applicationId, PartnerId: partnerId, closure: closure)
	}
}