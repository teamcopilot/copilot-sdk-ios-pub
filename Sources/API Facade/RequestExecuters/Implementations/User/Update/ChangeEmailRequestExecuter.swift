//
// Created by Alex Gold on 10/07/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

class ChangeEmailRequestExecuter: RequestExecuter<UserMe, ChangeEmailError> {

	typealias Dependencies = HasUserServiceInteraction
	private let dependencies: Dependencies

	private let email: String

	init(newEmail: String, dependencies: Dependencies) {
		self.email = newEmail
		self.dependencies = dependencies
	}

	override func execute(_ closure: @escaping (Response<UserMe, ChangeEmailError>) -> Void) {
		dependencies.userServiceInteraction.changeEmail(newEmail: email, closure: closure)
	}
}

