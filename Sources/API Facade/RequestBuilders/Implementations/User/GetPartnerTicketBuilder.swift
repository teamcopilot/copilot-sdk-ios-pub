//
// Created by Alex Gold on 14/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

class GetPartnerTicketBuilder: RequestBuilder<GetPartnerTicketResponse, GetPartnerTicketError> {
	private let dependencies: HasAuthenticationServiceInteraction
	private let partnerId: String

	init(partnerId: String, dependencies: HasAuthenticationServiceInteraction) {
		self.partnerId = partnerId
		self.dependencies = dependencies
		super.init()
	}

	override func build() -> RequestExecuter<GetPartnerTicketResponse, GetPartnerTicketError> {
		GetPartnerTicketRequestExecutor(partnerId: partnerId, dependencies: dependencies)
	}
}