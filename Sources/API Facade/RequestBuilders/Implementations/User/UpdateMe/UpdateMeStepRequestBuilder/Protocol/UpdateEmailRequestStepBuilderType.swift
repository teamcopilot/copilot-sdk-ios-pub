//
// Created by Alex Gold on 09/07/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

public protocol UpdateEmailRequestStepBuilderType {
	func withEmail(_ newEmail: String) -> RequestBuilder<UserMe, ChangeEmailError>
}