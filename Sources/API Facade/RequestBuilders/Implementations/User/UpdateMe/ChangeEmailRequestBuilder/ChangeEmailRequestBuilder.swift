//
// Created by Alex Gold on 09/07/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

class ChangeEmailRequestBuilder: RequestBuilder<UserMe, ChangeEmailError> {

	typealias Dependencies = HasUserServiceInteraction
	private let dependencies: Dependencies

	private let email: String

	init(newEmail: String, dependencies: Dependencies) {
		self.email = newEmail
		self.dependencies = dependencies
	}

	override func build() -> RequestExecuter<UserMe, ChangeEmailError> {
		ChangeEmailRequestExecuter(newEmail: email, dependencies: dependencies)
	}
}
