//
// Created by Alex Gold on 13/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

public protocol SphereUserAPIAccess: UserAPIAccess {
	func getPartnerTicket(partnerId: String) -> RequestBuilder<GetPartnerTicketResponse, GetPartnerTicketError>
}