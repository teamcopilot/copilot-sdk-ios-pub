//
// Created by Alex Gold on 13/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

public enum GetPartnerTicketError: CopilotError {
	case InvalidCredentials(debugMessage: String)
	case InvalidApplicationId(debugMessage: String)
	case InvalidPartnerId(debugMessage: String)
	case RequiresRelogin(debugMessage: String)
	case ConnectivityError(debugMessage: String)
	case GeneralError(debugMessage: String)

	static func generalError(message: String) -> GetPartnerTicketError {
		.GeneralError(debugMessage: message)
	}
}

public class GetPartnerTicketErrorResolver: ErrorResolver{
	public typealias T = GetPartnerTicketError

	public func fromRequiresReloginError(debugMessage: String) -> GetPartnerTicketError {
		.RequiresRelogin(debugMessage: debugMessage)
	}

	public func fromInvalidParametersError(debugMessage: String) -> GetPartnerTicketError {
		.InvalidPartnerId(debugMessage: debugMessage)
	}

	public func fromGeneralError(debugMessage: String) -> GetPartnerTicketError {
		.generalError(message: debugMessage)
	}

	public func fromConnectivityError(debugMessage: String) -> GetPartnerTicketError {
		.ConnectivityError(debugMessage: debugMessage)
	}

	public func fromTypeSpecificError(_ statusCode: Int, _ reason: String, _ message: String) -> GetPartnerTicketError? {
		if isInvalidPartnerId(statusCode, reason) {
			return .InvalidPartnerId(debugMessage: "Provided partner id is not valid.")
		} else if isInvalidApplicationId(statusCode, reason) {
			return .InvalidApplicationId(debugMessage: "ApplicationId is invalid, check your manifest configuration")
		} else if isInvalidCredentials(statusCode, reason) {
			return .InvalidCredentials(debugMessage: "partnerId and applicationId mismatch")
		}
		else {
			return .GeneralError(debugMessage: message)
		}
	}
	
	func isInvalidCredentials(_ statusCode: Int, _ reason: String) -> Bool {
		statusCode == ServerError.Constants.unauthorized && reason == ServerError.Constants.partnerIdAppIdMismatchReason
	}

	func isInvalidPartnerId(_ statusCode: Int, _ reason: String) -> Bool {
		statusCode == ServerError.Constants.badRequestErrorCode && reason == ServerError.Constants.badRequestInvalidPartnerId
	}
}
