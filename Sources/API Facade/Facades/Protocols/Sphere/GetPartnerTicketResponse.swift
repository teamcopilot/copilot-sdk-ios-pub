//
// Created by Alex Gold on 13/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation


public class GetPartnerTicketResponse {
	private struct Keys {
		static let ticketKey = "partnerTicket"
	}

	public var partnerTicket: String

	init(partnerTicket: String) {
		self.partnerTicket = partnerTicket
	}

	public static func parse(_ dictionary: [String: Any]) -> GetPartnerTicketResponse? {
		guard let partnerTicket = dictionary[Keys.ticketKey] as? String else {
			ZLogManagerWrapper.sharedInstance.logError(message: "The response does not contain a '\(Keys.ticketKey)' element")
			return nil
		}
		return .init(partnerTicket: partnerTicket)
	}
}

