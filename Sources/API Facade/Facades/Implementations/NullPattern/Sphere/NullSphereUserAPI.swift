//
// Created by Alex Gold on 14/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

class NullSphereUserAPI: SphereUserAPIAccess {
	func getPartnerTicket(partnerId: String) -> RequestBuilder<GetPartnerTicketResponse, GetPartnerTicketError> {
		NullRequestBuilder<GetPartnerTicketResponse, GetPartnerTicketError>()
	}

	func elevate() -> ElevateUserRequestStepBuilderType {
		NullElevateUserRequestStepBuilder()
	}

	func sendVerificationEmail() -> RequestBuilder<Void, SendVerificationEmailError> {
		NullRequestBuilder<Void, SendVerificationEmailError>()
	}

	func resetPassword(for email: String) -> RequestBuilder<Void, ResetPasswordError> {
		NullRequestBuilder<Void, ResetPasswordError>()
	}

	func fetchMe() -> RequestBuilder<UserMe, FetchMeError> {
		NullRequestBuilder<UserMe, FetchMeError>()
	}

	func updateMe() -> UpdateRequestStepBuilderType {
		NullUpdateRequestStepBuilder()
	}

	func deleteMe() -> RequestBuilder<Void, DeleteUserError> {
		NullRequestBuilder<Void, DeleteUserError>()
	}
}
