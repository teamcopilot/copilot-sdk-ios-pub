 //
// Created by Alex Gold on 13/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

public class NullSphere: SphereAccess {
	public let app: AppAPIAccess = NullAppAPI()
	public let auth: AuthAPIAccess = NullAuthAPI()
	public let user: SphereUserAPIAccess = NullSphereUserAPI()
	public let thing: ThingAPIAccess = NullThingAPI()
	public var defaultAuthProvider: AuthenticationProvider {
		NullAuthenticationProvider()
	}
}