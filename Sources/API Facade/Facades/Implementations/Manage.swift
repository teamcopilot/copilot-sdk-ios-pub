//
//  Manage.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 08/05/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class Manage {
    public let sphere: SphereAccess
    @available(* , deprecated, renamed: "sphere")
    public let copilotConnect: CopilotConnectAccess
    public let yourOwn: YourOwnAccess

    init(authenticationProviderContainer: AuthenticationProviderContainer, reporter: ReportAPIAccess, configurationProvider: ConfigurationProvider, sessionObserver: SessionLifeTimeObserver, sequentialExecutionHelper: MoyaSequentialExecutionHelper) {
        switch (configurationProvider.manageType) {
        case .sphere:
            sphere = Sphere(authenticationProviderContainer: authenticationProviderContainer, reporter: reporter, configurationProvider: configurationProvider, sessionObserver: sessionObserver, sequentialExecutionHelper: sequentialExecutionHelper)
            copilotConnect = NullCopilotConnect()
            yourOwn = NullYourOwn()
        case .copilotConnect:
            sphere = NullSphere()
            copilotConnect = CopilotConnect(authenticationProviderContainer: authenticationProviderContainer, reporter: reporter, configurationProvider: configurationProvider, sessionObserver: sessionObserver, sequentialExecutionHelper: sequentialExecutionHelper)
            yourOwn = NullYourOwn()
        case .yourOwn:
            sphere = NullSphere()
            copilotConnect = NullCopilotConnect()
            yourOwn = YourOwn(authenticationProviderContainer: authenticationProviderContainer, reporter: reporter, sessionObserver: sessionObserver)
        }
    }
}
