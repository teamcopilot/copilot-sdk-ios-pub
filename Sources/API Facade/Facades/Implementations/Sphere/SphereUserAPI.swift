//
// Created by Alex Gold on 14/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

class SphereUserAPI: UserAPI, SphereUserAPIAccess {
	func getPartnerTicket(partnerId: String) -> RequestBuilder<GetPartnerTicketResponse, GetPartnerTicketError> {
		GetPartnerTicketBuilder(partnerId: partnerId, dependencies: dependencies)
	}
}