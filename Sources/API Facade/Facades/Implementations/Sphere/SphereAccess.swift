//
// Created by Alex Gold on 13/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

public protocol SphereAccess: AnyObject {
	var app: AppAPIAccess { get }
	var auth: AuthAPIAccess { get }
	var user: SphereUserAPIAccess { get }
	var thing: ThingAPIAccess { get }
	var defaultAuthProvider: AuthenticationProvider { get }
}