//
// Created by Alex Gold on 02/01/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import UIKit

class ColorUtils {
	static let LUMINANCE_THRESHOLD: Double = 160

	static func calculateLuminance(hexColor: String) -> Double {

		let red = Int(String(hexColor[String.Index(utf16Offset: 1, in: hexColor)...String.Index(utf16Offset: 2, in: hexColor)]), radix: 16)
		let green = Int(String(hexColor[String.Index(utf16Offset: 3, in: hexColor)...String.Index(utf16Offset: 4, in: hexColor)]), radix: 16)
		let blue = Int(String(hexColor[String.Index(utf16Offset: 5, in: hexColor)...String.Index(utf16Offset: 6, in: hexColor)]), radix: 16)

		return sqrt(0.299 * pow(Double(red!), 2) + 0.587 * pow(Double(green!), 2) + 0.114 * pow(Double(blue!), 2))
	}

	static func calculateLuminance(of color: UIColor) -> Double {
		let hexColor = convertUIColorToHex(color: color)
		return calculateLuminance(hexColor: hexColor)
	}

	static func convertUIColorToHex(color: UIColor) -> String {
		let colorComponents = color.cgColor.components!
		if colorComponents.count < 4 {
			return String(format: "#%02x%02x%02x", Int(colorComponents[0]*255.0), Int(colorComponents[0]*255.0),Int(colorComponents[0]*255.0)).uppercased()
		}
		return String(format: "#%02x%02x%02x", Int(colorComponents[0]*255.0), Int(colorComponents[1]*255.0),Int(colorComponents[2]*255.0)).uppercased()
	}

	static func setColorBasedOnLuminance(color: UIColor, forLightColor dark: UIColor, forDarkColor light: UIColor) -> UIColor {
		if calculateLuminance(of: color) > LUMINANCE_THRESHOLD {
			return dark
		} else {
			return light
		}
	}
}