//
// Created by Alex Gold on 11/01/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

extension String {
	internal func withAlpha(_ value: String) -> String {
		var returnString = self
		returnString.insert(contentsOf: value, at: returnString.index(returnString.startIndex, offsetBy: 1))
		return returnString
	}

	internal func isValidColor() -> Bool {
		let hexColorRegex = "^#([A-Fa-f0-9]{6})$"
		let hexColorTest = NSPredicate(format: "SELF MATCHES %@", hexColorRegex)
		return hexColorTest.evaluate(with: self)
	}

	internal func validateAlpha() -> String {
		let hexColorRegex = "^([A-Fa-f0-9]{2})$"
		let hexColorTest = NSPredicate(format: "SELF MATCHES %@", hexColorRegex)
		let isValid = hexColorTest.evaluate(with: self)
		return isValid ? self : "FF"
	}

	internal func validateUrl() -> String? {
		let regex = "((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)"
		let urlTest = NSPredicate(format: "SELF MATCHES %@", regex)
		let isUrlValid = urlTest.evaluate(with: self)
		if (isUrlValid) { return self } else { return nil }
	}
}