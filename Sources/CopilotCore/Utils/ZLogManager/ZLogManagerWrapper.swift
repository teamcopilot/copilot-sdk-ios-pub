import Foundation
import os

public class ZLogManagerWrapper {

    public static let sharedInstance = ZLogManagerWrapper()
    
    private var shouldLog: Bool = false

    // Logger is optional and only initialized if running on iOS 14 or later
    private var logger: Any?

    public required init() {
        if #available(iOS 14.0, *) {
            logger = Logger(subsystem: "copilot-cx", category: "general")
        }
    }
    
    public func enableDebugLogs() {
        shouldLog = true
    }
    
    public func disableDebugLogs(){
        shouldLog = false
    }

    public func logFatal(message: String!, functionName: String = #function) {
        log(level: "FATAL", message: message, functionName: functionName, osLogLevel: .fault)
    }

    public func logError(message: String!, functionName: String = #function) {
        log(level: "ERROR", message: message, functionName: functionName, osLogLevel: .error)
    }

    public func logWarning(message: String!, functionName: String = #function) {
        log(level: "WARNING", message: message, functionName: functionName, osLogLevel: .default)
    }

    public func logInfo(message: String!, functionName: String = #function) {
        log(level: "INFO", message: message, functionName: functionName, osLogLevel: .info)
    }

    public func logDebug(message: String!, functionName: String = #function) {
        log(level: "DEBUG", message: message, functionName: functionName, osLogLevel: .debug)
    }

    private func log(level: String, message: String?, functionName: String, osLogLevel: OSLogType) {
        guard shouldLog else { return }
        let formattedMessage = "\(message ?? "nil") [Function: \(functionName)]"
        
        if #available(iOS 14.0, *) {
            if let logger = logger as? Logger {
                switch osLogLevel {
                case .fault:
                    logger.critical("\(formattedMessage)")
                case .error:
                    logger.error("\(formattedMessage)")
                case .info:
                    logger.info("\(formattedMessage)")
                case .debug:
                    logger.debug("\(formattedMessage)")
                default:
                    logger.log("\(formattedMessage)")
                }
            }
        } else {
            print("\(level): \(formattedMessage)")
        }
    }
}
