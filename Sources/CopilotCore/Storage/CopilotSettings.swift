//
// Created by Alex Gold on 13/09/2022.
// Copyright (c) 2022 Copilot.cx. All rights reserved.
//

import Foundation

public protocol CopilotSettings {
	var navigationCommands: [String] { get set }
}
