//
// Created by Alex Gold on 13/09/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import Foundation

class CopilotSettingsUserDefaults: CopilotSettings {
	private struct Keys {
		static let COPILOT_USER_DEFAULTS_MAME = "copilot_user_defaults"
		static let NAVIGATION_COMMANDS = "navigation_commands"
	}

	private let ud = UserDefaults(suiteName: Keys.COPILOT_USER_DEFAULTS_MAME)

	var navigationCommands: [String] {
		get {
			guard let navigationCommands = ud?.stringArray(forKey: Keys.NAVIGATION_COMMANDS) else { return [] }
			return navigationCommands
		}
		set(commands) {
			ud?.set(commands, forKey: Keys.NAVIGATION_COMMANDS)
		}
	}
}