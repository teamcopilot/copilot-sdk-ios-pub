//
//  UserGeneralParametersRepository.swift
//  User
//
//  Created by Tom Milberg on 03/06/2018.
//  Copyright © 2018 Falcore. All rights reserved.
//

import Foundation

class CopilotUserGeneralParametersRepository: UserGeneralParametersRepository {
    
    var userId: String?
    private let signUpMethod: CredentialsType

    private struct Constants {
        static let signUpMethodParamKey = "sign_up_method"
    }
    
    init(signUpMethod: CredentialsType, userId: String? = nil) {
        self.signUpMethod = signUpMethod
        self.userId = userId
    }
    
    var additionalParams: [String: String] {
        [Constants.signUpMethodParamKey : signUpMethod.rawValue]
    }
    
    var additionalParamsKeys: [String] {
        []
    }
}
