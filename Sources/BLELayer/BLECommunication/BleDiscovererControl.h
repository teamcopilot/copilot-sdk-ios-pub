//
//  BleDiscovererControl.h
//  ZemingoBLELayer
//
//  Created by Miko Halevi on 7/10/17.
//  Copyright © 2017 Adaya. All rights reserved.
//
#import <CoreBluetooth/CoreBluetooth.h>

@class BLEDiscoverer;

#define BLEDiscovererDomainName         @"BLEDiscovererDomainName"

typedef NS_ENUM(NSInteger, BLEDiscoveryControlError) {
    BLEDiscoveryControlErrorUnknown,
    BLEDiscoveryControlErrorBleOff,
    BLEDiscoveryControlErrorOtherDiscovererActive
};


@protocol BleDiscovererControl <NSObject>

- (CBCentralManager *)bleDiscovererAttemptToScan:(BLEDiscoverer *) bleDiscoverer WithErrorPtr:(NSError **)errorPtr;
- (CBCentralManager *)bleDiscovererAttemptStopScanning:(BLEDiscoverer *) bleDiscoverer;
- (BOOL)bleDiscovererStopScanningComleted:(BLEDiscoverer *) bleDiscoverer;

@end

