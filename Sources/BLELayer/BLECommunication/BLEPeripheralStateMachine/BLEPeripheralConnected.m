//
//  BLEPeripheralConnected.m
//  FlirNvrDvr
//
//  Created by Adaya on 3/23/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import "BLEPeripheralConnected.h"
#import "BLEPeripheralIdle.h"
#import "BLEPeripheralDisconnect.h"
#import "BLEPeripheralErrors.h"

@interface BLEPeripheralConnected ()

@property (strong, nonatomic) CBPeripheral* peripheral;
@property (strong, nonatomic) NSSet* foundCharectaristics;

@end

@implementation BLEPeripheralConnected

- (instancetype)initWithPeripheral: (CBPeripheral *) peripheral andAvailableCharectaristics:(NSSet *)availableCharectaristics
{
    self = [super init];
    if (self) {
        self.peripheral = peripheral;
        self.foundCharectaristics = availableCharectaristics;
        
    }
    return self;
}



- (BOOL) isSamePeripheral:(CBPeripheral*) peripheral{
    return ([[peripheral.identifier UUIDString] caseInsensitiveCompare:[self.peripheral.identifier UUIDString]] == NSOrderedSame);
}

- (id<BLEPeripheralConnectionState>) connectToDevice: (CBPeripheral *) peripheral usingManager: (CBCentralManager *) centralManager andRequiredServices:(NSArray *)bleServiceRequirements{
    return nil;
}

-(id<BLEPeripheralConnectionState>)connected:(CBPeripheral *)peripheral{
    return nil;
}

-(id<BLEPeripheralConnectionState>)servicesDiscovered:(CBPeripheral *)peripheral withError:(NSError *)error transitionFailure:(NSError *__autoreleasing *)failure{
    return nil;
}

-(id<BLEPeripheralConnectionState>)charectaristicsDiscovered:(CBPeripheral *)peripheral forService:(CBService *)service withError:(NSError *)error transitionFailure:(NSError *__autoreleasing *)failure{
    return nil;
}

- (id<BLEPeripheralConnectionState>) disconnectUsing: (CBCentralManager*) centralManager{
        return [[BLEPeripheralDisconnect alloc] initWithPeripheral:self.peripheral andCentralManager:centralManager];
}

- (id<BLEPeripheralConnectionState>) connectionFailure: (CBPeripheral *) peripheral{
    if([self isSamePeripheral: peripheral]){
        return [[BLEPeripheralIdle alloc] init];
    }
    return nil;
}

- (id<BLEPeripheralConnectionState>) discoveryFailure: (CBPeripheral *) peripheral{
    if([self isSamePeripheral: peripheral]){
        return [[BLEPeripheralIdle alloc] init];
    }
    return nil;
}

- (id<BLEPeripheralConnectionState>) autoDisconnected: (CBPeripheral *) peripheral{
    if([self isSamePeripheral: peripheral]){
        return [[BLEPeripheralIdle alloc] init];
    }
    return nil;
}

- (id<BLEPeripheralConnectionState>) unExpectedDisconnection{
    // same as auto but there is no need verify peripheral
    return [[BLEPeripheralIdle alloc] init];
}



-(BLEPeripheralStates)state{
    return kBLEPeripheralConnected;
}



-(NSSet *)availableCharectaristics{
    return self.foundCharectaristics;
}

-(BOOL)isConnected{
    return YES;
}


- (CBCharacteristic *)getCharacteristicWithCharacteristicID:(NSString *)characteristicID {
    CBCharacteristic *result = nil;
    for (CBCharacteristic *tempChar in self.foundCharectaristics) {
        if ([[tempChar.UUID UUIDString] caseInsensitiveCompare:characteristicID] == NSOrderedSame) {
            result = tempChar;
            break;
        }
    }
    return result;
}

-(NSError *)getValueForCharacteristic:(NSString *)characteristicID{
    CBCharacteristic *relevantChar = [self getCharacteristicWithCharacteristicID:characteristicID];
    if (relevantChar == nil) {
        return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorCharectaristicNotFound userInfo:nil];
    }
    [self.peripheral readValueForCharacteristic:relevantChar];
    return nil;
}

-(NSError *)writeValue:(NSData *)value forCharacteristic:(NSString *)characteristicID shouldWaitForResponse:(BOOL *)shouldWaitForResponse{
    CBCharacteristic *relevantChar = [self getCharacteristicWithCharacteristicID:characteristicID];
    if (relevantChar == nil) {
        return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorCharectaristicNotFound userInfo:nil];
    }
    //checking the chatacteristic properties here to distinguish between write and write without response states
    *shouldWaitForResponse = (relevantChar.properties & CBCharacteristicPropertyWriteWithoutResponse) == 0;
    
    [self.peripheral writeValue:value forCharacteristic:relevantChar type:CBCharacteristicWriteWithResponse];
    return nil;
}

-(NSError *)registerForCharacteristic:(NSString *)characteristicID{
    CBCharacteristic *relevantChar = [self getCharacteristicWithCharacteristicID:characteristicID];
    if (relevantChar == nil) {
        return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorCharectaristicNotFound userInfo:nil];
    }
    [self.peripheral setNotifyValue: YES forCharacteristic:relevantChar];
    return nil;
}

-(NSError *)unregisterForCharacteristic:(NSString *)characteristicID{
    CBCharacteristic *relevantChar = [self getCharacteristicWithCharacteristicID:characteristicID];
    if (relevantChar == nil) {
        return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorCharectaristicNotFound userInfo:nil];
    }
    [self.peripheral setNotifyValue: NO forCharacteristic:relevantChar];
    return nil;
}


@end
