//
//  BLEPeripheralConnectionStateMachine.m
//  FlirNvrDvr
//
//  Created by Adaya on 3/22/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import "BLEPeripheralConnectionStateMachine.h"
#import "BLEPeripheralConnectionState.h"
#import "BLEPeripheralIdle.h"

@interface BLEPeripheralConnectionStateMachine ()

@property (strong, nonatomic) id<BLEPeripheralConnectionState> currentState;
@property (strong, nonatomic) NSString* LOCK;
@end

@implementation BLEPeripheralConnectionStateMachine


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.currentState = [[BLEPeripheralIdle alloc] init];
        self.LOCK = @"StateMachineLock";
    }
    return self;
}

-(void)reset{
    self.currentState = [[BLEPeripheralIdle alloc] init];

}

-(BLEPeripheralStates)currentPeripheralState{
    return self.currentState.state;
}



-(NSSet *)availableCharectaristics{
    return [self.currentState availableCharectaristics];
}

-(BOOL)isConnected{
    return [self.currentState isConnected];
}

-(BOOL) tryToChangeState: (id<BLEPeripheralConnectionState>) state{
    if(state!=nil){
        NSLog(@"Changing state from %@ to %@", self.currentState, state);
        self.currentState = state;
    }
    else{
        NSLog(@"Staying in state %@", self.currentState);
    }
    return state!=nil;
}

-(NSError *)writeValue:(NSData *)value forCharacteristic:(NSString *)characteristicID shouldWaitForResponse:(BOOL *)shouldWaitForResponse{
    return [self.currentState writeValue:value forCharacteristic:characteristicID shouldWaitForResponse:shouldWaitForResponse];
}

-(NSError *)getValueForCharacteristic:(NSString *)characteristicID{
    return [self.currentState getValueForCharacteristic:characteristicID];
}

-(NSError *)registerForCharacteristic:(NSString *)characteristicID{
    return [self.currentState registerForCharacteristic:characteristicID];
}

-(NSError *)unregisterForCharacteristic:(NSString *)characteristicID{
    return [self.currentState unregisterForCharacteristic:characteristicID];
}

#pragma State transitions



- (BOOL) connectToDevice: (CBPeripheral *) peripheral usingManager:(CBCentralManager *)centralManager andRequiredServices: (NSArray*) bleServiceRequirements{
    NSLog(@"Attempt to change state due to connectToDevice");
    @synchronized(self.LOCK){
        return [self tryToChangeState: [self.currentState connectToDevice: peripheral usingManager:centralManager andRequiredServices:bleServiceRequirements]];
    }
}

- (BOOL) connected: (CBPeripheral*) peripheral{
    NSLog(@"Attempt to change state due to connected");
    
    @synchronized(self.LOCK){
        return [self tryToChangeState: [self.currentState connected: peripheral]];
    }
}
- (BOOL) servicesDiscovered: (CBPeripheral*) peripheral withError:(NSError*)error transitionFailure: (NSError **) failure{
    NSLog(@"Attempt to change state due to servicesDiscovered");
    
    @synchronized(self.LOCK){
        return [self tryToChangeState: [self.currentState servicesDiscovered:peripheral withError:error transitionFailure:failure]];
    }
}
- (BOOL) charectaristicsDiscovered: (CBPeripheral*) peripheral forService:(CBService*) service withError:(NSError*)error transitionFailure: (NSError **) failure{
    NSLog(@"Attempt to change state due to charectaristicsDiscovered");
    @synchronized(self.LOCK){
        return [self tryToChangeState: [self.currentState charectaristicsDiscovered:peripheral forService:service withError:error transitionFailure:failure]];
    }
}

- (BOOL) disconnectUsing: (CBCentralManager *) centralManager{
    NSLog(@"Attempt to change state due to disconnectUsing");
    @synchronized(self.LOCK){
        return [self tryToChangeState: [self.currentState disconnectUsing: centralManager]];
    }
}

- (BOOL) connectionFailure: (CBPeripheral *) peripheral{
    NSLog(@"Attempt to change state due to connectionFailure");
    @synchronized(self.LOCK){
        return [self tryToChangeState: [self.currentState connectionFailure: peripheral]];
    }
}

- (BOOL) discoveryFailure: (CBPeripheral *) peripheral{
    NSLog(@"Attempt to change state due to discoveryFailure");
    @synchronized(self.LOCK){
        return [self tryToChangeState: [self.currentState discoveryFailure: peripheral]];
    }
}

- (BOOL) autoDisconnected: (CBPeripheral *) peripheral{
    NSLog(@"Attempt to change state due to autoDisconnected");
    @synchronized(self.LOCK){
        return [self tryToChangeState: [self.currentState autoDisconnected: peripheral]];
    }
}

- (BOOL) unExpectedDisconnection{
    NSLog(@"Attempt to change state due to unExpectedDisconnection");
    @synchronized(self.LOCK){
        return [self tryToChangeState: [self.currentState unExpectedDisconnection]];
    }
}



@end
