//
//  BLEPeripheralConnectionStateMachine.h
//  FlirNvrDvr
//
//  Created by Adaya on 3/22/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BLEPeripheralConnectionState.h"

@interface BLEPeripheralConnectionStateMachine : NSObject

- (BOOL) connectToDevice: (CBPeripheral *) peripheral usingManager: (CBCentralManager *) centralManager andRequiredServices: (NSArray*) bleServiceRequirements;
- (BOOL) connected: (CBPeripheral*) peripheral;
- (BOOL) servicesDiscovered: (CBPeripheral*) peripheral withError:(NSError*)error transitionFailure: (NSError **) failure;
- (BOOL) charectaristicsDiscovered: (CBPeripheral*) peripheral forService:(CBService*) service withError:(NSError*)error transitionFailure: (NSError **) failure;
- (BOOL) disconnectUsing: (CBCentralManager*) centralManager;
- (BOOL) connectionFailure: (CBPeripheral *) peripheral;
- (BOOL) discoveryFailure: (CBPeripheral *) peripheral;
- (BOOL) autoDisconnected: (CBPeripheral *) peripheral;
- (BOOL) unExpectedDisconnection;

- (void) reset;

- (NSSet*) availableCharectaristics;
- (BLEPeripheralStates) currentPeripheralState;
- (BOOL) isConnected;
- (NSError *)writeValue:(NSData *)value forCharacteristic:(NSString *)characteristicID shouldWaitForResponse:(BOOL *)shouldWaitForResponse;
- (NSError*) getValueForCharacteristic:(NSString *)characteristicID;
- (NSError*) registerForCharacteristic:(NSString *)characteristicID;
- (NSError*) unregisterForCharacteristic:(NSString *)characteristicID;
@end
