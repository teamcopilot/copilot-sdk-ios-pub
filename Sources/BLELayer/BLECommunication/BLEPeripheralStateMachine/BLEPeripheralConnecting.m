//
//  BLEPeripheralConnecting.m
//  FlirNvrDvr
//
//  Created by Adaya on 3/22/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import "BLEPeripheralConnecting.h"
#import "BLEPeripheralIdle.h"
#import "BLEPeripheralConnected.h"
#import "BLEPeripheralDisconnect.h"
#import "BLEPeripheralErrors.h"
#import "BLEServiceRequirements.h"

@interface BLEPeripheralConnecting ()
@property (strong, nonatomic) CBPeripheral* peripheral;
@property (nonatomic, strong) NSMutableSet* foundCharectaristics;
@property (nonatomic, strong) NSArray* serviceRequirements;

@end

@implementation BLEPeripheralConnecting

- (instancetype)initWithPeripheral: (CBPeripheral *) peripheral usingCentralManager:(CBCentralManager *)centralManager andServiceRequirements:(NSArray *)serviceRequirements
{
    self = [super init];
    if (self) {
        self.peripheral = peripheral;
        self.foundCharectaristics = [NSMutableSet set];
        self.serviceRequirements = serviceRequirements;
        [centralManager connectPeripheral:peripheral options:nil];
    }
    return self;
}


- (BOOL) isSamePeripheral:(CBPeripheral*) peripheral{
    return ([[peripheral.identifier UUIDString] caseInsensitiveCompare:[self.peripheral.identifier UUIDString]] == NSOrderedSame);
}

- (id<BLEPeripheralConnectionState>) connectToDevice: (CBPeripheral *) peripheral  usingManager: (CBCentralManager *) centralManager andRequiredServices: (NSArray*) servicesAsString{
    if([self isSamePeripheral: peripheral]){
        return self;
    }
    return nil;
}



-(id<BLEPeripheralConnectionState>)connected:(CBPeripheral *)peripheral{
    if(![self isSamePeripheral: peripheral]) {
        return nil;
    }
    NSMutableArray* services = [NSMutableArray array];
    for (BLEServiceRequirements* service in self.serviceRequirements) {
        [services addObject:service.serviceId];
    }

    [peripheral discoverServices:nil];
     return [self checkForFullConnection];
}


-(id<BLEPeripheralConnectionState>)servicesDiscovered:(CBPeripheral *)peripheral withError:(NSError*)error  transitionFailure: (NSError **) failure{
    if(![self isSamePeripheral: peripheral]) {
        *failure = [NSError errorWithDomain:BLEManagerErrorDomainName
                                       code:BLEManagerErrorInvalidPeripheralForResponse
                                   userInfo:nil];
        return self;
    }
    
    if (error) {
        NSLog(@"%@", error);
        *failure = error;
        return self;
    }
    
    for (CBService *service in peripheral.services) {
        NSLog(@"Discovered service %@", service);
    }
    
    NSMutableArray* coudntFind = [NSMutableArray array];
    for (BLEServiceRequirements *relavantService in self.serviceRequirements) {
        CBUUID *wantedServiceUUID = relavantService.serviceId;
        BOOL found = NO;
        for (CBService *service in peripheral.services) {
            if ([service.UUID.UUIDString isEqualToString: wantedServiceUUID.UUIDString]) {
               // LogDebug(@"     Discovered service %@", service);
                [peripheral discoverCharacteristics:nil forService:service];
                found = YES;
                break;
            }
        }
        if(!found){
            [coudntFind addObject:relavantService.serviceId.UUIDString];
        }
    }
    if (coudntFind.count == 0) {
        //everything is cool
    }
    else {
        
        NSDictionary* info = @{@"missingCount" : @(coudntFind.count) ,@"missing": coudntFind};
        
        *failure = [NSError errorWithDomain:BLEManagerErrorDomainName
                                       code:BLEManagerErrorServiceNotFound
                                   userInfo:info];
        return self;

    }

    
    return [self checkForFullConnection];
}

-(id<BLEPeripheralConnectionState>)charectaristicsDiscovered:(CBPeripheral *)peripheral forService:(CBService *)service withError:(NSError*)error  transitionFailure: (NSError **) failure{
    if(![self isSamePeripheral: peripheral]) {
        *failure = [NSError errorWithDomain:BLEManagerErrorDomainName
                                       code:BLEManagerErrorInvalidPeripheralForResponse
                                   userInfo:nil];
        return self;
    }
    
    NSLog(@"Service %@ charec (%@), error=%@ (additional services %@):", service.UUID.UUIDString, @(service.characteristics.count), error, @(service.includedServices.count));
    for (CBCharacteristic *characteristic in service.characteristics){
        NSLog(@"     - characteristic %@", characteristic);
    }
    
    BLEServiceRequirements* serviceRequirements = nil;
    for (BLEServiceRequirements *relavantService in self.serviceRequirements){
        if([relavantService.serviceId.UUIDString caseInsensitiveCompare:service.UUID.UUIDString] == NSOrderedSame){
            serviceRequirements = relavantService;
            break;
        }
    }
    
    if(serviceRequirements == nil){
        return self;
    }
    
    for (CBUUID *wantedCharectaristicUUID in serviceRequirements.charectaristicIds) {
        for (CBCharacteristic *characteristic in service.characteristics) {
            if ([characteristic.UUID.UUIDString caseInsensitiveCompare: wantedCharectaristicUUID.UUIDString] == NSOrderedSame) {
                NSLog(@"     Discovered characteristic %@", characteristic);
                if ([self isFoundCharectaristicsContains:characteristic]) {
                    NSLog(@"characteristic already found. will look for another");
                }
                else {
                   [self.foundCharectaristics addObject:characteristic];
                }
                
            }
        }
    }
   
    
    return [self checkForFullConnection];
}


-(id<BLEPeripheralConnectionState>)checkForFullConnection{
    int requiredCharsCount = 0;
    for (BLEServiceRequirements *relavantService in self.serviceRequirements){
        requiredCharsCount += relavantService.charectaristicIds.count;
    }
    if(self.foundCharectaristics.count >= requiredCharsCount){
       
        return [[BLEPeripheralConnected alloc] initWithPeripheral:self.peripheral andAvailableCharectaristics:self.foundCharectaristics];
    }
    else{
        NSLog(@"Not all chars were found... looking for %@, found %@", @(requiredCharsCount), @(self.foundCharectaristics.count));
        return nil;
    }
}




- (id<BLEPeripheralConnectionState>)  disconnectUsing: (CBCentralManager*) centralManager{
    return [[BLEPeripheralDisconnect alloc] initWithPeripheral:self.peripheral andCentralManager:centralManager];
//    return nil;
}

- (id<BLEPeripheralConnectionState>) connectionFailure: (CBPeripheral *) peripheral{
    if([self isSamePeripheral: peripheral]) {
        return [[BLEPeripheralIdle alloc] init];
    }
    return nil;
}

- (id<BLEPeripheralConnectionState>) discoveryFailure: (CBPeripheral *) peripheral{
    if([self isSamePeripheral: peripheral]) {
        return [[BLEPeripheralIdle alloc] init];
    }
    return nil;
}

- (id<BLEPeripheralConnectionState>) autoDisconnected: (CBPeripheral *) peripheral{
    if([self isSamePeripheral: peripheral]) {
        return [[BLEPeripheralIdle alloc] init];
    }
    return nil;
}

- (id<BLEPeripheralConnectionState>) unExpectedDisconnection{
    // same as auto but there is no need verify peripheral
    return [[BLEPeripheralIdle alloc] init];
}

-(BLEPeripheralStates)state{
    return kBLEPeripheralConnecting;
}


-(NSSet *)availableCharectaristics{
    return self.foundCharectaristics;
}

-(BOOL)isConnected{
    return NO;
}


-(NSError *)writeValue:(NSData *)value forCharacteristic:(NSString *)characteristicID shouldWaitForResponse:(BOOL *)shouldWaitForResponse{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

-(NSError *)getValueForCharacteristic:(NSString *)characteristicID{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

-(NSError *)registerForCharacteristic:(NSString *)characteristicID{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

-(NSError *)unregisterForCharacteristic:(NSString *)characteristicID{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

#pragma mark - private

- (BOOL)isFoundCharectaristicsContains:(CBCharacteristic *) characteristic {
    for (CBCharacteristic *cha in self.foundCharectaristics) {
        if([characteristic.UUID.UUIDString caseInsensitiveCompare:cha.UUID.UUIDString] == NSOrderedSame) {
            return true;
        }
    }
    return false;
}

@end
