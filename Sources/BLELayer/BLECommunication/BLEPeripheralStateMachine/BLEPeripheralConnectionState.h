//
//  BLEPeripheralConnectionState.h
//  FlirNvrDvr
//
//  Created by Adaya on 3/22/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

typedef enum {
    kBLEPeripheralIdle                      = 88001,
    kBLEPeripheralDiscovererScanning                  = 88002,
    kBLEPeripheralDiscovererFinishedScanning          = 88003,
    kBLEPeripheralConnecting                = 88004,
    kBLEPeripheralConnected                 = 88005,
    kBLEPeripheralGettingValue              = 88006,
    kBLEPeripheralDisconnecting             = 88007,
    
} BLEPeripheralStates;

@protocol BLEPeripheralConnectionState <NSObject>


- (id<BLEPeripheralConnectionState>) connectToDevice: (CBPeripheral*) peripheral usingManager: (CBCentralManager *) centralManager andRequiredServices: (NSArray*) bleServiceRequirements;
- (id<BLEPeripheralConnectionState>) connected: (CBPeripheral*) peripheral ;
- (id<BLEPeripheralConnectionState>) servicesDiscovered: (CBPeripheral*) peripheral withError:(NSError*)error transitionFailure: (NSError **) failure;
- (id<BLEPeripheralConnectionState>) charectaristicsDiscovered: (CBPeripheral*) peripheral forService:(CBService*) service withError:(NSError*)error transitionFailure: (NSError **) failure;
- (id<BLEPeripheralConnectionState>) disconnectUsing: (CBCentralManager *) centralManager;
- (id<BLEPeripheralConnectionState>) connectionFailure:(CBPeripheral*) peripheral;
- (id<BLEPeripheralConnectionState>) discoveryFailure:(CBPeripheral*) peripheral;
- (id<BLEPeripheralConnectionState>) autoDisconnected:(CBPeripheral*) peripheral;
- (id<BLEPeripheralConnectionState>) unExpectedDisconnection;

@property (readonly) BLEPeripheralStates state;


- (NSSet*) availableCharectaristics;
- (BOOL) isConnected;
- (NSError *)writeValue:(NSData *)value forCharacteristic:(NSString *)characteristicID shouldWaitForResponse:(BOOL *)shouldWaitForResponse;
- (NSError*) getValueForCharacteristic:(NSString *)characteristicID;
- (NSError*) registerForCharacteristic:(NSString *)characteristicID;
- (NSError*) unregisterForCharacteristic:(NSString *)characteristicID;

@end
