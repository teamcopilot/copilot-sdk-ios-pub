//
//  BleDiscoverer.m
//  ZemingoBLELayer
//
//  Created by Miko Halevi on 7/10/17.
//  Copyright © 2017 Adaya. All rights reserved.
//

#import "BLEDiscoverer.h"
#import "PeripheralInfo.h"
#import "BLEServiceRequirements.h"

@interface BLEDiscoverer ()

@property (nonatomic, strong) id<BleDiscovererControl> discovererControl;
@property (nonatomic, strong) NSArray *discoveryServicesIds;

@end

@implementation BLEDiscoverer

@synthesize discovererDelegate;

- (instancetype)initWithDiscovererControl:(id<BleDiscovererControl>) discovererControl discovererDelegate:(id<BLEPeripheralDiscovererDelegate>)discovererDelegate discoveryServices: (NSArray *)discoveryServices {
    self = [super init];
    if (self) {
        
        self.discovererControl = discovererControl;
        self.discovererDelegate = discovererDelegate;
        
        NSMutableArray* arr = [NSMutableArray array];
        for (BLEServiceRequirements* req in discoveryServices) {
            [arr addObject: req.serviceId];
        }
        
        self.discoveryServicesIds = arr;
    }
    return self;
}

#pragma mark - BleDiscovererProtocol

- (NSError *)scanForBLEPeripherals {
    NSError *error = nil;
    if ([self.discovererControl respondsToSelector:@selector(bleDiscovererAttemptToScan:WithErrorPtr:)]) {
        CBCentralManager *centralMan = [self.discovererControl bleDiscovererAttemptToScan:self WithErrorPtr:&error];
        if (centralMan) {
            [centralMan scanForPeripheralsWithServices:self.discoveryServicesIds options:nil];
        }
        else {
            NSLog(@"cannot start scanning");
        }
    }
    else{
        error = [NSError errorWithDomain:BLEDiscovererProtocolDomainName code:BLEDiscovererProtocolErrorDelegateNotResponding userInfo:nil];
        NSLog(@"discover control is not responding to attempt to scan selector with discover control %@",self.discovererControl);
    }

    return error;
}

- (void)stopScan {
    if ([self.discovererControl respondsToSelector:@selector(bleDiscovererAttemptStopScanning:)]) {
        CBCentralManager *centralMan = [self.discovererControl bleDiscovererAttemptStopScanning:self];
        if (centralMan) {
            [centralMan stopScan];
            if ([self.discovererControl respondsToSelector:@selector(bleDiscovererStopScanningComleted:)]) {
                 [self.discovererControl bleDiscovererStopScanningComleted:self];
            }
        }
        else {
            NSLog(@"could not stop scan");
        }
    }
    else {
        NSLog(@"discovery control not responds to its selector with control: %@",self.discovererControl);
    }
    
}

#pragma mark - public

- (void)didDiscoverPeripheralWithPeripheralInfo:(PeripheralInfo *)peripheralInfo{
    if ([self.discovererDelegate respondsToSelector:@selector(BLEPeripheralFoundWithPeripheralInfo:)]) {
        [self.discovererDelegate BLEPeripheralFoundWithPeripheralInfo:peripheralInfo];
    }
    else {
        NSLog(@"discoverer delegate is not responding to selector with delegate: %@",self.discovererDelegate);
    }
}

- (void)bleSuspendedWitherror:(NSError *)error {
    if ([self.discovererDelegate respondsToSelector:@selector(BLEPeripheralScanStoppedUnexpectedWithError:)]) {
        [self.discovererDelegate BLEPeripheralScanStoppedUnexpectedWithError:error];
    }
    else {
        NSLog(@"discoverer delegate is not responding to selector with delegate: %@",self.discovererDelegate);
    }
}


@end
