//
//  BLEDiscovererProtocol.h
//  FlirNvrDvr
//
//  Created by Elad Urson on 25/1/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#define BLEDiscovererProtocolDomainName         @"BLEDiscovererProtocolDomainName"

typedef NS_ENUM(NSInteger, BLEDiscovererProtocolError) {
    BLEDiscovererProtocolErrorUnknown,
    BLEDiscovererProtocolErrorDelegateNotResponding,
};


@class PeripheralInfo;

@protocol BLEPeripheralDiscovererDelegate <NSObject>

- (void)BLEPeripheralFoundWithPeripheralInfo:(PeripheralInfo *) peripheralInfo;
//Called when scanning stopped with error
- (void)BLEPeripheralScanStoppedUnexpectedWithError:(NSError *)error;

@end

@protocol BLEDiscovererProtocol <NSObject>

@required

@property (nonatomic, weak) id <BLEPeripheralDiscovererDelegate> discovererDelegate;
//error will be nil upon successful operation
- (NSError *) scanForBLEPeripherals;
- (void) stopScan;

@end

