//
//  PeripheralInfo.h
//  ZemingoBLELayer
//
//  Created by Miko Halevi on 7/10/17.
//  Copyright © 2017 Adaya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PeripheralInfo : NSObject

@property (nonatomic, strong, readonly) NSUUID *identifier;
@property (nonatomic, strong, readonly) NSString *name;

- (instancetype)initWithIdentifier:(NSUUID *)identifier name:(NSString *)name;

@end
