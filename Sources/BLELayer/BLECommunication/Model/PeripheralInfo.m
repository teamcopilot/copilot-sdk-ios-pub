//
//  PeripheralInfo.m
//  ZemingoBLELayer
//
//  Created by Miko Halevi on 7/10/17.
//  Copyright © 2017 Adaya. All rights reserved.
//

#import "PeripheralInfo.h"

@interface PeripheralInfo ()

@property (nonatomic, strong) NSUUID *identifier;
@property (nonatomic, strong) NSString *name;

@end

@implementation PeripheralInfo

- (instancetype)initWithIdentifier:(NSUUID *)identifier name:(NSString *)name{
    self = [super init];
    if (self) {
        if (identifier == nil) {
            NSLog(@"got empty identifier when trying to create peripheral info");
            return nil;
        }
        self.identifier = identifier;
        self.name = name;
    }
    return self;
}


@end
