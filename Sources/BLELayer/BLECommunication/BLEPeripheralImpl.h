//
//  BLEPeripheralImpl.h
//  ZemingoBLELayer
//
//  Created by Miko Halevi on 7/11/17.
//  Copyright © 2017 Adaya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEPeripheralProtocol.h"

@class CBCentralManager;
@class CBPeripheral;
@class CBService;

@interface BLEPeripheralImpl : NSObject <BLEPeripheralProtocol>

-(instancetype)initWithCentralManager:(CBCentralManager *)centralManager peripheral:(CBPeripheral *)peripheral requiredServices:(NSArray *)requiredServices delegate:(id<BLEPeripheralDelegate>) delegate;

- (void)didConnect;
- (void)didFailToConnectWithError:(NSError *)error;
- (void)didDisconnectWithError:(NSError *)error;
- (void)didDiscoverServices:(NSError *)error;
- (void)didDiscoverCharacteristicForService:(CBService *)service error:(NSError *)error;
- (void)handleUnexpectedDisconnection;
- (NSUUID *)getPeripheralIdentifier;

@end
