//
//  BLEPeripheralImpl.m
//  ZemingoBLELayer
//
//  Created by Miko Halevi on 7/11/17.
//  Copyright © 2017 Adaya. All rights reserved.
//

#import "BLEPeripheralImpl.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "BLEPeripheralConnectionStateMachine.h"
#import "BLEServiceRequirements.h"
#import "BLEPeripheralErrors.h"

@interface BLEPeripheralImpl()<CBPeripheralDelegate>

@property (nonatomic, strong)CBCentralManager                         *centralManager;
@property (nonatomic, strong)CBPeripheral                             *peripheral;
@property (nonatomic, strong) BLEPeripheralConnectionStateMachine     *stateMachine;
@property (nonatomic, strong) NSArray                                 *requiredServices;
@property (nonatomic, strong)NSObject                                 *lock;

@end

@implementation BLEPeripheralImpl

@synthesize peripheralDelegate;

-(instancetype)initWithCentralManager:(CBCentralManager *)centralManager peripheral:(CBPeripheral *)peripheral requiredServices:(NSArray *)requiredServices delegate:(id<BLEPeripheralDelegate>) delegate {
    self = [super init];
    if (self) {

        peripheral.delegate = self;
        self.peripheral = peripheral;
        self.centralManager = centralManager;
        self.requiredServices = requiredServices;
        self.peripheralDelegate = delegate;
        self.stateMachine = [[BLEPeripheralConnectionStateMachine alloc] init];
        [self.stateMachine connectToDevice:peripheral usingManager:centralManager andRequiredServices:requiredServices];
        
        self.lock = [[NSObject alloc] init];
    }
    return self;
}

#pragma mark - Public

- (NSUUID *)getPeripheralIdentifier {
    return self.peripheral.identifier;
}

- (void)didConnect {
  
    NSLog(@"Peripheral connected with state %@", @(self.peripheral.state)); //CBPeripheralStateConnected == 2
    
    if([self.stateMachine connected:self.peripheral]){

        if ([self.peripheralDelegate respondsToSelector:@selector(connectionSuccessToBLEPeripheral)]) {
            [self.peripheralDelegate connectionSuccessToBLEPeripheral];
        }
        else {
            (@"no peripheralDelegate");
        }
    }
    else{
        //TODO: Miko - should we add some code here?! 
        //        [peripheral setDelegate:nil];
        //        LogErr(@"Connction to peripheral was done on wrong state. NOT discovering services");
        //        if ([self.peripheralDelegate respondsToSelector:@selector(connectionFailedToBLEPeripheralWithError:)]) {
        //            [self.peripheralDelegate connectionFailedToBLEPeripheralWithError:[NSError errorWithDomain:BLEManagerErrorDomainName
        //                                                                                                  code:BLEManagerErrorInvalidCurrentState
        //                                                                                              userInfo:nil]];
        //        }
    }

}


- (void)didFailToConnectWithError:(NSError *)error {
    
    if([self.stateMachine connectionFailure:self.peripheral]){
        if (error) {
            NSLog(@"Error in connection failure, %@", error);
        }
        
        if ([self.peripheralDelegate respondsToSelector:@selector(connectionFailedToBLEPeripheralWithError:)]) {
            [self.peripheralDelegate connectionFailedToBLEPeripheralWithError:error];
        } else {
            NSLog(@"no valid delegate");
            
        }
    }
    else{
        if (error) {
            NSLog(@"Error in connection failure, but state machine is agnostic to it%@", error);
        }
    }
    
}

- (void)didDisconnectWithError:(NSError *)error {
    
    if([self.stateMachine autoDisconnected:self.peripheral]){
        if (error) {
            NSLog(@"got error when disconnecting%@", error);
        }
        if ([self.peripheralDelegate respondsToSelector:@selector(didDisconnectFromConnectedBLEPeripheralWithError:)]) {
            [self.peripheralDelegate didDisconnectFromConnectedBLEPeripheralWithError:error];
        } else {
            NSLog(@"no valid peripheralDelegate");
        }
    }
    else {
        NSLog(@"unexpected disconnecting (in wrong state=%d). error=%@", self.stateMachine.currentPeripheralState, error );
        
    }
    
}

//called when central manager status changed to an unconnected state

- (void)handleUnexpectedDisconnection {
    
    NSLog(@"unexpected disconnecting in state=%d", self.stateMachine.currentPeripheralState);
    if([self.stateMachine unExpectedDisconnection]){
        
        if ([self.peripheralDelegate respondsToSelector:@selector(didDisconnectFromConnectedBLEPeripheralWithError:)]) {
            NSError *error = [NSError errorWithDomain:BLEManagerErrorDomainName
                                                 code:BLEManagerErrorUnexpectedDisconnection
                                             userInfo:nil];
            
            [self.peripheralDelegate didDisconnectFromConnectedBLEPeripheralWithError:error];
        } else {
            NSLog(@"no valid peripheralDelegate");
        }
    }
    else {
        NSLog(@"unexpected disconnecting in state=%d no actions.", self.stateMachine.currentPeripheralState);
    }

}

#pragma mark - CBPeripheralDelegate - Connection to periperal

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {

    NSError* failure;
    if([self.stateMachine servicesDiscovered:peripheral withError:error transitionFailure:&failure]){
        if(failure!=nil){
            NSLog(@"Discovering characteristics failed %@", failure);
            if ([self.peripheralDelegate respondsToSelector:@selector(connectionFailedToBLEPeripheralWithError:)]) {
                [self.peripheralDelegate connectionFailedToBLEPeripheralWithError:failure];
            }
            else {
                NSLog(@"no delegate");
            }
            [self.centralManager cancelPeripheralConnection:peripheral];
        }
        else{
            if ([self.peripheralDelegate respondsToSelector:@selector(connectionSuccessToBLEPeripheral)]) {
                [self.peripheralDelegate connectionSuccessToBLEPeripheral];
            }
            else {
                NSLog(@"no peripheralDelegate");
            }
        }
    }
    else{
        //LogErr(@"Discovering services was done on wrong state. NOT discovering characteristics");
        //            if ([self.peripheralDelegate respondsToSelector:@selector(connectionFailedToBLEPeripheralWithError:)]) {
        //                [self.peripheralDelegate connectionFailedToBLEPeripheralWithError:[NSError errorWithDomain:BLEManagerErrorDomainName
        //                                                                                                code:BLEManagerErrorInvalidCurrentState
        //                                                                                            userInfo:nil]];
        //            }
    }

}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    
    NSError* failure;
    if([self.stateMachine charectaristicsDiscovered:peripheral forService:service withError:error transitionFailure:&failure]){
        if(failure!=nil){
            NSLog(@"Discovering characteristics failed %@", failure);
            if ([self.peripheralDelegate respondsToSelector:@selector(connectionFailedToBLEPeripheralWithError:)]) {
                [self.peripheralDelegate connectionFailedToBLEPeripheralWithError:failure];
            }
            else {
                NSLog(@"no delegate");
            }
            [self.centralManager cancelPeripheralConnection:peripheral];
        }
        else{
            if ([self.peripheralDelegate respondsToSelector:@selector(connectionSuccessToBLEPeripheral)]) {
                [self.peripheralDelegate connectionSuccessToBLEPeripheral];
            }
            else {
                NSLog(@"no peripheralDelegate");
            }
        }
    }
    else{
        //
        
        //        LogErr(@"Discovering services was done on wrong state. NOT discovering characteristics");
        //        if ([self.peripheralDelegate respondsToSelector:@selector(connectionFailedToBLEPeripheralWithError:)]) {
        //            [self.peripheralDelegate connectionFailedToBLEPeripheralWithError:[NSError errorWithDomain:BLEManagerErrorDomainName
        //                                                                                                  code:BLEManagerErrorInvalidCurrentState
        //                                                                                              userInfo:nil]];
        //        }
        
    }

}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (error) {
        NSLog(@"Failed to register for characteristic notification %@ ERROR: %@", characteristic, [error localizedDescription]);
    }
}

#pragma mark - CBPeripheralDelegate

- (void) peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    if ([self.peripheralDelegate respondsToSelector:@selector(didSetValueForCharacteristic:withError:)]) {
        [self.peripheralDelegate didSetValueForCharacteristic:characteristic.UUID.UUIDString withError:error];
    }
    else {
        NSLog(@"no peripheralDelegate responds to selector wirh delegate: %@",self.peripheralDelegate);
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if([self.stateMachine isConnected]) {
        if (error) {
            NSLog(@"%@", error);
            if ([self.peripheralDelegate respondsToSelector:@selector(didGetValue:forCharacteristic:withError:)]) {
                [self.peripheralDelegate didGetValue:nil forCharacteristic:[characteristic.UUID UUIDString] withError:error];
            } else {
                NSLog(@"no peripheralDelegate");
            }
            return;
        }
        NSData *data = characteristic.value;
#warning todo - what does update value exactly mean?
        if ([self.peripheralDelegate respondsToSelector:@selector(didGetValue:forCharacteristic:withError:)]) {
            [self.peripheralDelegate didGetValue:data forCharacteristic:[characteristic.UUID UUIDString] withError:nil];
        } else {
            NSLog(@"no peripheralDelegate");
        }
    }
    else{
        NSLog(@"Updated value for charectaristic was done on wrong state=%d ", self.stateMachine.currentPeripheralState);
        if ([self.peripheralDelegate respondsToSelector:@selector(didGetValue:forCharacteristic:withError:)]) {
            [self.peripheralDelegate didGetValue:nil forCharacteristic:[characteristic.UUID UUIDString] withError:[NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentStateForReaponse userInfo:nil]];
        } else {
            NSLog(@"no peripheralDelegate");
        }
    }
}


#pragma mark - BLEPeripheralProtocol

- (BOOL) disconnectFromBLEPeripheral{
    @synchronized(self.lock) {
        if([self.stateMachine disconnectUsing:self.centralManager]){
            return YES;
        }
        else{
            NSLog(@"Trying to disconnect from peripheral while state is %d", self.stateMachine.currentPeripheralState);
            return NO;
        }
        
    }
}

- (void) setValue:(NSData *)value forCharacteristic:(NSString *)characteristicID {
    @synchronized(self.lock) {
        BOOL shouldWaitForRespone = YES;
        NSError* error = [self.stateMachine writeValue:value forCharacteristic:characteristicID shouldWaitForResponse:&shouldWaitForRespone];
        
        if (!shouldWaitForRespone && [self.peripheralDelegate respondsToSelector:@selector(didSetValueForCharacteristic:withError:)]) {
            [self.peripheralDelegate didSetValueForCharacteristic:characteristicID withError:error];
        } else {
            if (shouldWaitForRespone) {
                NSLog(@"should wait for charecteristic response");
            }
            else {
                NSLog(@"no peripheralDelegate responds to selector wirh delegate: %@",self.peripheralDelegate);
            }
            
        }
    }

}

- (void) getValueforCharacteristic:(NSString *)characteristicID {
    @synchronized(self.lock) {
        NSError* error = [self.stateMachine getValueForCharacteristic:characteristicID];
        if(error){
            if ([self.peripheralDelegate respondsToSelector:@selector(didGetValue:forCharacteristic:withError:)]) {
                [self.peripheralDelegate didGetValue:nil forCharacteristic:characteristicID withError:error];
            } else {
                NSLog(@"no peripheralDelegate responds to selector wirh dlegate: %@",self.peripheralDelegate);
            }
        
        }
    }

}

- (void) registerForCharacteristic:(NSString*) characteristicID {
    @synchronized(self.lock) {
        NSError* error = [self.stateMachine registerForCharacteristic:characteristicID];
        if(error){
            if ([self.peripheralDelegate respondsToSelector:@selector(didGetValue:forCharacteristic:withError:)]) {
                [self.peripheralDelegate didGetValue:nil forCharacteristic:characteristicID withError:error];
            } else {
                NSLog(@"no delegate");
            }
            
        }
    }
}
- (void)unregisterForCharacteristic:(NSString *)characteristicID {
    @synchronized(self.lock) {
        NSError* error = [self.stateMachine unregisterForCharacteristic:characteristicID];
        if(error){
            if ([self.peripheralDelegate respondsToSelector:@selector(didGetValue:forCharacteristic:withError:)]) {
                [self.peripheralDelegate didGetValue:nil forCharacteristic:characteristicID withError:error];
            } else {
                NSLog(@"no delegate");
            }
            
        }
    }
}


@end
