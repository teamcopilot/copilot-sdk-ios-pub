 //
//  ESC_BT_Proxy.m
//  ESC_BT_Proxy
//
//  Created by Elad Urson on 5/1/16.
//  Copyright © 2016 Elad Urson. All rights reserved.
//

#import "BLECommunicationFactory.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "BLEPeripheralConnectionStateMachine.h"
#import "BLEServiceRequirements.h"
#import "BLEStateManagerProtocol.h"
#import "BleDiscoverer.h"
#import "BLEPeripheralImpl.h"
#import "PeripheralInfo.h"



@interface BLECommunicationFactory () <BLEStateManagerProtocol, CBPeripheralDelegate, CBCentralManagerDelegate, BleDiscovererControl > {

}

@property (nonatomic, strong) CBCentralManager                                      *centralManager;

@property (nonatomic, weak) BLEDiscoverer *currentActiveDiscoverer;
@property (nonatomic, strong) NSObject *discovererLock;

@property (nonatomic, strong)NSMutableArray *peripherals;
@property (nonatomic, strong) NSObject *peripheralsLock;

@property (nonatomic, strong)NSHashTable *statusDelegates;


@end

@implementation BLECommunicationFactory

#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    if (self) {
        CBCentralManager *centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        self.centralManager = centralManager;
        self.peripherals = [NSMutableArray array];
        
        self.discovererLock = [[NSObject alloc] init];
        self.peripheralsLock = [[NSObject alloc] init];
        
        self.statusDelegates = [NSHashTable weakObjectsHashTable];
    }
    return self;
}


+ (BLECommunicationFactory *) sharedManager {
    static dispatch_once_t onceToken = 0;
    static BLECommunicationFactory *sharedManager = nil;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[BLECommunicationFactory alloc] init];
    });
    
    return sharedManager;
}


- (void) dealloc {
    self.centralManager.delegate = nil;
    self.centralManager = nil;
    self.peripherals = nil;
}


#pragma mark - CBCentralManagerDelegate - Connection to peripheral

// method called whenever we have successfully connected to the BLE peripheral
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    BLEPeripheralImpl *blePeripheral = [self getPeripheralImplWithCBPeripheral:peripheral];

    if (blePeripheral) {
        [blePeripheral didConnect];
    }
    else {
        NSLog(@"fatal error, got peripheral delegate but no blePeripheralProtocol object was found with CBPeripheral identifier: %@, name: %@", peripheral.name, peripheral.identifier);
    }
    
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    BLEPeripheralImpl *blePeripheral = [self getPeripheralImplWithCBPeripheral:peripheral];
    
    if (blePeripheral) {
        @synchronized (self.peripheralsLock) {
            [self.peripherals removeObject:blePeripheral];
            [blePeripheral didDisconnectWithError:error];
        }
    }
    else {
        NSLog(@"fatal error, got peripheral delegate but no blePeripheralProtocol object was found with CBPeripheral identifier: %@, name: %@", peripheral.name, peripheral.identifier);
    }

}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    BLEPeripheralImpl *blePeripheral = [self getPeripheralImplWithCBPeripheral:peripheral];
    if (blePeripheral) {
        @synchronized (self.peripheralsLock) {
            [self.peripherals removeObject:blePeripheral];
            [blePeripheral didFailToConnectWithError:error];
        }
    }
    else {
        NSLog(@"fatal error, got peripheral delegate but no blePeripheralProtocol object was found with CBPeripheral identifier: %@, name: %@", peripheral.name, peripheral.identifier);
    }
}


#pragma mark - CBCentralManagerDelegate

// method called whenever the device state changes.
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    
#warning TODO: include another state for getting power on and it is when the power was off before.
    
    //determine state
    BOOL success = NO;
    NSString *errorString = nil;
    int errorCode = -1;
    switch ([central state]) {
        case CBCentralManagerStatePoweredOn:
            NSLog(@"CoreBluetooth BLE hardware is powered on and ready");
            success = YES;
            break;
        case CBCentralManagerStatePoweredOff:
            errorString = @"CoreBluetooth BLE hardware is powered off";
            errorCode = BLEManagerStatusErrorScanStoppedExternallyPowerOff;
            break;
        case CBCentralManagerStateUnauthorized:// will receive in case of background BLE request
            errorString = @"CoreBluetooth BLE state is unauthorized";
            break;
        case CBCentralManagerStateUnknown:
            errorString = @"CoreBluetooth BLE state is unknown";
            break;
        case CBCentralManagerStateUnsupported:
            errorString = @"CoreBluetooth BLE hardware is unsupported on this platform";
            errorCode = BLEManagerStatusErrorNotSupported;

            break;
        default:
            errorString = [NSString stringWithFormat:@"Unrecognized state of CentralManager = %ld",(long)[central state]];
            break;
    }

    // disconnect current peripherals and inform active discoverer
    if (!success) {

        [self autoDisconnectedFromPeripherals];
        
        if (self.currentActiveDiscoverer) {
            [self.currentActiveDiscoverer bleSuspendedWitherror:[NSError errorWithDomain:BLEPeripheralDiscovererErrorDomainName code: (errorCode==-1 ?BLEManagerStatusErrorScanStoppedExternallyGeneral:errorCode) userInfo:@{errorString:NSLocalizedDescriptionKey}]];
        }
    }
    
    //notifiy status delegates
    @synchronized (self.statusDelegates) {
        if (self.statusDelegates.count) {
            NSError* error;
            if(success){
                error = nil;
            }else {
                error = [NSError errorWithDomain:BLEManagerStatusErrorDomainName code: (errorCode==-1 ?BLEManagerStatusErrorScanStoppedExternallyGeneral:errorCode) userInfo:@{errorString:NSLocalizedDescriptionKey}];
            }
            for (id obj in self.statusDelegates) {
                if ([obj conformsToProtocol:@protocol(BLEStatusDelegate)]) {
                    id <BLEStatusDelegate> delegate = obj;
                    if ([delegate respondsToSelector:@selector(onStatusChangedToConnected:orError:)]) {
                        [delegate onStatusChangedToConnected:success orError:error];
                    }
                    else {
                        NSLog(@"delegate not responding to selector with delegate %@", delegate);
                    }
                }
                else {
                    NSLog(@"found memeber on status delegates that not conform to status delegate protocol with object: %@", obj);
                }
            }
                

        }
    }

}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"Discovered name=%@, RSSI=%@, ad_data=%@", peripheral.name, RSSI, advertisementData);

    if (self.currentActiveDiscoverer) {
        NSString* name =[advertisementData objectForKey:@"kCBAdvDataLocalName"];
        if(!name){
            name = peripheral.name;
        }
        PeripheralInfo *perhipheralInfo = [[PeripheralInfo alloc] initWithIdentifier:peripheral.identifier name:name];
        [self.currentActiveDiscoverer didDiscoverPeripheralWithPeripheralInfo:perhipheralInfo];
    }
    else {
        NSLog(@"got peripheral discovery delegation without current discoverer");
    }
}

#pragma mark - Private

//Notify status



// use only with central manager lock - make sure that there is no array changes while iterating over it

- (BLEPeripheralImpl *)getPeripheralImplWithCBPeripheral:(CBPeripheral *)peripheral {
    return [self getPeripheralImplWithPeripheralIdentifier:peripheral.identifier];
}

- (BLEPeripheralImpl *)getPeripheralImplWithPeripheralIdentifier:(NSUUID *)peripheralIdentifier {
    @synchronized (self.peripheralsLock) {
        BLEPeripheralImpl *peripheralObject;
        
        for (BLEPeripheralImpl *existingPeripheral in self.peripherals) {
            if ([[existingPeripheral getPeripheralIdentifier] isEqual:peripheralIdentifier]) {
                peripheralObject = existingPeripheral;
                break;
            }
        }
        return peripheralObject;
    }
    
}

-(void) autoDisconnectedFromPeripherals{
    @synchronized (self.peripheralsLock) {
        for (BLEPeripheralImpl *peripheral in self.peripherals) {
            [peripheral handleUnexpectedDisconnection];
        }
        [self.peripherals removeAllObjects];
    }
}

#pragma mark - Public


- (id <BLEDiscovererProtocol>) createBleDiscovererWithDiscoveryServices:(NSArray *)discoveryServices delegate:(id<BLEPeripheralDiscovererDelegate>)delegate {
    BLEDiscoverer *discoverer = [[BLEDiscoverer alloc] initWithDiscovererControl:self discovererDelegate:delegate discoveryServices:discoveryServices];
    return discoverer;
}

- (id<BLEPeripheralProtocol>)connectToPeripheralWithPeripheralIdentifier:(NSUUID *) identifier requiredServices:(NSArray *)requiredServices peripheralDelegate:(id<BLEPeripheralDelegate>) periphrralDelegate{
    
    BLEPeripheralImpl *peripheralImpl;
    
    @synchronized (self.peripheralsLock) {
        if (!requiredServices || !identifier) {
            NSLog(@"missing parameter with identifier: %@, requiredServices: %@",identifier,requiredServices);
            return nil;
        }
        else {
            //checking if peripheral exist already
            
            peripheralImpl = [self getPeripheralImplWithPeripheralIdentifier:identifier];
            if (peripheralImpl) {
                NSLog(@"coult not connect - peripheral already exist with identifier:%@",identifier);
                return nil;
            }
            
            CBPeripheral *peripheral = [self.centralManager retrievePeripheralsWithIdentifiers:@[identifier]].firstObject;
            if (!peripheral) {
                NSLog(@"no peripheral found with identifier");
                return nil;
            }
            else {
                peripheralImpl = [[BLEPeripheralImpl alloc] initWithCentralManager:self.centralManager peripheral:peripheral requiredServices:requiredServices delegate:periphrralDelegate];
                [self.peripherals addObject:peripheralImpl];

            }

        }
    }
    return peripheralImpl;
}

- (void)addStatusDelelate:(id<BLEStatusDelegate>)statusDelegate {
    @synchronized (self.statusDelegates) {
        [self.statusDelegates addObject:statusDelegate];
    }
    
}

- (void)removeStatusDelegate:(id<BLEStatusDelegate>)statusDelegate {
    @synchronized (self.statusDelegates) {
        [self.statusDelegates removeObject:statusDelegate];
    }
}


#pragma mark - DiscovererControl

// this method will return the central maanager only if there is no current scanner active or if the current scanner is the invoker.

- (CBCentralManager *)bleDiscovererAttemptToScan:(BLEDiscoverer *) bleDiscoverer WithErrorPtr:(NSError **)errorPtr{
    @synchronized (self.discovererLock) {
        if ((self.currentActiveDiscoverer == nil || self.currentActiveDiscoverer == bleDiscoverer ) && self.isBLEOn) {
            self.currentActiveDiscoverer = bleDiscoverer;
            return self.centralManager;
        }
        else {
            if (!self.isBLEOn) {
                *errorPtr = [NSError errorWithDomain:BLEDiscovererDomainName code:BLEDiscoveryControlErrorBleOff userInfo:nil];
            }
            else {
                *errorPtr = [NSError errorWithDomain:BLEDiscovererDomainName code:BLEDiscoveryControlErrorOtherDiscovererActive userInfo:nil];
            }
            
            NSLog(@"trying to start scanning while another discoverer is scanning with current active discoverer :%@",self.currentActiveDiscoverer);
            return nil;
        }
    }
}

- (CBCentralManager *)bleDiscovererAttemptStopScanning:(BLEDiscoverer *) bleDiscoverer {
    @synchronized (self.discovererLock) {
        if (self.currentActiveDiscoverer == bleDiscoverer) {
            return self.centralManager;
        }
        else {
            NSLog(@"trying to stop scanning while the active scanner is not the invoker with invoker: %@, active scanner :%@",bleDiscoverer , self.currentActiveDiscoverer);
            return nil;
        }
    }
}

- (BOOL)bleDiscovererStopScanningComleted:(BLEDiscoverer *) bleDiscoverer {
    @synchronized (self.discovererLock) {
        if (self.currentActiveDiscoverer == bleDiscoverer) {
            self.currentActiveDiscoverer = nil;
            return  YES;
        }
        else {
            NSLog(@"trying to finish scanning while the active scanner is not the invoker with invoker: %@, active scanner :%@",bleDiscoverer , self.currentActiveDiscoverer);
            return NO;
        }
    }
}


#pragma mark - BLEStateManagerProtocol

- (BOOL) isBLEOn {
    switch (self.centralManager.state) {
        case CBCentralManagerStatePoweredOn:
            return YES;
            break;
        case CBCentralManagerStatePoweredOff:
        case CBCentralManagerStateUnauthorized:
        case CBCentralManagerStateUnknown:
        case CBCentralManagerStateUnsupported:
        default:
            return NO;
            break;
    }
}

@end
