//
//  ESCPeripheral.m
//  ZemingoBLELayer
//
//  Created by Adaya on 28/11/2016.
//  Copyright © 2016 Adaya. All rights reserved.
//

#import "Peripheral.h"

#import "CommandFlowExecuter.h"
#import "BLECommunicationFactory.h"
#import "PeripheralInfo.h"

@interface Peripheral () <BLEPeripheralDelegate>

@property (nonatomic, strong) PeripheralInfo* peripheralInfo;
@property (nonatomic, weak) id<BLEPeripheralProtocol> peripheralProtocol;
@property (nonatomic, strong) CommandFlowExecuter* commandExecuter;
@property (nonatomic, weak) id<PeripheralConnectionDelegate> delegate;
@end

@implementation Peripheral


-(instancetype)initWithPeripheralInfo:(PeripheralInfo *) peripheralInfo requiredServices:(NSArray *)requiredServices connectionDelegate:(id<PeripheralConnectionDelegate>) delegate{
    self = [super init];
    if (self) {
        self.peripheralInfo = peripheralInfo;
        id<BLEPeripheralProtocol> peripheralProtocol = [[BLECommunicationFactory sharedManager] connectToPeripheralWithPeripheralIdentifier:peripheralInfo.identifier requiredServices:requiredServices peripheralDelegate:self];
        
        if (peripheralProtocol) {
            self.delegate = delegate;
            self.peripheralProtocol = peripheralProtocol;
            self.commandExecuter = [[CommandFlowExecuter alloc] initWithPeripheralProtocol: peripheralProtocol];
        }
        else {
            NSLog(@"could not create peripheral protocol with identifier: %@",peripheralInfo);
            return nil;
        }
    }
    return self;
}

#pragma mark - Public

-(BOOL)disconnect{
    return [self.peripheralProtocol disconnectFromBLEPeripheral];
}


- (void)addCommandFlow:(id<CommandFlowProtocol>)commandFlow{
    [self.commandExecuter addCommandFlow:commandFlow];
}
    
- (void)interruptCommandFlow:(id<CommandFlowProtocol>)commandFlow{
    [self.commandExecuter interruptCommandFlow:commandFlow];
}

-(void)dismiss{
    self.delegate = nil;
    self.peripheralProtocol.peripheralDelegate = nil;
}

#pragma mark BLEPeripheralDelegate

- (void)connectionSuccessToBLEPeripheral{
    if ([self.delegate respondsToSelector:@selector(peripheralDidConnect:)]) {
        [self.delegate peripheralDidConnect:self];
    }
    else {
        NSLog(@"no delegate respond to selector with delegate: %@",self.delegate);
    }
}


- (void)connectionFailedToBLEPeripheralWithError: (NSError *)error{
    if ([self.delegate respondsToSelector:@selector(peripheral:connectionFailedWithError:)]) {
        [self.delegate peripheral:self connectionFailedWithError:error];
    }
    else {
        NSLog(@"no delegate respond to selector with delegate: %@",self.delegate);
    }
}


- (void)didDisconnectFromConnectedBLEPeripheralWithError:(NSError *)error {
    if ([self.delegate respondsToSelector:@selector(peripheral:didDisconnectWithError:)]) {
        [self.delegate peripheral:self didDisconnectWithError:error];
    }
    else {
        NSLog(@"no delegate respond to selector with delegate: %@",self.delegate);
    }
}


- (void)didSetValueForCharacteristic:(NSString *)characteristicID withError:(NSError *)error{
    [self.commandExecuter didSetValueForCharacteristic:characteristicID withError:error];
}

- (void)didGetValue:(NSData *)value forCharacteristic:(NSString *)characteristicID withError:(NSError *)error{
    [self.commandExecuter didGetValue:value forCharacteristic:characteristicID withError:error];
}



@end
