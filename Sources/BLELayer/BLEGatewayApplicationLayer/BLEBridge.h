//
//  BLEBridge.h
//  ZemingoBLELayer
//
//  Created by Ofir Zucker on 27/06/2017.
//  Copyright © 2017 Adaya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BridgeCompletionBlocks.h"
#import "Peripheral.h"
#import "BLEDiscovererProtocol.h"

@class Peripheral;
@protocol BLEStatusDelegate;

@interface BLEBridge : NSObject

- (instancetype)initWithRequiredServices:(NSArray *)requiredServices discoveryServices:(NSArray *)discoveryServices;


- (id<BLEDiscovererProtocol>)createBLEScannerWithDelegate:(id<BLEPeripheralDiscovererDelegate>)delegate;
- (Peripheral *)connectToPeripheralWithPeripheralInfo:(PeripheralInfo *) peripheralInfo perhiperalConnectionDelegate:(id<PeripheralConnectionDelegate>)delegate ;

- (void)addStatusDelegate:(id<BLEStatusDelegate>) delegate;
- (void)removeStatusDelegate:(id<BLEStatusDelegate>) delegate;
- (BOOL) isBLEOn;

@end
