//
//  CharectaristicParserProtocol.h
//  ConnectToFlirWithBLE
//
//  Created by Adaya on 3/15/16.
//  Copyright © 2016 Adaya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEPeripheralProtocol.h"


@protocol CommandFlowProtocol <NSObject>
// boolean should determine if the char is still relevant
- (void) initiateWithPeripheralProtocol: (id<BLEPeripheralProtocol>) peripheral;
- (void) teardownWithPeripheralProtocol: (id<BLEPeripheralProtocol>) peripheral;;
- (BOOL) didGetValue:(NSData *)value withError: (NSError*) error;
- (BOOL) didSetValueWithError: (NSError *) error;
- (NSString*) name;
- (NSTimeInterval)timeout;
- (void) sendCompletion: (NSError*) error;
- (BOOL) supportsCharacteristic: (NSString *)characteristicID;
@end
