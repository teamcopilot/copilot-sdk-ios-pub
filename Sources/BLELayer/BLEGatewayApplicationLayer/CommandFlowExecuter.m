//
//  CommandFlowExecuter.m
//  ZemingoBLELayer
//
//  Created by Adaya on 19/01/2017.
//  Copyright © 2017 Adaya. All rights reserved.
//

#import "CommandFlowExecuter.h"
#import "CommandFlowProtocol.h"
#import "CommandFlowExecuterErrors.h"
#import "Constants.h"

#define LOGIN 0x10
#define SET_PWD 0x11
#define SET_NAME 0x12

@interface CommandFlowExecuter ()

@property (nonatomic, weak) id<BLEPeripheralProtocol> peripheralProtocol;
@property (nonatomic, strong) NSMutableArray<id<CommandFlowProtocol>>* commandFlows;
@property (nonatomic, strong) NSMutableArray<NSTimer*>* timers;

@end

@implementation CommandFlowExecuter

-(instancetype)initWithPeripheralProtocol:(id<BLEPeripheralProtocol>)peripheralProtocol{
    
    self = [super init];
    if (self) {
        self.peripheralProtocol = peripheralProtocol;
        self.commandFlows = [NSMutableArray array];
        self.timers = [NSMutableArray array];
    }
    return self;
}

- (void)didSetValueForCharacteristic:(NSString *)characteristicID withError:(NSError *)error{
    [self forwardNotificationToRelevantCommandFlowsForCharacteristic:characteristicID andExecutionBlock:^(id<CommandFlowProtocol> commandFlow) {
        BOOL shouldContinue = NO;
        if ([commandFlow respondsToSelector:@selector(didSetValueWithError:)]){
            shouldContinue = [commandFlow didSetValueWithError:error];
            [self checkForCommandFlow:commandFlow completed:!shouldContinue withError:error];
        }else {
            NSLog(@"The command flow %@ should respond to selector didSetValue", commandFlow);
        }
    }];
}

- (void)didGetValue:(NSData *)value forCharacteristic:(NSString *)characteristicID withError:(NSError *)error{
    [self forwardNotificationToRelevantCommandFlowsForCharacteristic:characteristicID andExecutionBlock:^(id<CommandFlowProtocol> commandFlow) {
        BOOL shouldContinue = NO;
        if ([commandFlow respondsToSelector:@selector(didGetValue:withError:)]){
            shouldContinue = [commandFlow didGetValue:value withError:error];
            [self checkForCommandFlow:commandFlow completed:!shouldContinue withError:error];
        }else {
            NSLog(@"The command flow %@ should respond to selector didGetValue:", commandFlow);
        }
    }];
}

- (void) addCommandFlow: (id<CommandFlowProtocol>) commandFlow{
    [self.commandFlows addObject:commandFlow];
    [self startTimeoutTimerForCommandFlow:commandFlow];
    
    if ([commandFlow respondsToSelector:@selector(initiateWithPeripheralProtocol:)]){
        [commandFlow initiateWithPeripheralProtocol:self.peripheralProtocol];
    }else {
        NSLog(@"The command flow %@ should respond to selector registerForChar:", commandFlow);
    }
}
    //manually stop command flow - usually works for notification based command flow
- (void)interruptCommandFlow:(id<CommandFlowProtocol>)commandFlow {
    [self checkForCommandFlow:commandFlow completed:YES withError:nil];
}

-(void)commandFlowExecutionTimedOut: (NSTimer *) timer{
    NSLog(@"Command timeout!");
#warning - consider invoking completed in a single thread
    id<CommandFlowProtocol> commandFlow = [timer.userInfo objectForKey:kCommandFlowTimerKey];
    if (commandFlow){
        [timer invalidate];
        [self.timers removeObject:timer];
        NSLog(@"Timer %@ for CommandFlow %@ has been invalidated and removed", timer, commandFlow);
        [self checkForCommandFlow:commandFlow completed:YES withError:[NSError errorWithDomain:CFE_ERROR code:CFE_ERROR_COMMAND_TIMEOUT userInfo:nil]];
    }
}

#pragma mark Utils

-(void) checkForCommandFlow: (id<CommandFlowProtocol>) commandFlow completed: (BOOL) completed withError: (NSError*) error{
    
    if(completed){
        NSLog(@"Completed current command: %@, completed? %@, error=%@", commandFlow, @(completed), error);
        if ([self.commandFlows containsObject:commandFlow]){
            [self.commandFlows removeObject:commandFlow];
        }
        
        [self invalidateTimeoutForCommandFlow:commandFlow];
        
        if ([commandFlow respondsToSelector:@selector(teardownWithPeripheralProtocol:)]){
            [commandFlow teardownWithPeripheralProtocol:self.peripheralProtocol];
            if ([commandFlow respondsToSelector:@selector(sendCompletion:)]){
                [commandFlow sendCompletion:error];
            }else {
                NSLog(@"The command flow %@ should respond to selector sendCompletion:", commandFlow);
            }
        }else {
            NSLog(@"The command flow %@ should respond to selector unregisterForChar:", commandFlow);
        }
    }
}

#pragma mark - private methods

- (NSArray<id<CommandFlowProtocol>>*)fetchCommandFlowsSupportingCharacteristic: (NSString *) characteristicID {
    NSMutableArray * commandFlows = [NSMutableArray array];
    [self.commandFlows enumerateObjectsUsingBlock:^(id<CommandFlowProtocol>  _Nonnull cmdFlow, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([cmdFlow respondsToSelector:@selector(supportsCharacteristic:)]){
            if ([cmdFlow supportsCharacteristic:characteristicID]){
                [commandFlows addObject:cmdFlow];
            }
        }else {
            NSLog(@"The command flow %@ should respond to selector supportsCharacteristic:", cmdFlow);
        }
    }];
    
    return commandFlows;
}

- (void)forwardNotificationToRelevantCommandFlowsForCharacteristic: (NSString *) characteristicID andExecutionBlock: (void(^)(id<CommandFlowProtocol> commandFlow)) executionBlock{
    NSArray *supportingCommandFlows = [self fetchCommandFlowsSupportingCharacteristic:characteristicID];
    
    if (supportingCommandFlows.count > 0){
        [supportingCommandFlows enumerateObjectsUsingBlock:^(id  _Nonnull cmdFlow, NSUInteger idx, BOOL * _Nonnull stop) {
            if (executionBlock){
                executionBlock(cmdFlow);
            }
        }];
    }else {
        NSLog(@"Received a notification for %@ but it is not supported by any command flows. Ignoring", characteristicID);
        return;
    }
}

#pragma mark - Timer methods

- (void)startTimeoutTimerForCommandFlow: (id<CommandFlowProtocol>) commandFlow {
    if ([commandFlow respondsToSelector:@selector(timeout)]){
        if ([commandFlow timeout] > 0){
            NSTimer * timer = [NSTimer timerWithTimeInterval:[commandFlow timeout] target:self selector:@selector(commandFlowExecutionTimedOut:) userInfo:@{kCommandFlowTimerKey: commandFlow} repeats:NO];
            
            [self.timers addObject:timer];
            
            if([NSRunLoop currentRunLoop] != [NSRunLoop mainRunLoop]){
                NSLog(@"Expecting to set the timer on the main run loop!");
            }
            [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
        }
    }else {
        NSLog(@"CommandFlowProtocol %@ doesn't respond to timeout", commandFlow);
    }
}

- (void)invalidateTimeoutForCommandFlow: (id<CommandFlowProtocol>) commandFlow {
    [self.timers enumerateObjectsUsingBlock:^(NSTimer * _Nonnull timer, NSUInteger idx, BOOL * _Nonnull stop) {
        id<CommandFlowProtocol> timerCommandFlow = [timer.userInfo objectForKey:kCommandFlowTimerKey];
        if (commandFlow == timerCommandFlow){
            [timer invalidate];
            [self.timers removeObject:timer];
            NSLog(@"Timer %@ for CommandFlow %@ has been removed with success", timer, commandFlow);
        }
    }];
}

@end
