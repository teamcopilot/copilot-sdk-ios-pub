//
//  ESCPeripheral.h
//  ZemingoBLELayer
//
//  Created by Adaya on 28/11/2016.
//  Copyright © 2016 Adaya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEPeripheralProtocol.h"
#import "BridgeCompletionBlocks.h"
#import "CommandFlowProtocol.h"

@class Peripheral;

@protocol PeripheralConnectionDelegate <NSObject>

-(void) peripheralDidConnect:(Peripheral *)peripheral;
-(void) peripheral:(Peripheral *)peripheral didDisconnectWithError:(NSError *)error;
-(void) peripheral:(Peripheral *)peripheral connectionFailedWithError:(NSError *)error;


@end

@interface Peripheral : NSObject

@property (nonatomic, readonly, strong) PeripheralInfo* peripheralInfo;

-(instancetype)initWithPeripheralInfo:(PeripheralInfo *) peripheralInfo requiredServices:(NSArray *)requiredServices connectionDelegate:(id<PeripheralConnectionDelegate>) delegate;
-(BOOL)disconnect;
-(void)addCommandFlow: (id<CommandFlowProtocol>) commandFlow;
-(void)interruptCommandFlow: (id<CommandFlowProtocol>) commandFlow;

@end
