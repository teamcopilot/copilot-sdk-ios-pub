//
// Created by Alex Gold on 09/07/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import Foundation

public enum ChangeEmailError: CopilotError {
	static func generalError(message: String) -> ChangeEmailError {
		.generalError(debugMessage: message)
	}

	case requiresRelogin(debugMessage: String)
	case connectivityError(debugMessage: String)
	case emailAlreadyExists(debugMessage: String)
	case invalidEmail(debugMessage: String)
	case generalError(debugMessage: String)
	case emailCannotBeChangedForAnonymousUser(debugMessage: String)
}

public class ChangeEmailErrorResolver: ErrorResolver {

	public typealias T = ChangeEmailError

	public func fromRequiresReloginError(debugMessage: String) -> ChangeEmailError {
		.requiresRelogin(debugMessage: debugMessage)
	}

	public func fromGeneralError(debugMessage: String) -> ChangeEmailError {
		.generalError(message: debugMessage)
	}

	public func fromConnectivityError(debugMessage: String) -> ChangeEmailError {
		.connectivityError(debugMessage: debugMessage)
	}

	public func fromInvalidParametersError(debugMessage: String) -> ChangeEmailError {
		.invalidEmail(debugMessage: debugMessage)
	}

	public func fromTypeSpecificError(_ statusCode: Int, _ reason: String, _ message: String) -> ChangeEmailError? {
		if isUserAlreadyExists(statusCode, reason) {
			return .emailAlreadyExists(debugMessage: message)
		} else if isInvalidEmail(statusCode, reason) || isMissingField(statusCode, reason) {
			return .invalidEmail(debugMessage: message)
		} else if isEmailCannotBeChangedForAnonymousUserError(statusCode, reason) {
			return .emailCannotBeChangedForAnonymousUser(debugMessage: message)
		}
		return nil
	}

	private func isMissingField(_ statusCode: Int, _ reason: String) -> Bool {
		statusCode == ServerError.Constants.validationError && reason == ServerError.Constants.validationErrorReason
	}

	private func isEmailCannotBeChangedForAnonymousUserError(_ statusCode: Int, _ reason: String) -> Bool {
		statusCode == ServerError.Constants.forbidden && reason == ServerError.Constants.forbiddenChangeEmailForAnonymousUser
	}
}

extension ChangeEmailError: CopilotLocalizedError {
	public func errorPrefix() -> String {
		return "Change email"
	}

	public var errorDescription: String? {
		switch self {
		case .requiresRelogin(debugMessage: let debugMessage):
			return requiresReloginMessage(debugMessage: debugMessage)
		case .connectivityError(debugMessage: let debugMessage):
			return connectivityErrorMessage(debugMessage: debugMessage)
		case .emailAlreadyExists(debugMessage: _):
			return toString("The provided email already exists.")
		case .invalidEmail(debugMessage: _):
			return toString("The provided email is not a legal email.")
		case .emailCannotBeChangedForAnonymousUser(debugMessage: let debugMessage):
			return toString("Cannot change email for anonymous user \(debugMessage)")
		case .generalError(debugMessage: let debugMessage):
			return generalErrorMessage(debugMessage: debugMessage)
		}
	}
}
