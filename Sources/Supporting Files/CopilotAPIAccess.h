//
//  CopilotAPIAccess.h
//  CopilotAPIAccess
//
//  Created by Richard Houta on 16/09/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CopilotAPIAccess.
FOUNDATION_EXPORT double CopilotAPIAccessVersionNumber;

//! Project version string for CopilotAPIAccess.
FOUNDATION_EXPORT const unsigned char CopilotAPIAccessVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CopilotAPIAccess/PublicHeader.h>
