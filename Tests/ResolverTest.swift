//
//  ResolverTest.swift
//  CopilotAPIAccessTests
//
//  Created by Adaya on 05/03/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess
import XCTest

protocol ResolverTest{
    associatedtype T
    associatedtype R : ErrorResolver
    func test_connectivity()
    func test_general()
    func test_invalidParameters()
    func test_requiresRelogin()
    func test_Unrecognized()
    func resolver() -> R
    func customEntityNotFound() -> Bool
    func customForbidden() -> Bool
    func customMarkedForDeletion() -> Bool
    func customInvalidApplicationId() -> Bool
    func customPasswordPolicy() -> Bool
    func customInvalidPermissions() -> Bool
    func customUserAlreadyExists() -> Bool
    func customResetPasswordFailed() -> Bool
    func customInvalidEmail() ->Bool
    func customUserAlreadyVerified() -> Bool
    func customEmailIsNotVerified() -> Bool
}

extension ResolverTest{
    func assertUnrecognized(){
        for i in 400..<900 {
            let error = resolver().fromTypeSpecificError(i, "aresaon", "any")
            XCTAssertNil(error)
        }
        XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.validationError, TestConstants.validationErrorReason, "any"), "Validation")
        XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.unauthorized, TestConstants.unauthorizedReason, "any"), "Unauthorized")
        XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.internalServerError, TestConstants.internalServerErrorReason, "any"), "Internal Server")
        if customEntityNotFound(){
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.entityNotFound, TestConstants.entityNotFoundReason, "any"), "EntityNotFound")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.entityNotFound, TestConstants.entityNotFoundReason, "any"), "EntityNotFound")
        }
        if customForbidden(){
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.fobidden, TestConstants.forbiddenReason, "any"), "Forbidden")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.fobidden, TestConstants.forbiddenReason, "any"), "Forbidden")
        }
        if customMarkedForDeletion(){
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.markedForDeletion, TestConstants.markedForDeletionReason, "any"), "MarkedForDeletion")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.markedForDeletion, TestConstants.markedForDeletionReason, "any"), "MarkedForDeletion")
        }
        if customInvalidApplicationId(){
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.invalidApplicationId, TestConstants.invalidApplicationIdReason, "any"), "InvalidApplicationId")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.invalidApplicationId, TestConstants.invalidApplicationIdReason, "any"), "InvalidApplicationId")
        }
        if customPasswordPolicy(){
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.passwordPolicyViolation, TestConstants.passwordPolicyViolationReason, "any"), "Password Policy")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.passwordPolicyViolation, TestConstants.passwordPolicyViolationReason, "any"), "Password Policy")
        }
        if customInvalidPermissions(){
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.invalidPermissions, TestConstants.invalidPermissionsReason, "any"), "InvalidPermissions")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.invalidPermissions, TestConstants.invalidPermissionsReason, "any"), "InvalidPermissions")
        }
        
        if customUserAlreadyExists(){
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.userAlreadyExists, TestConstants.userAlreadyExistsReason, "any"), "UserAlreadyExists")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.userAlreadyExists, TestConstants.userAlreadyExistsReason, "any"), "UserAlreadyExists")
        }
        if customResetPasswordFailed(){
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.fobidden, TestConstants.resetPasswordFailed, "any"), "ResetPasswordFailed")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.fobidden, TestConstants.resetPasswordFailed, "any"), "ResetPasswordFailed")
        }

        if customInvalidEmail() {
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.invalidEmail, TestConstants.invalidEmailReason, "any"), "invalidEmail")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.invalidEmail, TestConstants.invalidEmailReason, "any"), "invalidEmail")
        }
        if customUserAlreadyVerified(){
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.userAlreadyVerified, TestConstants.userAlreadyVerifiedReason, "any"), "User already verified")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.userAlreadyVerified, TestConstants.userAlreadyVerifiedReason, "any"), "User already verified")
        }
        if customEmailIsNotVerified(){
            XCTAssertNotNil(resolver().fromTypeSpecificError(TestConstants.emailNotVerified, TestConstants.emailNotVerifiedReason, "any"), "emailNotVerified")
        }
        else{
            XCTAssertNil(resolver().fromTypeSpecificError(TestConstants.emailNotVerified, TestConstants.emailNotVerifiedReason, "any"), "emailNotVerified")
        }
    }
    
    func customEntityNotFound() -> Bool{
        return false
    }
    func customForbidden() -> Bool{
        return false
    }
    func customMarkedForDeletion() -> Bool{
        return false
    }
    func customInvalidApplicationId() -> Bool{
        return false
    }
    
    func customPasswordPolicy() -> Bool{
        return false
    }
    
    func customInvalidPermissions() -> Bool{
        return false
    }
    func customUserAlreadyExists() ->Bool{
        return false
    }
    func customResetPasswordFailed() -> Bool{
        return false
    }

    func customInvalidEmail() ->Bool{
        return false
    }
    func customUserAlreadyVerified() -> Bool{
        return false
    }
    func customEmailIsNotVerified() -> Bool{
        return false
    }
}

struct TestConstants {
    static let validationError = 400
    static let validationErrorReason = "common.missingFields"
    static let unauthorized = 401
    static let unauthorizedReason = "auth.unauthorized"
    static let internalServerError = 500
    static let internalServerErrorReason = "util.internalError"
    static let reasonKey = "reason"
    static let errorMessage = "errorMessage"
    static let entityNotFound = 404
    static let entityNotFoundReason = "util.entityNotFound"
    static let fobidden = 403
    static let forbiddenReason = "util.operationForbidden"
    static let markedForDeletion = 403
    static let markedForDeletionReason = "auth.markedForDeletion"
    static let invalidApplicationId = 403
    static let invalidApplicationIdReason = "auth.applicationIdIsNotValid"
    static let passwordPolicyViolation = 400
    static let passwordPolicyViolationReason = "auth.passwordPolicyViolation"
    static let userAlreadyExists = 409
    static let userAlreadyExistsReason = "auth.userAlreadyExists"
    static let invalidPermissions = 403
    static let invalidPermissionsReason = "auth.invalidPermissions"
    static let resetPasswordFailed = "auth.passwordResetFailed"

    static let invalidEmail = 400
    static let invalidEmailReason = "auth.invalidEmail"
    static let userAlreadyVerifiedReason = "auth.userAlreadyVerified"
    static let userAlreadyVerified = 409
    //*reset password* -> *412* / `auth.emailNotVerified`. -> @Adaya, `ResetPasswordError.EmailIsNotVerified`
    static let emailNotVerified = 412
    static let emailNotVerifiedReason = "auth.emailNotVerified"
}
