//
//  FacadeAppTests.swift
//  CopilotAPIAccessTests
//
//  Created by Ofer Meroz on 23/10/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class FacadeAppTests: XCTestCase {

    static var testConfigurationResponse: [String : Any] {
        let privacyPolicyResponse: [String : Any] = ["url" : "http://copilot.com/privacyPolicy/test",
                                                     "version" : 1]
        let termsAndConditionsResponse: [String : Any] = ["url" : "http://copilot.com/termsAndConditions/test",
                                                          "version" : 1]
        let faqResponse: [String : Any] = ["url" : "http://copilot.com/faq/test",
                                           "version" : 1]
        
        return ["privacyPolicy" : privacyPolicyResponse,
         "termsAndConditions" : termsAndConditionsResponse,
         "faq" : faqResponse]
    }
    
    private var appAPI: AppAPIAccess!
    
    override func setUp() {
        super.setUp()
        
        let authServiceInteraction = AppTestsAuthenticationServiceInteraction()
        let systemConfigurationServiceInteraction = AppTestsSystemConfigurationServiceInteraction()
		let bundle = Bundle(for: type(of: self))
        let dependencies = TestAppDependencies(authenticationServiceInteraction: authServiceInteraction, configurationServiceInteraction: systemConfigurationServiceInteraction, configurationProvider: BundleConfigurationProvider(bundle: bundle))
        appAPI = ApplicationAPI(dependencies: dependencies)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchConfig() {
        let expectation = self.expectation(description: "fetchConfig call closure should be executed")

        appAPI
            .fetchConfig()
            .build()
            .execute { (response) in
                switch response {
                case .success(let config):
                    let testConfig = Configuration(withDictionary: FacadeAppTests.testConfigurationResponse)
                    XCTAssertEqual(config.privacyPolicy!, testConfig!.privacyPolicy!)
                    XCTAssertEqual(config.termsAndConditions!, testConfig!.termsAndConditions!)
                    XCTAssertEqual(config.faq!, testConfig!.faq!)
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                    break
                }

                expectation.fulfill()
        }

        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testFetchPasswordPolicy() {
        let expectation = self.expectation(description: "fetchPasswordPolicyConfig call closure should be executed")

        appAPI
            .fetchPasswordPolicyConfig()
            .build()
            .execute { (response) in
                switch response {
                case .success:
                    break
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                    break
                }

                expectation.fulfill()
        }

        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testCheckAppVersion() {
        let expectation = self.expectation(description: "checkAppVersion call closure should be executed")

        appAPI
            .checkAppVersionStatus()
            .build()
            .execute { (response) in
                switch response {
                case .success:
                    break
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                    break
                }

                expectation.fulfill()
        }

        waitForExpectations(timeout: 30, handler: nil)
    }
    
}

fileprivate struct TestAppDependencies: HasAuthenticationServiceInteraction, HasSystemConfigurationServiceInteraction, HasConfigurationProvider {
    let authenticationServiceInteraction: AuthenticationServiceInteractable
    let configurationServiceInteraction: SystemConfigurationServiceInteractable
    let configurationProvider: ConfigurationProvider
}

fileprivate class AppTestsSystemConfigurationServiceInteraction : SystemConfigurationServiceInteractable {
    
    func checkIfUpgradeRequired(appVersion: String, upgradeCheckClosure: @escaping UpgradeCheckClosure) {
        
        if let appVersionFromPlist = Bundle(for: type(of: self)).object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
            XCTAssertEqual(appVersion, appVersionFromPlist)
        }
        
        let appVersionStatus = AppVersionStatus(withDictionary: ["status" : AppVersionStatus.VersionStatus.ok.rawValue,
                                                                      "storeUrl" : "http://copilot.com/storeUrl/test"])
        let res: Response<AppVersionStatus, CheckAppVersionStatusError> = .success(appVersionStatus!)
        upgradeCheckClosure(res)
    }
    
    func getConfiguration(getConfigurationClosure: @escaping GetConfigurationClosure) {
        let config = Configuration(withDictionary: FacadeAppTests.testConfigurationResponse)
        let res: Response<Configuration, FetchConfigurationError> = .success(config!)
        getConfigurationClosure(res)
    }
}

fileprivate class AppTestsAuthenticationServiceInteraction: BaseTestAuthenticationServiceInteraction {
    
    override func getPasswordRulesPolicy(closure: @escaping GetPasswordRulesPolicyClosure) {
        let rules = [PasswordRules.MinimumLength(minimumLength: 8)]
        let res: Response<[PasswordRule], FetchPasswordRulesPolicyError> = .success(rules)
        closure(res)
    }
}
