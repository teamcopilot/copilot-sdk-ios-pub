//
// Created by Alex Gold on 13/09/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import Foundation
import XCTest
@testable import CopilotAPIAccess

class AppNavigationTest: XCTestCase {
	var reporter = MockReporter()
	var navigationDelegate = MockNavigationDelegate()
	var userDefaults = MockUserDefaults()
	lazy var navigationReporter = AppNavigationReporter(reporter: reporter, appNavigationDelegate: navigationDelegate, userDefaults: userDefaults)

	override func setUp() {
		super.setUp()
		reporter = MockReporter()
		navigationDelegate = MockNavigationDelegate()
		userDefaults = MockUserDefaults()
		navigationReporter = AppNavigationReporter(reporter: reporter, appNavigationDelegate: navigationDelegate, userDefaults: userDefaults)
	}

	func testReportSupportedActions_NewCommands_shouldLog() {
		navigationDelegate.navigationCommands = ["first", "second", "third"]
		let existingCommandsWillLog = [
			"first",
			"second",
		]

		userDefaults.navigationCommands = existingCommandsWillLog
		navigationReporter.reportSupportedActions()
 		XCTAssertTrue(reporter.eventsReported.count == 3
				&& Set(reporter.eventsReported) == Set(navigationDelegate.navigationCommands))
	}

	func testReportSupportedActions_SameCommands_shouldNotLog() {
		navigationDelegate.navigationCommands = ["first", "second", "third"]
		userDefaults.navigationCommands = ["first", "second", "third"]

		navigationReporter.reportSupportedActions()
		XCTAssertTrue(reporter.eventsReported.isEmpty)
	}

	func testReportSupportedActions_differentOrderSameCommands_shouldNotLog() {
		navigationDelegate.navigationCommands = ["first", "second", "third"]
		userDefaults.navigationCommands = ["second", "third", "first"]

		navigationReporter.reportSupportedActions()
		XCTAssertTrue(reporter.eventsReported.isEmpty)
	}

	func testReportSupportedActions_sameAmountDifferentItems_shouldLog() {
		navigationDelegate.navigationCommands = ["first", "second", "third"]
		userDefaults.navigationCommands = ["fourth", "fifth", "sixth"]

		navigationReporter.reportSupportedActions()
		XCTAssertTrue(reporter.eventsReported.count == 3
				&& Set(reporter.eventsReported) == Set(navigationDelegate.navigationCommands))
	}

	func testReportSupportedActions_newCommandsIsEmptyList_shouldLog() {
		navigationDelegate.navigationCommands = [String]()
		userDefaults.navigationCommands = ["first", "second"]

		navigationReporter.reportSupportedActions()
		XCTAssertTrue(reporter.didLogEvent && reporter.eventsReported.count == 1
				&& reporter.eventsReported == [""])
	}
}

class MockReporter: ReportAPIAccess {
	var didLogEvent: Bool = false
	var eventsReported = [String]()

	func log(event: AnalyticsEvent) {
		eventsReported.append(event.customParams["command_name"] ?? "")
		didLogEvent = true
	}
}

class MockNavigationDelegate: AppNavigationDelegate {

	var navigationCommands: [String] = [String]()

	func handleAppNavigation(_ appNavigationCommand: String) {

	}

	func getSupportedAppNavigationCommands() -> [String] {
		navigationCommands
	}
}

class MockUserDefaults: CopilotSettings {
	var userDefaults: [String : Any] = [String:Any]()

	var navigationCommands: [String] {
		get {
			userDefaults["navigationCommands"] as! [String]
		}
		set(commands) {
			userDefaults["navigationCommands"] = commands
		}
	}
}