//
//  BaseTestUserServiceInteraction.swift
//  CopilotAPIAccessTests
//
//  Created by Ofer Meroz on 18/11/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
@testable import CopilotAPIAccess

class BaseTestUserServiceInteraction: UserServiceInteractable {
    var me: UserMe?
    
    func me(getCurrentUserClosure: @escaping GetCurrentUserClosure) {
        // to be overridden when needed
    }
    
    func updateUser(id: String, userInfo: UserInfo?, customSettings: [String : Any]?, updateUserClosure: @escaping UpdateUserClosure) {
        // to be overridden when needed
    }
    
    func updateDeviceDetails(copilotSdkVersion: String, pnsToken: Data, isSandbox: Bool, updateCurrentDeviceClosure: @escaping UpdateDeviceDetailsClosure) {
        // to be overridden when needed
    }

    func deleteMe(deleteUserClosure: @escaping (Response<Void, DeleteUserError>) -> Void) {
        // to be overridden when needed
    }

    func changeEmail(newEmail: String, closure: @escaping (Response<UserMe, ChangeEmailError>) -> Void) {
        // to be overridden when needed
    }
}