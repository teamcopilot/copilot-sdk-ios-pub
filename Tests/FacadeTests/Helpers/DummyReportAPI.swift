//
//  DummyReportAPI.swift
//  CopilotAPIAccessTests
//
//  Created by Revital Pisman on 04/11/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class DummyReportAPI: ReportAPIAccess {
    
    func log(event: AnalyticsEvent) {
        //Do nothing
    }
}
