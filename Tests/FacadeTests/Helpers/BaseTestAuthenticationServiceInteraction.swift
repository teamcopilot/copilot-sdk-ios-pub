//
//  BaseTestAuthenticationServiceInteraction.swift
//  CopilotAPIAccessTests
//
//  Created by Ofer Meroz on 28/10/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
@testable import CopilotAPIAccess

class BaseTestAuthenticationServiceInteraction: AuthenticationServiceInteractable {
    func sendVerificationEmail(closure: @escaping SendVerificationEmailClosure) {
        // to be overridden when needed
    }
    
    func changePassword(newPassword: String, oldPassword: String, closure: @escaping ChangePasswordClosure) {
        // to be overridden when needed
    }
    
    func register(withEmail email: String, password: String, firstName: String, lastName: String, consents: [String : Bool], registerClosure: @escaping RegisterClosure) {
        // to be overridden when needed
    }
    
    func registerAnonymously(withConsents consents: [String : Bool], registerClosure: @escaping RegisterAnonymouslyClosure) {
        // to be overridden when needed
    }
    
    func elevateAnonymous(withEmail email: String, password: String, firstName: String, lastName: String, elevateClosure: @escaping ElevateAnonymousClosure) {
        // to be overridden when needed
    }
    
    func login(withEmail email: String, password: String, loginClosure: @escaping LoginClosure) {
        // to be overridden when needed
    }
    
    func requireResetPassword(withEmail email: String, closure: @escaping RequireResetPasswordClosure) {
        // to be overridden when needed
    }
    
    func getPasswordRulesPolicy(closure: @escaping GetPasswordRulesPolicyClosure) {
        // to be overridden when needed
    }
    
    func approveTermsOfUse(for termsOfUseVersion: String, closure: @escaping ApproveTermsOfUseClosure) {
        // to be overridden when needed
    }
    
    func setConsents(details consentsDetails: [String : Bool], closure: @escaping SetConsetClosure) {
        // to be overridden when needed
    }
    
    func consentRefused(closure: @escaping ConsetRefusedClosure) {
        // to be overridden when needed
    }
    
    func logout(logoutClosure: (Response<Void, LogoutError>) -> Void) {
        // to be overridden when needed
    }
    
    func attemptToSilentLogin(closure: @escaping SilentLoginClosure) {
        // to be overridden when needed
    }

    func getPartnerTicket(applicationID: String, PartnerId: String, closure: @escaping (CopilotAPIAccess.Response<CopilotAPIAccess.GetPartnerTicketResponse, CopilotAPIAccess.GetPartnerTicketError>) -> Void) {
        // to be overridden when needed
    }
}
