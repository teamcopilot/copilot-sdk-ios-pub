//
//  SessionBasedAnalyticsRepositoryTests.swift
//  CopilotAPIAccessTests
//
//  Created by Revital Pisman on 25/09/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class SessionBasedAnalyticsRepositoryTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_parametersRepositoryConcurrentManipulation() {
        let sessionBasedAnalyticsRepository = SessionBasedAnalyticsRepository()
        
        let credentialsType = CredentialsType.email
        let userId = "userId"
        let userEmail = "userEmail"
        let userGeneralParamsRepo = CopilotUserGeneralParametersRepository(signUpMethod: credentialsType, userId: userId)
        
        for _ in 0...1000 {
            DispatchQueue.global(qos: .background).async {
                sessionBasedAnalyticsRepository.addGeneralParamsForCurrentSession(generalParamsForSession: userGeneralParamsRepo)
            }
            
            DispatchQueue.global(qos: .background).async {
                sessionBasedAnalyticsRepository.sessionEnded()
            }
        }
    }
    
    func test_addParametersRepositoryWithSameParameters() {
        let sessionBasedAnalyticsRepository = SessionBasedAnalyticsRepository()
        
        let userIdKey = "userId"
        
        let userIdA = "userIdA"
        let userEmail = "userEmail"
        let mockA = MockGeneralParametersRepository([userIdKey: userIdA, userEmail: userEmail])
        
        sessionBasedAnalyticsRepository.addGeneralParamsForCurrentSession(generalParamsForSession: mockA)
        
        let userIdB = "userIdB"
        let mockB = MockGeneralParametersRepository([userIdKey: userIdB, userEmail: userEmail])
        
        sessionBasedAnalyticsRepository.addGeneralParamsForCurrentSession(generalParamsForSession: mockB)
        
        
        let currentRepoInActiveSession = sessionBasedAnalyticsRepository.activeGeneralParamsRepoForSession.first!
        
        XCTAssertEqual(currentRepoInActiveSession.generalParameters[userIdKey], userIdB, "Failed replacing new repo with the old repo")
        XCTAssertEqual(sessionBasedAnalyticsRepository.activeGeneralParamsRepoForSession.count, 1, "Failed replacing new repo with the old repo, only 1 repo should exist")
    }
    
    func test_addParametersRepositoryWithAtLeastOneSameParameterAndOneDifferent() {
        let sessionBasedAnalyticsRepository = SessionBasedAnalyticsRepository()
        
        let userIdKey = "userId"
        
        let userIdA = "userIdA"
        let mockA = MockGeneralParametersRepository([userIdKey: userIdA])
        
        sessionBasedAnalyticsRepository.addGeneralParamsForCurrentSession(generalParamsForSession: mockA)
        
        let userIdB = "userIdB"
        let userEmail = "userEmail"
        let mockB = MockGeneralParametersRepository([userIdKey: userIdB, userEmail: userEmail])
        
        sessionBasedAnalyticsRepository.addGeneralParamsForCurrentSession(generalParamsForSession: mockB)
        
        let currentLastRepoInActiveSession = sessionBasedAnalyticsRepository.activeGeneralParamsRepoForSession.last!
        
        XCTAssertEqual(currentLastRepoInActiveSession.generalParameters[userIdKey], userIdB, "Failed to add new repo")
        XCTAssertEqual(currentLastRepoInActiveSession.generalParameters[userEmail], userEmail, "Failed to add new repo")
        XCTAssertEqual(sessionBasedAnalyticsRepository.activeGeneralParamsRepoForSession.count, 2, "Failed to app new repo, 2 repositories should exist")
    }
    
    func test_addParametersRepositoryWithDifferentParameters() {
        let sessionBasedAnalyticsRepository = SessionBasedAnalyticsRepository()
        
        let userId = "userId"
        let mockA = MockGeneralParametersRepository([userId: userId])
        
        let userEmail = "userEmail"
        let mockB = MockGeneralParametersRepository([userEmail: userEmail])
        
        sessionBasedAnalyticsRepository.addGeneralParamsForCurrentSession(generalParamsForSession: mockA)
        sessionBasedAnalyticsRepository.addGeneralParamsForCurrentSession(generalParamsForSession: mockB)
        
        XCTAssertEqual(sessionBasedAnalyticsRepository.activeGeneralParamsRepoForSession.count, 2, "Failed to add both repositories")
    }

}
