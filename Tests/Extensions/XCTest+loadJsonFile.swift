//
//  XCTest+loadJsonFile.swift
//  CopilotAPIAccessTests
//
//  Created by Elad on 25/03/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import XCTest


extension XCTestCase {
    func loadJsonFileWith(_ name: String) -> Dictionary<String, Any> {
        guard let pathString = Bundle(for: type(of: self)).path(forResource: name, ofType: "json") else {
            fatalError("UnitTestData.json not found")
        }

        guard let jsonString = try? String(contentsOfFile: pathString, encoding: .utf8) else {
            fatalError("Unable to convert UnitTestData.json to String")
        }

//        print("The JSON string is: \(jsonString)")

        guard let jsonData = jsonString.data(using: .utf8) else {
            fatalError("Unable to convert UnitTestData.json to Data")
        }

        guard let jsonDictionary = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
            fatalError("Unable to convert UnitTestData.json to JSON dictionary")
        }
        
        return jsonDictionary
    }
}
