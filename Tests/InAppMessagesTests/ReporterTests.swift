//
// Created by Alex Gold on 12/06/2022.
// Copyright (c) 2022 Zemingo. All rights reserved.
//

import Foundation
import XCTest
@testable import CopilotAPIAccess

class ReporterTests: XCTestCase {

	func testReportBlocked() {
		let expectation = self.expectation(description: "reportBlocked")
		let dummyReporter = Reporter()
		let dependencies = InAppDependencies(inAppServiceInteraction: InAppServiceInteraction(configurationProvider: MockConfigurationProvider(), sequentialExecutionHelper: MoyaSequentialExecutionHelper()), reporter: dummyReporter)
		let reporterManager = InAppReporterManager(dependencies: dependencies)

		let concurrentQueue = DispatchQueue(label: "reportBlocked", qos: .utility, attributes: .concurrent)

		let messageIds = [1, 1, 2, 1, 3, 3, 4, 1, 1, 4]
		messageIds.enumerated().forEach { index, messageId in
			concurrentQueue.sync { //will crash if async, reporterManager is not thread safe.
				reporterManager.reportInAppBlocked(reason: .appConditions, messageId: "\(messageId)", generalParameters: [:])
				if index == messageIds.count - 1 && dummyReporter.eventCount == 4 { //if we reached the end of the array AND all four distinct events have been blocked.
					XCTAssertTrue(true)
					expectation.fulfill()
				}
			}
		}
		waitForExpectations(timeout: 2)
	}

	class Reporter: DummyReportAPI {
		var eventCount: Int = 0

		override func log(event: AnalyticsEvent) {
			eventCount += 1
		}
	}
}
