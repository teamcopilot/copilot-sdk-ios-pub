//
//  AnalyticsEventsTests.swift
//  CopilotAPIAccessTests
//
//  Created by Elad on 25/03/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class AnalyticsEventsTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testAnalyticsEventReported() {
        let dictionary = loadJsonFileWith("get-in-app-messages-valid")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        let inbox = InAppInbox()
        inbox.populateMessages(inappmessages!.messages)

        XCTAssertEqual(inbox.messages[0].status, .pending)
        inbox.analyticsEventReceived(OnboardingEndedAnalyticsEvent())
        XCTAssertEqual(inbox.messages[0].status, .ready)
        print(inbox.messages[0].status)
    }

}
