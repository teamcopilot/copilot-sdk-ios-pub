//
//  InAppInboxTest.swift
//  CopilotAPIAccessTests
//
//  Created by Revital Pisman on 24/12/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class InAppInboxTest: XCTestCase {
    
    
    override func setUp() {

    }
    
    override func tearDown() {

    }
    
    
    //MARK: - test populateMessages

    func testInsertEmptyMessagesWhenLocalMessagesInNotEmpty() {
        
        let dictionary = loadJsonFileWith("get-in-app-messages-valid")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        let inbox = InAppInbox()
        inbox.populateMessages(inappmessages!.messages)

        inbox.populateMessages([])
        
        XCTAssertEqual(inbox.messages.count, 0)
    }
    
    func testPopulateMessagesSameIdDifferentOrder() {
        let inbox = InAppInbox()
        let dictionary = loadJsonFileWith("get-in-app-messages-valid")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        inbox.populateMessages(inappmessages!.messages)
        
        
        let dictionary2 = loadJsonFileWith("get-in-app-messages-valid-sameId-differentorder")
        let inappmessages2 = InAppMessages(withDictionary: dictionary2)
        inbox.populateMessages(inappmessages2!.messages)
        
        for (index, element) in (inappmessages2?.messages.enumerated())! {
            XCTAssertEqual(inbox.messages[index].id, element.id)
        }
    }
    
    func testPopulateMessagesDifferentIds() {
        let inbox = InAppInbox()
        let dictionary = loadJsonFileWith("get-in-app-messages-valid")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        inbox.populateMessages(inappmessages!.messages)
        
        
        let dictionary2 = loadJsonFileWith("get-in-app-messages-valid-different-ids")
        let inappmessages2 = InAppMessages(withDictionary: dictionary2)
        inbox.populateMessages(inappmessages2!.messages)
        
        XCTAssertEqual(inbox.messages.count, inappmessages2?.messages.count)
        for (index, element) in (inappmessages2?.messages.enumerated())! {
            XCTAssertEqual(inbox.messages[index].id, element.id)
        }
    }
    
    func testPopulateMessagesSameIdDifferentStatus() {
        let inbox = InAppInbox()
        let dictionary = loadJsonFileWith("get-in-app-messages-valid")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        inappmessages?.messages[0].status = .evicted
        inbox.populateMessages(inappmessages!.messages)
        
        
        let dictionary2 = loadJsonFileWith("get-in-app-messages-valid-sameId-differentorder")
        let inappmessages2 = InAppMessages(withDictionary: dictionary2)
        inbox.populateMessages(inappmessages2!.messages)
        XCTAssertEqual(inbox.messages[1].status, .evicted)
        for (index, element) in (inappmessages2?.messages.enumerated())! {
            XCTAssertEqual(inbox.messages[index].id, element.id)
        }
    }
 
    //MARK: - test markMessageAsEvicted
    
    func testMarkAsEvicted() {
        let inbox = InAppInbox()
        let dictionary = loadJsonFileWith("get-in-app-messages-valid")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        inbox.populateMessages(inappmessages!.messages)
        inbox.messages.forEach {
            $0.markAsEvicted()
        }
        
        inbox.messages.forEach {
            XCTAssertEqual($0.status, .evicted)
        }
    }
//    func testMarkMessageAsEvicted() {
//
//        guard let inbox = inbox else { return }
//
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let message1 = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                           priority: IamPriority.high,
//                                           status: InAppStatus.pending,
//                                           creationDate: currentDate,
//                                           expirationDate: currentDate.add(days: 3)!)
//
//        let message2 = InAppMessage(id: UUID().uuidString,
//                                          model: simpleIamModel,
//                                          trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                          priority: IamPriority.high,
//                                          status: InAppStatus.pending,
//                                          creationDate: currentDate,
//                                          expirationDate: currentDate.add(days: 3)!)
//
//        messages.append(message1)
//        messages.append(message2)
//
//        inbox.populateMessages(messages)
//
//        inbox.markMessageAsEvicted(message2)
//
//        for message in inbox.messages {
//            if message.id == message1.id {
//                //status should be pending
//                XCTAssertEqual(message.status, InAppStatus.pending)
//            } else {
//                //status should be evicted
//                XCTAssertEqual(message.status, InAppStatus.evicted)
//            }
//        }
//    }
    
    //MARK: - test readyToTrigger
    
    func testReadyToTrigger() {
        let inbox = InAppInbox()
        let dictionary = loadJsonFileWith("get-in-app-messages-valid")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        inappmessages?.messages[0].status = .ready
        inbox.populateMessages(inappmessages!.messages)
        
        XCTAssertNil(inbox.messageTriggered)
        
        inbox.readyToTrigger()
    }
        
    //MARK: - test analyticsEventReported
    
//    func testAnalyticsEventReportedWithNoMessagesInInbox() {
//
//        let inbox: InAppInbox = InAppInbox(touchPolicyValidator: DefaultTouchPolicyValidator())
//
//        let testTrigger = CheckTrigger() { (message, wasCalled) in
//            XCTFail("should not get in here because there is no messages in iamInbox")
//        }
//        inbox.delegate = testTrigger
//
//        provideAnalyticsEventDispatcher().dispatcherLogCustomEvent(event: MockAnalyticsEvent())
//
//        XCTAssertTrue(!testTrigger.wasCalled)
//    }
    
//    func testAnalyticsEventReportedWithMessageInInbox() {
//
//        let inbox: InAppInbox = InAppInbox(touchPolicyValidator: DefaultTouchPolicyValidator())
//
//        let testTrigger = CheckTrigger() { (message, wasCalled) in
//            XCTAssertNotNil(message)
//            guard let _ = message else {
//                XCTFail("this test should contain messages that not marked as evicted")
//                return
//            }
//            XCTAssertTrue(wasCalled)
//        }
//        inbox.delegate = testTrigger
//
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let iamStatusPending = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                           priority: IamPriority.high,
//                                           status: InAppStatus.pending,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 3)!)
//        messages.append(iamStatusPending)
//
//        inbox.populateMessages(messages)
//
//        provideAnalyticsEventDispatcher().dispatcherLogCustomEvent(event: MockAnalyticsEvent())
//    }
    
//    func testAnalyticsEventReportedWithEvictedMessageInInbox() {
//
//        let inbox: InAppInbox = InAppInbox(touchPolicyValidator: DefaultTouchPolicyValidator())
//
//        let testTrigger = CheckTrigger() { (message, wasCalled) in
//            XCTFail("should not get in here because the message status is evicted")
//        }
//
//        inbox.delegate = testTrigger
//
//
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let iamStatusPending = InAppMessage(id: UUID().uuidString,
//                                            model: simpleIamModel,
//                                            trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                            priority: IamPriority.high,
//                                            status: InAppStatus.evicted,
//                                            creationDate: currentDate.subtract(days: 2),
//                                            expirationDate: currentDate.add(days: 3)!)
//        messages.append(iamStatusPending)
//
//        inbox.populateMessages(messages)
//
//        provideAnalyticsEventDispatcher().dispatcherLogCustomEvent(event: MockAnalyticsEvent())
//
//        XCTAssertTrue(!testTrigger.wasCalled)

}



