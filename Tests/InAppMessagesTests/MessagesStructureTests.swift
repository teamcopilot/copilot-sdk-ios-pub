//
//  MessagesStructureTests.swift
//  CopilotAPIAccessTests
//
//  Created by Elad on 24/03/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class MessagesStructureTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testWrongActionParameter() {
        let dictionary = loadJsonFileWith("get-in-app-messages-new-action")
        let messages: [Dictionary<String, Any>] = dictionary["messages"] as! [Dictionary<String, Any>]
        messages.forEach {
            XCTAssertNil(InAppMessage(withDictionary: $0))
        }
    }
    
    func testWrongPresentationParameter() {
        let dictionary = loadJsonFileWith("get-in-app-messages-new-presentation")
        let messages: [Dictionary<String, Any>] = dictionary["messages"] as! [Dictionary<String, Any>]
        messages.forEach {
            XCTAssertNil(InAppMessage(withDictionary: $0))
        }
    }
    
    func testWrongTriggerParameter() {
        let dictionary = loadJsonFileWith("get-in-app-messages-new-trigger")
        let messages: [Dictionary<String, Any>] = dictionary["messages"] as! [Dictionary<String, Any>]
        messages.forEach {
            XCTAssertNil(InAppMessage(withDictionary: $0))
        }
    }
    
    func testMessageWithNoId() {
        let dictionary = loadJsonFileWith("get-in-app-messages-no-id")
        let messages: [Dictionary<String, Any>] = dictionary["messages"] as! [Dictionary<String, Any>]
        messages.forEach {
            XCTAssertNil(InAppMessage(withDictionary: $0))
        }
    }
    
    func testNoMessagesParameter() {
        let dictionary = loadJsonFileWith("get-in-app-messages-no-messages")
        XCTAssertNil(InAppMessages(withDictionary: dictionary))
    }
    
    func testInvalidPresentation() {
        let dictionary = loadJsonFileWith("get-in-app-messages-presentation-invalid")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        XCTAssertEqual(inappmessages?.messages.count, 3)
        
        
    
    }
    
    func testMissingPresentationParameter() {
        let dictionary = loadJsonFileWith("get-in-app-messages-presentation-missing")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        XCTAssertEqual(inappmessages?.messages.count, 3)
    }
    
    func testMissingReportParameter() {
        let dictionary = loadJsonFileWith("get-in-app-messages-report-missing")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        XCTAssertEqual(inappmessages?.messages.count, 4)
    }
    
    func testInvalidTrigger() {
        let dictionary = loadJsonFileWith("get-in-app-messages-trigger-invalid")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        XCTAssertEqual(inappmessages?.messages.count, 3)
    }
    
    func testMissingTrigger() {
        let dictionary = loadJsonFileWith("get-in-app-messages-trigger-missing")
        let inappmessages = InAppMessages(withDictionary: dictionary)
        XCTAssertEqual(inappmessages?.messages.count, 3)
    }
    
    func testValidMessages() {
        let dictionary = loadJsonFileWith("get-in-app-messages-valid")
        let messages: [Dictionary<String, Any>] = dictionary["messages"] as! [Dictionary<String, Any>]
        messages.forEach {
            XCTAssertNotNil(InAppMessage(withDictionary: $0))
        }
    }
}
