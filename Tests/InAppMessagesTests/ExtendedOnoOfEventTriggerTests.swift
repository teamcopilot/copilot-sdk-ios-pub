//
//  ExtendedOnoOfEventTriggerTest.swift
//  CopilotAPIAccessTests
//
//  Created by Elad on 28/04/2021.
//  Copyright © 2021 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class ExtendedOnoOfEventTriggerTests: XCTestCase {

    private struct Consts {
        static let eventName = "screen_load"
        static let otherEventName = "other_event_name"
        static let someProperty = "some_property"
        static let someProperty2 = "some_property_2"
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMatchEventProperyValue_numericPropertyOnlyOr() {
        
        let json = loadJsonFileWith("MatchEventPropertyValue_numericPropertyOnlyOr")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "7.0"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "7"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "6.0"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "8.0"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "bla"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "11.1"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "10.5"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "8"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "-5"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "false"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "true"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : ""])))
    }
    
    
    func testMatchEventPropertyValue_numericPropertyOnlyAnd() {
        //checks value that comform to: 7.0 < X < 10.5
        let json = loadJsonFileWith("MatchEventPropertyValue_numericPropertyOnlyAnd")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "7.0"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "7.01"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "10.5"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "10.49"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "6.0"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "8.0"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "bla"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "11.1"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "8"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "-5"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "false"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : ""])))
    }
    
    func testMatchEventPropertyValue_numericPropertyEquals() {
        // checks value that 7.0 = X  || X = 10.5
        let json = loadJsonFileWith("MatchEventPropertyValue_numericPropertyEquals")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "7"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "7.0"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "10"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "10.5"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "6.0"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "8.0"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "bla"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "11.1"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "8"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "-5"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "false"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : ""])))
    }
    
    func testMatchEventPropertyValue_ExcludesValueOf() {
        let json = loadJsonFileWith("MatchEventPropertyValue_ExcludesValueOf")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "forget_password_screen"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: ["another_property" : "forget_password_screen"])))

    }
    
    func testMatchEventPropertyValue_boolean() {
        let json = loadJsonFileWith("MatchEventPropertyValue_boolean")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "true"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "false"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "0"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "1"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : ""])))
    }
    
    func testMatchEventPropertyValue_emptyOr() {
        let json = loadJsonFileWith("MatchEventPropertyValue_emptyOr")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "true"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "false"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "0"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "1"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : ""])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: ["another property" : ""])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.otherEventName, eventProperties: ["another property" : ""])))
    }
    
    func testMatchEventPropertyValue_emptyAnd() {
        let json = loadJsonFileWith("MatchEventPropertyValue_emptyAnd")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "true"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "false"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "0"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "1"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : ""])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: ["another property" : "dont care"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.otherEventName, eventProperties: ["another property" : "dont care"])))
    }
    
    func testMatchEventPropertyValue_propertyNotFound() {
        let json = loadJsonFileWith("MatchEventPropertyValue_propertyNotFound")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: ["another property" : "dont care"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "true"])))
    }
    
    func testMatchEventPropertyValue_multipleBooleanPropertiesAnd() {
        let json = loadJsonFileWith("MatchEventPropertyValue_multipleBooleanPropertiesAnd")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "true", Consts.someProperty2 : "false"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "false", Consts.someProperty2 : "true"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "true", Consts.someProperty2 : "true"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "false", Consts.someProperty2 : "false"])))
    }
    
    func testMatchEventPropertyValue_multipleBooleanPropertiesOr() {
        let json = loadJsonFileWith("MatchEventPropertyValue_multipleBooleanPropertiesOr")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "true", Consts.someProperty2 : "false"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "false", Consts.someProperty2 : "true"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "true", Consts.someProperty2 : "DontCare"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "false", Consts.someProperty2 : "false"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "0", Consts.someProperty2 : "false"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "DontCare", Consts.someProperty2 : "false"])))
    }
    
    func testMatchEventPropertyValue_listAndNumeric() {
        let json = loadJsonFileWith("MatchEventPropertyValue_listAndNumeric")
        guard let trigger = ExtendedOneOfEventTrigger(withDictionary: json) else {
            XCTFail("parse json to extendedOneOfEventTrigger failed")
            return
        }
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "login_screen", Consts.someProperty2 : "6"])))
        XCTAssertTrue(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "login_screen", Consts.someProperty2 : "10"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "login_screen", Consts.someProperty2 : "5"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "login_screen", Consts.someProperty2 : "11"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "signup_screen", Consts.someProperty2 : "6"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "signup_screen", Consts.someProperty2 : "7"])))
        XCTAssertFalse(trigger.analyticsEventLogged(createAnalyticsEvent(eventName: Consts.eventName, eventProperties: [Consts.someProperty : "reset_password_screen", Consts.someProperty2 : "6"])))
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    private func createAnalyticsEvent(eventName: String, eventProperties: Dictionary<String, String>) -> AnalyticsEvent {
        AnalyticsEventForTesting(eventName: eventName, eventProperties: eventProperties)
    }

}


struct AnalyticsEventForTesting: AnalyticsEvent {
    
    let eventName: String
    let eventProperties: Dictionary<String, String>?
    
    init(eventName: String, eventProperties: Dictionary<String, String>) {
        self.eventName = eventName
        self.eventProperties = eventProperties
    }
    
    var customParams: Dictionary<String, String> {
        return eventProperties ?? [:]
    }

    var eventOrigin: AnalyticsEventOrigin {
        return .App
    }
}
