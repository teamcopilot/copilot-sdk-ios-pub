//
//  SimplePresentationTypeTests.swift
//  CopilotAPIAccessTests
//
//  Created by Elad on 09/03/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class SimplePresentationTypeTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // TODO : Fix this test
    
//    func testUrlValidation() {
//        let urlArrayToValidate = ["htt://someUrl.com", "www.someUrl.com", "http://someUrl.c", "http://someUrl.com", "http://someUrl.com", "https://someUrl.com", "https://someUrl.comcomcom", "https://some Url.com", "ww.someUrl.com"]
//
//        urlArrayToValidate.forEach {
//            XCTAssertEqual(verifyUrl(urlString: $0), $0.isValidURL)
//        }
//    }
    
    func testInvalidCtaDataParser() {
        
        /*
         In this test I've checked those cases:
         1. ctaWitnWrongTypeKey
         2. ctaWithNoTextKey
         3. messageWithWrongCtaType
         4. messageWithNoCtaAction
         5. ctaWithWrongTypeValue
         6. ctaWithoutTextColor
         7. ctaWithoutBackgroundColor
         8. ctaWithoutWrongCtaTextValue
         */
        
        let wrongCtaDictionary = loadJsonFileWith("WrongCtaSimpleInappModel")
        let messages: [Dictionary<String, Any>] = wrongCtaDictionary["messages"] as! [Dictionary<String, Any>]
        messages.forEach {
            XCTAssertNil(InAppMessage(withDictionary: $0))
        }
    }
    
    func testInvalidMessageTitle() {
        
        /*
         In this test I've checked those cases:
         1. message with wrong title key
         2. message with wrong title value
         */
        
        let wrongCtaDictionary = loadJsonFileWith("WrongMessage")
        let messages: [Dictionary<String, Any>] = wrongCtaDictionary["messages"] as! [Dictionary<String, Any>]
        messages.forEach {
            XCTAssertNil(InAppMessage(withDictionary: $0))
        }
    }
    
    //MARK: - Helper methods
    
    private func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
}


