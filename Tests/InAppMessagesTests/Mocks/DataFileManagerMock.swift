//
// Created by Alex Gold on 25/10/2021.
// Copyright (c) 2021 Zemingo. All rights reserved.
//

import Foundation


class DataFileManagerMock: DataManager {

	private let manager = UserDefaults.init(suiteName: "copilot_test")
	private let sizeThreshold = 10 * 1000 * 1000 //20 MB

	//MARK: - DataManager protocol implementation

	func saveDataToDevice(data: Data, key: String) {
		manager?.set(data, forKey: key)
		ZLogManagerWrapper.sharedInstance.logInfo(message: "TEST_ saved to device")
	}

	func readDataFromDevice(key: String) -> Data? {
		ZLogManagerWrapper.sharedInstance.logInfo(message: "TEST_ read from device")
		return manager?.object(forKey: key) as? Data
	}

	func deleteDataFromDevice(filename: String) {
		manager?.removeObject(forKey: filename)
		ZLogManagerWrapper.sharedInstance.logInfo(message: "TEST_ delete from device")
	}

	func clearCache() {
		manager?.removePersistentDomain(forName: "copilot_test")
		ZLogManagerWrapper.sharedInstance.logInfo(message:"TEST_ Cache cleared")
	}
	//TODO: Add delete when over threshold
}
