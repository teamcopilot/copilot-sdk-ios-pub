//
//  ImageCachingTests.swift
//  CopilotAPIAccessTests
//
//  Created by Alex Gold on 26/10/2021.
//  Copyright © 2021 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class ImageCachingTests: XCTestCase {

	private var manager: DataFileManagerMock!
	private var imageCacher: ImageCacher!

	let imageUrls = [
		"https://via.placeholder.com/1468x468?text=Image+1111111",
		"https://w.wallhaven.cc/full/28/wallhaven-283jqy.jpg",
		"https://w.wallhaven.cc/full/5w/wallhaven-5weyz8.jpg",
		"https://w.wallhaven.cc/full/l3/wallhaven-l3kkzq.jpg",
		"https://w.wallhaven.cc/full/lm/wallhaven-lmle8q.png",
		"https://w.wallhaven.cc/full/rd/wallhaven-rdg7mw.jpg",
		"https://w.wallhaven.cc/full/wq/wallhaven-wqery6.jpg",
	]

	override func setUp() {
		super.setUp()
		manager = DataFileManagerMock()
		imageCacher = ImageCacher(dataManager: manager)
	}

	override func tearDown() {
		manager.clearCache()
	}

	func testImagePreloadWithDelay() {

		let expectation = self.expectation(description: "imageCaching")
		imageUrls.forEach { imageUrl in
			imageCacher.loadImage(forUrl: imageUrl)
		}

		sleep(3)


		imageCacher.loadImage(forUrl: imageUrls[2]) { result in
			switch result {
			case .Success(let image):
				XCTAssertNotEqual(image, UIImage())
				expectation.fulfill()
			case .Failure:
				XCTFail()
				expectation.fulfill()
			}
		}

		waitForExpectations(timeout: 5, handler: nil)
	}

	func testImageLoadImmediately() {

		let expectation = self.expectation(description: "imageCachingImmediately")
		imageUrls.forEach { imageUrl in
			imageCacher.loadImage(forUrl: imageUrl)
		}

		imageCacher.loadImage(forUrl: imageUrls[3]) { result in
			switch result {
			case .Success(let image):
				XCTAssertNotEqual(image, nil)
				expectation.fulfill()
			case .Failure:
				XCTFail()
				expectation.fulfill()
			}
		}
		waitForExpectations(timeout: 5, handler: nil)
	}

	func testImageLoadingInParallel() {
		let concurrentQueue = DispatchQueue(label: "concurrent.queue", attributes: .concurrent)
		let expectation = self.expectation(description: "imageCachingParallel")

		var counter = 0

		for i in 0..<imageUrls.count {
			concurrentQueue.async {
				self.imageCacher.loadImage(forUrl: self.imageUrls[i]) { result in
					switch result {
					case .Success(let image):
						XCTAssertNotEqual(image, nil)
						counter += 1
						print("Counter: ", counter)
					case .Failure:
						XCTFail()
					}
					if counter == self.imageUrls.count { expectation.fulfill() }
				}
			}
		}

		waitForExpectations(timeout: 7, handler: nil)
		XCTAssertEqual(counter, imageUrls.count)
	}
}
