//
//  InAppUnsupportedMessagesFilterTest.swift
//  CopilotAPIAccessTests
//
//  Created by Michael Noy on 04/08/2021.
//  Copyright © 2021 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class InAppUnsupportedMessagesFilterTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_filterMessages_allSupported() {
        let filter = InAppUnsupportedMessagesFilter()

        let appNavigationHandler = Generator.getAppNavigationDelegate(supportedCommands: ["command1", "otherCommand"])
        let messageDictionary: Dictionary<String, Any> = loadJsonFileWith("app-navigation-command1")
        let inAppMessages = InAppMessages(withDictionary: messageDictionary)!

        let filteredMessages = filter.filter(appNavigationHandler: appNavigationHandler, messages: inAppMessages.messages)

        XCTAssertEqual(inAppMessages.messages.count, 1)
        XCTAssertEqual(filteredMessages.count, 1)
        XCTAssertEqual(inAppMessages.messages[0].id, filteredMessages[0].id)
    }

    func test_filterMessages_filterOutUnhandled() {
        let filter = InAppUnsupportedMessagesFilter()

        let appNavigationHandler = Generator.getAppNavigationDelegate(supportedCommands: ["command2", "otherCommand"])
        let messageDictionary: Dictionary<String, Any> = loadJsonFileWith("app-navigation-command1")
        let inAppMessages = InAppMessages(withDictionary: messageDictionary)!

        let filteredMessages = filter.filter(appNavigationHandler: appNavigationHandler, messages: inAppMessages.messages)

        XCTAssertEqual(inAppMessages.messages.count, 1)
        XCTAssertEqual(filteredMessages.count, 0)
    }

    func test_filterMessages_otherActionTypes() {
        let filter = InAppUnsupportedMessagesFilter()

        let appNavigationHandler = Generator.getAppNavigationDelegate(supportedCommands: ["command2", "otherCommand"])
        let messageDictionary: Dictionary<String, Any> = loadJsonFileWith("app-navigation-other-action")
        let inAppMessages = InAppMessages(withDictionary: messageDictionary)!


        let filteredMessages = filter.filter(appNavigationHandler: appNavigationHandler, messages: inAppMessages.messages)

        XCTAssertEqual(inAppMessages.messages.count, 1)
        XCTAssertEqual(filteredMessages.count, 1)
    }

    class Generator {
        public static func getAppNavigationDelegate(supportedCommands: [String]) -> AppNavigationDelegate? {
            MyAppNavigationDelegate(supportedCommands: supportedCommands)
        }
    }

    class MyAppNavigationDelegate: AppNavigationDelegate {

        let supportedCommands: [String]

        init(supportedCommands: [String]) {
            self.supportedCommands = supportedCommands
        }

        func getSupportedAppNavigationCommands() -> [String] {
            supportedCommands
        }

        func handleAppNavigation(_ appNavigationCommand: String) {
            print("App navigation \(appNavigationCommand) was handled!")
        }
    }
}
