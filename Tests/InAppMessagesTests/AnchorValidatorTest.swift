//
//  AnchorValidatorTest.swift
//  CopilotAPIAccessTests
//
//  Created by Alex Gold on 25/05/2022.
//  Copyright © 2022 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class AnchorValidatorTest: XCTestCase {

    func testFullscreenAnchors() {
        let validator = AnchorValidator()
        let passAnchors = [
            AnchorableViewData(viewId: "1", anchor: .Top(.Below)),
            AnchorableViewData(viewId: "2", anchor: .ID("1", .Above)),
            AnchorableViewData(viewId: "3", anchor: .ID("2", .Above)),
            AnchorableViewData(viewId: "4", anchor: .ID("3", .Above)),
            AnchorableViewData(viewId: "5", anchor: .ID("4", .Above)),
            AnchorableViewData(viewId: "6", anchor: .ID("5", .Above)),
        ]

        let loop = [
            AnchorableViewData(viewId: "1", anchor: .Top(.Above)),
            AnchorableViewData(viewId: "2", anchor: .ID("1", .Above)),
            AnchorableViewData(viewId: "3", anchor: .ID("4", .Above)),
            AnchorableViewData(viewId: "4", anchor: .ID("5", .Above)),
            AnchorableViewData(viewId: "5", anchor: .ID("3", .Above)),
        ]

        /*

										                                             +---+     +----+
										|                                          / | 5 |<--- | 6  |
										|                                        /-  +---+     +----+
										|     +---+    +---+     +---+    +---+ /
										|<--- | 1 |----| 2 |<--- | 3 |    | 4 |<
										|     +---+    +---+     +---+    +---+ <-----       +---+     +---+     +----+
										|                                 |   ^       \----- | 7 |<--- | 8 |<--- | 9  |
										|                                 \    \             +---+     +---+     +----+
										                                   |    \
										                                   |     \-
										                                   \       \
										                        v           |       \
										                                    |        \ +----+
										                                    \          | 10 |
										                                     |         +----+
										                                      v         ^
										                                     +----+  -/
										                                     | 11 |-/
										                                     +----+
										*/

        let loopWithStragglers = [
            AnchorableViewData(viewId: "1", anchor: .Top(.Below)),
            AnchorableViewData(viewId: "2", anchor: .ID("1", .Below)),
            AnchorableViewData(viewId: "3", anchor: .ID("2", .Below)),
            AnchorableViewData(viewId: "4", anchor: .ID("11", .Below)),
            AnchorableViewData(viewId: "5", anchor: .ID("4", .Below)),
            AnchorableViewData(viewId: "6", anchor: .ID("5", .Below)),
            AnchorableViewData(viewId: "7", anchor: .ID("4", .Below)),
            AnchorableViewData(viewId: "8", anchor: .ID("7", .Below)),
            AnchorableViewData(viewId: "9", anchor: .ID("8", .Below)),
            AnchorableViewData(viewId: "10", anchor: .ID("4", .Below)),
            AnchorableViewData(viewId: "11", anchor: .ID("10", .Below)),
        ]

        XCTAssertTrue(validator.validateLoops(anchors: passAnchors))
        XCTAssertFalse(validator.validateLoops(anchors: loopWithStragglers))
        XCTAssertFalse(validator.validateLoops(anchors: loop))
    }

    func testOrderAnchors() {
        let validator = AnchorValidator()

        let anchors = [
            AnchorableViewData(viewId: "1", anchor: .Top(.Below)),
            AnchorableViewData(viewId: "2", anchor: .ID("1", .Below)),
            AnchorableViewData(viewId: "5", anchor: .ID("4", .Below)),
            AnchorableViewData(viewId: "4", anchor: .ID("3", .Below)),
            AnchorableViewData(viewId: "6", anchor: .ID("5", .Below)),
            AnchorableViewData(viewId: "3", anchor: .ID("2", .Below)),
        ]
        let sorted = validator.sortAnchors(anchors: anchors)
        XCTAssertTrue(validator.validateAnchorsInOrder(anchors: sorted))
    }

    func testFullScreenPresentationModels() {
        runViewTypeTest(onJson: "fullScreenPresentationTestJson", willSucceed: { $0?.count == 4 })
    }

    func testPresentationModelFail_NoViewId() { //one of the view types has an empty string for a ViewId
        runViewTypeTest(onJson: "fullScreenPresentationTestJsonFail_NoViewId", willSucceed: { $0 == nil} )
    }

    func testPresentationModelFail_BadAnchor() { //typo in one of the anchors
        runViewTypeTest(onJson: "fullScreenPresentationTestJson_BadAnchor", willSucceed: { $0 == nil} )
    }

    func testPresentationModelFail_DoubleViewId() {
        runViewTypeTest(onJson: "fullScreenPresentationTestJson_DoubleViewId", willSucceed: { $0 == nil} )
    }

    func runViewTypeTest(onJson name: String, willSucceed: ([ViewData]?) -> Bool) {
        let dictionary = loadJsonFileWith(name)
        let messagesDict = dictionary["messages"] as? [[String: Any]]
        messagesDict?.first?.forEach { key, value in
            if key == "presentation" {
                if let viewTypeDict = (value as? [String: Any])!["viewTypes"] as? [[String: Any]] {
                    let viewTypes = FullscreenInappViewMapper().mapToViewData(fromDictionary: viewTypeDict)
                    XCTAssertTrue(willSucceed(viewTypes))
                    return
                }
            }
        }
    }

    override func loadJsonFileWith(_ name: String) -> [String: Any] {
        guard let pathString = Bundle(for: AnchorValidatorTest.self).path(forResource: name, ofType: "json") else {
            fatalError("UnitTestData.json not found")
        }

        guard let jsonString = try? String(contentsOfFile: pathString, encoding: .utf8) else {
            fatalError("Unable to convert UnitTestData.json to String")
        }

        guard let jsonData = jsonString.data(using: .utf8) else {
            fatalError("Unable to convert \(name).json to Data")
        }

        guard let jsonDictionary = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
            fatalError("Unable to convert \(name).json to JSON dictionary")
        }

        return jsonDictionary
    }
}
