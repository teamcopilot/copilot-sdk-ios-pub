//
//  TimeBasedTouchPolicyTests.swift
//  CopilotAPIAccessTests
//
//  Created by Elad on 06/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class TimeBasedTouchPolicyTests: XCTestCase {

	//dictionary example
//    ▿ 0 : 2 elements
//      - key : "_type"
//      - value : TimeBased
//    ▿ 1 : 2 elements
//      - key : "minSecondsBetweenInteractions"
//      - value : 600


	override func setUp() {
		// Put setup code here. This method is called before the invocation of each test method in the class.
	}

	override func tearDown() {
		// Put teardown code here. This method is called after the invocation of each test method in the class.
	}

	func testCanInteractWithUserBeforeMinTimeFromPreviousInteractionPassed() {
		let timeBasedTouchPolicyDictionary: [String : Any] = ["minSecondsBetweenInteractions" : 6 as TimeInterval]
		let timeBasedTouchPolicy = TimeBasedTouchPolicyValidator(withDictionary: timeBasedTouchPolicyDictionary)!

		XCTAssertTrue(timeBasedTouchPolicy.canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint: false))
		sleep(1)

		XCTAssertFalse(timeBasedTouchPolicy.canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint: false))

		sleep(6)

		XCTAssertTrue(timeBasedTouchPolicy.canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint: false))
	}

	func testCanInteractWithUserAfterMinTimeFromPreviousInteractionPassed() {
		let timeBasedTouchPolicyDictionary: [String : Any] = ["minSecondsBetweenInteractions" : 5 as TimeInterval]
		let timeBasedPolicy = TimeBasedTouchPolicyValidator(withDictionary: timeBasedTouchPolicyDictionary)!

		XCTAssertEqual(timeBasedPolicy.canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint: false), true)

		sleep(6)
		XCTAssertEqual(timeBasedPolicy.canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint: false), true)
	}

	func testCanInteractWithUserAfterMinTimeFromPreviousInteractionPassedTwice() {
		let timeBasedTouchPolicyDictionary: [String : Any] = ["minSecondsBetweenInteractions" : 5 as TimeInterval]
		let timeBasedPolicy = TimeBasedTouchPolicyValidator(withDictionary: timeBasedTouchPolicyDictionary)!

		XCTAssertEqual(timeBasedPolicy.canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint: false), true)
		sleep(3)

		XCTAssertEqual(timeBasedPolicy.canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint: false), false)
		sleep(4)

		XCTAssertEqual(timeBasedPolicy.canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint: false), true)

	}

	func testCanInteractWithUserConcurrently() {
		let timeBasedTouchPolicyDictionary: [String: Any] = ["minSecondsBetweenInteractions" : 5 as TimeInterval]
		let timeBasedPolicy = TimeBasedTouchPolicyValidator(withDictionary: timeBasedTouchPolicyDictionary)


		let concurrentQueue = DispatchQueue(label: "concurrent.queue", attributes: .concurrent)
		let expectation = self.expectation(description: "concurrent_CanInteractWithUser")
		let canInteractClosure = { () -> Bool in
			if timeBasedPolicy!.canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint: false) {
				print("canInteract True")
				return true
			} else {
				print("canInteract False")
				return false
			}
		}

		concurrentQueue.async {
			XCTAssertEqual(canInteractClosure(), true)
		}
//        usleep(200)
		concurrentQueue.async {
			XCTAssertEqual(canInteractClosure(), false)
			expectation.fulfill()
		}

		waitForExpectations(timeout: 2, handler: nil)
	}

	func testCanInteractWithUserConcurrentlyForLoop() {
		let timeBasedTouchPolicyDictionary: [String: Any] = ["minSecondsBetweenInteractions" : 5 as TimeInterval]
		let timeBasedPolicy = TimeBasedTouchPolicyValidator(withDictionary: timeBasedTouchPolicyDictionary)

		let concurrentQueue = DispatchQueue(label: "concurrent.queue", attributes: .concurrent)
		let expectation = self.expectation(description: "concurrent_CanInteractWithUserForLoop")

		let canInteractClosure = { () -> Bool in
			if timeBasedPolicy!.canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint: false) {
				print("canInteract True")
				return true
			} else {
				print("canInteract False")
				return false
			}

		}

		var concurrencyCounter = 0
		for i in 0..<63 {
			concurrentQueue.async {
				if canInteractClosure() {
					concurrencyCounter += 1
				}
				if i == 62 { expectation.fulfill() }
			}
		}
		waitForExpectations(timeout: 2, handler: nil)
		XCTAssertEqual(concurrencyCounter, 1)
	}

	func testDifferentTouchpointReceivedFromConfig() {
		var touchpointPolicy: TouchPolicyValidator = TimeBasedTouchPolicyValidator(withDictionary: ["minSecondsBetweenInteractions": 6 as TimeInterval])!
		let newTouchPointPolicyValidator = FakeTouchpointPolicy()

		touchpointPolicy = touchpointPolicy.resolveUpdatedConfiguration(newTouchPointPolicyValidator: newTouchPointPolicyValidator)
		XCTAssertTrue(touchpointPolicy is FakeTouchpointPolicy)
	}
}

class FakeTouchpointPolicy: TouchPolicyValidator {
	func canInteractWithUserAndUpdateInteractedIfNeeded(shouldIgnoreTouchpoint ignoreTouchpoint: Bool) -> Bool {
		true
	}

	func resolveUpdatedConfiguration(newTouchPointPolicyValidator: TouchPolicyValidator) -> TouchPolicyValidator {
		self
	}
}
