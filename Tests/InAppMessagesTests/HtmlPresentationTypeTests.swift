//
//  HtmlPresentationTypeTests.swift
//  CopilotAPIAccessTests
//
//  Created by Elad on 22/03/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class HtmlPresentationTypeTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHtmlCtasAndContentValidation() {
        
        /*
         In this test I've checked those cases:
         1. missmatch cta redirect id value
         2. missmatch cplt value to ctas redirect id
         3. cta with no redirect id, the cplt in content no exist in ctas array
         4. no cta that match cplt value
         5. wrong content key
         6. wrong content value
         */
        
        let htmlDictionary = loadJsonFileWith("HtmlTest")
        let messages: [Dictionary<String, Any>] = htmlDictionary["messages"] as! [Dictionary<String, Any>]
        messages.forEach {
            XCTAssertNil(MessagePresentationMapper.map(withDictionary: $0))
        }
    }
    
    func testHtmlBadContent() {
        
        /*
        In this test I've checked those cases:
        1. wrong content key
        2. wrong content value
        */
        
        let htmlDictionary = loadJsonFileWith("WrongHtmlConent")
        let messages: [Dictionary<String, Any>] = htmlDictionary["messages"] as! [Dictionary<String, Any>]
        messages.forEach {
            XCTAssertNil(MessagePresentationMapper.map(withDictionary: $0))
        }
    }
    
    func testInvalidHtmlCtaDataParser() {
        
        /*
         In this test I've checked those cases:
         1. Wrong type key
         2. Wrong type value
         3. No action key
         4. Wrong redirectId key
         5. Wrong redirectId value
         6. Wrong cta action type key
         */
        
        let wrongCtaDictionary = loadJsonFileWith("WrongCtaHtmlInappModel")
        let messages: [Dictionary<String, Any>] = wrongCtaDictionary["messages"] as! [Dictionary<String, Any>]
        messages.forEach {
            XCTAssertNil(HtmlInAppPresentationModel(withDictionary: $0))
        }
    }
    
    func testHtmlHasAtLeastOneButtonToPresent() {
        
        let htmlWithNoButtonToPresent = loadJsonFileWith("HtmlWithNoButtonsToPresent")
        XCTAssertNil(MessagePresentationMapper.map(withDictionary: htmlWithNoButtonToPresent))
    }
}
