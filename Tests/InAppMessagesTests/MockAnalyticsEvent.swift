//
//  MockAnalyticsEvent.swift
//  CopilotAPIAccess
//
//  Created by Elad on 13/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation
@testable import CopilotAPIAccess

struct MockAnalyticsEvent: AnalyticsEvent {
    var customParams: Dictionary<String, String> = ["mock param key" : "mock param value"]
    
    var eventName: String = "onboarding_started"
    
    var eventOrigin: AnalyticsEventOrigin = .User
}
