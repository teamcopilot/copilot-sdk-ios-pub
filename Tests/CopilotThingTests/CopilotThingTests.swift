//
//  CopilotThingTests.swift
//  CopilotThingTests
//
//  Created by Adaya on 11/12/2017.
//  Copyright © 2017 Falcore. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class CopilotThingTests: XCTestCase {
    
    private let date = Date()
    private var dateTimeInterval: TimeInterval {
        return date.timeIntervalSince1970
    }
    private var dateInSeconds: Double {
        return Double(dateTimeInterval)
    }
    private var dateInMiliseconds: Double {
        return dateInSeconds * 1000
    }
    
    let thingStatusName = "Battery"
    let thingStatusValue = "45"
    
    private var thingReportedStatusDictWithDateInMiliseconds: [String: Any] {
        return ["date": dateInMiliseconds, "name": thingStatusName, "statusValue": thingStatusValue]
    }
    private var thingReportedStatusDictWithDateInSeconds: [String: Any] {
        return ["date": dateInSeconds, "name": thingStatusName, "statusValue": thingStatusValue]
    }
    
    private var statuses: [[String : Any]] {
        return [thingReportedStatusDictWithDateInMiliseconds, thingReportedStatusDictWithDateInSeconds]
    }
    
    private var thingStatusDictWithDateInMiliseconds: [String: Any] {
        return ["lastSeen": dateInMiliseconds, "statuses": statuses]
    }
    private var thingStatusDictWithDateInSeconds: [String: Any] {
        return ["lastSeen": dateInSeconds, "statuses": statuses]
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_thingReportedStatusRecievedWithDateInMiliseconds() {
        if let thingReportedStatusWithDateInMiliseconds = ThingReportedStatus(withDictionary: thingReportedStatusDictWithDateInMiliseconds) {
            XCTAssertEqual(thingReportedStatusWithDateInMiliseconds.time.timeIntervalSince1970, dateTimeInterval, "Should be equal to the recived time from server")
        } else {
            XCTFail()
        }
    }
    
    func test_thingReportedStatusRecievedWithDateInSeconds() {
        if let thingReportedStatusWithDateInSeconds = ThingReportedStatus(withDictionary: thingReportedStatusDictWithDateInSeconds) {
            XCTAssertEqual(thingReportedStatusWithDateInSeconds.time.timeIntervalSince1970, dateTimeInterval, "Should be equal to the recived time from server")
        } else {
            XCTFail()
        }
    }
    
    func test_thingStatusRecievedWithDateInMiliseconds() {
        if let ThingStatusWithDateInMiliseconds = ThingStatus(withDictionary: thingStatusDictWithDateInMiliseconds) {
            XCTAssertEqual(ThingStatusWithDateInMiliseconds.lastSeen.timeIntervalSince1970, dateTimeInterval, "Should be equal to the recived time from server")
            for thingReportedStatus in ThingStatusWithDateInMiliseconds.reportedStatuses {
                XCTAssertEqual(thingReportedStatus.time.timeIntervalSince1970, dateTimeInterval, "Should be equal to the recived time from server")
            }
        } else {
            XCTFail()
        }
    }
    
    func test_thingStatusRecievedWithDateInSeconds() {
        if let ThingStatusWithDateInSeconds = ThingStatus(withDictionary: thingStatusDictWithDateInSeconds) {
            XCTAssertEqual(ThingStatusWithDateInSeconds.lastSeen.timeIntervalSince1970, dateTimeInterval, "Should be equal to the recived time from server")
            for thingReportedStatus in ThingStatusWithDateInSeconds.reportedStatuses {
                XCTAssertEqual(thingReportedStatus.time.timeIntervalSince1970, dateTimeInterval, "Should be equal to the recived time from server")
            }
        } else {
            XCTFail()
        }
    }
    
    func test_sentThingReportedStatusToServer() {
        let thingReportedStatus = ThingReportedStatus(time: date, name: thingStatusName, value: thingStatusValue)
        if let timeToServer = thingReportedStatus.toDictionary()["date"] as? Double {
            XCTAssertEqual(timeToServer, dateInMiliseconds, "Thing status time (which sent to the server) should be in miliseconds and equal to the object time")
        } else {
            XCTFail()
        }
    }
    
    func test_sentThingStatusSentToServer() {
        let thingReportedStatus = ThingReportedStatus(time: date, name: thingStatusName, value: thingStatusValue)
        let thingStatus = ThingStatus(lastSeen: date, reportedStatuses: [thingReportedStatus])
        if let timeToServer = thingStatus.toDictionary()["lastSeen"] as? Double {
            XCTAssertEqual(timeToServer, dateInMiliseconds, "Thing status time (which sent to the server) should be in miliseconds and equal to the object time")
        } else {
            XCTFail()
        }
    }
    
}
