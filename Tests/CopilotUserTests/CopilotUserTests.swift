//
//  CopilotUserTests.swift
//  CopilotUserTests
//
//  Created by Adaya on 11/12/2017.
//  Copyright © 2017 Falcore. All rights reserved.
//

import XCTest

class CopilotUserTests: XCTestCase {

    func testLoadVersion() {
        let version = VersionResolver().copilotVersion
        print("Version: \(version)")
        XCTAssertNotEqual(version, "Unknown")
    }
}
