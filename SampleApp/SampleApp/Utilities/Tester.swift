//
//  Tester.swift
//  SampleApp
//
//  Created by Adaya on 20/02/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess


public enum TestError : Error
{
    case e(message : String)
}

extension TestError : LocalizedError{
    public var errorDescription: String?{
        switch self {
        case .e(let message):
            return message
        }
    }
}

public class Tester{

    let EMAIL = "adaya+4@zemingo.com"
    let PWD = "aA123456"
    let FN = "First"
    let LN = "Last"
    let LN2 = "Last2"
    let CONSENTKEY = "ckey"
    let THINGID = "pid"
    let EMPTYID = ""
    let FW = "firmware1"
    let FW2 = "firmware2"
    let MODEL = "model1"


    var currentTests : [Test] = []
    var finalClosure :((CopilotAPIAccess.Response<Void, TestError>) -> Swift.Void)?

    func canRun(_ closure: @escaping (CopilotAPIAccess.Response<Void, TestError>)-> Swift.Void) -> Bool{
        if currentTests.count > 0{
            closure(.failure(error: .e(message: "Test currently running")))
            return false
        }
        return true
    }

    func runXErrors(_ closure: @escaping (CopilotAPIAccess.Response<Void, TestError>) -> Swift.Void){
        if !canRun(closure){
            return;
        }
        finalClosure = closure
        currentTests = [
            ] as [Test]
        let accumulatedErrors : [TestError] = []
        runNextTest(0, accumulatedErrors, closure)
    }

    func runConnectionErrors(_ closure: @escaping (CopilotAPIAccess.Response<Void, TestError>) -> Swift.Void){
        if !canRun(closure){
            return;
        }
        finalClosure = closure
        currentTests = [
            TestOne<Thing, AssociateThingError>("Associate ",
                                                Copilot.instance.manage.sphere.thing.associateThing(withPhysicalId: EMPTYID, firmware: FW, model: MODEL),
                                                AssociateThingError.connectivityError(debugMessage: "any"),
                                                {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            , TestOne<CanAssociateResponse, CanAssociateThingError>("Can associate",
                                                                    Copilot.instance.manage.sphere.thing.checkIfCanAssociate(withPhysicalId: EMPTYID),
                                                                    .connectivityError(debugMessage: "any"),
                                                                    {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            , TestOne<Thing, UpdateThingError>("Update thing ",
                                               Copilot.instance.manage.sphere.thing.updateThing(withPhysicalId: THINGID).with(name: "name"),
                                               UpdateThingError.connectivityError(debugMessage: "any"),
                                               {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            , TestOne<Thing, FetchSingleThingError>("Fetch single thing ",
                                                    Copilot.instance.manage.sphere.thing.fetchThing(withPhysicalId: THINGID),
                                                    FetchSingleThingError.connectivityError(debugMessage: "any"),
                                                    {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            , TestOne<[Thing], FetchThingsError>("Fetch things",
                                                    Copilot.instance.manage.sphere.thing.fetchThings(),
                                                    .connectivityError(debugMessage: "any"),
                                                    {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            , TestOne<Void, DisassociateThingError>("dissociate thing doesnt exist",
                                                    Copilot.instance.manage.sphere.thing.disassociateThing(withPhysicalId : "any"),
                                                    .connectivityError(debugMessage: "any"),
                                                    {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            ,TestOne<UserMe, FetchMeError>("fetch me ",
                                                 Copilot.instance.manage.sphere.user.fetchMe(),
                                                 .connectivityError(debugMessage: "any"),
                                                 {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            , TestOne<Void, ElevateAnonymousUserError>("Elevate user already exists",
                                                   Copilot.instance.manage.sphere.user.elevate().with(email: EMAIL, password: PWD, firstname: FN, lastname: LN),
                                                   .connectivityError(debugMessage: "any"),
                                                   {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            ,TestOne<Void, ResetPasswordError>("Reset password",
                                               Copilot.instance.manage.sphere.user.resetPassword(for: ""),
                                               .connectivityError(debugMessage: "any"),
                                               {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            ,TestOne<UserMe, UpdateUserDetailsError>("Update user invalid error",
                                                   Copilot.instance.manage.sphere.user.updateMe().with(firstname: ""),
                                                   .connectivityError(debugMessage: "any"),
                                                   {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            ,TestOne<Void, ApproveTermsOfUseError>("Terms of use",
                                                   Copilot.instance.manage.sphere.user.updateMe().approveTermsOfUse(forVersion: "1"),
                                                   .connectivityError(debugMessage: "any"),
                                                   {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            ,TestOne<Void, UpdateUserConsentError>("Update user consent invalid error",
                                                   Copilot.instance.manage.sphere.user.updateMe().withCustomConsent("", value: true),
                                                   .connectivityError(debugMessage: "any"),
                                                   {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            ,TestOne<Void, ChangePasswordError>("ChangePassword",
                                                Copilot.instance.manage.sphere.user.updateMe().withNewPassword(PWD+"hh", verifyWithOldPassword: "wrongpassword"),
                                                .connectivityError(debugMessage: "any"),
                                                {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            ,TestOne<Void, SendVerificationEmailError>("Send verification email",
                                                Copilot.instance.manage.sphere.user.sendVerificationEmail(),
                                                .connectivityError(debugMessage: "any"),
                                                {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            , TestOne<Void, SignupError>("Signup ",
                                         Copilot.instance.manage.sphere.auth.signup().withCopilotAnalysisConsent(true).with(email: "any", password: PWD, firstname: "", lastname: LN),
                                         SignupError.connectivityError(debugMessage: "any"),
                                         {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            , TestOne<Void, SignupAnonymouslyError>("Signup annonymously",
                                         Copilot.instance.manage.sphere.auth.signup().withCopilotAnalysisConsent(true).anonymously,
                                         SignupAnonymouslyError.connectivityError(debugMessage: "any"),
                                         {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            , TestOne<Void, LoginError>("Login credentials",
                                        Copilot.instance.manage.sphere.auth.login().with(email: "any", password: PWD),
                                        LoginError.connectivityError(debugMessage: "any"),
                                        {(e) in switch e{ case .connectivityError(_): return true default: return false}})
            ] as [Test]
        let accumulatedErrors : [TestError] = []
        runNextTest(0, accumulatedErrors, closure)
    }

    func runThingErrors(_ closure: @escaping (CopilotAPIAccess.Response<Void, TestError>) -> Swift.Void){
        if !canRun(closure){
            return;
        }
        finalClosure = closure
        currentTests = [
            TestOne<Thing, AssociateThingError>("Associate invalid",
                                                Copilot.instance.manage.sphere.thing.associateThing(withPhysicalId: EMPTYID, firmware: FW, model: MODEL),
                                                 .invalidParameters(debugMessage: "any"),
                                                 {(e) in switch e{ case .invalidParameters(_): return true default: return false}})
            , TestOne<Thing, AssociateThingError>("Associated already",
                                                    Copilot.instance.manage.sphere.thing.associateThing(withPhysicalId: THINGID, firmware: FW, model: MODEL),
                                                   .thingAlreadyAssociated(debugMessage: "any"),
                                                   {(e) in switch e{ case .thingAlreadyAssociated(_): return true default: return false}})
            , TestOne<CanAssociateResponse, CanAssociateThingError>("Can associate invalid",
                                                  Copilot.instance.manage.sphere.thing.checkIfCanAssociate(withPhysicalId: EMPTYID),
                                                  .invalidParameters(debugMessage: "any"),
                                                  {(e) in switch e{ case .invalidParameters(_): return true default: return false}})
            , TestOne<Thing, UpdateThingError>("Update thing not associated",
                                                                    Copilot.instance.manage.sphere.thing.updateThing(withPhysicalId: THINGID).with(name: "name"),
                                                                    UpdateThingError.thingIsNotAssociated(debugMessage: "any"),
                                                                    {(e) in switch e{ case .thingIsNotAssociated(_): return true default: return false}})
            , TestOne<Thing, UpdateThingError>("Update thing doesnt exist",
                                               Copilot.instance.manage.sphere.thing.updateThing(withPhysicalId: "any").with(name: "name"),
                                               UpdateThingError.thingNotFound(debugMessage: "any"),
                                               {(e) in switch e{ case .thingNotFound(_): return true default: return false}})
            , TestOne<Thing, FetchSingleThingError>("Fetch thing invalid",
                                               Copilot.instance.manage.sphere.thing.fetchThing(withPhysicalId: EMPTYID),
                                               FetchSingleThingError.invalidParameters(debugMessage: "any"),
                                               {(e) in switch e{ case .invalidParameters(_): return true default: return false}})
            , TestOne<Thing, FetchSingleThingError>("Fetch thing not assocaited",
                                                    Copilot.instance.manage.sphere.thing.fetchThing(withPhysicalId: THINGID),
                                                    FetchSingleThingError.thingIsNotAssociated(debugMessage: "any"),
                                                    {(e) in switch e{ case .thingIsNotAssociated(_): return true default: return false}})
            , TestOne<Thing, FetchSingleThingError>("Fetch thing doesnt exist",
                                                    Copilot.instance.manage.sphere.thing.fetchThing(withPhysicalId: "any"),
                                                    FetchSingleThingError.thingNotFound(debugMessage: "any"),
                                                    {(e) in switch e{ case .thingNotFound(_): return true default: return false}})
            , TestOne<Void, DisassociateThingError>("dissociate thing doesnt exist",
                                                    Copilot.instance.manage.sphere.thing.disassociateThing(withPhysicalId : "any"),
                                                    DisassociateThingError.thingNotFound(debugMessage: "any"),
                                                    {(e) in switch e{ case .thingNotFound(_): return true default: return false}})
            , TestOne<Void, DisassociateThingError>("dissociate thing invalid",
                                                     Copilot.instance.manage.sphere.thing.disassociateThing(withPhysicalId:  EMPTYID),
                                                     DisassociateThingError.invalidParameters(debugMessage: "any"),
                                                     {(e) in switch e{ case .invalidParameters(_): return true default: return false}})

            ] as [Test]
        let accumulatedErrors : [TestError] = []
        runNextTest(0, accumulatedErrors, closure)
    }



    func runUserErrors(_ asAnonymous: Bool, _ closure: @escaping (CopilotAPIAccess.Response<Void, TestError>) -> Swift.Void){
        if !canRun(closure){
            return;
        }
        finalClosure = closure
        // FetchMe - no specific errors
//        ApproveTermsOfUseError - cannot check invalid
        currentTests = [
           TestOne<Void, ElevateAnonymousUserError>("Elevate invalid",
                                                Copilot.instance.manage.sphere.user.elevate().with(email: EMAIL, password: PWD, firstname: "", lastname: LN),
                                         .invalidParameters(debugMessage: "any"),
                                         {(e) in switch e{ case .invalidParameters(_): return true default: return false}})
            , TestOne<Void, ElevateAnonymousUserError>("Elevate user already exists",
                                         Copilot.instance.manage.sphere.user.elevate().with(email: EMAIL, password: PWD, firstname: FN, lastname: LN),
                                         .userAlreadyExists(debugMessage: "any"),
                                         {(e) in switch e{ case .userAlreadyExists(_): return true default: return false}})
            ,TestOne<Void, ElevateAnonymousUserError>("Elevate Email invalid",
                                                 Copilot.instance.manage.sphere.user.elevate().with(email: "any", password: PWD, firstname: FN, lastname: LN),
                                                 .invalidEmail(debugMessage: "any"),
                                                 {(e) in switch e{ case .invalidEmail(_): return true default: return false}})
            ,TestOne<Void, ResetPasswordError>("Reset password invalid params",
                                                   Copilot.instance.manage.sphere.user.resetPassword(for: ""),
                                                   .invalidParameters(debugMessage: "any"),
                                                   {(e) in switch e{ case .invalidParameters(_): return true default: return false}})
            ,TestOne<Void, ResetPasswordError>("Reset password - not verified",
                                               Copilot.instance.manage.sphere.user.resetPassword(for: "adaya+5@zemingo.com"),// todo - change to "+donotverify"
                                               .emailIsNotVerified(debugMessage: "any"),
                                               {(e) in switch e{ case .emailIsNotVerified(_): return true default: return false}})
            ,TestOne<UserMe, UpdateUserDetailsError>("Update user invalid error",
                                               Copilot.instance.manage.sphere.user.updateMe().with(firstname: ""),
                                               .invalidParameters(debugMessage: "any"),
                                               {(e) in switch e{ case .invalidParameters(_): return true default: return false}})
            ,TestOne<Void, UpdateUserConsentError>("Update user consent invalid error",
                                                   Copilot.instance.manage.sphere.user.updateMe().withCustomConsent("", value: true),
                                                   .invalidParameters(debugMessage: "any"),
                                                   {(e) in switch e{ case .invalidParameters(_): return true default: return false}})
            ,TestOne<Void, ChangePasswordError>("ChangePassword invalid",
                                                Copilot.instance.manage.sphere.user.updateMe().withNewPassword("", verifyWithOldPassword: PWD),
                                                .invalidParameters(debugMessage: "any"),
                                                {(e) in switch e{ case .invalidParameters(_): return true default: return false}})

//            TODO - complex tests
//            ,TestOne<Void, SendVerificationEmailError>("SendVerificationEmail",
//                                                       Copilot.instance.user.sendVerificationEmail(),
//        case retryRequired(retryInSeconds: Int, debugMessage: String)
//        case userAlreadyVerified(debugMessage: String)
//                                                       {(e) in switch e{ case .connectivityError(_): return true default: return false}})
//            ,TestOne<Void, UpdateDeviceDetailsError>("Update user consent invalid error",
//                                                   Copilot.instance.user.updateMe().withPushToken(pnsToken:, isSandbox: <#T##Bool#>),
//                                                   .invalidParameters(debugMessage: "any"),
//                                                   {(e) in switch e{ case .invalidParameters(_): return true default: return false}})
            ] as [Test]
        if asAnonymous{

            currentTests.append( TestOne<Void, ElevateAnonymousUserError>("Elevate pwd",
                                                                      Copilot.instance.manage.sphere.user.elevate().with(email: "a@b.c", password: "123", firstname: FN, lastname: LN),
                                                                      .passwordPolicyViolation(debugMessage: "any"),
                                                                      {(e) in switch e{ case .passwordPolicyViolation(_): return true default: return false}})
            )
            currentTests.append(TestOne<Void, ChangePasswordError>("ChangePassword Fail on Anonymous (policy)",
                                                                   Copilot.instance.manage.sphere.user.updateMe().withNewPassword("pwd1", verifyWithOldPassword: PWD),
                                                                   .cannotChangePasswordToAnonymousUser(debugMessage: "any"),
                                                                   {(e) in switch e{ case .cannotChangePasswordToAnonymousUser(_): return true default: return false}})
            )
            currentTests.append(TestOne<Void, ChangePasswordError>("ChangePassword Fail on Anonymous (credentials)",
                                                                   Copilot.instance.manage.sphere.user.updateMe().withNewPassword(PWD+"hh", verifyWithOldPassword: "wrongpassword"),
                                                                   .cannotChangePasswordToAnonymousUser(debugMessage: "any"),
                                                                   {(e) in switch e{ case .cannotChangePasswordToAnonymousUser(_): return true default: return false}})
            )
        }
        else{
            currentTests.append(TestOne<Void, ElevateAnonymousUserError>("Elevate cannot elavate non",
                                                 Copilot.instance.manage.sphere.user.elevate().with(email: "a@b.c", password: "123", firstname: FN, lastname: LN),
                                                 .cannotElevateANonAnonymousUser(debugMessage: "any"),
                                                 {(e) in switch e{ case .cannotElevateANonAnonymousUser(_): return true default: return false}})
            )
            currentTests.append(TestOne<Void, ChangePasswordError>("ChangePassword policy",
                                            Copilot.instance.manage.sphere.user.updateMe().withNewPassword("pwd1", verifyWithOldPassword: PWD),
                                            .passwordPolicyViolation(debugMessage: "any"),
                                            {(e) in switch e{ case .passwordPolicyViolation(_): return true default: return false}})
            )
            currentTests.append(TestOne<Void, ChangePasswordError>("ChangePassword violation",
                                            Copilot.instance.manage.sphere.user.updateMe().withNewPassword(PWD+"hh", verifyWithOldPassword: "wrongpassword"),
                                            .invalidCredentials(debugMessage: "any"),
                                            {(e) in switch e{ case .invalidCredentials(_): return true default: return false}})
            )
        }
        let accumulatedErrors : [TestError] = []
        runNextTest(0, accumulatedErrors, closure)
    }

    func runAuthErrors(_ closure: @escaping (CopilotAPIAccess.Response<Void, TestError>) -> Swift.Void){
        if !canRun(closure){
            return;
        }
        finalClosure = closure
        currentTests = [
            TestOne<Void, SignupError>("Signup pwd",
                                                 Copilot.instance.manage.sphere.auth.signup().withCopilotAnalysisConsent(true).with(email: "a@b.c", password: "123", firstname: FN, lastname: LN),
                                                 SignupError.passwordPolicyViolation(debugMessage: "any"),
                                                 {(e) in switch e{ case .passwordPolicyViolation(_): return true default: return false}})
            , TestOne<Void, SignupError>("Signup invalid",
                                       Copilot.instance.manage.sphere.auth.signup().withCopilotAnalysisConsent(true).with(email: EMAIL, password: PWD, firstname: "", lastname: LN),
                                       SignupError.invalidParameters(debugMessage: "any"),
                                       {(e) in switch e{ case .invalidParameters(_): return true default: return false}})
            , TestOne<Void, SignupError>("Signup email invalid",
                                         Copilot.instance.manage.sphere.auth.signup().withCopilotAnalysisConsent(true).with(email: "any", password: PWD, firstname: FN, lastname: LN),
                                         SignupError.invalidEmail(debugMessage: "any"),
                                         {(e) in switch e{ case .invalidEmail(_): return true default: return false}})
            , TestOne<Void, SignupError>("Signup user already exists",
                                         Copilot.instance.manage.sphere.auth.signup().withCopilotAnalysisConsent(true).with(email: EMAIL, password: PWD, firstname: FN, lastname: LN),
                                         SignupError.userAlreadyExists(debugMessage: "any"),
                                         {(e) in switch e{ case .userAlreadyExists(_): return true default: return false}})
            , TestOne<Void, LoginError>("Login credentials",
                                         Copilot.instance.manage.sphere.auth.login().with(email: "any", password: PWD),
                                         LoginError.unauthorized(debugMessage: "any"),
                                         {(e) in switch e{ case .unauthorized(_): return true default: return false}})
            , TestOne<Void, LoginError>("Login invalid",
                                        Copilot.instance.manage.sphere.auth.login().with(email: "", password: PWD),
                                        LoginError.invalidParameters(debugMessage: "any"),
                                        {(e) in switch e{ case .invalidParameters(_): return true default: return false}})
        ] as [Test]
        let accumulatedErrors : [TestError] = []
        runNextTest(0, accumulatedErrors, closure)
    }

    func runWithoutLogin(_ closure: @escaping (CopilotAPIAccess.Response<Void, TestError>) -> Swift.Void){
        if !canRun(closure){
            return;
        }
        finalClosure = closure
        currentTests = [
            TestOne<Void, ElevateAnonymousUserError>("Elevate",
                                                      Copilot.instance.manage.sphere.user.elevate().with(email: EMAIL, password: PWD, firstname: FN, lastname: LN),
                                                      .requiresRelogin(debugMessage: "any"),
                                                      {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<UserMe, UpdateUserDetailsError>("Update Me",
                                                  Copilot.instance.manage.sphere.user.updateMe().with(lastname: LN2),
                                                  .requiresRelogin(debugMessage: "any"),
                                                  {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<UserMe, FetchMeError>("FetchMe",
                                                  Copilot.instance.manage.sphere.user.fetchMe(),
                                                  .requiresRelogin(debugMessage: "any"),
                                                  {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<Void, ApproveTermsOfUseError>("ApproveTermsOfUse",
                                                  Copilot.instance.manage.sphere.user.updateMe().approveTermsOfUse(forVersion: "1"),
                                                  .requiresRelogin(debugMessage: "any"),
                                                  {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<Void, UpdateUserConsentError>("UpdateUserConsentError",
                                                  Copilot.instance.manage.sphere.user.updateMe().withCustomConsent(CONSENTKEY, value: true),
                                                  .requiresRelogin(debugMessage: "any"),
                                                  {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<Void, ChangePasswordError>("ChangePassword",
                                                   Copilot.instance.manage.sphere.user.updateMe().withNewPassword("pwd1", verifyWithOldPassword: "pwd2"),
                                                   .requiresRelogin(debugMessage: "any"),
                                                   {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<Void, SendVerificationEmailError>("Send verification email",
                                                       Copilot.instance.manage.sphere.user.sendVerificationEmail(),
                                                       .requiresRelogin(debugMessage: "any"),
                                                       {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<CanAssociateResponse, CanAssociateThingError>("CanAssociate",
                                                   Copilot.instance.manage.sphere.thing.checkIfCanAssociate(withPhysicalId: THINGID),
                                                   .requiresRelogin(debugMessage: "any"),
                                                   {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<Thing, AssociateThingError>("Associate",
                                                Copilot.instance.manage.sphere.thing.associateThing(withPhysicalId: EMPTYID, firmware: FW, model: MODEL)
                                                    ,
                                                   .requiresRelogin(debugMessage: "any"),
                                                   {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<Thing, UpdateThingError>("UpdateThing",
                                                   Copilot.instance.manage.sphere.thing.updateThing(withPhysicalId: THINGID).with(firmware: FW2),
                                                   .requiresRelogin(debugMessage: "any"),
                                                   {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<Thing, FetchSingleThingError>("Fetch Thing",
                                                   Copilot.instance.manage.sphere.thing.fetchThing(withPhysicalId: THINGID),
                                                   .requiresRelogin(debugMessage: "any"),
                                                   {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<[Thing], FetchThingsError>("Things",
                                                    Copilot.instance.manage.sphere.thing.fetchThings(),
                                                   .requiresRelogin(debugMessage: "any"),
                                                   {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ,TestOne<Void, DisassociateThingError>("Dissacoaitate",
                                                   Copilot.instance.manage.sphere.thing.disassociateThing(withPhysicalId: THINGID),
                                                   .requiresRelogin(debugMessage: "any"),
                                                   {(e) in switch e{ case .requiresRelogin(_): return true default: return false}})
            ] as [Test]

        let accumulatedErrors : [TestError] = []
        runNextTest(0, accumulatedErrors, closure)
    }

    func runNextTest(_ index: Int, _ accumulatedErrors : [TestError], _ closure: @escaping (CopilotAPIAccess.Response<Void, TestError>) -> Swift.Void){
        if index >= currentTests.count{
            if accumulatedErrors.count == 0{
                closure(.success(()))
            }
            else{
                ZLogManagerWrapper.sharedInstance.logInfo(message: "There where \(accumulatedErrors.count) errors out of \(currentTests.count) tests:")
                for testError in accumulatedErrors{
                    ZLogManagerWrapper.sharedInstance.logInfo(message: testError.localizedDescription)
                }
                closure(.failure(error: .e(message: "There where \(accumulatedErrors.count) errors. See the log please")))
            }
            finalClosure = nil
            currentTests = []
        }
        else{
            let currentTest = currentTests[index]
            ZLogManagerWrapper.sharedInstance.logInfo(message: "running test [\(index)] out of \(currentTests.count) - \(currentTest.getName())")
            currentTest.run { (response) in
                ZLogManagerWrapper.sharedInstance.logInfo(message: "Completed test [\(index)] out of \(self.currentTests.count) - \(currentTest.getName()) with response \(response)")
                var currentErrors = accumulatedErrors
                switch response{
                case .success:
                    break
                case .failure(let error):
                    currentErrors.append(error)
                }
                self.runNextTest(index+1, currentErrors, closure)
            }
        }
    }
}

public protocol Test{
     func getName() -> String
     func run( _ closure: @escaping (CopilotAPIAccess.Response<Void, TestError>)-> Swift.Void)
}

public class TestOne<T, E: Error> : Test{

    let expectedError: E?
    let builder:RequestBuilder<T,E>
    let isExpectedErrorClosure: ((E) -> Bool)?
    let name : String

    init( _ testName: String, _ reqbuilder: RequestBuilder<T,E>, _ expected: E, _  closure: @escaping ((E) -> Bool)){
        self.builder = reqbuilder
        self.name = testName
        self.isExpectedErrorClosure = closure
        self.expectedError = expected
    }

    init( _ testName: String, _ reqbuilder: RequestBuilder<T,E>){
        self.builder = reqbuilder
        self.name = testName
        self.isExpectedErrorClosure = nil
        self.expectedError = nil
    }

    public func getName() -> String {
        return name
    }

    func isExpectedError(_ error: E) -> Bool{
        if let checker = isExpectedErrorClosure{
            return checker(error)
        }
        else{
            return false
        }
    }


    public func run( _ closure: @escaping (CopilotAPIAccess.Response<Void, TestError>)-> Swift.Void){
        builder.build().execute { (response) in
            ZLogManagerWrapper.sharedInstance.logInfo(message: "Execution completed for test \(self.getName())")
            var testError: TestError?
            switch (response){

            case .success(_):
                if let expected = self.expectedError{
                    testError = .e(message: "[\(self.getName())] Received success but expceted error \(expected)")
                }
            case .failure(let error):
                if let expected = self.expectedError{
                    // check equality
                    if !self.isExpectedError(error){
                        testError = .e(message: "[\(self.getName())] Received \(error) but expceted error \(expected)")
                    }
                }
                else{
                    testError = .e(message: "[\(self.getName())] Received \(error) but expceted success")
                }
            }

            if let finalTestError = testError{
                closure(.failure(error: finalTestError))
            }
            else{
                closure(.success(()))
            }
        }
    }
}

