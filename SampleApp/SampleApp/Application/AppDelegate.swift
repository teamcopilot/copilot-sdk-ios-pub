//
//  AppDelegate.swift
//  SampleApp
//
//  Created by Yulia Felberg on 11/10/2017.
//  Copyright © 2017 Zemingo. All rights reserved.
//

import UIKit
import UserNotifications
import CopilotAPIAccess
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        if Bundle.main.manageType == .YourOwn {
            AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(true)
        }
        
        // Copilot configuration
        let firebaseProvider = FirebaseAnalyticsProvider() // Based on the actual target
        let eventLogProvider = AppLogEventProvider()
//        let tuyaLogProvider = SimpleBaseEventLogProvider(externalEventLogger: TuyaEventLogger())
        Copilot.setup(analyticsProviders: [firebaseProvider, eventLogProvider])
            .registerAppNavigationDelegate(self)
			.enableDebugLogs()

        if Bundle.main.manageType == .YourOwn {
            let copilotTokenProvider = AuthenticationServiceInteraction()
            Copilot.instance.manage.yourOwn.auth.setCopilotTokenProvider(copilotTokenProvider)
        }

        //Configure messaging
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
        }
        
        Messaging.messaging().delegate = self
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}
//class TuyaEventLogger: ExternalEventLogger{
//    func logCustomEvent(userId: String?, eventName: String, parameters: Dictionary<String, String>){
//        print("Event logged by Tuya userId = \(userId), eventName = \(eventName), parametes = \(parameters)")
//    }
//}
    
//MARK: - Notifications
extension AppDelegate {
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        ZLogManagerWrapper.sharedInstance.logDebug(message: "APNS: Failed to register for remote notifications")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        ZLogManagerWrapper.sharedInstance.logDebug(message: "APNS: Got token for notifications")
//        Copilot
//            .instance
//            .user
//            .updateMe()
//            .withPushToken(pnsToken: deviceToken, isSandbox: true)
//            .build()
//            .execute { (response) in
//                switch response  {
//                    case .success:
//                        ZLogManagerWrapper.sharedInstance.logInfo(message: "success in updateDevice")
//                    case .failure(let error):
//                        switch error {
//                        case .invalidParameters(_):
//                            fallthrough
//                        case .requiresRelogin:
//                            fallthrough
//                        case .generalError(_):
//                            fallthrough
//                        case .connectivityError(_):
//                            ZLogManagerWrapper.sharedInstance.logDebug(message: error.localizedDescription)
//                    }
//                }
//        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        ZLogManagerWrapper.sharedInstance.logDebug(message: "APNS: Got message(didReceiveRemoteNotification): \(userInfo)")
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        ZLogManagerWrapper.sharedInstance.logDebug(message: "APNS: Got message(didReceiveRemoteNotification No completion): \(userInfo)")
    }
    
}

//MARK: - UNUserNotificationCenterDelegate
extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        ZLogManagerWrapper.sharedInstance.logDebug(message: "APNS: Got message(userNotificationCenter): \(response.notification.request.content.userInfo)")
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        ZLogManagerWrapper.sharedInstance.logDebug(message: "APNS: will present: \(notification.request.content.userInfo)")
        
        completionHandler([.alert, .badge, .sound]);
    }
}

//MARK: - MessagingDelegate
extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        ZLogManagerWrapper.sharedInstance.logDebug(message: "APNS: Got FCM message: \(remoteMessage)")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        ZLogManagerWrapper.sharedInstance.logDebug(message: "APNS: Got FCM token: \(fcmToken)")
    }
}

extension AppDelegate: AppNavigationDelegate {

    func getSupportedAppNavigationCommands() -> [String] {
        ["alert", "navigation","openMyself"]
    }

    func handleAppNavigation(_ appNavigationCommand: String) {
        switch appNavigationCommand {
        case "alert":
            var topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
            topWindow?.rootViewController = UIViewController()
            topWindow?.windowLevel = UIWindow.Level.alert + 1
            
            let alert = UIAlertController(title: "App Navigation", message: "App navigation action with value of \"\(appNavigationCommand)\" has retrieved", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel) { _ in
                topWindow?.isHidden = true
                topWindow = nil
            })
            
            topWindow?.makeKeyAndVisible()
            topWindow?.rootViewController?.present(alert, animated: true, completion: nil)

        case "navigation":
            if let vc = ViewControllersFactory.createControllerWithType(.loginWithEmailAndPassword) {
                let navController = UIApplication.shared.windows.first?.rootViewController
                (navController as? UINavigationController)?.pushViewController(vc, animated: true)
            }

        case "openMyself":
            if let vc = ViewControllersFactory.createControllerWithType(.loginWithEmailAndPassword) {
                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let mainViewController : MainViewController = mainStoryboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                let navController = UINavigationController.init(rootViewController: mainViewController)
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = navController
                self.window?.makeKeyAndVisible()
                navController.pushViewController(vc, animated: true)
            }

        default:
            ZLogManagerWrapper.sharedInstance.logInfo(message: "unknown app navigation url value retrieved")
        }
    }
}
