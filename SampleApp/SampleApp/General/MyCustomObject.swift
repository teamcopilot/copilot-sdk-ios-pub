//
//  MyCustomObject.swift
//  SampleApp
//
//  Created by Revital Pisman on 16/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct MyCustomObject: Codable {
    let myCustomString: String
    let myCustomInt: Int
    let myCustomBoolean: Bool
    
    enum CodingKeys: String, CodingKey {
        case myCustomString = "MY_CUSTOM_STRING_KEY"
        case myCustomInt = "MY_CUSTOM_INT_KEY"
        case myCustomBoolean = "MY_CUSTOM_BOOLEAN_KEY"
    }
}

extension MyCustomObject {
    
    func toString() -> String {
        return "MyCustomObject { " +
            "myCustomString = " + myCustomString +
            ", myCustomInt = " + String(myCustomInt) +
            ", myCustomBoolean = " + String(myCustomBoolean) + " }"
    }
}
