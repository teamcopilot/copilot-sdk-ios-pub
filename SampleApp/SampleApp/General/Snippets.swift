//
//  Snippets.swift
//  SampleAppDevIOTCO
//
//  Created by Revital Pisman on 01/05/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess
import Firebase
import Moya


func Snippets() {
    
    // - MARK: Application
    
    // MARK: - Check App Version
    Copilot.instance
        .manage
        .sphere
        .app
        .checkAppVersionStatus()
        .build()
        .execute { (result) in
            switch result {
            case .success(let appVersionStatus):
                switch(appVersionStatus.versionStatus) {
                case .ok:
                    break
                case .notSupported:
                    break
                case .newVersionRecommended:
                    break
                }
                
            case .failure(error: let checkAppVersionError):
                switch(checkAppVersionError) {
                case .appVersionBadFormat(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .generalError(let debugMessage):
                    break
                }
            }
    }
    
    // MARK: Configuration
    Copilot.instance
        .manage
        .sphere
        .app
        .fetchConfig()
        .build()
        .execute { (response) in
            switch response {
            case .success(let configuration):
                break
                
            case .failure(error: let fetchConfigurationError):
                switch(fetchConfigurationError) {
                case .connectivityError(let debugMessage):
                    break
                case .generalError(let debugMessage):
                    break
                }
            }
    }
    
    // MARK: Password policy
    Copilot.instance
        .manage
        .sphere
        .app
        .fetchPasswordPolicyConfig()
        .build()
        .execute { (response) in
            switch response {
            case .success(let rules):
                break
                
            case .failure(error: let fetchPasswordPolicyError):
                switch(fetchPasswordPolicyError) {
                case .connectivityError(let debugMessage):
                    break
                case .generalError(let debugMessage):
                    break
                }
            }
    }
    
    // MARK: - Authentication
    
    Copilot.instance
        .manage
        .sphere
        .auth
        .signup()
        .withCopilotAnalysisConsent(true)
        .withCustomConsent("analyseUseOfXRayVision", value: true)
        .with(email: "ck@dailyplanet.com", password: "Superman1234", firstname: "Clark", lastname: "Kent")
        .build()
        .execute { (response) in
            switch response {
            case .success:
                break
                
            case .failure(error: let registerError):
                switch(registerError) {
                case .connectivityError(let debugMessage):
                    break
                case .generalError(let debugMessage):
                    break
                case .userAlreadyExists(let debugMessage):
                    break
                case .passwordPolicyViolation(let debugMessage):
                    break
                case .invalidApplicationId(let debugMessage):
                    break
                case .invalidEmail(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                }
            }
    }
    
    Copilot.instance
        .manage
        .sphere
        .auth
        .signup()
        .withNoGDPRConsentRequired
        .anonymously
        .build()
        .execute { (response) in
            switch response {
            case .success:
                break
                
            case .failure(error: let signupError):
                switch(signupError) {
                case .invalidApplicationId(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                }
            }
    }
    
    Copilot.instance
        .manage
        .sphere
        .auth
        .login()
        .with(email: "ck@dailyplanet.com", password: "Superman1234")
        .build()
        .execute { (response) in
            switch response {
            case .success:
                break
                
            case .failure(error: let loginError):
                switch(loginError) {
                case .invalidParameters(let debugMessage):
                    break
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .markedForDeletion(let debugMessage):
                    break
                case .unauthorized(let debugMessage):
                    break
				case .accountSuspended:
					break
				}
            }
    }
    
    //Silent login
    Copilot.instance
        .manage
        .sphere
        .auth
        .login()
        .silently
        .build()
        .execute { (response) in
            switch response {
            case .success():
                break
                
            case .failure(error: let silentLoginError):
                switch(silentLoginError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                }
            }
    }
    
    //Logout
    Copilot.instance
        .manage
        .sphere
        .auth
        .logout()
        .build()
        .execute { (response) in
            switch response {
            case .success():
                break
                
            case .failure(error: let logoutError):
                switch(logoutError) {
                case .internalError(_):
                    break
                }
            }
    }
    
    // - MARK: User
    
    //Fetch user details
    Copilot.instance
        .manage
        .sphere
        .user
        .fetchMe()
        .build()
        .execute { (response) in
            switch response {
            case .success(let userMe):
                break
                
            case .failure(error: let fetchMeError):
                switch(fetchMeError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                }
            }
    }
    
    //Update user details
    Copilot.instance
        .manage
        .sphere
        .user
        .updateMe()
        .with(firstname: "Clark")
        .with(lastname: "Kent")
        .with(customValue: true, forKey: "sensibleToKryptonite")
        .with(customValue: "Kalel", forKey: "birthName")
        .with(customValue: ["Martha Kent", "Jonathan Kent"], forKey: "adoptiveParents")
        .with(customValue: ["planet": "Krypton"], forKey: "origin")
        .build()
        .execute { (response) in
            switch response {
            case .success(let userMe):
                break
                
            case .failure(error: let updateMeError):
                switch(updateMeError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                }
            }
    }
    
    //Approve terms of use
    Copilot.instance
        .manage
        .sphere
        .user
        .updateMe()
        .approveTermsOfUse(forVersion: "17")
        .build()
        .execute { (response) in
            switch response {
            case .success():
                break
                
            case .failure(error: let updateMeError):
                switch(updateMeError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                }
            }
    }
    
    //Update user's consent
    Copilot.instance
        .manage
        .sphere
        .user
        .updateMe()
        .allowCopilotUserAnalysis(true)
        .withCustomConsent("analyseUseOfXRayVision", value: true)
        .build()
        .execute { (response) in
            switch response {
            case .success():
                break
                
            case .failure(error: let updateConsentError):
                switch(updateConsentError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                }
            }
    }
    
    //Change password
    Copilot.instance
        .manage
        .sphere
        .user
        .updateMe()
        .withNewPassword("ManOfSteel1234", verifyWithOldPassword: "Superman1234")
        .build()
        .execute{ (response) in
            switch response {
            case .success():
                break
                
            case .failure(error: let updatePasswordError):
                switch(updatePasswordError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                case .invalidCredentials(let debugMessage):
                    break
                case .passwordPolicyViolation(let debugMessage):
                    break
                case .cannotChangePasswordToAnonymousUser(let debugMessage):
                    break
                }
            }
    }
    
    //Send verification email
    Copilot.instance
        .manage
        .sphere
        .user
        .sendVerificationEmail()
        .build()
        .execute{ (response) in
            switch response{
            case .success():
                break
                
            case .failure(error: let sendVerificationEmailError):
                switch(sendVerificationEmailError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                case .retryRequired(let retryInSeconds, let debugMessage):
                    break
                case .userAlreadyVerified(let debugMessage):
                    break
                }
            }
    }
    
    //Reset password
    Copilot.instance
        .manage
        .sphere
        .user
        .resetPassword(for: "ck@dailyplanet.com")
        .build()
        .execute { (response) in
            switch response {
            case .success():
                break
                
            case .failure(error: let resetPasswordError):
                switch(resetPasswordError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                case .emailIsNotVerified(let debugMessage):
                    break
                }
            }
    }
    
    //Elevate anonymous user
    Copilot.instance
        .manage
        .sphere
        .user
        .elevate()
        .with(email: "ck@dailyplanet.com", password: "Superman1234", firstname: "Clark", lastname: "Kent")
        .build()
        .execute { (response) in
            switch response {
            case .success():
                break
                
            case .failure(error: let elevateError):
                switch(elevateError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                case .cannotElevateANonAnonymousUser(let debugMessage):
                    break
                case .userAlreadyExists(let debugMessage):
                    break
                case .passwordPolicyViolation(let debugMessage):
                    break
                case .invalidEmail(let debugMessage):
                    break
                }
            }
    }
    
    
    
    // - MARK: Thing
    
    //Thing Association
    Copilot.instance
        .manage
        .sphere
        .thing
        .associateThing(withPhysicalId: "physicalId", firmware: "firmware", model: "model")
        .build()
        .execute { (response) in
            switch response {
            case .success(let thing):
                break
                
            case .failure(error: let associateError):
                switch(associateError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                case .thingAlreadyAssociated(let debugMessage):
                    break
                case .thingNotAllowed(let debugMessage):
                    break
                }
            }
    }
    
    //Disassociate thing
    Copilot.instance
        .manage
        .sphere
        .thing
        .disassociateThing(withPhysicalId: "ConnectedSupermanCape17")
        .build()
        .execute { (response) in
            switch response {
            case .success():
                break
                
            case .failure(error: let disassociateError):
                switch(disassociateError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                case .thingNotFound(let debugMessage):
                    break
                }
            }
    }
    
    //Check if can associate
    Copilot.instance
        .manage
        .sphere
        .thing
        .checkIfCanAssociate(withPhysicalId: "ConnectedSupermanCape17")
        .build()
        .execute { (response) in
            switch response {
            case .success(let checkIfCanAssociateResponse):
                break
                
            case .failure(error: let checkIfCanAssociateeError):
                switch(checkIfCanAssociateeError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                }
            }
    }
    
    //Fetching things
    Copilot.instance
        .manage
        .sphere
        .thing
        .fetchThings()
        .build()
        .execute { (response) in
            switch response {
            case .success(let things):
                break
                
            case .failure(error: let fetchError):
                switch(fetchError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                }
            }
    }
    
    //Fetch single thing
    Copilot.instance
        .manage
        .sphere
        .thing
        .fetchThing(withPhysicalId: "ConnectedSupermanCape17")
        .build()
        .execute { (response) in
            switch response {
            case .success(let thing):
                break
                
            case .failure(error: let fetchError):
                switch(fetchError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                case .thingNotFound(let debugMessage):
                    break
                case .thingIsNotAssociated(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                }
            }
    }
    
    //Update thing details
    Copilot.instance
        .manage
        .sphere
        .thing
        .updateThing(withPhysicalId: "ConnectedSupermanCape17")
        .with(name: "Coolest cape ever")
        .with(firmware: "2.3")
        .with(status: ThingStatus(lastSeen: Date(), reportedStatuses: []))
        .with(customValue: true, forKey: "autoDetection").build().execute { (response) in
            switch response {
            case .success(let thing):
                break
                
            case .failure(error: let updateThingError):
                switch(updateThingError) {
                case .generalError(let debugMessage):
                    break
                case .connectivityError(let debugMessage):
                    break
                case .requiresRelogin(let debugMessage):
                    break
                case .thingNotFound(let debugMessage):
                    break
                case .thingIsNotAssociated(let debugMessage):
                    break
                case .invalidParameters(let debugMessage):
                    break
                }
            }
    }
    
    
    
    // - MARK: Report
    
    //Managing providers
    let firebaseProvider = FirebaseAnalyticsProvider()
    Copilot.setup(analyticsProviders: [firebaseProvider])
    
    
    // - MARK: Predefined events
    
    //Sign up
    Copilot.instance.report.log(event: SignupAnalyticsEvent())
    
    //Login
    Copilot.instance.report.log(event: LoginAnalyticsEvent())
    
    //Logout
    Copilot.instance.report.log(event: LogoutAnalyticsEvent())
    
    //Successful Elevate Anonymous
    Copilot.instance.report.log(event: SuccessfulElevateAnonymousAnalyticEvent())
    
    
    // - MARK: User Actions
    
    //Accept Terms of use
    Copilot.instance.report.log(event: AcceptTermsAnalyticsEvent(version: "1.7"))
    
    
    // - MARK: Onboarding Events
    
    //Onboarding Started / Ended
    Copilot.instance.report.log(event: OnboardingStartedAnalayticsEvent(flowID: "fromPush"))
    
    Copilot.instance.report.log(event: OnboardingEndedAnalyticsEvent(flowID: "fromPush", screenName: "MainScreen"))
    
    
    // - MARK: Thing Events
    
    //Tap connect device
    Copilot.instance.report.log(event: TapConnectDeviceAnalyticsEvent())
    
    //Thing connected
    Copilot.instance.report.log(event: ThingConnectedAnalyticsEvent(thingID: "33:22:1B:23:6A", screenName: "MainScreen"))
    
    //Thing discovered
    Copilot.instance.report.log(event: ThingDiscoveredAnalyticsEvent(thingID: "33:22:1B:23:6A"))
    
    //Thing info
    Copilot.instance.report.log(event: ThingInfoAnalyticsEvent(thingFirmware: "1.0.22", thingModel: "Spaceship - Rev(A)", thingId: "33:22:1B:23:6A"))
    
    //Thing Connection Failed
    Copilot.instance.report.log(event: ThingConnectionFailedAnalyticsEvent(failureReason: "Timeout"))
    
    //Consumable Usage
    Copilot.instance.report.log(event: ConsumableUsageAnalyticsEvent(thingId: "33:22:1B:23:6A", consumableType: "ink", screenName: "PrintingScreen"))
    
    
    // - MARK: General Events
    
    //Tap Menu
    Copilot.instance.report.log(event: TapMenuAnalyticsEvent(screenName: "MainScreen"))
    
    //Tap Menu Item
    Copilot.instance.report.log(event: TapMenuItemAnalyticsEvent(menuItem: "Settings"))
    
    //Firmware upgrade started
    Copilot.instance.report.log(event: FirmwareUpgradeStartedAnalyticsEvent())
    
    //Firmware upgrade completed
    Copilot.instance.report.log(event: FirmwareUpgradeCompletedAnalyticsEvent(firmwareUpgradeStatus: .Success))
    
    //Error Report
    Copilot.instance.report.log(event: ErrorAnalyticsEvent(errorType: "CustomError", screenName: "SettingsScreen"))
    
    //Contact Support
    Copilot.instance.report.log(event: ContactSupportAnalyticsEvent(supportCase: "I need help", thingId: "33:22:1B:23:6A", screenName: "SupportScreen"))
    
    //Consumable Depleted
    Copilot.instance.report.log(event: ConsumableDepletedAnalyticsEvent(thingId: "33:22:1B:23:6A", consumableType: "ink", screenName: "PrintingScreen"))
    
    
    // - MARK: Custom events
    
    //Dispaching custom events
    struct TapSpaceshipLaunchAnalytisEvent: AnalyticsEvent {
        
        private let spaceshipDestination: String
        private let daysOfTravel: Int
        
        init(spaceshipDestination:String, daysOfTravel :Int) {
            self.spaceshipDestination = spaceshipDestination
            self.daysOfTravel = daysOfTravel
        }
        
        var customParams: Dictionary<String, String> {
            return ["spaceship_destination" : spaceshipDestination,
                    "days_of_travel" : String(daysOfTravel),
                    "screenName" : "spaceship_ready_for_launch_screen"]
        }
        
        var eventName: String {
            return "tap_spaceship_launch"
        }
        
        var eventOrigin: AnalyticsEventOrigin {
            return .App
        }
    }
    
    //Dispatching the event
    Copilot.instance.report.log(event: TapSpaceshipLaunchAnalytisEvent(spaceshipDestination: "earth", daysOfTravel: 3))
    
    
    
    
    
    //External Auth
    
    //Before starting a session external user should set token provider which implements CopilotTokenProvider
    
    class ExternalAuthenticationServiceInteraction: CopilotTokenProvider {
        
        func generateUserAuthKey(for userId: String, withClosure closure: @escaping GenerateUserAuthKeyClosure) {
            
            RequestManager().generateAccessToken(userId: userId) { (response) in
                /*
                let generateAccessTokenResponse: Response<String, GenerateUserAuthKeyError>
                
                switch response {
                case .success(let authKey):
                    generateAccessTokenResponse = .success((authKey))
                    
                case .failure(let error):
                    switch error {
                    case .generalError:
                        generateAccessTokenResponse = .failure(error: .generalError(debugMessage: ""))
                    case .connectivityError:
                        generateAccessTokenResponse = .failure(error: .connectivityError(debugMessage: ""))
                    }
                }
                
                closure(generateAccessTokenResponse)
                */
            }
            
        }
        
    }
    
    //Set the token provider (recommanded in the app delegate)
    let copilotTokenProvider = ExternalAuthenticationServiceInteraction()
    Copilot.instance.manage.yourOwn.auth.setCopilotTokenProvider(copilotTokenProvider)
    
    
    //Start session
    Copilot.instance.manage.yourOwn.sessionStarted(withUserId: "user_id", isCopilotAnalysisConsentApproved: true)
    
    //End session
    Copilot.instance.manage.yourOwn.sessionEnded()
    
    //Update copilot analysis consent
    Copilot.instance.manage.yourOwn.setCopilotAnalysisConsent(isConsentApproved: false)
    
    
    // External App request manager for example
    
    enum ExternalError: Error {
        case generalError
        case connectivityError
    }
    
    class RequestManager {
        
        let provider = MoyaProvider<SampleAppService>(plugins: [NetworkLoggerPlugin()])
        
        // MARK: Network Request
        
        func generateAccessToken(userId: String, completion: @escaping (Result<String, ExternalError>) -> Void) {
            let generateAccessTokenApi = SampleAppService.generateUserAuthKey(userId: userId)
            
            provider.request(generateAccessTokenApi) { result in
                let requestExecutorResponse: Result<String, ExternalError>
                
                switch result {
                case .failure(let error):
                    requestExecutorResponse = .failure(.connectivityError)
                    
                case .success(let moyaResponse):
                    do {
                        if let dict = try moyaResponse.mapJSON(failsOnEmptyData: false) as? [String: Any],
                            let authKeyFromDict = dict["auth_key"] as? String {
                            requestExecutorResponse = .success(authKeyFromDict)
                        } else {
                            requestExecutorResponse = .failure(.generalError)
                        }
                    }
                    catch {
                        ZLogManagerWrapper.sharedInstance.logError(message: "failed to parse response with response : \(moyaResponse)")
                        requestExecutorResponse = .failure(.generalError)
                    }
                }
                
                completion(requestExecutorResponse)
            }
        }
    }
}

