//
//  NetworkParameters.swift
//  SampleAppDevIOTCO
//
//  Created by Revital Pisman on 20/06/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct NetworkParameters {
    
    static let shared = NetworkParameters()
    
    private struct Keys {
        static let dummyURL = "www.thisisnotreal.website"
    }
    
    private var environmentUrl: String? {
        var pathResult: String?
        
        guard let copilotEnvironmentUrl = Bundle.main.copilotEnvironmentUrl else {
            ZLogManagerWrapper.sharedInstance.logError(message: "Couldn't find Path in Copilot-Info.plist")
            return pathResult
        }
        
        pathResult = copilotEnvironmentUrl
        
        return pathResult
    }
    
    
    var baseURL: URL {
        var url: URL?
        
        if let environmentUrl = environmentUrl {
            
            //Create the URL
            if let baseURL = URL(string: environmentUrl) {
                url = baseURL
            }
            else {
                ZLogManagerWrapper.sharedInstance.logError(message: "URL wasn't created from base URL: \(environmentUrl)")
            }
        }
        else {
            ZLogManagerWrapper.sharedInstance.logError(message: "URL wasn't created. Path not found.")
        }
        
        if let url = url {
            return url
        }
        else {
            ZLogManagerWrapper.sharedInstance.logError(message: "Path is missing from Copilot-Info.plist, returning dummy URL")
            return URL(string: Keys.dummyURL)!
        }
    }
    
    private init() {
        
    }
}
