//
//  SampleAppService.swift
//  SampleAppDevIOTCO
//
//  Created by Revital Pisman on 19/06/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import Moya

//StageToken: Basic U1RBR0UySU9UQ09fU0VSVklDRV9BQ0NPVU5UOkwwa3V6cjBaeU92OGxnanRBU2NtNU9oNzU5amYyaA==
//DevToken: Basic SU9UQ09fU0VSVklDRV9BQ0NPVU5UOkpzNk85MDljR3ZucFJtNGY5dGppUjNIeVh2Z0MyRA==
//SimToken: Basic U0lNSU9UQ09fU0VSVklDRV9BQ0NPVU5UOjNmSEI4NmY4ajZ3T1RJZ1ZMMTVmZVhVNnA4ZlNtNQ==

enum SampleAppService {
    case generateUserAuthKey(userId: String)
}

// MARK: - TargetType Protocol Implementation
extension SampleAppService: TargetType {
    
    private struct Consts {
        
        static let managementPath = "v2/api/management/your_own/auth"
        
        static let generateAccessTokenPath = Consts.managementPath + "/generate_user_auth_key"
    }
    
    var baseURL: URL {
        return NetworkParameters.shared.baseURL
    }
    
    var path: String {
        switch self {
        case .generateUserAuthKey:
            return Consts.generateAccessTokenPath
        }
    }
    
    var method: Moya.Method {
        switch self {
        case.generateUserAuthKey:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case let .generateUserAuthKey(userId):  // Always send parameters as JSON in request body
            return .requestParameters(parameters: ["user_id": userId], encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String: String]? {
        let clientId = Bundle.main.copilotServicAccountClientId
        let clientSecret = Bundle.main.copilotServicAccountClientSecret
        if let clientId = clientId, let clientSecret=clientSecret {
            let authorizationBase64 = "\(clientId):\(clientSecret)".toBase64()
            return ["Authorization" : "Basic \(authorizationBase64)"]
        } else {
            return [:]
        }
    }
}
