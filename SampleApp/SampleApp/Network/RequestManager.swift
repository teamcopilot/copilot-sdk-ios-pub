//
//  RequestManager.swift
//  SampleAppDevIOTCO
//
//  Created by Revital Pisman on 19/06/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import Moya
import CopilotAPIAccess

enum ExternalError: Error {
    case generalError
    case connectivityError
}

class RequestManager {
    
    let provider = MoyaProvider<SampleAppService>(plugins: [NetworkLoggerPlugin()])
    
    // MARK: Network Request
    
    func generateAccessToken(userId: String, completion: @escaping (Result<String, ExternalError>) -> Void) {
        let generateAccessTokenApi = SampleAppService.generateUserAuthKey(userId: userId)
        
        provider.request(generateAccessTokenApi) { result in
            let requestExecutorResponse: Result<String, ExternalError>
            
            switch result {
            case .failure(let error):
                requestExecutorResponse = .failure(.connectivityError)
                
            case .success(let moyaResponse):
                do {
                    if let dict = try moyaResponse.mapJSON(failsOnEmptyData: false) as? [String: Any],
                        let authKeyFromDict = dict["auth_key"] as? String {
                        requestExecutorResponse = .success(authKeyFromDict)
                    } else {
                        requestExecutorResponse = .failure(.generalError)
                    }
                }
                catch {
                    ZLogManagerWrapper.sharedInstance.logError(message: "failed to parse response with response : \(moyaResponse)")
                    requestExecutorResponse = .failure(.generalError)
                }
            }
            
            completion(requestExecutorResponse)
        }
    }
}
