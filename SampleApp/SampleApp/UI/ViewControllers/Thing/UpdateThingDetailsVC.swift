//
//  UpdateThingDetailsVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class UpdateThingDetailsVC: KeyboardHandlerVC {
    
    //MARK: - Properties
    
    @IBOutlet weak var physicalIDTextField: UITextField!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var firmwareTextField: UITextField!
    
    @IBOutlet weak var statusKey1Switch: UISwitch!
    @IBOutlet weak var statusKey2Switch: UISwitch!
    @IBOutlet weak var boolCustomValueSwitch: UISwitch!
    @IBOutlet weak var stringCustomValueSwitch: UISwitch!
    @IBOutlet weak var intCustomValueSwitch: UISwitch!
    @IBOutlet weak var objectCustomValueSwitch: UISwitch!
    @IBOutlet weak var collectionCustomValueSwitch: UISwitch!
    
    private var updateThing: UpdateThingRequestBuilderType?
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Update thing details"
        
        view.hideKeyboardWhenTapped()
        
        let randomInt = Int.random(in: 1...100)
        
        nameTextField.text = "newName" + String(randomInt)
        firmwareTextField.text = "newFW" + String(randomInt)
    }
    
    //MARK: - IBActions
    
    @IBAction func updateThingDetailsButtonPressed(_ sender: Any) {
        guard let physicalID = physicalIDTextField.text,
            let name = nameTextField.text,
            let firmware = firmwareTextField.text  else {
                showAlert("All fields should be filled")
                return
        }
        
        showLoadingView()
        
        updateThing = Copilot.instance
            .manage
            .sphere
            .thing
            .updateThing(withPhysicalId: physicalID)
            .with(name: name)
            .with(firmware: firmware)
        updateCustomValue(bySwitch: boolCustomValueSwitch, value: Constants.thingBoolCustomValue.value, forKey: Constants.thingBoolCustomValue.key)
        updateCustomValue(bySwitch: stringCustomValueSwitch, value: Constants.thingStringCustomValue.value, forKey: Constants.thingStringCustomValue.key)
        updateCustomValue(bySwitch: intCustomValueSwitch, value: Constants.thingIntCustomValue.value, forKey: Constants.thingIntCustomValue.key)
        updateCustomValue(bySwitch: objectCustomValueSwitch, value: Constants.thingObjectCustomValue.value, forKey: Constants.thingObjectCustomValue.key)
        updateCustomValue(bySwitch: collectionCustomValueSwitch, value: Constants.thingCollectionCustomValue.value, forKey: Constants.thingCollectionCustomValue.key)
        updateThing?.build().execute({ [weak self] (response) in
            DispatchQueue.main.async(execute:
                {
                    self?.hideLoadingView() { [weak self] in
                        switch response {
                        case .success(let thing):
                            self?.showAlert("Update thing \(thing.thingInfo.physicalId) succeeded")
                            
                        case .failure(let updateMeError):
                            let msg: String
                            
                            switch updateMeError {
                            case .connectivityError(let debugMessage):
                                msg = "Connectivity: \(debugMessage)"
                            case .generalError(let debugMessage):
                                msg = "General: \(debugMessage)"
                            case .invalidParameters(let debugMessage):
                                msg = "Invalid Parameters: \(debugMessage)"
                            case .requiresRelogin(let debugMessage):
                                msg = "Requires Relogin: \(debugMessage)"
                            case .thingNotFound(let debugMessage):
                                msg = "Thing Not Found: \(debugMessage)"
                            case .thingIsNotAssociated(let debugMessage):
                                msg = "Thing Is Not Associated: \(debugMessage)"
                            }
                            
                            self?.showAlert("Update thing failed with error:\n\(msg)\n\n\(updateMeError.localizedDescription)")
                        }
                    }
            })
        })
    }
    
    @IBAction func updateStatusesButtonPressed(_ sender: Any) {
        if let vc = ViewControllersFactory.createControllerWithType(.thingStatus) {
            self.navigationController?.show(vc, sender: nil)
        }
    }
    
    //MARK: - Private Functions
    
    private func updateCustomValue<T: Encodable>(bySwitch customValueSwitch: UISwitch, value: T,  forKey key: String) {
        if customValueSwitch.isOn {
            _ = updateThing?.with(customValue: value, forKey: key)
        } else {
            _ = updateThing?.removeCustomValue(forKey: key)
        }
    }
    
}
