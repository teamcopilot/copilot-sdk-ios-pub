//
//  ThingStatusVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 16/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class ThingStatusVC: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var physicalIDTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    private var datasource = [ThingReportedStatus]()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Thing Status"
        
        view.hideKeyboardWhenTapped()
    }
    
    //MARK: - IBActions
    
    @IBAction func updateStatusKey1ButtonPressed(_ sender: Any) {
        guard let physicalID = physicalIDTextField.text else {
            showAlert("All fields should be filled")
            return
        }
        
        showLoadingView()
        
        let thingReportedStatus1 = ThingReportedStatus.init(time: Date(), name: "thingReportedStatus1", value: "thingReportedStatusValue1")
        let thingStatus1 = ThingStatus.init(lastSeen: Date(), reportedStatuses: [thingReportedStatus1])
        
        Copilot.instance
            .manage
            .sphere
            .thing
            .updateThing(withPhysicalId: physicalID)
            .with(status: thingStatus1)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let thing):
                                self?.showAlert("Update thing \(thing.thingInfo.physicalId) succeeded")
                                
                            case .failure(let updateMeError):
                                let msg: String
                                
                                switch updateMeError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "Invalid Parameters: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .thingNotFound(let debugMessage):
                                    msg = "Thing Not Found: \(debugMessage)"
                                case .thingIsNotAssociated(let debugMessage):
                                    msg = "Thing Is Not Associated: \(debugMessage)"
                                }
                                
                                self?.showAlert("Update thing failed with error:\n\(msg)\n\n\(updateMeError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
    @IBAction func updateStatusKey2ButtonPressed(_ sender: Any) {
        guard let physicalID = physicalIDTextField.text else {
            showAlert("All fields should be filled")
            return
        }
        
        showLoadingView()
        
        let thingReportedStatus2 = ThingReportedStatus.init(time: Date(), name: "thingReportedStatus2", value: "thingReportedStatusValue2")
        let thingStatus2 = ThingStatus.init(lastSeen: Date(), reportedStatuses: [thingReportedStatus2])
        
        Copilot.instance
            .manage
            .sphere
            .thing
            .updateThing(withPhysicalId: physicalID)
            .with(status: thingStatus2)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let thing):
                                self?.showAlert("Update thing \(thing.thingInfo.physicalId) succeeded")
                                
                            case .failure(let updateMeError):
                                let msg: String
                                
                                switch updateMeError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "Invalid Parameters: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .thingNotFound(let debugMessage):
                                    msg = "Thing Not Found: \(debugMessage)"
                                case .thingIsNotAssociated(let debugMessage):
                                    msg = "Thing Is Not Associated: \(debugMessage)"
                                }
                                
                                self?.showAlert("Update thing failed with error:\n\(msg)\n\n\(updateMeError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
    @IBAction func updateStatus100KeysPressed(_ sender: UIButton) {
        guard let physicalID = physicalIDTextField.text else {
            showAlert("All fields should be filled")
            return
        }
        
        showLoadingView()
        
        for index in 1 ... 100 {
            let thingReportedStatus = ThingReportedStatus.init(time: Date(), name: "thingReportedStatusIndex\(index)", value: "thingReportedStatusIndex\(index)")
            let thingStatus = ThingStatus.init(lastSeen: Date(), reportedStatuses: [thingReportedStatus])
            
            Copilot.instance
                .manage
                .sphere
                .thing
                .updateThing(withPhysicalId: physicalID)
                .with(status: thingStatus)
                .build()
                .execute { [weak self] (response) in
                    DispatchQueue.main.async(execute:
                        {
                            self?.hideLoadingView() { [weak self] in
                                switch response {
                                case .success(let thing):
                                    self?.showAlert("Update thing \(thing.thingInfo.physicalId) succeeded")
                                    
                                case .failure(let updateMeError):
                                    let msg: String
                                    
                                    switch updateMeError {
                                    case .connectivityError(let debugMessage):
                                        msg = "Connectivity: \(debugMessage)"
                                    case .generalError(let debugMessage):
                                        msg = "General: \(debugMessage)"
                                    case .invalidParameters(let debugMessage):
                                        msg = "Invalid Parameters: \(debugMessage)"
                                    case .requiresRelogin(let debugMessage):
                                        msg = "Requires Relogin: \(debugMessage)"
                                    case .thingNotFound(let debugMessage):
                                        msg = "Thing Not Found: \(debugMessage)"
                                    case .thingIsNotAssociated(let debugMessage):
                                        msg = "Thing Is Not Associated: \(debugMessage)"
                                    }
                                    
                                    self?.showAlert("Update thing failed with error:\n\(msg)\n\n\(updateMeError.localizedDescription)")
                                }
                            }
                    })
            }
        }
    }
    
    @IBAction func fetchStatusesButtonPressed(_ sender: Any) {
        guard let physicalID = physicalIDTextField.text else {
            showAlert("All fields should be filled")
            return
        }
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .thing
            .fetchThing(withPhysicalId: physicalID)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let thing):
                                if let reportedStatuses = thing.thingStatus?.reportedStatuses, !reportedStatuses.isEmpty {
                                    self?.showAlert("Fetch statuses for thing \(thing.thingInfo.physicalId) succedded")
                                    self?.datasource = reportedStatuses
                                    self?.tableView.reloadData()
                                } else {
                                    self?.showAlert("Fetch statuses for thing \(thing.thingInfo.physicalId) succedded but there are no statuses")
                                }
                                
                            case .failure(error: let fetchSingleAssociatedThingError):
                                let msg: String
                                
                                switch fetchSingleAssociatedThingError {
                                case .thingIsNotAssociated(let debugMessage):
                                    msg = "Thing Is Not Associated: \(debugMessage)"
                                case .thingNotFound(let debugMessage):
                                    msg = "Thing Not Found: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "Invalid Parameters: \(debugMessage)"
                                }
                                
                                self?.showAlert("Fetch single associated thing failed with error: \(msg)\n\n\(fetchSingleAssociatedThingError.localizedDescription)")
                            }
                        }
                })
        }
        
    }
    
}

extension ThingStatusVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var generalCell = UITableViewCell()
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: ThingStatusTableViewCell.stringFromClass(), for: indexPath) as? ThingStatusTableViewCell {
            
            let status = datasource[indexPath.row]
            let date = status.time
            
            cell.set(name: status.name, value: String(status.value), time: date.description)
            
            generalCell = cell
            
        }
        
        return generalCell
    }
    
}
