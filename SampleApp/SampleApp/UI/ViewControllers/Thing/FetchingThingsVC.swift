//
//  FetchingThingsVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class FetchingThingsVC: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var physicalIDTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    private var datasource = [Thing]()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Fetching things"
        
        view.hideKeyboardWhenTapped()
    }
    
    //MARK: - IBActions
    
    @IBAction func fetchAssociatedThingsButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .thing
            .fetchThings()
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let things):
                                if things.isEmpty {
                                    self?.showAlert("Fetch associated things succedded but there are no associated things")
                                } else {
                                    self?.showAlert("Fetch associated things succedded")
                                }
                                self?.datasource = things
                                self?.tableView.reloadData()
                                
                            case .failure(error: let fetchAssociatedThingsError):
                                let msg: String
                                
                                switch fetchAssociatedThingsError {
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                }
                                
                                self?.datasource.removeAll()
                                self?.tableView.reloadData()
                                self?.showAlert("Fetch associated things failed with error: \(msg)\n\n\(fetchAssociatedThingsError.localizedDescription)")
                            }
                        }
                })
        }
        
    }
    
    @IBAction func fetchSingleAssociatedThingButtonPressed(_ sender: Any) {
        guard let physicalID = physicalIDTextField.text else {
            showAlert("All fields should be filled")
            return
        }
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .thing
            .fetchThing(withPhysicalId: physicalID)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let thing):
                                self?.showAlert("Fetch associated thing \(thing.thingInfo.physicalId) succedded")
                                self?.datasource.removeAll()
                                self?.datasource.append(thing)
                                self?.tableView.reloadData()
                                
                            case .failure(error: let fetchSingleAssociatedThingError):
                                let msg: String
                                
                                switch fetchSingleAssociatedThingError {
                                case .thingIsNotAssociated(let debugMessage):
                                    msg = "Thing Is Not Associated: \(debugMessage)"
                                case .thingNotFound(let debugMessage):
                                    msg = "Thing Not Found: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "Invalid Parameters: \(debugMessage)"
                                }
                                
                                self?.datasource.removeAll()
                                self?.tableView.reloadData()
                                self?.showAlert("Fetch single associated thing failed with error: \(msg)\n\n\(fetchSingleAssociatedThingError.localizedDescription)")
                            }
                        }
                })
        }
        
    }
    
}

extension FetchingThingsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var generalCell = UITableViewCell()
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: ThingTableViewCell.stringFromClass(), for: indexPath) as? ThingTableViewCell {
            
            let thing = datasource[indexPath.row]
            
            cell.set(physicalID: thing.thingInfo.physicalId, firmware: thing.thingInfo.firmware, model: thing.thingInfo.model, name: thing.thingInfo.name)
            
            generalCell = cell
            
        }
        
        return generalCell
    }
    
}
