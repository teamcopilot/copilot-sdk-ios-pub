//
//  ThingAssociationVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class ThingAssociationVC: KeyboardHandlerVC {
    
    //MARK: - Properties
    
    @IBOutlet weak var physicalIDTextField: UITextField!
    @IBOutlet weak var firmwareTextField: UITextField!
    @IBOutlet weak var modelTextField: UITextField!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Thing association"
        
        view.hideKeyboardWhenTapped()
        
        let randomInt = Int.random(in: 1...100)
        
        physicalIDTextField.text = "thing" + String(randomInt)
        firmwareTextField.text = "fw1"
        modelTextField.text = "model1"
    }
    
    //MARK: - IBActions
    
    @IBAction func associateButtonPressed(_ sender: Any) {
        guard let physicalID = physicalIDTextField.text,
            let firmware = firmwareTextField.text,
            let model = modelTextField.text else {
                showAlert("All fields should be filled")
                return
        }
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .thing
            .associateThing(withPhysicalId: physicalID, firmware: firmware, model: model).build().execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let thing):
                                self?.showAlert("Associate thing \(thing.thingInfo.physicalId) succedded")
                                
                            case .failure(let associateThingError):
                                let msg: String
                                
                                switch associateThingError {
                                case .invalidParameters(let debugMessage):
                                    msg = "Invalid Parameters: \(debugMessage)"
                                case .thingAlreadyAssociated(let debugMessage):
                                    msg = "Thing Already Associated: \(debugMessage)"
                                case .thingNotAllowed(let debugMessage):
                                    msg = "Thing Not Allowed: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                }
                                
                                self?.showAlert("Associate thing failed with error: \(msg)\n\n\(associateThingError.localizedDescription)")
                            }
                        }
                })
        }
        
    }
    
    @IBAction func disassociateButtonPressed(_ sender: Any) {
        guard let physicalID = physicalIDTextField.text else {
            return
        }
        
        Copilot.instance
            .manage
            .sphere
            .thing
            .disassociateThing(withPhysicalId: physicalID)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        switch response {
                        case .success():
                            self?.showAlert("Disassociate Thing succedded")
                            
                        case .failure(error: let disassociateError):
                            let msg: String
                            
                            switch disassociateError {
                            case.thingNotFound(let debugMessage):
                                msg = "Thing Not Found: \(debugMessage)"
                            case .invalidParameters(let debugMessage):
                                msg = "Invalid Parameters: \(debugMessage)"
                            case .requiresRelogin(let debugMessage):
                                msg = "Requires Relogin: \(debugMessage)"
                            case .connectivityError(let debugMessage):
                                msg = "Connectivity: \(debugMessage)"
                            case .generalError(let debugMessage):
                                msg = "General: \(debugMessage)"
                            }
                            
                            self?.showAlert("\(msg)\n\n\(disassociateError.localizedDescription)")
                        }
                })
        }
        
    }
    
    @IBAction func checkIfCanAssociateButtonPressed(_ sender: Any) {
        guard let physicalID = physicalIDTextField.text else {
            return
        }
        
        Copilot.instance
            .manage
            .sphere
            .thing
            .checkIfCanAssociate(withPhysicalId: physicalID)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        switch response {
                        case .success(let checkIfCanAssociateResponse):
                            self?.showAlert("Check If Can Associate succedded \(checkIfCanAssociateResponse.description)")
                            
                        case .failure(let checkIfCanAssociateError):
                            let msg: String
                            
                            switch checkIfCanAssociateError {
                            case .invalidParameters(let debugMessage):
                                msg = "Invalid Parameters: \(debugMessage)"
                            case .requiresRelogin(let debugMessage):
                                msg = "Requires Relogin: \(debugMessage)"
                            case .connectivityError(let debugMessage):
                                msg = "Connectivity: \(debugMessage)"
                            case .generalError(let debugMessage):
                                msg = "General: \(debugMessage)"
                            }
                            
                            self?.showAlert("\(msg)\n\n\(checkIfCanAssociateError.localizedDescription)")
                        }
                })
        }
        
    }
    
}
