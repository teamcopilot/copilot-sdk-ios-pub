//
//  ThingTableViewCell.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

class ThingTableViewCell: UITableViewCell {

    //MARK: - Properties
    
    @IBOutlet weak var physicalIDLabel: UILabel!
    @IBOutlet weak var firmwareLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    //MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    //MARK: - Functions

    func set(physicalID: String, firmware: String, model: String, name: String?) {
        physicalIDLabel.text = physicalID
        firmwareLabel.text = firmware
        modelLabel.text = model
        nameLabel.text = name ?? "NULL"
    }

}
