//
//  TestsVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 17/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class TestsVC: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var tableView: UITableView!
    
    private var datasource = Tests.allCases
    
    let tester = Tester()
    
    private enum Tests: Int, CaseIterable {
        case checkAllApis
        case checkConnectivityErrors
        case checkAuthErrors
        case checkUserAPIErrors
        
        var title: String {
            switch self {
            case .checkAllApis:
                return "Check all APIs fail when not logged in"
            case .checkConnectivityErrors:
                return "Check connectivity errors"
            case .checkAuthErrors:
                return "Check auth errors"
            case .checkUserAPIErrors:
                return "Check User API erros"
            }
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Tests"
    }
}

extension TestsVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TestsTableViewCell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = datasource[indexPath.row].title
        
        return cell
    }
    
}

extension TestsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let test = datasource[indexPath.row]
        let testTitle = test.title
        
        if testTitle == Tests.checkAllApis.title {
            tester.runWithoutLogin(self.testResponse)
            
        } else if testTitle == Tests.checkConnectivityErrors.title {
            tester.runConnectionErrors(self.testResponse)
            
        } else if testTitle == Tests.checkAuthErrors.title {
            tester.runAuthErrors(self.testResponse)
            
        } else if testTitle == Tests.checkUserAPIErrors.title {
            showLoadingView()
            
            Copilot.instance
                .manage
                .sphere
                .user
                .fetchMe()
                .build()
                .execute { [weak self] (response) in
                    DispatchQueue.main.async(execute:
                        {
                            self?.hideLoadingView() { [weak self] in
                                guard let strongSelf = self else { return }
                                switch response{
                                case .success(let user):
                                    strongSelf.tester.runUserErrors(user.accountStatus.credentialsType == .anonymous, strongSelf.testResponse)
                                    
                                case .failure(let error):
                                    strongSelf.showAlert("Failed loading user due to \(error.localizedDescription)")
                                }
                            }
                    })
            }
        }
        
    }
    
}
