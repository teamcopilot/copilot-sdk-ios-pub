//
//  BGTestVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 25/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess
import CoreLocation


class BGTestVC: UIViewController {
    
    @IBOutlet weak var loginUser1Button: UIButton!
    @IBOutlet weak var loginUser2Button: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var canLoginSilentlyLabel: UILabel!
    
    let locationManager = CLLocationManager()
    
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Background Test"
        
        locationManager.delegate = self
        
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.showsBackgroundLocationIndicator = true
        
        //Location should be always on
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let canLoginSilently = Copilot.instance.manage.sphere.defaultAuthProvider.canLoginSilently
        canLoginSilentlyLabel.text = canLoginSilently ? "YES" : "NO"
        
        if canLoginSilently {
            logoutButton.isEnabled = true
            loginUser1Button.isEnabled = false
            loginUser2Button.isEnabled = false
        }
    }
    
    
    @IBAction func loginUser1ButtonPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .auth
            .login()
            .with(email: "revital+23@zemingo.com", password: "Aa123456")
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        switch response {
                        case .success:
                            Copilot.instance.report.log(event: LoginAnalyticsEvent())
                            
                            let canLoginSilently = Copilot.instance.manage.sphere.defaultAuthProvider.canLoginSilently
                            self?.canLoginSilentlyLabel.text = canLoginSilently ? "YES" : "NO"
                            
                            self?.loginUser1Button.isEnabled = false
                            self?.loginUser2Button.isEnabled = false
                            self?.logoutButton.isEnabled = true
                            self?.showAlert("Login succeeded")
                            
                        case .failure(error: let loginError):
                            let msg: String
                            
                            switch loginError {
                            case .connectivityError(let debugMessage):
                                msg = "Connectivity: \(debugMessage)"
                            case .generalError(let debugMessage):
                                msg = "General: \(debugMessage)"
                            case .invalidParameters(let debugMessage):
                                msg = "invalid Parameters: \(debugMessage)"
                            case .markedForDeletion(let debugMessage):
                                msg = "Marked For Deletion: \(debugMessage)"
                            case .unauthorized(let debugMessage):
                                msg = "Unauthorized: \(debugMessage)"
							case .accountSuspended:
								msg = "Suspended"
							}
                            
                            self?.showAlert("Login failed with error:\n\(msg)\n\n\(loginError.localizedDescription)")
                        }
                })
        }
    }
    
    @IBAction func loginUser2ButtonPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .auth
            .login()
            .with(email: "revital+93@zemingo.com", password: "Aa123456")
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        switch response {
                        case .success:
                            Copilot.instance.report.log(event: LoginAnalyticsEvent())
                            
                            let canLoginSilently = Copilot.instance.manage.sphere.defaultAuthProvider.canLoginSilently
                            self?.canLoginSilentlyLabel.text = canLoginSilently ? "YES" : "NO"
                            
                            self?.loginUser1Button.isEnabled = false
                            self?.loginUser2Button.isEnabled = false
                            self?.logoutButton.isEnabled = true
                            self?.showAlert("Login succeeded")
                            
                        case .failure(error: let loginError):
                            let msg: String
                            
                            switch loginError {
                            case .connectivityError(let debugMessage):
                                msg = "Connectivity: \(debugMessage)"
                            case .generalError(let debugMessage):
                                msg = "General: \(debugMessage)"
                            case .invalidParameters(let debugMessage):
                                msg = "invalid Parameters: \(debugMessage)"
                            case .markedForDeletion(let debugMessage):
                                msg = "Marked For Deletion: \(debugMessage)"
							case .accountSuspended:
								msg = "Suspended:"
							case .unauthorized(let debugMessage):
                                msg = "Unauthorized: \(debugMessage)"
                            }
                            
                            self?.showAlert("Login failed with error:\n\(msg)\n\n\(loginError.localizedDescription)")
                        }
                })
        }
    }
    
    @IBAction func fetchMeButtonPressed(_ sender: Any) {
        Copilot.instance.manage.sphere.user.fetchMe().build().execute { [weak self] (response) in
            DispatchQueue.main.async(execute:
                {
                    switch response {
                    case .success(let userMe):
                        Copilot.instance.report.log(event: ScreenLoadAnalyticsEvent(screenName: "fetchMe succeeded, userID: \(userMe.userDetails.id)"))
                        self?.showAlert("Fetch Me succeeded")
                        
                    case .failure(let fetchMeError):
                        let msg: String
                        
                        switch fetchMeError {
                        case .connectivityError(_):
                            msg = "Connectivity Error"
                        case .requiresRelogin(_):
                            msg = "Requires Relogin Error"
                        case .generalError(_):
                            msg = "General Error"
                        }
                        
                        Copilot.instance.report.log(event: ScreenLoadAnalyticsEvent(screenName: "fetchMe failed because error: \(msg)"))
                        
                        self?.showAlert("Fetch Me failed")
                    }
            })
        }
    }
    
    @IBAction func thingConnectedEventButtonPressed(_ sender: Any) {
        reportThingConnectedEvent()
    }
    
    @IBAction func screenLoadEventButtonPressed(_ sender: Any) {
        Copilot.instance.report.log(event: ScreenLoadAnalyticsEvent(screenName: "ScreenBGTest"))
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .auth
            .logout()
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        switch response {
                        case .success:
                            Copilot.instance.report.log(event: LogoutAnalyticsEvent())
                            
                            let canLoginSilently = Copilot.instance.manage.sphere.defaultAuthProvider.canLoginSilently
                            self?.canLoginSilentlyLabel.text = canLoginSilently ? "YES" : "NO"
                            
                            self?.loginUser1Button.isEnabled = true
                            self?.loginUser2Button.isEnabled = true
                            self?.logoutButton.isEnabled = false
                            self?.showAlert("Logout succeeded")
                            
                        case .failure(let logoutError):
                            let msg: String
                            
                            switch logoutError {
                            case .internalError(let error):
                                msg = "Internal: \(error.localizedDescription)"
                            }
                            
                            self?.showAlert("Logout failed with error:\n\(msg)\n\n\(logoutError.localizedDescription)")
                        }
                })
                
        }
    }
    
    private func reportThingConnectedEvent() {
        Copilot.instance.report.log(event: ThingConnectedAnalyticsEvent(thingID: "ThingIDTest", screenName: "ScreenTest"))
    }
}

extension BGTestVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let state = UIApplication.shared.applicationState
        if (state == .background || state == .inactive) && count < 2 {
            count += 1
            
            let canLoginSilently = Copilot.instance.manage.sphere.defaultAuthProvider.canLoginSilently
            ZLogManagerWrapper.sharedInstance.logInfo(message: "canLoginSilently \(canLoginSilently)")
            
            Copilot.instance.manage.sphere.user.fetchMe().build().execute { (response) in
                switch response {
                case .success(let userMe):
                    Copilot.instance.report.log(event: ScreenLoadAnalyticsEvent(screenName: "fetchMe succeeded, userID: \(userMe.userDetails.id)"))
                    
                case .failure(let fetchMeError):
                    let msg: String
                    
                    switch fetchMeError {
                    case .connectivityError(_):
                        msg = "Connectivity Error"
                    case .requiresRelogin(_):
                        msg = "Requires Relogin Error"
                    case .generalError(_):
                        msg = "General Error"
                    }
                    
                    Copilot.instance.report.log(event: ScreenLoadAnalyticsEvent(screenName: "fetchMe failed because error: \(msg)"))
                }
            }
        }
        
    }
    
}
