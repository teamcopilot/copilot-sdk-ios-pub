//
//  ExternalSessionVC.swift
//  SampleAppDevIOTCO
//
//  Created by Revital Pisman on 12/05/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class ExternalSessionVC: UIViewController {

    //MARK: - Properties
    
    @IBOutlet weak var consentApprovalLabel: UILabel!
    @IBOutlet weak var userIDLabel: UILabel!
    @IBOutlet weak var userIDTextField: UITextField!
    @IBOutlet weak var consentStatusSwitch: UISwitch!
    @IBOutlet weak var sessionStartedButton: UIButton!
    @IBOutlet weak var sessionEndedButton: UIButton!
    @IBOutlet weak var updateConsentButton: UIButton!
    @IBOutlet weak var consentSwitch: UISwitch!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "External Session"
        
        view.hideKeyboardWhenTapped()
        updateUI()
    }
    
    //MARK: - IBActions
    
    @IBAction func checkButtonPressed(_ sender: Any) {
        updateUI()
    }
    
    @IBAction func generateButtonPressed(_ sender: Any) {
        let randomUUIDString = UUID().uuidString
        
        userIDTextField.text = randomUUIDString
    }
    
    @IBAction func sessionStartedButtonPressed(_ sender: Any) {
        if let userId = userIDTextField.text, userId != "" {
            Copilot.instance
                .manage
                .yourOwn
                .sessionStarted(withUserId: userId, isCopilotAnalysisConsentApproved: consentStatusSwitch.isOn)
        } else {
            showAlert("Please generate an user ID!")
        }
        
        updateUI()
    }
    
    @IBAction func sessionEndedButtonPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .yourOwn
            .sessionEnded()
        
        userIDTextField.text = ""
        updateUI()
    }
    
    @IBAction func updateConsentStatusButtonPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .yourOwn
            .setCopilotAnalysisConsent(isConsentApproved: consentStatusSwitch.isOn)
        
        updateUI()
    }
    
    @IBAction func reportAnalyticsEventPressed(_ sender: Any) {
        let event = ScreenLoadAnalyticsEvent.init(screenName: "External Session Test")
        Copilot.instance.report.log(event: event)
    }
    
    private func getGeneralItem(withKey key: String) -> Any? {
        if let objectAsData = UserDefaults.standard.object(forKey: key) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: objectAsData)
        }
        
        return nil
    }
    
    private func updateUI() {
        if let userId = getGeneralItem(withKey: "customUserIdKey") as? String {
            userIDLabel.text = userId
            userIDTextField.text = userId
        } else {
            userIDLabel.text = "NULL"
        }
        
        if let isCopilotAnalysisConsentApproved = getGeneralItem(withKey: "customAnalysisConsentKey") as? Bool {
            consentApprovalLabel.text = isCopilotAnalysisConsentApproved ? "Yes" : "No"
            consentSwitch.isOn = isCopilotAnalysisConsentApproved
        } else {
            consentApprovalLabel.text = "NULL"
        }
    }
}
