//
//  SilentLoginVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 11/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class SilentLoginVC: UIViewController {

    @IBOutlet weak var canLoginSientlyLabel: UILabel!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Silent login"
        
        canLoginSientlyLabel.text = Copilot.instance.manage.sphere.defaultAuthProvider.canLoginSilently ? "YES" : "NO"
    }
    
    //MARK: - IBActions

    @IBAction func silentLoginButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .auth
            .login()
            .silently
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success:
                                self?.showAlert("Silent login succeeded")
                                
                            case .failure(let silentLoginError):
                                let msg: String
                                
                                switch silentLoginError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                }
                                
                                self?.showAlert("Login failed with error:\n\(msg)\n\n\(silentLoginError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
