//
//  LoginWithEmailAndPasswordVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 11/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess


class LoginWithEmailAndPasswordVC: UIViewController {

    //MARK: - Properties
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Login with email and password"
        
        #if DEBUG
        emailTextField.text = "elad.s@zemingo.com"
        passwordTextField.text = "Aa123456"
        #endif
    }

    //MARK: - IBActions
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        guard let email = emailTextField.text,
            let password = passwordTextField.text else {
                showAlert("All fields should be filled")
                return
        }
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .auth
            .login()
            .with(email: email, password: password)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success:
                                self?.showAlert("Login succeeded")
                                
                            case .failure(error: let loginError):
                                let msg: String
                                
                                switch loginError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "invalid Parameters: \(debugMessage)"
                                case .markedForDeletion(let debugMessage):
                                    msg = "Marked For Deletion: \(debugMessage)"
                                case .unauthorized(let debugMessage):
                                    msg = "Unauthorized: \(debugMessage)"
								case .accountSuspended:
									msg = "suspended"
								}
                                
                                self?.showAlert("Login failed with error:\n\(msg)\n\n\(loginError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
