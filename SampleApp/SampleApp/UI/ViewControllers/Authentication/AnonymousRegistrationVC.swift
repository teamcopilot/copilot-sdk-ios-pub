//
//  AnonymousRegistrationVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 11/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess


class AnonymousRegistrationVC: UIViewController {

    //MARK: - Properties
    
    @IBOutlet weak var consentKey1Switch: UISwitch!
    @IBOutlet weak var consentKey2Switch: UISwitch!
    @IBOutlet weak var analyticsConsentKey: UISwitch!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Anonymous Registration"
    }
    
    //MARK: - IBActions
    
    @IBAction func registerAnonymousButtonPressed(_ sender: Any) {
        
        let consentKey1IsOn = consentKey1Switch.isOn
        let consentKey2IsOn = consentKey2Switch.isOn
        let analyticsConsentKeyIsOn = analyticsConsentKey.isOn
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .auth
            .signup()
            .withCopilotAnalysisConsent(analyticsConsentKeyIsOn)
            .withCustomConsent(Constants.consentKey1Name, value: consentKey1IsOn)
            .withCustomConsent(Constants.consentKey2Name, value: consentKey2IsOn)
            .anonymously
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:{
                    self?.hideLoadingView() { [weak self] in
                        switch response {
                        case .success:
                            self?.showAlert("Anonymous registration succeeded")
                            
                        case .failure(error: let registerAnonymousError):
                            let msg: String
                            
                            switch registerAnonymousError {
                            case .connectivityError(let debugMessage):
                                msg = "Connectivity: \(debugMessage)"
                            case .generalError(let debugMessage):
                                msg = "General: \(debugMessage)"
                            case .invalidApplicationId(let debugMessage):
                                msg = "Invalid Application Id: \(debugMessage)"
                            case .invalidParameters(let debugMessage):
                                msg = "invalid Parameters: \(debugMessage)"
                            }
                            
                            self?.showAlert("Anonymous registration failed with error:\n\(msg)\n\n\(registerAnonymousError.localizedDescription)")
                        }
                    }
                })
        }
    }
    
}
