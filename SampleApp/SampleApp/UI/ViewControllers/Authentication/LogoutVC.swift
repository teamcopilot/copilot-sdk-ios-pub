//
//  LogoutVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 11/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class LogoutVC: UIViewController {

    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Logout"
    }

    //MARK: - IBActions
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .auth
            .logout()
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success:
                                self?.showAlert("Logout succeeded")
                                
                            case .failure(let logoutError):
                                let msg: String
                                
                                switch logoutError {
                                case .internalError(let error):
                                    msg = "Internal: \(error.localizedDescription)"
                                }
                                
                                self?.showAlert("Logout failed with error:\n\(msg)\n\n\(logoutError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
