//
//  EmailAndPasswordRegistrationVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 11/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess


class EmailAndPasswordRegistrationVC: KeyboardHandlerVC {
    
    //MARK: - Properties
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var consentKey1Switch: UISwitch!
    @IBOutlet weak var consentKey2Switch: UISwitch!
    @IBOutlet weak var analyticsConsentKey: UISwitch!
	@IBOutlet weak var deleteUserButton: UIButton!
	
	//MARK: - Lifecycle
	lazy var loginTestButton: UIButton = {
		let bt = UIButton(type: .system)
		bt.translatesAutoresizingMaskIntoConstraints = false
		bt.setTitle("Login with wrong email and password", for: .normal)
		bt.setTitleColor(.white, for: .normal)
        let inset: CGFloat = 10
        bt.contentEdgeInsets = .init(top: inset, left: inset, bottom: inset, right: inset)
		bt.backgroundColor = .black
		bt.layer.cornerRadius = 8
		bt.addTarget(self, action: #selector(handleLoginTestd), for: .touchUpInside)
		return bt
	}()
    
    override func viewDidLoad() {
        super.viewDidLoad()
		view.addSubview(loginTestButton)
		
		[
			loginTestButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			loginTestButton.topAnchor.constraint(equalTo: deleteUserButton.bottomAnchor, constant: 15),
		].forEach { $0.isActive = true }
		
        self.title = "Email and password registration"
        
        let randomInt = Int.random(in: 1...9999999)
        
        emailTextField.text = "testemail" + String(randomInt) + "@grr.la"
        passwordTextField.text = "Aa123456"
        firstNameTextField.text = "John"
        lastNameTextField.text = "Doe"
    }
    
    //MARK: - IBActions
    
    @IBAction func signupButtonPressed(_ sender: Any) {
        guard let email = emailTextField.text,
            let password = passwordTextField.text,
            let firstName = firstNameTextField.text,
            let lastName = lastNameTextField.text else {
                showAlert("All fields should be filled")
                return
        }
        
        let consentKey1IsOn = consentKey1Switch.isOn
        let consentKey2IsOn = consentKey2Switch.isOn
        let analyticsConsentKeyIsOn = analyticsConsentKey.isOn
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .auth
            .signup()
            .withCopilotAnalysisConsent(analyticsConsentKeyIsOn)
            .withCustomConsent(Constants.consentKey1Name, value: consentKey1IsOn)
            .withCustomConsent(Constants.consentKey2Name, value: consentKey2IsOn)
            .with(email: email, password: password, firstname: firstName, lastname: lastName)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success:
                                self?.showAlert("Signup succeeded")
                                
                            case .failure(error: let registerError):
                                let msg: String
                                
                                switch registerError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .userAlreadyExists(let debugMessage):
                                    msg = "User Already Exists: \(debugMessage)"
                                case .passwordPolicyViolation(let debugMessage):
                                    msg = "Password Policy Violation: \(debugMessage)"
                                case .invalidApplicationId(let debugMessage):
                                    msg = "Invalid Application Id: \(debugMessage)"
                                case .invalidEmail(let debugMessage):
                                    msg = "invalid Email: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "invalid Parameters: \(debugMessage)"
                                }
                                self?.showAlert("Signup failed with error:\n\(msg)\n\n\(registerError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
    @IBAction func signupWithoutProvidingConsentPressed(_ sender: Any) {
        guard let email = emailTextField.text,
            let password = passwordTextField.text,
            let firstName = firstNameTextField.text,
            let lastName = lastNameTextField.text else {
                showAlert("All fields should be filled")
                return
        }
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .auth
            .signup()
            .withNoGDPRConsentRequired
            .with(email: email, password: password, firstname: firstName, lastname: lastName)
            .build().execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success:
                                self?.showAlert("Signup succeeded")
                                
                            case .failure(error: let registerError):
                                let msg: String
                                
                                switch registerError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .userAlreadyExists(let debugMessage):
                                    msg = "User Already Exists: \(debugMessage)"
                                case .passwordPolicyViolation(let debugMessage):
                                    msg = "Password Policy Violation: \(debugMessage)"
                                case .invalidApplicationId(let debugMessage):
                                    msg = "Invalid Application Id: \(debugMessage)"
                                case .invalidEmail(let debugMessage):
                                    msg = "invalid Email: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "invalid Parameters: \(debugMessage)"
                                }
                                self?.showAlert("Signup failed with error:\n\(msg)\n\n\(registerError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
	@IBAction func DeleteUser(_ sender: Any) {
        Copilot.instance
                .manage
                .sphere
                .user
                .deleteMe()
                .build().execute { response in
            DispatchQueue.main.async {
                switch response {
                case .success:
                    self.showAlert("User deleted successfully")
                case .failure(let error):
                    self.showAlert("Delete user failed with error:\n\(error.localizedDescription)")
                }
            }
        }
    }
	
	@objc fileprivate func handleLoginTestd() {
        let wrongPassword = "123"
        let email = emailTextField.text ?? ""
		Copilot.instance.manage.sphere.auth.login().with(email: email, password: wrongPassword).build().execute { response in
            DispatchQueue.main.async {
                switch (response) {
                case .failure(error: let error):
                    switch (error) {
                    case .accountSuspended:
                        self.showAlert("Account suspended")
                    default:
                        self.showAlert("Login failed")
                    }
                case .success():
                    self.showAlert("login success")
                }
            }
		}
	}
}
