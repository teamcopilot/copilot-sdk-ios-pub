//
//  PredefinedEventsVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess


fileprivate protocol Event {
    var title: String { get }
    var analyticsEvent: AnalyticsEvent { get }
}

class PredefinedEventsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    private var datasource = PredefinedEvents.allCases
    
    private enum PredefinedEvents: Int, CaseIterable {
        case accountCreationEvents
        case userActions
        case onboardingEvents
        case thingEvents
        case generalEvents
        
        var title: String {
            switch self {
            case .accountCreationEvents:
                return "Account Creation Events"
            case .userActions:
                return "User Actions"
            case .onboardingEvents:
                return "Onboarding Events"
            case .thingEvents:
                return "Thing Events"
            case .generalEvents:
                return "General Events"
            }
        }
        
        var events: [Event] {
            switch self {
            case .accountCreationEvents:
                return AccountCreationEvents.allCases
            case .userActions:
                return UserActions.allCases
            case .onboardingEvents:
                return OnboardingEvents.allCases
            case .thingEvents:
                return ThingEvents.allCases
            case .generalEvents:
                return GeneralEvents.allCases
            }
        }
    }
    
    private enum AccountCreationEvents: String, Event, CaseIterable {
        case signup = "Sign up"
        case login = "Login"
        case logout = "Logout"
        case successfulElevateAnonymous = "Successful Elevate Anonymous"
        
        var title: String {
            return rawValue
        }
        
        var analyticsEvent: AnalyticsEvent {
            switch self {
            case .signup:
                return SignupAnalyticsEvent()
            case .login:
                return LoginAnalyticsEvent()
            case .logout:
                return LogoutAnalyticsEvent()
            case .successfulElevateAnonymous:
                return SuccessfulElevateAnonymousAnalyticEvent()
            }
        }
    }
    
    private enum UserActions: String, Event, CaseIterable {
        case acceptTermsOfUse = "Accept Terms of use"
        
        var title: String {
            return rawValue
        }
        
        var analyticsEvent: AnalyticsEvent {
            switch self {
            case .acceptTermsOfUse:
                return AcceptTermsAnalyticsEvent(version: "1.7")
            }
        }
    }
    
    private enum OnboardingEvents: String, Event, CaseIterable {
        case onboardingStarted = "Onboarding Started"
        case onboardingEnded = "Onboarding Ended"
        
        var title: String {
            return rawValue
        }
        
        var analyticsEvent: AnalyticsEvent {
            switch self {
            case .onboardingStarted:
                return OnboardingStartedAnalayticsEvent()
            case .onboardingEnded:
                return OnboardingEndedAnalyticsEvent()
            }
        }
    }
    
    private enum ThingEvents: String, Event, CaseIterable {
        case tapConnectDevice = "Tap connect device"
        case thingConnected = "Thing connected"
        case thingDiscovered = "Thing discovered"
        case thingInfo = "Thing info"
        case thingConnectionFailed = "Thing Connection Failed"
        case consumableUsage = "Consumable Usage"
        case consumableDepleted = "Consumable Depleted"
        
        var title: String {
            return rawValue
        }
        
        var analyticsEvent: AnalyticsEvent {
            switch self {
            case .tapConnectDevice:
                return TapConnectDeviceAnalyticsEvent()
            case .thingConnected:
                return ThingConnectedAnalyticsEvent(thingID: "33:22:1B:23:6A")
            case .thingDiscovered:
                return ThingDiscoveredAnalyticsEvent(thingID: "33:22:1B:23:6A")
            case .thingInfo:
                return ThingInfoAnalyticsEvent(thingFirmware: "1.0.22", thingModel: "Spaceship - Rev(A)", thingId: "33:22:1B:23:6A")
            case .thingConnectionFailed:
                return ThingConnectionFailedAnalyticsEvent(failureReason: "Timeout")
            case .consumableUsage:
                return ConsumableUsageAnalyticsEvent(thingId: "33:22:1B:23:6A", consumableType: "ink", screenName: "PrintingScreen")
            case .consumableDepleted:
                return ConsumableDepletedAnalyticsEvent(thingId: "33:22:1B:23:6A", consumableType: "ink", screenName: "PrintingScreen")
            }
        }
    }
    
    private enum GeneralEvents: String, Event, CaseIterable {
        case screenLoad = "Screen Load"
        case tapMenu = "Tap Menu"
        case tapMenuItem = "Tap Menu Item"
        case firmwareUpgradeStarted = "Firmware upgrade started"
        case firmwareUpgradeCompleted = "Firmware upgrade completed"
        case errorReport = "Error Report"
        case contactSupport = "Contact support"
        
        var title: String {
            return rawValue
        }
        
        var analyticsEvent: AnalyticsEvent {
            switch self {
            case .screenLoad:
                return ScreenLoadAnalyticsEvent(screenName: "Predefined Events Sample App")
            case .tapMenu:
                return TapMenuAnalyticsEvent()
            case .tapMenuItem:
                return TapMenuItemAnalyticsEvent(menuItem: "Settings")
            case .firmwareUpgradeStarted:
                return FirmwareUpgradeStartedAnalyticsEvent()
            case .firmwareUpgradeCompleted:
                return FirmwareUpgradeCompletedAnalyticsEvent(firmwareUpgradeStatus: .Success)
            case .errorReport:
                return ErrorAnalyticsEvent(errorType: "CustomError")
            case .contactSupport:
                return ContactSupportAnalyticsEvent(supportCase: "I need help", thingId: "33:22:1B:23:6A", screenName: "SupportScreen")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Predefined Events"
    }

}

extension PredefinedEventsVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource[section].events.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return datasource[section].title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "predefinedEventTableViewCell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = datasource[indexPath.section].events[indexPath.row].title
        
        return cell
    }
    
}

extension PredefinedEventsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        guard let eventCategory = PredefinedEvents(rawValue: indexPath.section) else {
            print("PredefinedEventsVC - Failed to create component section: \(indexPath.section)")
            return
        }
        
        guard eventCategory.events.count >= indexPath.row else {
            print("PredefinedEventsVC - Not valid index row: \(indexPath.row)")
            return
        }
        
        let analyticsEvent = eventCategory.events[indexPath.row].analyticsEvent
        Copilot.instance.report.log(event: analyticsEvent)
    }
    
}
