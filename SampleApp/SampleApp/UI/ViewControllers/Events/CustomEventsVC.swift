//
//  CustomEventsVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class CustomEventsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Custom Events"
    }
    
    @IBAction func customEvent1ButtonPressed(_ sender: Any) {
        Copilot.instance.report.log(event: CustomAnalyticsEvent(eventName: "Custom_Event_1", customParams: [
            "customFieldName1": "Custom Event 1",
            "customFieldName2": "Custom Event 2"
        ]))
    }
    
    @IBAction func customEvent2ButtonPressed(_ sender: Any) {
        Copilot.instance.report.log(event: CustomAnalyticsEvent(eventName: "Custom_Event_2", customParams: [
            "smartDeviceId": "1234567890"
        ]))
    }
}
