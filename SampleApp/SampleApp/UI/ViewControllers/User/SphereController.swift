//
// Created by Alex Gold on 14/02/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class SphereController: UIViewController {

	lazy var input: UITextField = {
		let input = UITextField()
		input.borderStyle = .roundedRect
		input.translatesAutoresizingMaskIntoConstraints = false
		return input
	}()

	lazy var button: UIButton = {
		let bt = UIButton(type: .system)
		bt.setTitle("Get Ticket", for: .normal)
		bt.addTarget(self, action: #selector(handleButton), for: .touchUpInside)
		bt.translatesAutoresizingMaskIntoConstraints = false
		return bt
	}()

	lazy var responseLabel: UILabel = {
		let lb = UILabel()
		lb.numberOfLines = 0
		lb.translatesAutoresizingMaskIntoConstraints = false
		return lb
	}()

	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = .white
		
		navigationItem.title = "Sphere"
		
		let stackView = UIStackView(arrangedSubviews: [input, button, responseLabel])
		stackView.translatesAutoresizingMaskIntoConstraints = false
		stackView.axis = .vertical

		view.addSubview(stackView)
		[
			stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
			stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 36),
			stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			responseLabel.heightAnchor.constraint(equalToConstant: 260)
		].forEach { $0.isActive = true }
	}

	@objc fileprivate func handleButton() {
		let partnerId = input.text ?? ""
		Copilot.instance.manage
				.sphere
				.user
				.getPartnerTicket(partnerId: partnerId)
				.build()
				.execute { response in
					print("response: \(response)")
					switch response {
					case .success(let response):
						self.setLabel(response.partnerTicket)
						break
					case .failure(error: let error):
						switch error {
						case .InvalidPartnerId(let message):
							print("message: \(message)")
						case .ConnectivityError(let message):
							print("message: \(message)")
						default:
							print("sdf")
						}
						self.setLabel("Failure: \(error)")
						print("Error: \(error)")
					}
				}
	}

	fileprivate func setLabel(_ text: String) {
		DispatchQueue.main.async {
			self.responseLabel.text = text
		}
	}
}

