//
//  FetchUserDetailsVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class FetchUserDetailsVC: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var fetchUserResultLabel: UILabel!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Fetch user details"
    }
    
    //MARK: - IBActions
    
    @IBAction func fetchUserDetailsButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .user
            .fetchMe()
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let userMe):
                                self?.showAlert("Fetch user details succeeded")
                                
                                var consetsKeysString = ""
                                
                                if let consentsKeys = userMe.accountStatus.consentsKeys {
                                    consentsKeys.forEach{
                                        consetsKeysString += "\($0), "
                                    }
                                }
                                
                                var customSettingsKeysString = ""
                                
                                if let customSettinsKeys = userMe.userDetails.customSettingsKeys {
                                    customSettinsKeys.forEach{
                                        customSettingsKeysString += "\($0), "
                                    }
                                } else {
                                    customSettingsKeysString = "No custom settings keys"
                                }
                                
                                let msg = "Email: \(userMe.userDetails.email ?? "")" + "\n" +
                                    "First name: \(userMe.userDetails.userInfo?.firstName ?? "")" + "\n" +
                                    "Last name: \(userMe.userDetails.userInfo?.lastName ?? "")" + "\n" +
                                    "Email verification status: \(userMe.accountStatus.emailVerificationStatus)" + "\n" +
                                    "User analysis consent: \(userMe.accountStatus.copilotUserAnalysisConsent)" + "\n" +
                                    "Terms of use approved: \(userMe.accountStatus.termsOfUseApproved)" + "\n" +
                                    "Consents Keys: \(consetsKeysString)" + "\n" +
                                    "Custom settins keys: \(customSettingsKeysString)" + "\n"
                                self?.fetchUserResultLabel.text = msg
                                
                            case .failure(let fetchMeError):
                                let msg: String
                                
                                switch fetchMeError {
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                }
                                
                                self?.showAlert("Fetch user details failed with error: \(msg)\n\n\(fetchMeError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
