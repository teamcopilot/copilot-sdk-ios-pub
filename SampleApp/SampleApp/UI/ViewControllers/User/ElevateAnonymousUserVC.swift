//
//  ElevateAnonymousUserVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class ElevateAnonymousUserVC: KeyboardHandlerVC {
    
    //MARK: - Properties
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Elevate anonymous user"
        
        view.hideKeyboardWhenTapped()
        
        let randomInt = Int.random(in: 1...9999999)
        
        emailTextField.text = "testemail" + String(randomInt) + "@grr.la"
        passwordTextField.text = "Aa123456"
        firstNameTextField.text = "John"
        lastNameTextField.text = "Doe"
    }
    
    //MARK: - IBActions
    
    @IBAction func elevateButtonPressed(_ sender: Any) {
        guard let email = emailTextField.text,
            let password = passwordTextField.text,
            let firstName = firstNameTextField.text,
            let lastName = lastNameTextField.text else {
                showAlert("All fields should be filled")
                return
        }
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .user
            .elevate()
            .with(email: email, password: password, firstname: firstName, lastname: lastName)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success:
                                self?.showAlert("Elevate succeeded")
                                
                            case .failure(let elevateError):
                                let msg: String
                                
                                switch elevateError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "Invalid Parameters: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .cannotElevateANonAnonymousUser(let debugMessage):
                                    msg = "Cannot Elevate A Non Anonymous User: \(debugMessage)"
                                case .userAlreadyExists(let debugMessage):
                                    msg = "User Already Exists: \(debugMessage)"
                                case .passwordPolicyViolation(let debugMessage):
                                    msg = "Password Policy Violation: \(debugMessage)"
                                case .invalidEmail(let debugMessage):
                                    msg = "Invalid Email: \(debugMessage)"
                                }
                                
                                self?.showAlert("Elevate failed with error: \(msg)\n\n\(elevateError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
