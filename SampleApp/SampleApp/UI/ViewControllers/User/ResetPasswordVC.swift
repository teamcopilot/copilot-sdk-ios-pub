//
//  ResetPasswordVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class ResetPasswordVC: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var emailTextField: UITextField!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Reset password"
    }
    
    //MARK: - IBActions
    
    @IBAction func resetPasswordButtonPressed(_ sender: Any) {
        guard let email = emailTextField.text else {
            showAlert("All fields should be filled")
            return
        }
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .user
            .resetPassword(for: email)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(_):
                                self?.showAlert("Reset password succeeded")
                                
                            case .failure(let resetPasswordError):
                                let msg: String
                                
                                switch resetPasswordError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "Invalid Parameters: \(debugMessage)"
                                case .emailIsNotVerified(let debugMessage):
                                    msg = "Email Is Not Verified: \(debugMessage)"
                                }
                                
                                self?.showAlert("Reset password failed with error: \(msg)\n\n\(resetPasswordError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
