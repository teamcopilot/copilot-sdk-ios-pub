//
// Created by Alex Gold on 12/07/2023.
// Copyright (c) 2023 Copilot.cx. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class ChangeEmailController: UIViewController {

	lazy var input: UITextField = {
		let input = UITextField()
		input.borderStyle = .roundedRect
		input.keyboardType = .emailAddress
        input.placeholder = "New email"
        input.autocorrectionType = .no
        input.autocapitalizationType = .none
		input.translatesAutoresizingMaskIntoConstraints = false
		return input
	}()

	lazy var button: UIButton = {
		let bt = UIButton(type: .system)
		bt.setTitle("Change email", for: .normal)
		bt.addTarget(self, action: #selector(handleButton), for: .touchUpInside)
		bt.translatesAutoresizingMaskIntoConstraints = false
		return bt
	}()

	override func viewDidLoad() {
		view.backgroundColor = .white

		navigationItem.title = "Change email"

		let stackView = UIStackView(arrangedSubviews: [input, button])
		stackView.axis = .vertical
		stackView.translatesAutoresizingMaskIntoConstraints = false
		stackView.spacing = 10

		view.addSubview(stackView)
		[
			stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
			stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 36),
			stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
		].forEach {
			$0.isActive = true
		}
	}

	//write a swift function that will show a popup dialog that says "Email changed successfully"

	@objc fileprivate func handleButton(button: UIButton) {
		if let email = input.text {
			Copilot.instance
					.manage
					.sphere
					.user
					.updateMe()
					.withEmail(email)
					.build()
					.execute { response in
						switch response {
						case .success(let user):
							DispatchQueue.main.async {
								self.showAlert("Email changed successfully to \(user.userDetails.email)")
							}
						case .failure(error: let error):
							switch error {
							case .invalidEmail(debugMessage: let debugMessage):
								break
							case .emailAlreadyExists(debugMessage: let debugMessage):
								break
							case .requiresRelogin(debugMessage: let debugMessage):
								break
							case .connectivityError(debugMessage: let debugMessage):
								break
							case .generalError(debugMessage: let debugMessage):
								break
							case .emailCannotBeChangedForAnonymousUser(debugMessage: let debugMessage):
								break
							}
							DispatchQueue.main.async {
								self.showAlert("Error: \(error.localizedDescription)")
							}
							print("Error: \(error)")
						}
					}
		}

	}
}
