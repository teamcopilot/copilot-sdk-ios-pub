//
//  SendVerificationEmailVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class SendVerificationEmailVC: UIViewController {
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Send verification email"
    }
    
    //MARK: - IBActions
    
    //TODO: getMe for verification status
    @IBAction func sendEmailVerificationButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .user
            .sendVerificationEmail()
            .build()
            .execute{ [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response{
                            case .success():
                                self?.showAlert("Send email verification succeeded")
                                
                            case .failure(let sendEmailVerificationError):
                                let msg: String
                                
                                switch sendEmailVerificationError {
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .retryRequired(let retryInSeconds, let debugMessage):
                                    msg = "Retry Required: \(debugMessage)\nRetry In Seconds: \(retryInSeconds)"
                                case .userAlreadyVerified(let debugMessage):
                                    msg = "User Already Verified: \(debugMessage)"
                                }
                                
                                self?.showAlert("Send email verification failed with error: \(msg)\n\n\(sendEmailVerificationError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
