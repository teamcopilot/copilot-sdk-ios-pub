//
//  UpdateUserDetailsVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 15/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class UpdateUserDetailsVC: KeyboardHandlerVC {
    
    //MARK: - Properties
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var boolCustomValueSwitch: UISwitch!
    @IBOutlet weak var stringCustomValueSwitch: UISwitch!
    @IBOutlet weak var intCustomValueSwitch: UISwitch!
    @IBOutlet weak var objectCustomValueSwitch: UISwitch!
    @IBOutlet weak var collectionCustomValueSwitch: UISwitch!
    
    private var updateMe: UpdateMeDetailsRequestBuilderType?
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Update user details"
    }

    //MARK: - IBActions
    
    @IBAction func updateUserDetailsButtonPressed(_ sender: Any) {
        guard let firstName = firstNameTextField.text,
            let lastName = lastNameTextField.text else {
            return
        }
        
        showLoadingView()
        
        updateMe = Copilot.instance
            .manage
            .sphere
            .user
            .updateMe()
            .with(firstname: firstName)
            .with(lastname: lastName)
        updateCustomValue(bySwitch: boolCustomValueSwitch, value: Constants.userBoolCustomValue.value, forKey: Constants.userBoolCustomValue.key)
        updateCustomValue(bySwitch: stringCustomValueSwitch, value: Constants.userStringCustomValue.value, forKey: Constants.userStringCustomValue.key)
        updateCustomValue(bySwitch: intCustomValueSwitch, value: Constants.userIntCustomValue.value, forKey: Constants.userIntCustomValue.key)
        updateCustomValue(bySwitch: objectCustomValueSwitch, value: Constants.userObjectCustomValue.value, forKey: Constants.userObjectCustomValue.key)
        updateCustomValue(bySwitch: collectionCustomValueSwitch, value: Constants.userCollectionCustomValue.value, forKey: Constants.userCollectionCustomValue.key)
        updateMe?
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let userMe):
                                self?.showAlert("Update me succeeded with id: \(userMe.userDetails.id)")
                                
                            case .failure(let updateMeError):
                                let msg: String
                                
                                switch updateMeError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "Invalid Parameters: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                }
                                
                                self?.showAlert("Update me failed with error:\n\(msg)\n\n\(updateMeError.localizedDescription)")
                            }
                        }
                })
        }
        
    }
    
    @IBAction func fetchUserButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .user
            .fetchMe()
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let userMe):
                                if let firstName = userMe.userDetails.userInfo?.firstName {
                                    self?.firstNameTextField.text = firstName
                                }
                                if let lastName = userMe.userDetails.userInfo?.lastName {
                                    self?.lastNameTextField.text = lastName
                                }
                                
                                self?.boolCustomValueSwitch.isOn = userMe.userDetails.get(key: Constants.userBoolCustomValue.key, as: Bool.self) != nil
                                self?.stringCustomValueSwitch.isOn = userMe.userDetails.get(key: Constants.userStringCustomValue.key, as: String.self) != nil
                                self?.intCustomValueSwitch.isOn = userMe.userDetails.get(key: Constants.userIntCustomValue.key, as: Int.self) != nil
                                self?.objectCustomValueSwitch.isOn = userMe.userDetails.get(key: Constants.userObjectCustomValue.key, as: MyCustomObject.self) != nil
                                self?.collectionCustomValueSwitch.isOn = userMe.userDetails.get(key: Constants.userCollectionCustomValue.key, as: [MyCustomObject].self) != nil
                                
                            case .failure(let fetchMeError):
                                let msg: String
                                
                                switch fetchMeError {
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                }
                                
                                self?.showAlert("Fetch user details failed with error: \(msg)\n\n\(fetchMeError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
    @IBAction func updateUserName100TimesPressed(_ sender: UIButton) {
        guard let firstName = firstNameTextField.text, !firstName.isEmpty else { return }
        
        showLoadingView()
        
        for index in 1 ... 100 {
            
            
            updateMe = Copilot.instance
                .manage
                .sphere
                .user
                .updateMe()
                .with(firstname: "\(firstName) \(index)")
            updateCustomValue(bySwitch: boolCustomValueSwitch, value: Constants.userBoolCustomValue.value, forKey: Constants.userBoolCustomValue.key)
            updateCustomValue(bySwitch: stringCustomValueSwitch, value: Constants.userStringCustomValue.value, forKey: Constants.userStringCustomValue.key)
            updateCustomValue(bySwitch: intCustomValueSwitch, value: Constants.userIntCustomValue.value, forKey: Constants.userIntCustomValue.key)
            updateCustomValue(bySwitch: objectCustomValueSwitch, value: Constants.userObjectCustomValue.value, forKey: Constants.userObjectCustomValue.key)
            updateCustomValue(bySwitch: collectionCustomValueSwitch, value: Constants.userCollectionCustomValue.value, forKey: Constants.userCollectionCustomValue.key)
            updateMe?
                .build()
                .execute { [weak self] (response) in
                    DispatchQueue.main.async(execute:
                        {
                            self?.hideLoadingView() { [weak self] in
                                switch response {
                                case .success(let userMe):
                                    self?.showAlert("Update me succeeded with id: \(userMe.userDetails.id)")
                                    
                                case .failure(let updateMeError):
                                    let msg: String
                                    
                                    switch updateMeError {
                                    case .connectivityError(let debugMessage):
                                        msg = "Connectivity: \(debugMessage)"
                                    case .generalError(let debugMessage):
                                        msg = "General: \(debugMessage)"
                                    case .invalidParameters(let debugMessage):
                                        msg = "Invalid Parameters: \(debugMessage)"
                                    case .requiresRelogin(let debugMessage):
                                        msg = "Requires Relogin: \(debugMessage)"
                                    }
                                    
                                    self?.showAlert("Update me failed with error:\n\(msg)\n\n\(updateMeError.localizedDescription)")
                                }
                            }
                    })
            }
        }
    }
    
    private func updateCustomValue<T: Encodable>(bySwitch customValueSwitch: UISwitch, value: T,  forKey key: String) {
        if customValueSwitch.isOn {
            _ = updateMe?.with(customValue: value, forKey: key)
        } else {
            _ = updateMe?.removeCustomValue(forKey: key)
        }
    }
    
}
