//
//  ChangePasswordVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 15/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class ChangePasswordVC: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Change password"
        
        view.hideKeyboardWhenTapped()
    }
    
    //MARK: - IBActions
    
    @IBAction func changePasswordButtonPressed(_ sender: Any) {
        guard let oldPassword = oldPasswordTextField.text,
            let newPassword = newPasswordTextField.text else {
                showAlert("All fields should be filled")
                return
        }
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .user
            .updateMe()
            .withNewPassword(newPassword, verifyWithOldPassword: oldPassword)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success:
                                self?.showAlert("Change password succeeded")
                                
                            case .failure(let changePasswordError):
                                let msg: String
                                
                                switch changePasswordError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "invalid Parameters: \(debugMessage)"
                                case .invalidCredentials(let debugMessage):
                                    msg = "invalid Credentials: \(debugMessage)"
                                case .passwordPolicyViolation(let debugMessage):
                                    msg = "Password Policy Violation: \(debugMessage)"
                                case .cannotChangePasswordToAnonymousUser(let debugMessage):
                                    msg = "Cannot Change Password To Anonymous User: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                }
                                
                                self?.showAlert("Change password failed with error:\n\(msg)\n\n\(changePasswordError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
