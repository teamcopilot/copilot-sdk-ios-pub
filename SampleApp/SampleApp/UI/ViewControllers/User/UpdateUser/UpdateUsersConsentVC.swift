//
//  UpdateUsersConsentVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 15/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class UpdateUsersConsentVC: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var consentKey1Switch: UISwitch!
    @IBOutlet weak var consentKey2Switch: UISwitch!
    @IBOutlet weak var analyticsConsentKey: UISwitch!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Update user's consent"
    }
    
    //MARK: - IBActions
    
    @IBAction func updateUsersConsentButtonPressed(_ sender: Any) {
        let consentKey1IsOn = consentKey1Switch.isOn
        let consentKey2IsOn = consentKey2Switch.isOn
        let analyticsConsentKeyIsOn = analyticsConsentKey.isOn
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .user
            .updateMe()
            .allowCopilotUserAnalysis(analyticsConsentKeyIsOn)
            .withCustomConsent(Constants.consentKey1Name, value: consentKey1IsOn)
            .withCustomConsent(Constants.consentKey2Name, value: consentKey2IsOn)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success:
                                self?.showAlert("Update users consent succeeded")
                                
                            case .failure(let updateUsersConsentError):
                                let msg: String
                                
                                switch updateUsersConsentError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "invalid Parameters: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                }
                                
                                self?.showAlert("Update users consent failed with error:\n\(msg)\n\n\(updateUsersConsentError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
    @IBAction func markForDeletionButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .auth
            .logout()
            .markForDeletion
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success:
                                self?.showAlert("Mark for deletion succeeded")
                                
                            case .failure(let updateUsersConsentError):
                                let msg: String
                                
                                switch updateUsersConsentError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                }
                                
                                self?.showAlert("Mark for deletion failed with error:\n\(msg)\n\n\(updateUsersConsentError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
