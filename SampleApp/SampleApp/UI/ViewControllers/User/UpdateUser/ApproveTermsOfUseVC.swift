//
//  ApproveTermsOfUseVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 15/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class ApproveTermsOfUseVC: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var termsOfUseVersionTextField: UITextField!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Approve terms of use"
        
        view.hideKeyboardWhenTapped()
    }
    
    //MARK: - IBActions
    
    @IBAction func fetchTOUVersionButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .app
            .fetchConfig()
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let configuration):
                                self?.showAlert("Fetch configuration succeeded")
                                
                                if let termsOfUseVersion = configuration.termsAndConditions?.version {
                                    self?.termsOfUseVersionTextField.text = termsOfUseVersion
                                } else {
                                    self?.showAlert("Failed to get terms of use version from configuration")
                                }
                                
                            case .failure(let fetchConfigurationError):
                                let msg: String
                                
                                switch fetchConfigurationError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                }
                                
                                self?.showAlert("Fetch configuration failed with error:\n\(msg)\n\n\(fetchConfigurationError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
    @IBAction func approveTOUButtonPressed(_ sender: Any) {
        guard let touVersion = termsOfUseVersionTextField.text else {
            showAlert("All fields should be filled")
            return
        }
        
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .user
            .updateMe()
            .approveTermsOfUse(forVersion: touVersion)
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success:
                                self?.showAlert("Approve terms of use succeeded")
                                
                            case .failure(let approveTermsOfUseError):
                                let msg: String
                                
                                switch approveTermsOfUseError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .invalidParameters(let debugMessage):
                                    msg = "Invalid Parameters: \(debugMessage)"
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                }
                                
                                self?.showAlert("Approve terms of use failed with error:\n\(msg)\n\n\(approveTermsOfUseError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
