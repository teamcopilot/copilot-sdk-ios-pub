//
//  InAppMessagesViewController.swift
//  SampleApp
//
//  Created by Elad on 11/02/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess


fileprivate protocol Event {
    var title: String { get }
    var analyticsEvent: AnalyticsEvent { get }
}

class InAppMessagesVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    
    
    private var datasource = PredefinedEvents.allCases
    
    private enum PredefinedEvents: Int, CaseIterable {
        case accountCreationEvents
        case onboardingEvents
        case actions
        case inAppMessagesActions
        
        var title: String {
            switch self {
            case .accountCreationEvents:
                return "Account Creation Events"
            case .onboardingEvents:
                return "Onboarding Events"
            case .actions:
                return "actions"
            case .inAppMessagesActions:
                return "In app messages actions"
            }
        }

        var events: [Event] {
            switch self {
            case .accountCreationEvents:
                return AccountCreationEvents.allCases
            case .onboardingEvents:
                return OnboardingEvents.allCases
            case .actions, .inAppMessagesActions:
                return []
            }
        }
        
        var actions: [String] {
            switch self {
            case .accountCreationEvents, .onboardingEvents:
                return []
            case .actions:
                return ["popup", "actionSheet", "share"]
            case .inAppMessagesActions:
                return ["Enable", "Disable"]
            }
        }
    }
    
    private enum AccountCreationEvents: String, Event, CaseIterable {
        case signup = "Sign up"
        
        var title: String {
            return rawValue
        }
        
        var analyticsEvent: AnalyticsEvent {
            switch self {
            case .signup:
                return SignupAnalyticsEvent()
            }
        }
    }
    
    private enum OnboardingEvents: String, Event, CaseIterable {
        case onboardingStarted = "Onboarding Started"
        case onboardingEnded = "Onboarding Ended"
        case schduleOnBoardingEnded = "Schedule on boarding ended"
        
        var title: String {
            return rawValue
        }
        
        var analyticsEvent: AnalyticsEvent {
            switch self {
            case .onboardingStarted:
                return OnboardingStartedAnalayticsEvent()
            case .onboardingEnded:
                return OnboardingEndedAnalyticsEvent()
            case .schduleOnBoardingEnded:
                return OnboardingEndedAnalyticsEvent()
            }
        }
    }
    
    private enum Actions: String, CaseIterable {
        case showAlert = "show alert"
        case showShareScreen = "show share screen"
        
        var title: String {
            return rawValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "In App Messages"
    }
    @IBAction func dismissKeyboardPressed(_ sender: UIButton) {
        textField.resignFirstResponder()
    }
}

extension InAppMessagesVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0, 1://simulate analytics events
            guard let eventCategory = PredefinedEvents(rawValue: indexPath.section) else {
                print("PredefinedEventsVC - Failed to create component section: \(indexPath.section)")
                return
            }
            
            guard eventCategory.events.count >= indexPath.row else {
                print("PredefinedEventsVC - Not valid index row: \(indexPath.row)")
                return
            }
            
            let analyticsEvent = eventCategory.events[indexPath.row].analyticsEvent
            if indexPath.row == 2 {//schedule event
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    Copilot.instance.report.log(event: analyticsEvent)
                }
            } else {
                Copilot.instance.report.log(event: analyticsEvent)
            }
            
        case 2://some application actions
            if indexPath.row == 0 {//alert
                let someSimpleAlert = UIAlertController(title: "title", message: "popup message", preferredStyle: .alert)
                someSimpleAlert.addAction(UIAlertAction(title: "dismiss", style: .default, handler: nil))
                present(someSimpleAlert, animated: true, completion: nil)
            } else if indexPath.row == 1 {//actionSheet
                let someSimpleAlert = UIAlertController(title: "title", message: "actionSheet message", preferredStyle: .actionSheet)
                someSimpleAlert.addAction(UIAlertAction(title: "dismiss", style: .default, handler: nil))
                present(someSimpleAlert, animated: true, completion: nil)
            } else if indexPath.row == 2 {//uiactivity view controller
                let items = ["test share alert"]
                let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                present(ac, animated: true)
            }
        case 3:
            if indexPath.row == 0 {//enable iam
                Copilot.instance.inAppMessages.enable()
            } else if indexPath.row == 1 {//disable iam
                Copilot.instance.inAppMessages.disable()
            }
        default:
            print("should not get in here")
        }
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension InAppMessagesVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0...1:
            return datasource[section].events.count
        default:
            return datasource[section].actions.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return datasource[section].title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "predefinedEventTableViewCell", for: indexPath) as UITableViewCell
        
        if indexPath.section == 0 || indexPath.section == 1 {
            cell.textLabel?.text = datasource[indexPath.section].events[indexPath.row].title.uppercased()
        } else {
            cell.textLabel?.text = datasource[indexPath.section].actions[indexPath.row].uppercased()
        }
        
        
        return cell
    }
    
}
