//
//  InAppMessagesExtendedTriggersVC.swift
//  SampleApp
//
//  Created by Elad on 27/04/2021.
//  Copyright © 2021 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess


class InAppMessagesExtendedTriggersVC: UIViewController {
    
    private enum PredefinedEvents: Int, CaseIterable {
        case signup
        case onboardingStarted
        case onboardingEnded
        
        var title: String {
            switch self {
            case .signup:
                return "sign_up"
            case .onboardingStarted:
                return "onboarding_started"
            case .onboardingEnded:
                return "onboarding_ended"
            }
        }
    }
    
    //MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var eventNameTF: UITextField!
    @IBOutlet weak var screenNameTF: UITextField!
    @IBOutlet weak var someNumberTF: UITextField!
    @IBOutlet weak var anotherNumberTF: UITextField!
    @IBOutlet weak var customKeyTF: UITextField!
    @IBOutlet weak var customValueTF: UITextField!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    let underKeyboardLayoutConstraint = UnderKeyboardLayoutConstraint()
    
    //MARK: - Properties
    private var selectedEvent: PredefinedEvents? {
        willSet {
            eventNameTF.text = newValue?.title
        }
    }
    
    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "InAppMessages Extended Triggers"
        hideKeyboardWhenTappedAround()
        underKeyboardLayoutConstraint.setup(bottomConstraint, view: view)
    }
    
    
    //MARK: - Private
    
    private func sendEvent() {
        guard let eventName = eventNameTF.text else { return }
        let event = CustomAnalyticsEventForIamExtendedTriggers(eventName: eventName,
                                                               customFieldSomeNumber: someNumberTF.text,
                                                               customFieldAnotherNumber: anotherNumberTF.text,
                                                               screenName: screenNameTF.text,
                                                               customFieldKey: customKeyTF.text,
                                                               customFieldValue: customValueTF.text)
        Copilot.instance.report.log(event: event)
        
        var message = "event sent successfully with parameters of:\neventName:\(eventName)\n"
        
        if let screenName = screenNameTF.text, !screenName.isEmpty {
            message.append("screen name:\(screenName)\n")
        }
        if let someNumber = someNumberTF.text, !someNumber.isEmpty {
            message.append("some number:\(someNumber)\n")
        }
        if let anotherNumber = anotherNumberTF.text, !anotherNumber.isEmpty {
            message.append("some another number:\(anotherNumber)\n")
        }
        if let customParam = customKeyTF.text, !customParam.isEmpty, let customValue = customValueTF.text, !customValue.isEmpty {
            message.append("custom parameter key: \(customParam) and value: \(customValue)\n")
        }
        ZLogManagerWrapper.sharedInstance.logInfo(message: message)
    }
    
    //MARK: - Actions
    @IBAction func signupButtonPressed(_ sender: UIButton) {
        selectedEvent = .signup
    }
    
    @IBAction func onboardingStartedButtonPressed(_ sender: UIButton) {
        selectedEvent = .onboardingStarted
    }
    
    @IBAction func onboardingEndedButtonPressed(_ sender: UIButton) {
        selectedEvent = .onboardingEnded
    }
    
    @IBAction func sendEventButtonPressed(_ sender: UIButton) {
         if let eventNameText = eventNameTF.text, !eventNameText.isEmpty {
            sendEvent()
        } else {
            let someSimpleAlert = UIAlertController(title: "Error",
                                                    message: "Event Name text field should not be empty",
                                                    preferredStyle: .alert)
            someSimpleAlert.addAction(UIAlertAction(title: "dismiss",
                                                    style: .default,
                                                    handler: nil))
            present(someSimpleAlert,
                    animated: true,
                    completion: nil)
        }
    }
}

struct CustomAnalyticsEventForIamExtendedTriggers: AnalyticsEvent {
    
    let eventName: String
    let customFieldSomeNumber: String?
    let customFieldAnotherNumber: String?
    let screenName: String?
    let customFieldKey: String?
    let customFieldValue: String?
    
    init(eventName: String,
         customFieldSomeNumber: String? = nil,
         customFieldAnotherNumber: String? = nil,
         screenName: String? = nil,
         customFieldKey: String? = nil,
         customFieldValue: String? = nil) {
        self.eventName = eventName
        self.customFieldSomeNumber = customFieldSomeNumber
        self.customFieldAnotherNumber = customFieldAnotherNumber
        self.screenName = screenName
        self.customFieldKey = customFieldKey
        self.customFieldValue = customFieldValue
    }
    
    var customParams: Dictionary<String, String> {
        
        var paramsToSend: Dictionary<String , String> = [:]
        if let customFieldSomeNumber = customFieldSomeNumber, !customFieldSomeNumber.isEmpty {
            paramsToSend["some_number"] = customFieldSomeNumber
        }
        if let customFieldAnotherNumber = customFieldAnotherNumber, !customFieldAnotherNumber.isEmpty {
            paramsToSend["another_number"] = customFieldAnotherNumber
        }
        if let screenName = screenName, !screenName.isEmpty {
            paramsToSend[AnalyticsConstants.screenNameKey] = screenName
        }
        if let customKey = customFieldKey, !customKey.isEmpty, let customValue = customFieldValue, !customValue.isEmpty {
            paramsToSend[customKey] = customValue
        }
        return paramsToSend
    }

    var eventOrigin: AnalyticsEventOrigin {
        return .App
    }
}
