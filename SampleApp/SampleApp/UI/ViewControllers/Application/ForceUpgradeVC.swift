//
//  ForceUpgradeVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 11/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class ForceUpgradeVC: UIViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var appVersionStatusLabel: UILabel!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Force Upgrade"
        
        if let appVersion = Utils.getAppVersion(shouldConcatinateBuildNumber: true) {
            appVersionLabel.text = appVersion
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func checkAppVersionButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .app
            .checkAppVersionStatus()
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let appVersionStatus):
                                //TODO: Add status
                                
                                self?.showAlert("Check app version succeeded")
                                
                                switch appVersionStatus.versionStatus {
                                case .ok:
                                    self?.appVersionStatusLabel.text = "OK"
                                case .notSupported:
                                    self?.appVersionStatusLabel.text = "Not Supported"
                                case .newVersionRecommended:
                                    self?.appVersionStatusLabel.text = "New Version Recommended"
                                }
                                
                            case .failure(error: let checkAppVersionError):
                                let msg: String
                                
                                switch checkAppVersionError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                case .appVersionBadFormat(let debugMessage):
                                    msg = "App Version Bad Format: \(debugMessage)"
                                }
                                
                                self?.showAlert("Check app version failed with error:\n\(msg)\n\n\(checkAppVersionError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
}
