//
//  PasswordPolicyVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 11/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class PasswordPolicyVC: KeyboardHandlerVC {
    
    //MARK: - Properties
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var isValidLabel: UILabel!
    @IBOutlet weak var allRulesLabel: UILabel!
    @IBOutlet weak var failedRulesLabel: UILabel!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Password policy"
        
        view.hideKeyboardWhenTapped()
    }
    
    //MARK: - IBActions
    
    @IBAction func validatePasswordButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .app
            .fetchPasswordPolicyConfig()
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let passwordRule):
                                self?.showAlert("Fetch password policy config succeeded")
                                self?.validatePassword(with: passwordRule)
                                
                            case .failure(error: let fetchPasswordPolicyConfigError):
                                let msg: String
                                
                                switch fetchPasswordPolicyConfigError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                }
                                self?.showAlert("Fetch password policy config failed with error:\n\(msg)\n\n\(fetchPasswordPolicyConfigError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
    //MARK: - Private Functions
    
    private func validatePassword(with passwordRulesList: [PasswordRule]) {
        guard let password = passwordTextField.text else {
            return
        }
        
        var allRules = "All rules:\n"
        var failedRules = "Failed rules:\n"
        var errorMessage = ""
        var isValid = true
        
        for rule in passwordRulesList {
            allRules += rule.ruleId.rawValue + " = " + String(rule.numericalValue) + "\n"
            switch rule.ruleId {
            case .minimumLength,
                 .maximumLength,
                 .maximumConsecutiveIdentical,
                 .complexity_MinimumUpperCase,
                 .complexity_MinimumLowerCase,
                 .complexity_MinimumDigits,
                 .complexity_MinimumSpecialChars,
                 .rejectWhiteSpace:
                errorMessage = rule.ruleId.rawValue + " = " + String(rule.numericalValue) + "\n"
            }
            
            if !rule.isValid(password: password) {
                isValid = false
                failedRules += errorMessage
            }
        }
        
        isValidLabel.text = isValid ? "YES" : "NO"
        allRulesLabel.text = allRules
        failedRulesLabel.text = isValid ? "" : failedRules
    }

}
