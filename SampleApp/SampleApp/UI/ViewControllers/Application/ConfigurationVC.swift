//
//  ConfigurationVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 11/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class ConfigurationVC: UIViewController {

    //MARK: - Properties
    
    @IBOutlet weak var privacyPolicyLabel: UILabel!
    @IBOutlet weak var privacyPolicyVersionLabel: UILabel!
    
    @IBOutlet weak var termOfUseLabel: UILabel!
    @IBOutlet weak var termOfUseVersionLabel: UILabel!
    
    @IBOutlet weak var faqLabel: UILabel!
    @IBOutlet weak var faqVersionLabel: UILabel!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Configuration"
    }
    
    //MARK: - IBActions
    
    @IBAction func fetchConfigurationButtonPressed(_ sender: Any) {
        showLoadingView()
        
        Copilot.instance
            .manage
            .sphere
            .app
            .fetchConfig()
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let configuration):
                                self?.showAlert("Fetch configuration succeeded")
                                
                                if let privacyPolicyURL = configuration.privacyPolicy?.url,
                                    let version = configuration.privacyPolicy?.version {
                                    self?.privacyPolicyLabel.text = privacyPolicyURL.absoluteString
                                    self?.privacyPolicyVersionLabel.text = String(version)
                                }
                                
                                if let termsOfUseURL = configuration.termsAndConditions?.url,
                                    let version = configuration.termsAndConditions?.version {
                                    self?.termOfUseLabel.text = termsOfUseURL.absoluteString
                                    self?.termOfUseVersionLabel.text = String(version)
                                }
                                
                                if let faqURL = configuration.faq?.url,
                                    let version = configuration.faq?.version {
                                    self?.faqLabel.text = faqURL.absoluteString
                                    self?.faqVersionLabel.text = String(version)
                                }
                                
                            case .failure(let fetchConfigurationError):
                                let msg: String
                                
                                switch fetchConfigurationError {
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                }
                                
                                self?.showAlert("Fetch configuration failed with error:\n\(msg)\n\n\(fetchConfigurationError.localizedDescription)")
                            }
                        }
                })
        }
    }
}
