//
//  FetchKeysVC.swift
//  SampleApp
//
//  Created by Michael Noy on 13/07/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class FetchKeysVC : AuthenticatedViewController{
    
    @IBOutlet weak var key1: UITextField!
    @IBOutlet weak var key2: UITextField!
    @IBOutlet weak var key3: UITextField!

    @IBAction func button(_ sender: Any) {
        Copilot.instance.documentStorage.fetchKeys().build().execute({[weak self] (response) in
                     DispatchQueue.main.async(execute:{
                         switch response {
                         case .success(let documentKeys):
                             print("Success getting keys:")
                             if documentKeys.keys.count>=0 {
                                 self?.key1.text = documentKeys.keys[0]
                             }
                             if documentKeys.keys.count>=1 {
                                 self?.key2.text = documentKeys.keys[1]
                             }
                             if documentKeys.keys.count>=2 {
                                 self?.key3.text = documentKeys.keys[2]
                             }
                         case .failure(let errorMessage):
                            self?.showAlert("Error getting keys with message \(errorMessage)")
                         }
                     })
                 }
             )
    }
    
}
    
