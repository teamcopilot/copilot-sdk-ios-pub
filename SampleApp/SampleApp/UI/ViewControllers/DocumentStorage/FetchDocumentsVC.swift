//
//  FetchDocumentsVC.swift
//  SampleApp
//
//  Created by Michael Noy on 13/07/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class FetchDocumentsVC : AuthenticatedViewController{
    
    @IBOutlet weak var key1: UITextField!
    @IBOutlet weak var key2: UITextField!
    @IBOutlet weak var key3: UITextField!
    @IBOutlet weak var value1: UITextField!
    @IBOutlet weak var value2: UITextField!
    @IBOutlet weak var value3: UITextField!
    
    @IBAction func applyTestValues(_ sender: Any) {
        self.key1.text = "a"
        self.key2.text = "b"
        self.key3.text = "c"
    }
    
    @IBAction func fetchDocumentsClicked(_ sender: Any) {
              Copilot.instance.documentStorage.fetchDocuments()
                .addKey(key: key1.text!)
                .addKey(key: key2.text!)
                .addKey(key: key3.text!)
                .build().execute({[weak self] (response) in
                    DispatchQueue.main.async(execute:{
                        switch response {
                        case .success(let response):
                            print("Success fetching documents")
                            self?.value1.text = response[self?.key1.text! ?? ""]
                            self?.value2.text = response[self?.key2.text! ?? ""]
                            self?.value3.text = response[self?.key3.text! ?? ""]
                            
                        case .failure(error: let error):
                            self?.showAlert("Error fetching documents : \(error)")
                        }
                    }
            )}
        )
    }
}
    
