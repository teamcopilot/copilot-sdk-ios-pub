//
//  SaveDocumentsVC.swift
//  SampleApp
//
//  Created by Michael Noy on 13/07/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class SaveDocumentsVC : AuthenticatedViewController{
    @IBOutlet weak var key1: UITextField!

    @IBOutlet weak var key2: UITextField!
    @IBOutlet weak var key3: UITextField!
    @IBOutlet weak var value1: UITextField!
    @IBOutlet weak var value2: UITextField!
    @IBOutlet weak var value3: UITextField!
    
    @IBAction func applyTestValues(_ sender: Any) {
        self.key1.text = "a"
        self.key2.text = "b"
        self.key3.text = "c"
        
        self.value1.text = "{\"keyA\":\"valueA\"}"
        self.value2.text = "{\"keyB\":\"valueB\"}"
        self.value3.text = "{\"keyC\":\"valueC\"}"
    }
    
    @IBAction func saveDocumentsClicked(_ sender: Any) {
          Copilot.instance.documentStorage.saveDocuments()
            .addDoucment(key: self.key1.text!, document: self.value1.text!)
            .addDoucment(key: self.key2.text!, document: self.value2.text!)
            .addDoucment(key: self.key3.text!, document: self.value3.text!)
                    .build()
                    .execute({[weak self] response in
                    DispatchQueue.main.async(execute:{
                                 switch response {
                                 case .success(_):
                                    print("Success documents keys")
                                    
                                 case .failure(let error):
                                    self?.showAlert("Error saving documens \(error)")
                                 }
                             })
                })
    }
}
    
