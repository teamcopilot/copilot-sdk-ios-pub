//
//  DocumentStorageVc.swift
//  SampleAppStageIOTCO
//
//  Created by Michael Noy on 09/07/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class DocumentStorageVc : AuthenticatedViewController{
    
    @IBAction func fetchDocumentsClicked(_ sender: Any) {
        if let vc = ViewControllersFactory.createControllerWithType(.documentStorageFetchDocuments) {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    

    @IBAction func saveDocumentsClicked(_ sender: Any) {
        if let vc = ViewControllersFactory.createControllerWithType(.documentStorageSaveDocuments) {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
    }
    

    @IBAction func fetchKeysClicked(_ sender: Any) {
        if let vc = ViewControllersFactory.createControllerWithType(.documentStorageFetchKeys) {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
    }
}
