//
//  MainViewController.swift
//  SampleApp
//
//  Created by Revital Pisman on 07/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

fileprivate protocol Command {
    var title: String { get }
    var vc: UIViewController? { get }
}

class MainViewController: UIViewController {

    //MARK: - Properties
    
    @IBOutlet weak var infoStackView: UIStackView!
    @IBOutlet weak var loggedInUserLabel: UILabel!
    @IBOutlet weak var manageTypeLabel: UILabel!
    @IBOutlet weak var copilotAppIDLabel: UILabel!
    @IBOutlet weak var environmentLabel: UILabel!
    @IBOutlet weak var firebaseProjectIDLabel: UILabel!
    @IBOutlet weak var firebaseDatabaseURLLabel: UILabel!
    @IBOutlet weak var gdprCompliantLabel: UILabel!
    @IBOutlet weak var apiVersionLabel: UILabel!
    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var copilotSdkVersion: UILabel!
    @IBOutlet weak var userIdStackView: UIStackView!
    
    @IBOutlet weak var tableView: UITableView!
    
    private var datasource = CopilotComponents.allCases
    
    private enum CopilotComponents: Int, CaseIterable {
        case app
        case auth
        case user
        case thing
        case events
        case misc
        case iam
        
        var title: String {
            switch self {
            case .app:
                return "Application"
            case .auth:
                return "Authentication"
            case .user:
                return "User"
            case .thing:
                return "Thing"
            case .events:
                return "Events"
            case .misc:
                return "Misc."
            case .iam:
                return "InAppMessages"
            }
        }
        
        var commands: [Command] {
            switch self {
            case .app:
                return App.allCases
            case .auth:
                return Auth.allCases
            case .user:
                return User.allCases
            case .thing:
                return Thing.allCases
            case .events:
                return Events.allCases
            case .misc:
                return Misc.allCases
            case .iam:
                return InAppMessages.allCases
            }
        }
    }
    
    private enum App: String, Command, CaseIterable {
        case forceUpgrade = "Force Upgrade"
        case configuration = "Configuration"
        case passwordPolicy = "Password policy"
        
        var title: String {
            return rawValue
        }
        
        var vc: UIViewController? {
            switch self {
            case .forceUpgrade:
                return ViewControllersFactory.createControllerWithType(.forceUpgrade)
            case .configuration:
                return ViewControllersFactory.createControllerWithType(.configuration)
            case .passwordPolicy:
                return ViewControllersFactory.createControllerWithType(.passwordPolicy)
                
            }
        }
    }
    
    private enum Auth: String, Command, CaseIterable {
        case emailAndPasswordRegistration = "Email and password registration"
        case anonymousRegistration = "Anonymous Registration"
        case loginWithEmailAndPassword = "Login with email and password"
        case silentLogin = "Silent login"
        case logout = "Logout"
        
        var title: String {
            return rawValue
        }
        
        var vc: UIViewController? {
            switch self {
            case .emailAndPasswordRegistration:
                return ViewControllersFactory.createControllerWithType(.emailAndPasswordRegistration)
            case .anonymousRegistration:
                return ViewControllersFactory.createControllerWithType(.anonymousRegistration)
            case .loginWithEmailAndPassword:
                return ViewControllersFactory.createControllerWithType(.loginWithEmailAndPassword)
            case .silentLogin:
                return ViewControllersFactory.createControllerWithType(.silentLogin)
            case .logout:
                return ViewControllersFactory.createControllerWithType(.logout)
            }
        }
    }
    
    private enum User: String, Command, CaseIterable {
        case fetchUserDetails = "Fetch user details"
        case updateUser = "Update user"
        case sendVerificationEmail = "Send verification email"
        case resetPassword = "Reset password"
        case changeEmail = "Change email"
        case elevateAnonymousUser = "Elevate anonymous user"
        case Sphere = "Sphere"
        
        var title: String {
            return rawValue
        }
        
        var vc: UIViewController? {
            switch self {
            case .fetchUserDetails:
                return ViewControllersFactory.createControllerWithType(.fetchUserDetails)
            case .updateUser:
                return ViewControllersFactory.createControllerWithType(.updateUser)
            case .sendVerificationEmail:
                return ViewControllersFactory.createControllerWithType(.sendVerificationEmail)
            case .resetPassword:
                return ViewControllersFactory.createControllerWithType(.resetPassword)
            case .elevateAnonymousUser:
                return ViewControllersFactory.createControllerWithType(.elevateAnonymousUser)
            case .changeEmail:
                return ViewControllersFactory.createControllerWithType(.changeEmail)
            case .Sphere:
                return ViewControllersFactory.createControllerWithType(.sphere)
            }
        }
    }
    
    private enum Thing: String, Command, CaseIterable {
        case thingAssociation = "Thing association"
        case fetchingThings = "Fetching things"
        case updateThingDetails = "Update thing details"
        
        var title: String {
            return rawValue
        }
        
        var vc: UIViewController? {
            switch self {
            case .thingAssociation:
                return ViewControllersFactory.createControllerWithType(.thingAssociation)
            case .fetchingThings:
                return ViewControllersFactory.createControllerWithType(.fetchingThings)
            case .updateThingDetails:
                return ViewControllersFactory.createControllerWithType(.updateThingDetails)
            }
        }
    }
    
    private enum Events: String, Command, CaseIterable {
        case predefinedEvents = "Predefined events"
        case customEvents = "Custom events"
        
        var title: String {
            return rawValue
        }
        
        var vc: UIViewController? {
            switch self {
            case .predefinedEvents:
                return ViewControllersFactory.createControllerWithType(.predefinedEvents)
            case .customEvents:
                return ViewControllersFactory.createControllerWithType(.customEvents)
            }
        }
    }
    
    private enum Misc: String, Command, CaseIterable {
        case ble = "BLE"
        case tests = "Tests"
        case bgTest = "Background Test"
        case externalSession = "External Session"
        case documentStorage = "Doucment Storage"
        
        var title: String {
            return rawValue
        }
        
        var vc: UIViewController? {
            switch self {
            case .ble:
                return ViewControllersFactory.createControllerWithType(.ble)
            case .tests:
                return ViewControllersFactory.createControllerWithType(.tests)
            case .bgTest:
                return ViewControllersFactory.createControllerWithType(.bgTest)
            case .externalSession:
                return ViewControllersFactory.createControllerWithType(.externalSession)
            case .documentStorage:
                return ViewControllersFactory.createControllerWithType(.documentStorage)
            }
        }
    }
    
    private enum InAppMessages: String, Command, CaseIterable {
        case iam = "In App Messages"
        case extendedIamTriggers = "Extended Inapp Messages Triggers"
        
        var title: String {
            return rawValue
        }
        
        var vc: UIViewController? {
            switch self {
            case .iam:
                return ViewControllersFactory.createControllerWithType(.iam)
            case .extendedIamTriggers:
                return ViewControllersFactory.createControllerWithType(.iamExtendedTriggers)
            }
        }
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        performSilentLogin()
    }
    
    @IBAction func toggleInfoAppearance(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        infoStackView.arrangedSubviews.forEach {
            $0.isHidden = sender.isSelected
        }
     }
    
    
    //Actions
    
    @IBAction func copyUserIdTapped(_ sender: UIButton) {
        UIPasteboard.general.string = userIdLabel.text
        
        let alertController = UIAlertController(title: "",
                                                message: "user id copied to pasteboard",
                                                preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK",
                                                style: UIAlertAction.Style.default,
                                                handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - Private Functions
    
    private func setupUI() {
        if let manageTypeValue = Bundle.main.copilotManageType {
            manageTypeLabel.text = manageTypeValue
        }
        
        if let copilotApplicationId = Bundle.main.copilotApplicationId {
            copilotAppIDLabel.text = copilotApplicationId
        }
        
        if let copilotEnvironmentUrl = Bundle.main.copilotEnvironmentUrl {
            environmentLabel.text = copilotEnvironmentUrl
        }
        
        if let firebaseProjectID = Bundle.main.firebaseProjectID {
            firebaseProjectIDLabel.text = firebaseProjectID
        }
        
        if let firebaseDatabaseUrl = Bundle.main.firebaseDatabaseUrl {
            firebaseDatabaseURLLabel.text = firebaseDatabaseUrl
        }
        
        if let isGDPRCompliant = Bundle.main.isGDPRCompliant {
            gdprCompliantLabel.text = isGDPRCompliant ? "Yes" : "No"
            gdprCompliantLabel.textColor = isGDPRCompliant ? .green : .red
        }
        
        
        //This value is not exposed by the Copilot
        apiVersionLabel.text = "v4"
        let dictionary = Bundle.main.infoDictionary!
        let build = dictionary["CFBundleVersion"] as! String
        let version = Copilot.instance.copilotVersion()
        copilotSdkVersion.text = version + " (build \(build))"
    }
    
    private func performSilentLogin(){
        if Bundle.main.manageType == .YourOwn {
            if let userId = getGeneralItem(withKey: "customUserIdKey") as? String {
                loggedInUserLabel.text = "Logged in (" + userId + ")"
                loggedInUserLabel.textColor = .green
                
                userIdLabel.text = userId
                userIdStackView.isHidden = false
            } else {
                loggedInUserLabel.text = "Not logged in"
                loggedInUserLabel.textColor = .red
                userIdLabel.text = ""
                userIdStackView.isHidden = true
            }
        } else {
            Copilot.instance
                .manage
                .sphere
                .auth
                .login()
                .silently
                .build()
                .execute { [weak self] (response) in
                    DispatchQueue.main.async(execute:
                        {
                            switch response {
                            case .success:
                                self?.fetchUser()
                            case .failure(_):
                                self?.loggedInUserLabel.text = "Not logged in"
                                self?.loggedInUserLabel.textColor = .red
                                self?.userIdLabel.text = ""
                                self?.userIdStackView.isHidden = true
                            }
                    })
            }
        }
    }
    
    private func getGeneralItem(withKey key: String) -> Any? {
        if let objectAsData = UserDefaults.standard.object(forKey: key) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: objectAsData)
        }
        
        return nil
    }
    
    private func fetchUser() {
        Copilot.instance.manage.sphere.user.fetchMe().build().execute { [weak self] (response) in
            DispatchQueue.main.async(execute:
                {
                    switch response {
                    case .success(let userMe):
                        let user: String
                        
                        switch userMe.accountStatus.credentialsType {
                        case .anonymous:
                            user = "Anonymous"
                        case .email:
                            user = userMe.userDetails.email ?? "No email"
                        case .unknown:
                            user = "Unknown"
                        }
                        
                        self?.loggedInUserLabel.text = "Logged in (" + user + ")"
                        self?.loggedInUserLabel.textColor = .green
                        self?.userIdLabel.text = userMe.userDetails.id
                        self?.userIdStackView.isHidden = false
                        
                    case .failure(_):
                        self?.loggedInUserLabel.text = "Not logged in"
                        self?.loggedInUserLabel.textColor = .red
                        self?.userIdLabel.text = ""
                        self?.userIdStackView.isHidden = true
                    }
            })
        }
    }
    
}

extension MainViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource[section].commands.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return datasource[section].title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = datasource[indexPath.section].commands[indexPath.row].title
        
        return cell
    }
    
}

extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        guard let component = CopilotComponents(rawValue: indexPath.section) else {
            print("MainViewController - Failed to create component section: \(indexPath.section)")
            return
        }
        
        guard component.commands.count >= indexPath.row else {
            print("MainViewController - Not valid index row: \(indexPath.row)")
            return
        }
        
        let command = component.commands[indexPath.row]
        
        if let vc = command.vc {
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            //Nothing happens
            print("Failed create VC for command: \(command)")
        }
    }
    
}
