//
//  KeyboardHandlerVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 16/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

class KeyboardHandlerVC: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    private var keyboardHeight: CGFloat = 0
    
    //MARK - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //bind gesture recognizer
        let tapper = UITapGestureRecognizer(target: self, action:#selector(endEditing))
        view.addGestureRecognizer(tapper)
        self.scrollView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupKeyboardHandling()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        endEditing()
        removeKeyboardHandling()
    }
    
    //MARK - Handle keyboard
    
    private func setupKeyboardHandling() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func removeKeyboardHandling() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        handleKeyboardWillHide(notification)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        handleKeyboardWillShow(notification)
    }
    
    func handleKeyboardWillHide(_ notification: Notification) {
        guard let scrollView = scrollView else { return }
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
    }
    
    func handleKeyboardWillShow(_ notification: Notification) {
        guard let scrollView = scrollView else { return }
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        keyboardHeight = keyboardSize.height - getSafeAreaBottomInset()
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    //MARK - Handles tap
    
    @objc func endEditing() {
        view.endEditing(true)
    }
}

extension KeyboardHandlerVC: UIScrollViewDelegate {
    
    func getSafeAreaBottomInset() -> CGFloat {
        if #available(iOS 11.0, *) {
            return view.safeAreaInsets.bottom
        } else {
            return 0
        }
    }
}

