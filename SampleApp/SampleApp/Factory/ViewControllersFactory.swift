//
//  ViewControllersFactory.swift
//  SampleApp
//
//  Created by Revital Pisman on 10/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

struct ViewControllersFactory {
    
    private struct StoryboardNames {
        static let main = "Main"
        static let application = "Application"
        static let authentication = "Authentication"
        static let user = "User"
        static let thing = "Thing"
        static let events = "Events"
        static let misc = "Misc"
        static let inAppMessages = "InAppMessages"
        static let documentStorage = "DocumentsStorage"
    }
    
    private struct identifiers {
        static let forceUpgradeVCIdentifier = ForceUpgradeVC.stringFromClass()
        static let configurationVCIdentifier = ConfigurationVC.stringFromClass()
        static let passwordPolicyVCIdentifier = PasswordPolicyVC.stringFromClass()
        
        static let emailAndPasswordRegistrationVCIdentifier = EmailAndPasswordRegistrationVC.stringFromClass()
        static let anonymousRegistrationVCIdentifier = AnonymousRegistrationVC.stringFromClass()
        static let loginWithEmailAndPasswordVCIdentifier = LoginWithEmailAndPasswordVC.stringFromClass()
        static let silentLoginVCIdentifier = SilentLoginVC.stringFromClass()
        static let logoutVCIdentifier = LogoutVC.stringFromClass()
        
        static let fetchUserDetailsVCIdentifier = FetchUserDetailsVC.stringFromClass()
        static let updateUserVCIdentifier = UpdateUserVC.stringFromClass()
        static let sendVerificationEmailVCIdentifier = SendVerificationEmailVC.stringFromClass()
        static let resetPasswordVCIdentifier = ResetPasswordVC.stringFromClass()
        static let elevateAnonymousUserVCIdentifier = ElevateAnonymousUserVC.stringFromClass()
        
        static let updateUserDetailsVCIdentifier = UpdateUserDetailsVC.stringFromClass()
        static let approveTermsOfUseVCIdentifier = ApproveTermsOfUseVC.stringFromClass()
        static let updateUsersConsentVCIdentifier = UpdateUsersConsentVC.stringFromClass()
        static let changePasswordVCIdentifier = ChangePasswordVC.stringFromClass()
        
        static let thingAssociationVCIdentifier = ThingAssociationVC.stringFromClass()
        static let fetchingThingsVCIdentifier = FetchingThingsVC.stringFromClass()
        static let updateThingDetailsVCIdentifier = UpdateThingDetailsVC.stringFromClass()
        static let thingStatusVCIdentifier = ThingStatusVC.stringFromClass()
        
        static let predefinedEventsVCIdentifier = PredefinedEventsVC.stringFromClass()
        static let customEventsVCIdentifier = CustomEventsVC.stringFromClass()
        
        static let bleRootViewControllerIdentifier = BLERootViewController.stringFromClass()
        static let testsVCIdentifier = TestsVC.stringFromClass()
        static let bgTestVCIdentifier = BGTestVC.stringFromClass()
        static let externalSessionVCIdentifier = ExternalSessionVC.stringFromClass()
        
        static let iamVCIdentifier = InAppMessagesVC.stringFromClass()
        static let iamExtendedTriggersVCIdentifier = InAppMessagesExtendedTriggersVC.stringFromClass()
        static let documentStorageVCIdentifier = DocumentStorageVc.stringFromClass()
        static let fetchDocumentsVCIdentifier = FetchDocumentsVC.stringFromClass()
        static let fetchKeysVCIdentifier = FetchKeysVC.stringFromClass()
        static let saveDocumentsVCIdentifier = SaveDocumentsVC.stringFromClass()
    }
    
    public static func createControllerWithType(_ viewControllerType: ViewControllerType) -> UIViewController? {
        var storyboardName: String?
        var identifier: String?
        var viewController: UIViewController?
        
        switch viewControllerType {
        case .forceUpgrade:
            storyboardName = StoryboardNames.application
            identifier = identifiers.forceUpgradeVCIdentifier
        case .configuration:
            storyboardName = StoryboardNames.application
            identifier = identifiers.configurationVCIdentifier
        case .passwordPolicy:
            storyboardName = StoryboardNames.application
            identifier = identifiers.passwordPolicyVCIdentifier
            
        case .emailAndPasswordRegistration:
            storyboardName = StoryboardNames.authentication
            identifier = identifiers.emailAndPasswordRegistrationVCIdentifier
        case .anonymousRegistration:
            storyboardName = StoryboardNames.authentication
            identifier = identifiers.anonymousRegistrationVCIdentifier
        case .loginWithEmailAndPassword:
            storyboardName = StoryboardNames.authentication
            identifier = identifiers.loginWithEmailAndPasswordVCIdentifier
        case .silentLogin:
            storyboardName = StoryboardNames.authentication
            identifier = identifiers.silentLoginVCIdentifier
        case .logout:
            storyboardName = StoryboardNames.authentication
            identifier = identifiers.logoutVCIdentifier
            
        case .fetchUserDetails:
            storyboardName = StoryboardNames.user
            identifier = identifiers.fetchUserDetailsVCIdentifier
        case .updateUser:
            storyboardName = StoryboardNames.user
            identifier = identifiers.updateUserVCIdentifier
        case .sendVerificationEmail:
            storyboardName = StoryboardNames.user
            identifier = identifiers.sendVerificationEmailVCIdentifier
        case .resetPassword:
            storyboardName = StoryboardNames.user
            identifier = identifiers.resetPasswordVCIdentifier
        case .elevateAnonymousUser:
            storyboardName = StoryboardNames.user
            identifier = identifiers.elevateAnonymousUserVCIdentifier
            
        case .updateUserDetails:
            storyboardName = StoryboardNames.user
            identifier = identifiers.updateUserDetailsVCIdentifier
        case .approveTermsOfUse:
            storyboardName = StoryboardNames.user
            identifier = identifiers.approveTermsOfUseVCIdentifier
        case .updateUsersConsent:
            storyboardName = StoryboardNames.user
            identifier = identifiers.updateUsersConsentVCIdentifier
        case .changePassword:
            storyboardName = StoryboardNames.user
            identifier = identifiers.changePasswordVCIdentifier
            
        case .thingAssociation:
            storyboardName = StoryboardNames.thing
            identifier = identifiers.thingAssociationVCIdentifier
        case .fetchingThings:
            storyboardName = StoryboardNames.thing
            identifier = identifiers.fetchingThingsVCIdentifier
        case .updateThingDetails:
            storyboardName = StoryboardNames.thing
            identifier = identifiers.updateThingDetailsVCIdentifier
        case .thingStatus:
            storyboardName = StoryboardNames.thing
            identifier = identifiers.thingStatusVCIdentifier
            
        case .predefinedEvents:
            storyboardName = StoryboardNames.events
            identifier = identifiers.predefinedEventsVCIdentifier
        case .customEvents:
            storyboardName = StoryboardNames.events
            identifier = identifiers.customEventsVCIdentifier
            
        case .ble:
            storyboardName = StoryboardNames.main
            identifier = identifiers.bleRootViewControllerIdentifier
        case .tests:
            storyboardName = StoryboardNames.main
            identifier = identifiers.testsVCIdentifier
        case .bgTest:
            storyboardName = StoryboardNames.main
            identifier = identifiers.bgTestVCIdentifier
        case .externalSession:
            storyboardName = StoryboardNames.misc
            identifier = identifiers.externalSessionVCIdentifier
            
        case .iam:
            storyboardName = StoryboardNames.inAppMessages
            identifier = identifiers.iamVCIdentifier
            
        case .iamExtendedTriggers:
            storyboardName = StoryboardNames.inAppMessages
            identifier = identifiers.iamExtendedTriggersVCIdentifier
        
        case .documentStorage:
            storyboardName = StoryboardNames.documentStorage
            identifier = identifiers.documentStorageVCIdentifier
            
        case .documentStorageFetchKeys:
            storyboardName = StoryboardNames.documentStorage
            identifier = identifiers.fetchKeysVCIdentifier
            
        case .documentStorageFetchDocuments:
            storyboardName = StoryboardNames.documentStorage
            identifier = identifiers.fetchDocumentsVCIdentifier
            
        case .documentStorageSaveDocuments:
            storyboardName = StoryboardNames.documentStorage
            identifier = identifiers.saveDocumentsVCIdentifier

        case .sphere:
            return SphereController()

        case .changeEmail:
            return ChangeEmailController()

        }
        
        if let identifier = identifier, let storyboard = storyboardName {
            let storyBoard = UIStoryboard.init(name: storyboard, bundle: Bundle.main)
            viewController = storyBoard.instantiateViewController(withIdentifier: identifier)
        }
        
        return viewController
    }
}

