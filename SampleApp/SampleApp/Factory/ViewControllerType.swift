//
//  ViewControllerType.swift
//  SampleApp
//
//  Created by Revital Pisman on 10/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public enum ViewControllerType {
    case forceUpgrade
    case configuration
    case passwordPolicy
    
    case emailAndPasswordRegistration
    case anonymousRegistration
    case loginWithEmailAndPassword
    case silentLogin
    case logout
    
    case fetchUserDetails
    case updateUser
    case sendVerificationEmail
    case resetPassword
    case elevateAnonymousUser
    
    case updateUserDetails
    case approveTermsOfUse
    case updateUsersConsent
    case changePassword
    
    case thingAssociation
    case fetchingThings
    case updateThingDetails
    case thingStatus
    
    case predefinedEvents
    case customEvents
    
    case ble
    case tests
    case bgTest
    case externalSession
    
    case iam
    case iamExtendedTriggers
    case documentStorage
    case documentStorageFetchKeys
    case documentStorageFetchDocuments
    case documentStorageSaveDocuments

    case sphere
    case changeEmail
}
