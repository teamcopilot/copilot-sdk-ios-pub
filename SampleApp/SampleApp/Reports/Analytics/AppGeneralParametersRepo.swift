//
//  AppGeneralParametersRepo.swift
//  SampleApp
//
//  Created by Tom Milberg on 09/04/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class AppGeneralParametersRepo: GeneralParametersRepository {
    
    let generalParamKey1 = "generalParamKey1"
    let generalParamKey2 = "generalParamKey2"
    
    var generalParameters: [String : String] {
        let generalParams = [generalParamKey1 : "generalValue1", generalParamKey2 : "generalValue2"];
        return generalParams
    }
    
    var generalParametersKeys: [String] {
        return [generalParamKey1, generalParamKey2]
    }
}
