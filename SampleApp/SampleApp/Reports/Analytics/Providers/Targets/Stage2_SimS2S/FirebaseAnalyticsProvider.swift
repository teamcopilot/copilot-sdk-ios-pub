//
//  FirebaseAnalyticsProvider.swift
//  SampleAppDevIOTCO
//
//  Created by Revital Pisman on 11/06/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAnalytics
import CopilotAPIAccess

class FirebaseAnalyticsProvider: EventLogProvider {
    
    func enable() {
        // No implementation
    }
    
    func disable() {
        // No implementation
    }
    
    func setUserId(userId:String?){
        Analytics.setUserID(userId)
    }
    
    func transformParameters(parameters: Dictionary<String, String>) -> Dictionary<String, String> {
        return parameters
    }
    
    func logCustomEvent(eventName: String, transformedParams: Dictionary<String, String>) {
        Analytics.logEvent(eventName, parameters: transformedParams)
    }
    
    func logScreenLoadEvent(screenName: String, transformedParams: Dictionary<String, String>) {
        var eventParams = transformedParams
        eventParams.updateValue(screenName, forKey: AnalyticsConstants.screenNameKey)
        Analytics.logEvent(AnalyticsConstants.screenLoadEventName, parameters: eventParams)
    }
    
    var providerName: String {
        return "FirebaseAnalyticsProvider"
    }
}
