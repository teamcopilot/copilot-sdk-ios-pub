//
//  FirebaseAnalyticsProvider.swift
//  SampleApp
//
//  Created by Tom Milberg on 30/05/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAnalytics
import CopilotAPIAccess

class FirebaseAnalyticsProvider: EventLogProvider {
    
    func enable() {
        updateFirebaseAnalyticsStatus(shouldLogEvents: true)
    }

    func disable() {
        updateFirebaseAnalyticsStatus(shouldLogEvents: false)
    }

    func setUserId(userId:String?){
        Analytics.setUserID(userId)
    }
    
    init() {
        //App delegate runs firebase configure
        updateFirebaseAnalyticsStatus(shouldLogEvents: false)
    }
    
    private func updateFirebaseAnalyticsStatus(shouldLogEvents: Bool) {
        AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(shouldLogEvents)
    }
    
    func transformParameters(parameters: Dictionary<String, String>) -> Dictionary<String, String> {
        return parameters
    }
    
    func logCustomEvent(eventName: String, transformedParams: Dictionary<String, String>) {
        Analytics.logEvent(eventName, parameters: transformedParams)
    }

    var providerName: String {
        return "FirebaseAnalyticsProvider"
    }
}
