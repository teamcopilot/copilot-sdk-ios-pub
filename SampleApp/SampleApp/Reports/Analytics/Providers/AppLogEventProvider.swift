//
//  AppLogEventProvider.swift
//  SampleApp
//
//  Created by Tom Milberg on 09/04/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class AppLogEventProvider: EventLogProvider {
    
    func enable() {
        ZLogManagerWrapper.sharedInstance.enableDebugLogs()
    }
    
    func disable() {
        ZLogManagerWrapper.sharedInstance.disableDebugLogs()
    }

    func setUserId(userId: String?){
        ZLogManagerWrapper.sharedInstance.logInfo(message: "Setting user id : \(userId)")
    }
    
    func transformParameters(parameters: Dictionary<String, String>) -> Dictionary<String, String> {
        return parameters
    }
    
    func logCustomEvent(eventName: String, transformedParams: Dictionary<String, String>) {

        ZLogManagerWrapper.sharedInstance.logInfo(message: "Reporting custom event with event name: \(eventName)")

        transformedParams.forEach { (key, value) in
            ZLogManagerWrapper.sharedInstance.logInfo(message: "Reporting param key \(key) and value \(value)")
        }
    }
    
    var providerName: String {
        return "ProviderNameExample"
    }
}
