//
//  BLERootViewController.swift
//  SampleApp
//
//  Created by Adaya on 12/12/2017.
//  Copyright © 2017 Zemingo. All rights reserved.
//

import UIKit
import ZemingoBLELayer
import CopilotAPIAccess

class BLERootViewController: UIViewController {

    let bridge = BLEBridge(requiredServices: [], discoveryServices: [])
    
    @IBOutlet weak var scanButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        bridge?.add(self)

        // Do any additional setup after loading the view.
        title = "BLE"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scanButton.isEnabled = bridge!.isBLEOn()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func scanPressed(_ sender: Any) {
//        bridge?.createBLEScanner(with: )
        performSegue(withIdentifier: "showBLEScanScreen", sender: self)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showBLEScanScreen"{
            let controller = segue.destination as! BLEDevicesScanResultsViewController
            controller.bridge = bridge
        }
        
    }
 

}

extension BLERootViewController : BLEStatusDelegate{
    func onStatusChanged(toConnected connected: Bool, orError error: Error!) {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "Connection changed! \(connected) with error? \(error)")
        if let button = scanButton{
            button.isEnabled = connected
        }
    }
    
    
}
