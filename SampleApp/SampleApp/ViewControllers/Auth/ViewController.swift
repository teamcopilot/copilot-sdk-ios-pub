//
//  ViewController.swift
//  SampleApp
//
//  Created by Yulia Felberg on 11/10/2017.
//  Copyright © 2017 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess


extension UIViewController{
    public func showAlert(_ msg : String){
        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
     public func showAlertAsync(_ msg : String){
        DispatchQueue.main.async(execute:{
            ZLogManagerWrapper.sharedInstance.logInfo(message: "Alert: \(msg)")
           self.showAlert(msg)
        })
    }
    
    func testResponse(_ response: Response<Void, TestError>){
        
        switch response {
        case .success():
            DispatchQueue.main.async {
                self.showAlert("Done with tests")
            }
        case .failure(let error):
            var msg : String;
            switch error{
            case .e(let message):
                msg = message
            }
            ZLogManagerWrapper.sharedInstance.logInfo(message: msg)
            DispatchQueue.main.async(execute:{
                self.showAlert(msg)
            })
        }
    }
}

class ViewController: UIViewController {


    @IBOutlet weak var userNameTxt: UITextField!
    
    @IBOutlet weak var pwdTxt: UITextField!
    
    private var passwordRulesList: [PasswordRule]?
    let tester = Tester()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        silentLogin()
    }
    
    private func silentLogin(){
        Copilot.instance
            .manage
            .sphere
            .auth
            .login()
            .silently
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        self?.silentLoginResponse(response)
                })
        }
    }
    
    @IBAction func loginPresssed(_ sender: Any) {
        if let email = userNameTxt.text, let pwd = pwdTxt.text{
            login(email, pwd: pwd)
        }
        else{
            showAlert("Please set username and password")
        }
    }
    
    
    @IBAction func registerPressed(_ sender: Any) {
        if let email = userNameTxt.text, let pwd = pwdTxt.text{
            register(email, pwd: pwd)
        }
        else{
            showAlert("Please set username and password")
        }
    }
    
    @IBAction func registerAnonymously(_ sender: Any) {
        registerAnonymously()
    }
    
    @IBAction func resetPasswordPressed(_ sender: Any) {
        if let email = userNameTxt.text{
            resetPassword(email)
        }
        else{
            showAlert("Please set username")
        }
    }
    
    @IBAction func validatePasswordPressed(_ sender: Any) {
        if let pwd = pwdTxt.text{
            tryValidatePassword(pwd: pwd)
        }
        else{
            showAlert("Please set password")
        }
    }
    
    private func silentLoginResponse(_ response: Response<Void, LoginSilentlyError>){
        switch response{
        case .success:
            self.showLoggedinPage()
        case .failure (error: let error):
            self.showRegisterOrLLoginPage(err: error)
        }
    }
    
    private func showLoggedinPage(){
        performSegue(withIdentifier: "alreadyLoggedIn", sender: self)
    }
    
    private func showRegisterOrLLoginPage(err error: LoginSilentlyError){
    
        switch error{
        case .requiresRelogin:
            ZLogManagerWrapper.sharedInstance.logInfo(message: "Required login!")
        case .generalError(_):
            fallthrough
        case .connectivityError(_):
            ZLogManagerWrapper.sharedInstance.logError(message: "Error while attempting to silent login \(error.localizedDescription)")
            //remain in page self.showRegisterOrLLoginPage()
            DispatchQueue.main.async(execute:{
                self.showAlert("Silent login failed")
            })
        }
     
    }
    func resetPassword(_ email: String){
        Copilot.instance
            .manage
            .sphere
            .user
            .resetPassword(for: email)
            .build()
            .execute { [weak self] (response) in
                switch response {
                case .success:
                    self?.showAlertAsync("success in reset")
                case .failure(error: let resetPasswordError):
                    var msg: String?
                    switch resetPasswordError {
                    case .invalidParameters(let debugMessage):
                        msg = "Invalid: \(debugMessage)"
                    case .generalError(let debugMessage):
                        msg = "General: \(debugMessage)"
                    case .connectivityError(let debugMessage):
                        msg = "Connectivity: \(debugMessage)"
                    case .emailIsNotVerified(let debugMessage):
                        msg = "Email: \(resetPasswordError.localizedDescription)"
                    }
                    if msg == nil {
                        msg = "UNKOWN!!!"
                    }
                    ZLogManagerWrapper.sharedInstance.logInfo(message: msg!)
                    DispatchQueue.main.async(execute:{
                        self?.showAlert(msg!)
                    })
                }
        }
    }
    
    func register(_ email: String, pwd pass: String) {
        Copilot.instance
            .manage
            .sphere
            .auth
            .signup()
            .withCopilotAnalysisConsent(false)
            .withCustomConsent("TempConsent", value: false)
            .with(email: email, password: pass, firstname: "John", lastname: "Doe")
            .build()
            .execute { [weak self] (response) in
                switch response {
                    case .success:
                        ZLogManagerWrapper.sharedInstance.logInfo(message: "success in register")
                        Copilot.instance.report.log(event: SignupAnalyticsEvent())
                        
                        DispatchQueue.main.async(execute:{
                            self?.showLoggedinPage()
                        })
                    case .failure(error: let registerError):
                        var msg: String?
                        switch registerError {
                            
                        
                        case .userAlreadyExists:
                            msg = "User Already Exists"
                        case .passwordPolicyViolation(_):
                            msg = "PWD: \(registerError.localizedDescription)"
                        case .invalidApplicationId:
                            msg = "APPID: \(registerError.localizedDescription)"
                        case .invalidParameters( _):
                            msg = "INVALID: \(registerError.localizedDescription)"
                        case .generalError( _):
                            msg = "general: \(registerError.localizedDescription)"
                        case .connectivityError( _):
                            msg = "Connectivity: \(registerError.localizedDescription)"
                        case .invalidEmail(let debugMessage):
                            msg = "Invalid email: \(registerError.localizedDescription)"
                        }
                        if msg == nil{
                            msg = "UNKOWN!!!"
                        }
                        ZLogManagerWrapper.sharedInstance.logInfo(message: msg!)
                        DispatchQueue.main.async(execute:{
                            self?.showAlert(msg!)
                        })
                }
        }
    }
    
    func registerAnonymously() {
        Copilot.instance
            .manage
            .sphere
            .auth
            .signup()
            .withCopilotAnalysisConsent(false)
            .withCustomConsent("TempConsent", value: false)            
            .anonymously
            .build()
            .execute { [weak self] (response) in
                switch response {
                case .success:
                    ZLogManagerWrapper.sharedInstance.logInfo(message: "success in register")
                    DispatchQueue.main.async(execute:{
                        self?.showLoggedinPage()
                    })
                case .failure(error: let registerError):
                    var msg: String?
                    switch registerError {
                    
                    case .invalidApplicationId:
                        msg = "Invalidapp: \(registerError.localizedDescription)"
                    case .invalidParameters(_):
                         msg = "Invalid: \(registerError.localizedDescription)"
                    case .generalError(_):
                        fallthrough
                    case .connectivityError(_):
                        msg = registerError.localizedDescription
                    }
                    if msg == nil{
                        msg = "UNKOWN!!!"
                    }
                    ZLogManagerWrapper.sharedInstance.logInfo(message: msg!)
                    DispatchQueue.main.async(execute:{
                        self?.showAlert(msg!)
                    })
                }
        }
    }
    
    func login(_ email: String, pwd pass: String){
        Copilot.instance
            .manage
            .sphere
            .auth
            .login()
            .with(email: email, password: pass)
            .build()
            .execute { [weak self] (response) in
                switch response {
                case .success:
                    ZLogManagerWrapper.sharedInstance.logInfo(message: "success in login")
                    DispatchQueue.main.async(execute:{
                        self?.showLoggedinPage()
                    })
                case .failure(error: let loginError):
                     let msg: String
                    switch loginError {
                        
                    case .unauthorized(let debugMessage):
                        msg = "wrong credentials (\(debugMessage))"
                        
                        
                    case .markedForDeletion(let debugMessage):
                        msg =  "Marked For Deletion (\(debugMessage))"
                        
                    
                    case .invalidParameters(_):
                        msg = "Invalid Params: \(loginError.localizedDescription)"
                    case .generalError(_):
                        fallthrough
                    case .connectivityError(_ ):
                        msg = loginError.localizedDescription
					case .accountSuspended:
						msg = "Account suspended"
					}
                    ZLogManagerWrapper.sharedInstance.logInfo(message: msg)
                    DispatchQueue.main.async(execute:{
                        self?.showAlert(msg)
                    })
                }
        }
    }
    
    private func tryValidatePassword(pwd pass: String) {
        
        if passwordRulesList != nil {
            validatePassword(pwd: pass)
        }
        else {
            Copilot.instance
                .manage
                .sphere
                .app
                .fetchPasswordPolicyConfig()
                .build()
                .execute { (response) in
                    switch response {
                    case .success(let rules):
                        self.passwordRulesList = rules
                        DispatchQueue.main.async {
                            self.validatePassword(pwd: pass)
                        }
                    case .failure(let error):
                        let msg = error.localizedDescription
                            
                            ZLogManagerWrapper.sharedInstance.logInfo(message: msg)
                            DispatchQueue.main.async(execute:{
                                self.showAlert(msg)
                            })
                    
                    
                    }
            }
        }
    }
    
    private func validatePassword(pwd pass: String) {
        guard let passwordRulesList = passwordRulesList else {
            showAlert("No rules list when validating password, how?!")
            return
        }
        
        
        var errors = "Password is not valid: \n\n"
        var isValid = true
        
        for rule in passwordRulesList {
            
            if !rule.isValid(password: pass) {
                errors.append("\(rule.ruleId.rawValue)\n")
                isValid = false
            }
        }
        
        if isValid {
            showAlert("Password is valid")
        }
        else {
            showAlert(errors)
        }
    }
    
    
    @IBAction func checkConnectivityErrors(_ sender: Any) {
        tester.runConnectionErrors(self.testResponse)
    }
    
    @IBAction func checkAllApis(_ sender: Any) {
        tester.runWithoutLogin(self.testResponse)
    }
    
    @IBAction func checkAuthErrors(_ sender: Any) {
        tester.runAuthErrors(self.testResponse)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
}

