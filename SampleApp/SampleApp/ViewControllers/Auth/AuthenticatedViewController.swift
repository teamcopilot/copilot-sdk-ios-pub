//
//  AuthenticatedViewController.swift
//  SampleApp
//
//  Created by Adaya on 06/12/2017.
//  Copyright © 2017 Zemingo. All rights reserved.
//

import UIKit
import UserNotifications

import CopilotAPIAccess


class AuthenticatedViewController: UIViewController {

    var termsOfUse : DynamicUrl?
    let tester = Tester()
    
    @IBOutlet weak var userNameTxt: UITextField!
    
    @IBOutlet weak var pwdTxt: UITextField!
    
    @IBAction func logoutPressed(_ sender: Any) {
        Copilot.instance
            .manage
			.sphere
            .auth
            .logout()
            .build()
            .execute { [weak self] (response) in
                DispatchQueue.main.async(execute:
                    {
                        switch response {
                            case .success:
                                self?.navigationController?.popViewController(animated: true)
                            case .failure(let error):
                                self?.showAlert("Could not logout \(error)")
                        }
                    })
        }
    }
    
    @IBAction func elevateAnonymous(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .user
            .elevate()
            .with(email: userNameTxt.text!, password: pwdTxt.text!, firstname: "John", lastname: "Doe")
            .build()
            .execute { (response) in
                switch response{
                    
                case .success:
                    self.showAlertAsync("Elevate completed")
                case .failure(let error):
                   
                    self.showAlertAsync("\(error.localizedDescription)")
                }
               
        }
    }
    
    @IBAction func systemPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .app
            .fetchConfig()
            .build()
            .execute { [weak self] (response) in
                switch response {
                    case .success(let configuration):
                        self?.showAlertAsync( "got configuration. \(configuration)")
                        if let privacyPolicy = configuration.privacyPolicy {
                            ZLogManagerWrapper.sharedInstance.logInfo(message: "privacyPolicy:" + privacyPolicy.description)
                        }
                        
                        if let termsOfUse = configuration.termsAndConditions {
                            self?.termsOfUse = termsOfUse
                            ZLogManagerWrapper.sharedInstance.logInfo(message: "termsAndConditions:" + termsOfUse.description)
                        }
                        
                        if let faq = configuration.faq {
                            ZLogManagerWrapper.sharedInstance.logInfo(message: "faq:" + faq.description)
                        }
                    case .failure(error: let error):
                        DispatchQueue.main.async(execute:{
                            self?.showAlert("Failed fetching configuration \(error.localizedDescription)")
                        })
                }
        }
    }
    
    @IBAction func userPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .user
            .fetchMe()
            .build()
            .execute { (response) in
                self.showAlertAsync("User response \(response)")
        }
    }
    
    
    @IBAction func approveTermsOfUsePressed(_ sender: Any) {
        guard let termsLegal = termsOfUse else {
            self.showAlert("First your should fetch the system settings in order to receive the terms of use")
            return
        }
        Copilot.instance
            .manage
            .sphere
            .user
            .updateMe()
            .approveTermsOfUse(forVersion: termsLegal.version)
            .build()
            .execute { (response) in
                self.showAlertAsync("Approve response \(response)")
        }
    }
    
    
    @IBAction func sendVerificationEmail(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .user
            .sendVerificationEmail()
            .build()
            .execute { (response) in
                self.showAlertAsync("Send verification email completed: \(response)")
        }
    }
    
    @IBAction func changePwdPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .user
            .updateMe()
            .withNewPassword(userNameTxt.text!, verifyWithOldPassword: pwdTxt.text!)
            .build()
            .execute { (response) in
                self.showAlertAsync("Change password completed: \(response)")
        }
    }
    @IBAction func consentPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .user
            .updateMe()
            .withCustomConsent("TempConsent", value: true)
            .allowCopilotUserAnalysis(false)
            .build()
            .execute { (response) in
                self.showAlertAsync("set consent completed: \(response)")
        }
    }
    
    @IBAction func consentRefusedPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .auth
            .logout()
            .markForDeletion
            .build()
            .execute { (response) in
                self.showAlertAsync("set consent refused completed: \(response)")
        }
    }
    
    @IBAction func checkAppVersion() {
        Copilot.instance
            .manage
            .sphere
            .app
            .checkAppVersionStatus()
            .build()
            .execute { [weak self] (response) in
            switch response {
                case .success(let appVersionStatus):
                    switch appVersionStatus.versionStatus {
                        case .newVersionRecommended:
                            self?.showAlertAsync("newVersionRecommended")
                        
                        case .notSupported:
                            self?.showAlert("Current app version is not supported please check Url")
                            ZLogManagerWrapper.sharedInstance.logInfo(message: "App version not supported")
                        
                        case .ok:
                            ZLogManagerWrapper.sharedInstance.logInfo(message: "App version is ok")
                    }
                case .failure(error: let error):
                    DispatchQueue.main.async(execute: {
                        self?.showAlert("Failed checking version \(error.localizedDescription)")
                    })
            }
        }
    }
    
    @IBAction func registerAPNSToken(_ sender: Any) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
        
        ZLogManagerWrapper.sharedInstance.logDebug(message: "APNS: Registering for remote PNS, want to test? https://console.firebase.google.com/u/2/project/civic-planet-197408/notification")
    }
    
    @IBAction func registerFakeAPNSToken(_ sender: Any) {
        if let fakeToken = "FAKE_TOKEN".data(using: String.Encoding.utf8) {
            UIApplication.shared.delegate?.application?(UIApplication.shared, didRegisterForRemoteNotificationsWithDeviceToken: fakeToken)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func elevateAnonymous(_ email: String, pwd pass: String) {
        Copilot.instance
            .manage
            .sphere
            .user
            .elevate()
            .with(email: email, password: pass, firstname: "John", lastname: "Doe")
            .build()
            .execute { (result) in
                switch result {
                    case .success:
                        self.showAlertAsync("success in elevating anonymous")
                    case .failure(error: let elevationError):
                        var msg: String?
                        switch elevationError {
                            case .invalidParameters(_):
                                msg = "missingParameters \(elevationError.localizedDescription)"
                            case .requiresRelogin(_):
                                msg = "anonymousSessionExpired \(elevationError.localizedDescription)"
                            case .userAlreadyExists(_):
                                msg = "userAlreadyExists \(elevationError.localizedDescription)"
                            case .connectivityError(_)://V
                                    msg =   "failure in register: serviceError.communicationError: " + elevationError.localizedDescription
                            case .generalError(_):
                                msg =   "failure in register: serviceError.generalError: " + elevationError.localizedDescription
                            
                            case .passwordPolicyViolation(_):
                                msg = "passwordPolicyViolation \(elevationError.localizedDescription)"
                            case .cannotElevateANonAnonymousUser(_):
                                msg = "invalidPermissions - cannot elevate non anonymous \(elevationError.localizedDescription)"
                        case .invalidEmail(let debugMessage):
                            msg = "Invalid email: \(elevationError.localizedDescription)"
                        }
                        if msg == nil{
                            msg = "UNKOWN!!!"
                        }
                    ZLogManagerWrapper.sharedInstance.logInfo(message: msg!)
                    DispatchQueue.main.async(execute:{
                        self.showAlert(msg!)
                    })
                }
        }
    }
    
    @IBAction func checkUserAPIErrors(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .user
            .fetchMe()
            .build()
            .execute { (response) in
                switch response{
                case .success(let user):
                    self.tester.runUserErrors(user.accountStatus.credentialsType == .anonymous, self.testResponse)
                case .failure(let error):
                    DispatchQueue.main.async(execute:{
                        self.showAlert("Failed loading user due to \(error.localizedDescription)")
                    })
                }
                
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
}

