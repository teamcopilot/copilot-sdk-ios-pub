//
//  ThingViewController.swift
//  SampleApp
//
//  Created by Tom Milberg on 11/06/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
import UIKit
import CopilotAPIAccess


class ThingViewController: UIViewController {
    
    @IBOutlet weak var getThingIDTextField: UITextField!
    @IBOutlet weak var associateThingPhysicalIDTextField: UITextField!
    @IBOutlet weak var associateThingModelTextField: UITextField!
    @IBOutlet weak var associateThingFirmwareTextField: UITextField!
    @IBOutlet weak var updateThingIDTextField: UITextField!
    
    let tester = Tester()
    
    @IBAction func getThingsBtnPressed(_ sender: Any) {
        Copilot.instance
            .manage
            .sphere
            .thing
            .fetchThings()
            .build()
            .execute { (response) in
            self.showAlertAsync( "Get things response: \(response)")
        }
    }
    
    @IBAction func getThingWithIDBtnPressed(_ sender: Any) {
        if let thingPhysicalID = getThingIDTextField.text, thingPhysicalID.count > 0 {
            Copilot.instance
                .manage
                .sphere
                .thing
                .fetchThing(withPhysicalId: thingPhysicalID)
                .build()
                .execute { (response) in
                    self.showAlertAsync("Get single thing response: \(response)")
                    switch response {
                    case .success(let thing):
                        if let temperatureType = thing.get(key: "temperature", as: TemperatureType.self) {
                            ZLogManagerWrapper.sharedInstance.logInfo(message: "Selected TemperatureType: \(temperatureType.rawValue)")
                        }
                        
                        break
                    default:
                        break
                    }
            }
        }
        else {
            showAlertAsync( "Error: Thing ID is wrong!")
        }
    }
    
    @IBAction func canAssociateBtnPressed(_ sender: Any) {
        guard let physicalID = associateThingPhysicalIDTextField.text, physicalID.count > 0 else {
            self.showAlertAsync( "PhysicalID value is wrong!")
            return
        }
        Copilot.instance
            .manage
            .sphere
            .thing
            .checkIfCanAssociate(withPhysicalId: physicalID)
            .build()
            .execute { (response) in
                self.showAlertAsync("Can associate thing response: \(response)")
        }
    }
    
    @IBAction func associateThingBtnPressed(_ sender: Any) {
        
        guard let firmware = associateThingFirmwareTextField.text, firmware.count > 0 else {
           self.showAlertAsync("Firmware value is wrong!")
            return
        }
        
        guard let model = associateThingModelTextField.text, model.count > 0 else {
            self.showAlertAsync("Model value is wrong!")
            return
        }
        
        guard let physicalID = associateThingPhysicalIDTextField.text, physicalID.count > 0 else {
            self.showAlertAsync("PhysicalID value is wrong!")
            return
        }
        Copilot.instance
            .manage
            .sphere
            .thing
            .associateThing(withPhysicalId: physicalID, firmware: firmware, model: model)
            .build()
            .execute { (response) in
            self.showAlertAsync("Associate thing response: \(response)")
        }
    }
    
    @IBAction func disassociateThingBtnPressesed(_ sender: Any) {
        guard let physicalID = associateThingPhysicalIDTextField.text, physicalID.count > 0 else {
            self.showAlertAsync( "PhysicalID value is wrong!")
            return
        }
        Copilot.instance
            .manage
            .sphere
            .thing
            .disassociateThing(withPhysicalId: physicalID)
            .build()
            .execute { (response) in
                self.showAlertAsync("Disassociate thing response: \(response)")
        }
    }
    
    @IBAction func updateThingWithIDBtnPressed(_ sender: Any) {
        
        let wantedThingID = updateThingIDTextField.text
        //First getting all things
        Copilot.instance
            .manage
            .sphere
            .thing
            .fetchThings()
            .build()
            .execute { (response) in
            switch response{
            case .success(let things):
                //Get the first thing and update it's ***
                //Get the required thing
                let filteredThings = things.filter({ (thing) -> Bool in
                    return thing.thingInfo.physicalId == wantedThingID
                })
                
                if let thingToBeUpdated = filteredThings.first {
                    
                    //Create a new reported status array
                    let newDate = Date()
                    var newStatusesArray = [ThingReportedStatus(time: newDate, name: "Second Cool status", value: "Ha Ha")]
                    //Add the existing reported statuses to it
                    if let reportedStatuses = thingToBeUpdated.thingStatus?.reportedStatuses {
                        newStatusesArray.append(contentsOf: reportedStatuses)
                    }
                    
                    let status = ThingStatus(lastSeen: newDate, reportedStatuses: newStatusesArray)
                    
                    
                    //TODO: Currently, custom settings are not save on the server. So the following code will not get back when we fetch the thing data.
                    
                    Copilot.instance
                        .manage
                        .sphere
                        .thing
                        .updateThing(withPhysicalId: thingToBeUpdated.thingInfo.physicalId)
                        .with(name: thingToBeUpdated.thingInfo.name!)
                        .with(firmware: thingToBeUpdated.thingInfo.firmware)
                        .with(status: status)
                        .with(customValue: "CSvalue1", forKey: "CSkey1")
                        .with(customValue: "CSvalue2", forKey: "CSkey2")
                        .with(customValue: 42, forKey: "CSkey3")
                        .with(customValue: [1,2,3,4], forKey: "CSkey4")
                        .with(customValue: TemperatureType.celcius, forKey: "temperature")
                        .build()
                        .execute({ (response) in
                        self.showAlertAsync("Update thing response: \(response)")
                    })
                }
                else {
                    self.showAlertAsync("Update things wasn't executed becuase no things were found in response: \(response)")
                }
            case .failure(error: let error):
                self.showAlertAsync("Update things wasn't executed becuase getThings failed with error: \(error)")
            }
        }
    }
    
    @IBAction func testThingErrors(_ sender: Any) {
        tester.runThingErrors(self.testResponse)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
}

enum TemperatureType: String, Codable {
    case celcius, farenheit
}
