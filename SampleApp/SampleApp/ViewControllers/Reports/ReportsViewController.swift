//
//  AnalyticsViewController.swift
//  SampleApp
//
//  Created by Tom Milberg on 09/04/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
import UIKit
import CopilotAPIAccess


class ReportsViewController: UIViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //Report screen load event
        let screenLoadEvent = ScreenLoadAnalyticsEvent(screenName: "Reports_Screen")
        Copilot.instance.report.log(event: screenLoadEvent)
    }
    
    @IBAction func reportAnalyticsBtnPressed(_ sender: Any) {
        
        //Event 1

        Copilot.instance.report.log(event: CustomAnalyticsEvent(eventName: "Reports", customParams: [
            "CustomEvent1": "Tom",
            "CustomEvent2": "1.0.1"
        ]))
        
        
        //Event 1
        Copilot.instance.report.log(event: CustomAnalyticsEvent(eventName: "Custom_Event_2", customParams: ["smartDeviceId": "1234567890"]))
    }
    
    @IBAction func reportCopilotPredefinedEvents(_ sender: Any) {
    
        let tapMenuEvent = TapMenuAnalyticsEvent(screenName: "Reports screen")
        Copilot.instance.report.log(event: tapMenuEvent)
        
        let tapMenuItemEvent = TapMenuItemAnalyticsEvent(menuItem: "item 3")
        Copilot.instance.report.log(event: tapMenuItemEvent)

        let fwUpgradeStartedEvent = FirmwareUpgradeStartedAnalyticsEvent()
        Copilot.instance.report.log(event: fwUpgradeStartedEvent)
        
        let fwUpgradeCompletedEvent = FirmwareUpgradeCompletedAnalyticsEvent(firmwareUpgradeStatus: .Failure)
        Copilot.instance.report.log(event: fwUpgradeCompletedEvent)
        
        let errorEvent = ErrorAnalyticsEvent(errorType: "Custom Error", screenName: "Error View Controller")
        Copilot.instance.report.log(event: errorEvent)
        
        
        let loginEvent = LoginAnalyticsEvent()
        Copilot.instance.report.log(event: loginEvent)
        
        let logoutEvent = LogoutAnalyticsEvent()
        Copilot.instance.report.log(event: logoutEvent)
        
        let acceptTermsEvent = AcceptTermsAnalyticsEvent(version: "12.10.1")
        Copilot.instance.report.log(event: acceptTermsEvent)
        
        let tapConnectDeviceEvent = TapConnectDeviceAnalyticsEvent()
        Copilot.instance.report.log(event: tapConnectDeviceEvent)

        let onboardingStartedEvent = OnboardingStartedAnalayticsEvent(flowID: "aaa111")
        Copilot.instance.report.log(event: onboardingStartedEvent)

        let onboardingEndedEvent = OnboardingEndedAnalyticsEvent(flowID: "aaa111", screenName: "onboarding screen")
        Copilot.instance.report.log(event: onboardingEndedEvent)

        let thingConnectedEvent = ThingConnectedAnalyticsEvent(thingID: "12asdad21", screenName: "Thing Screen")
        Copilot.instance.report.log(event: thingConnectedEvent)

        let thingDiscoveredEvent = ThingDiscoveredAnalyticsEvent(thingID: "12asdad21")
        Copilot.instance.report.log(event: thingDiscoveredEvent)
        
        let thingDiscoveredEventNoId = ThingDiscoveredAnalyticsEvent()
        Copilot.instance.report.log(event: thingDiscoveredEventNoId)
        
        let thingConnetionFailedEvent = ThingConnectionFailedAnalyticsEvent(failureReason: "Connection Problem")
        Copilot.instance.report.log(event: thingConnetionFailedEvent)

        let thingInfoEvent = ThingInfoAnalyticsEvent(thingFirmware: "1.0.22", thingModel: "the best!", thingId: "Identifier#Serial")
        Copilot.instance.report.log(event: thingInfoEvent)
    }
}

