#!/bin/sh

#Git clone

set -e 
set -o pipefail

TARGET=$1
VERSION=$2
BUILD_VERSION=$3
APP_NAME="SampleApp${TARGET}IOTCO"
DATE=$(date +%d.%m.%y)

OUTPUT_PATH="Releases/$VERSION/$BUILD_VERSION"
OUTPUT_NAME="CopilotTester_iOS_${TARGET}_v${VERSION}_b${BUILD_VERSION}_$DATE"

xcodebuild clean -project ../SampleApp.xcodeproj -configuration Release -alltargets
#TODO: Tests
xcodebuild archive -project ../SampleApp.xcodeproj -scheme $APP_NAME -archivePath ../$OUTPUT_PATH/$APP_NAME
xcodebuild -exportArchive -archivePath ../$OUTPUT_PATH/$APP_NAME.xcarchive -exportPath ../$OUTPUT_PATH/$APP_NAME -exportOptionsPlist ExportOptionsPlist.plist
mv ../$OUTPUT_PATH/$APP_NAME/$APP_NAME.ipa ../$OUTPUT_PATH/$APP_NAME/$OUTPUT_NAME.ipa